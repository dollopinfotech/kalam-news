<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class SyncTranslations extends Command
{
    protected $signature = 'translations:sync';
    protected $description = 'Sync translation keys';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('Syncing translation keys...');

        $langPath = resource_path('lang');

        $translationKeys = $this->scanForTranslationKeys();

        foreach ($translationKeys as $key) {
            $this->updateJsonFiles($langPath, $key);
        }

        $this->info('Translation keys synced successfully.');
    }

    protected function scanForTranslationKeys()
    {

        $files = File::allFiles(base_path('resources/views'));
        $keys = [];

        foreach ($files as $file) {
            $content = File::get($file);
            preg_match_all("/__\(['\"](.*?)['\"]\)/", $content, $matches);
            $keys = array_merge($keys, $matches[1]);
        }

        return array_unique($keys);
    }

    protected function updateJsonFiles($langPath, $key)
    {
        $locales = array_keys(config('app.available_locales'));

        foreach ($locales as $locale) {
            $filePath = "{$langPath}/{$locale}.json";

            if (!File::exists($filePath)) {
                File::put($filePath, json_encode([], JSON_PRETTY_PRINT));
            }

            $translations = json_decode(File::get($filePath), true);

            if (!isset($translations[$key])) {
                $translations[$key] = '';
                File::put($filePath, json_encode($translations, JSON_PRETTY_PRINT));
            }
        }
    }
}
