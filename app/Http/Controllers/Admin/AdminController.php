<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\City;
use App\Models\User;
use App\Models\SiteSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Helpers\DashboardHelper;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState", "sendForgotPasswordOtp", "forgotPassword", "adminOtp", "adminForgotPassword", "adminResetPassword"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }

    public function adminLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'emailOrMobile' => ['required'],
            'password' => ['required'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $credentials = $request->only('emailOrMobile', 'password');
        $query = null;

        if (preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i', $credentials['emailOrMobile'])) {
            $data['admin_email'] = $credentials['emailOrMobile'];
            $query = Admin::where('admin_email', $credentials['emailOrMobile'])->first();
        } elseif (preg_match('/^[6-9][0-9]{9}$/', $credentials['emailOrMobile'])) {
            $data['admin_mobile'] = $credentials['emailOrMobile'];
            $query = Admin::where('admin_mobile', $credentials['emailOrMobile'])->first();
        }
        $data['password'] = $credentials['password'];
        if (!$query) {
            // $cookie = cookie('msg', 'User not found!');
            // return redirect()->back()->withCookie($cookie);
            return redirect()->back()->with('error', 'Invalid credentials');
        }

        if (!Hash::check($credentials['password'], $query->password)) {
            // $cookie = cookie('msg', 'Invalid credentials');
            // return redirect()->back()->withCookie($cookie);
            return redirect()->back()->with(['error' => 'Invalid credentials']);
        }

        $token = Auth::attempt($data);
        $query->update(['api_token' => hash('sha256', $token)]);

        $cookie = cookie('api_token', $token, config('auth.admin_token_expiry'));

        return redirect()->route('admin.dashboard')->withCookie($cookie)->with('success', 'Admin login successfully');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->flush();
        $request->session()->regenerateToken();
        $cookie = cookie()->forget('api_token');
        return redirect()->route('admin.auth.login')->withCookie($cookie);
    }

    public function dashboard(Request $request)
    {
        $metrics = DashboardHelper::getDashboardMetrics();
        $users = DashboardHelper::getDailyUserRegistrations();

        $interval = $request->input('interval', 'weekly');

        if (!in_array($interval, ['weekly', 'monthly'])) {
            $interval = 'weekly';
        }

        $packages = DashboardHelper::getPackageSalesData($interval);

        return view('admin.dashboard', compact('metrics', 'users', 'packages', 'interval'));
    }

    public function getPackageSoldData(Request $request)
    {
        $interval = $request->input('interval', 'weekly');

        if (!in_array($interval, ['weekly', 'monthly'])) {
            $interval = 'weekly';
        }

        $packages = DashboardHelper::getPackageSalesData($interval);


        $jsonData = [
            'labels' => $packages->pluck('date')->toArray(),
            'counts' => $packages->pluck('count')->toArray()
        ];

        return response()->json($jsonData);
    }

    public function showSiteSettings(Request $request)
    {
        $site_settings = "";
        if ($request->ajax()) {
            $allData = DataTables::of($site_settings)
                ->addIndexColumn()
                ->make(true);
            return $allData;
        }
        $siteSettings = SiteSettings::first();
        return view('admin.site-settings.index', compact('siteSettings'));
    }

    public function addSiteSettings(Request $request)
    {
        // dd($request->term_condition);
        $validatedData = $request->validate([
            'site_name' => 'required|string|max:255',
            'site_email' => 'required|email|max:255',
            'site_address' => 'required|string|max:255',
            'contact_number' => 'required|string|max:255',
            'logo' => 'nullable|file|image|max:2048',
            'favicon' => 'nullable|file|image|max:2048',
            'number_of_stage_level' => 'required|integer',
            'calling_number' => 'nullable|string',
            'whatsapp' => 'nullable|string',
            'telegram' => 'nullable|string',
            'email' => 'nullable|email',
            'term_condition' => 'required',
            'welcome_message' => 'required',
            'invite_message' => 'required',
        ]);

        $siteSettings = SiteSettings::first();

        if ($siteSettings) {
            // Update existing settings
            $siteSettings->site_name = $validatedData['site_name'];
            $siteSettings->site_email = $validatedData['site_email'];
            $siteSettings->site_address = $validatedData['site_address'];
            $siteSettings->contact_number = $validatedData['contact_number'];
            $siteSettings->number_of_stage_level = $validatedData['number_of_stage_level'];
            $siteSettings->calling_number = $validatedData['calling_number'];
            $siteSettings->whatsapp = $validatedData['whatsapp'];
            $siteSettings->telegram = $validatedData['telegram'];
            $siteSettings->email = $validatedData['email'];
            $siteSettings->term_condition = $validatedData['term_condition'];
            $siteSettings->welcome_message = $validatedData['welcome_message'];
            $siteSettings->invite_message = $validatedData['invite_message'];

            // Handle file uploads (if applicable)
            if ($request->hasFile('logo')) {
                $fileName = "logo" . time() . '.' . $request->file('logo')->getClientOriginalExtension();
                $path = $request->file('logo')->move(public_path() . '/images/logos', $fileName);
                $siteSettings->logo = 'images/logos/' . $fileName;
            }
            if ($request->hasFile('favicon')) {
                $fileName = "favicon" . time() . '.' . $request->file('favicon')->getClientOriginalExtension();
                $Path = $request->file('favicon')->move(public_path() . '/images/favicons', $fileName);
                $siteSettings->favicon = 'images/favicons/' . $fileName;
            }
            $siteSettings->save();

            return redirect()->route('admin.site.settings')->with('success', 'Site settings updated successfully.');
        } else {
            // Create new settings if none exist
            $siteSettings = new SiteSettings();
            $siteSettings->site_name = $validatedData['site_name'];
            $siteSettings->site_email = $validatedData['site_email'];
            $siteSettings->site_address = $validatedData['site_address'];
            $siteSettings->contact_number = $validatedData['contact_number'];
            $siteSettings->number_of_stage_level = $validatedData['number_of_stage_level'];
            $siteSettings->calling_number = $validatedData['calling_number'];
            $siteSettings->whatsapp = $validatedData['whatsapp'];
            $siteSettings->telegram = $validatedData['telegram'];
            $siteSettings->email = $validatedData['email'];
            $siteSettings->term_condition = $validatedData['term_condition'];
            $siteSettings->welcome_message = $validatedData['welcome_message'];
            $siteSettings->invite_message = $validatedData['invite_message'];

            if ($request->hasFile('logo')) {
                $fileName = "logo" . time() . '.' . $request->file('logo')->getClientOriginalExtension();
                $path = $request->file('logo')->move(public_path() . '/images/logos', $fileName);
                $siteSettings->logo = 'images/logos/' . $fileName;
            }

            if ($request->hasFile('favicon')) {
                $fileName = "favicon" . time() . '.' . $request->file('favicon')->getClientOriginalExtension();
                $Path = $request->file('favicon')->move(public_path() . '/images/favicons', $fileName);
                $siteSettings->favicon = 'images/favicons/' . $fileName;
            }
            $siteSettings->save();

            return redirect()->route('admin.site.settings')->with('success', 'Site settings added successfully.');
        }
    }

    public function showadminprofile()
    {
        $admin = auth()->user();
        // dd($admin->admin_id);
        $adminProfile = Admin::where('admin_id', $admin->admin_id)->first();

        return view('admin.profile', compact('adminProfile'));
    }

    public function updateadminProfile(Request $request)
    {
        $validatedData = $request->validate([
            'admin_email' => 'required|email|max:255',
            'admin_name' => 'required|string|max:255',
            'admin_mobile' => 'required|string|max:255',
            'admin_profile' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $admin = auth()->user();
        $adminProfile = Admin::where('admin_id', $admin->admin_id)->first();

        if ($adminProfile) {
            $adminProfile->update($validatedData);
        } else {
            // If the profile does not exist, create a new one
            $adminProfile = Admin::create($validatedData);
        }

        $adminImage = $adminProfile->admin_profile;

        if ($request->hasFile('admin_profile')) {
            $fileName = "admin_profile" . time() . '.' . $request->file('admin_profile')->getClientOriginalExtension();
            $path = $request->file('admin_profile')->move(public_path() . '/images/adminProfile', $fileName);
            $adminProfile->admin_profile = 'images/adminProfile/' . $fileName;

            if (file_exists($adminImage)) {
                unlink($adminImage);
            }
        }

        $adminProfile->save();
        return redirect()->back()->with('success', 'Profile updated successfully.');
    }

    public function getCitiesByState(Request $request)
    {
        $stateId = $request->state_id;

        $cities = City::where('state_id', $stateId)->get();

        return response()->json(['cities' => $cities]);
    }

    public function addUserBank(Request $request,)
    {
        // dd($request);
        // $bankAccounts = BankDetail::find();
        // $bankAccounts->user_id = $request->input('user_id', $bankAccounts->user_id);
        // $bankAccounts->customer_name = $request->input('customer_name', $bankAccounts->customer_name);

        // $bankAccounts->account_no = $request->input('account_no', $bankAccounts->account_no);
        // $bankAccounts->bank_name = $request->input('bank_name', $bankAccounts->bank_name);
        // $bankAccounts->ifsc_code = $request->input('ifsc_code', $bankAccounts->ifsc_code);
        // $bankAccounts->bank_branch = $request->input('bank_branch', $bankAccounts->bank_branch);

        // $bankAccounts->save();

        if (!empty($bankAccounts)) {
            return redirect()->with('success', 'User Bank updated successfully');
        } else {
            return redirect()->with('error', 'User Bank not updated');
        }
    }

    public function notFoundPage()
    {
        return view('misc.errorPage');
    }

    public function sendForgotPasswordOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'emailOrMobile' => 'required|string|exists:admins,admin_email',
        ]);

        $adminData = Admin::where('admin_email', $request->emailOrMobile)->first();

        $otp = rand(1234, 9999);

        $adminData->otp = $otp;
        $adminData->save();

        if ($validator->fails()) {
            return redirect('forgotPassword')->with('error', 'Invalid credentials');
        } else if ($adminData) {
            return view('admin.auth.otp', compact('adminData'))->with('success', 'OTP send successfully');
        } else {
            return redirect('login')->with('error', 'Invalid credentials');
        }
    }

    public function forgotPassword()
    {
        return view('admin.auth.forgotPassword');
    }

    public function adminForgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'admin_email' => 'required|string|exists:admins,admin_email',
            'otp' => 'required|string|exists:admins,otp',
        ]);

        $adminData = Admin::where('admin_email', $request->admin_email)->where('otp', $request->otp)->first();

        if ($validator->fails()) {
            return redirect('forgotPassword')->with('error', 'Invalid credentials');
        } else if ($adminData) {
            return view('admin.auth.resetPassword', compact('adminData'))->with('success', 'OTP match successfully');
        } else {
            return redirect('login')->with('error', 'Invalid credentials');
        }
    }

    public function adminResetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'admin_email' => 'required|string|exists:admins,admin_email',
            'password' => 'required',
            'password_confirmation' => 'required',
        ]);

        $password = $request->password;

        $adminData = Admin::where('admin_email', $request->admin_email)->update(['password' => Hash::make($password)]);

        if ($adminData) {
            return redirect('login')->with('success', 'Password change successfully');
        } else {
            return redirect('login')->with('error', 'Password not change');
        }
    }

    public function getadminList()
    {
        $admin = Admin::all();
        return view('admin.admins.index', ['admin' => $admin]);
    }

    public function addAdmin()
    {
        $roles = DB::table('roles')->where('is_active', 1)->get(['role_id', 'role_name']);
        return view('admin.admins.addAdmin', compact('roles'));
    }

    public function AdminStatus(Request $request, $admin_id)
    {
        $admin = Admin::find($admin_id);

        if (!$admin) {
            return response()->json(['error' => 'Admin not found.'], 404);
        }

        $status = $request->input('is_active');
        $admin->is_active = $status ? 1 : 0;
        $admin->save();

        if (!empty($admin)) {
            return redirect()->route('admin.adminList')->with('success', 'Admin Status Changed Successfully');
        } else {
            return redirect()->route('admin.adminList')->with('error', 'Admin Status Not Changed');
        }
    }

    public function storeAdmin(Request $request)
    {

        $admin = new Admin;
        $admin->role_id = $request->input('role_id');
        $admin->admin_name = $request->input('admin_name');
        $admin->admin_email = $request->input('admin_email');
        $admin->admin_mobile = $request->input('admin_mobile');
        $admin->country_code = $request->input('country_code');
        $admin->admin_address = $request->input('admin_address');
        $admin->password = bcrypt($request->input('password'));

        if ($request->hasFile('admin_profile')) {
            $fileName = "admin_profile_" . time() . '.' . $request->file('admin_profile')->getClientOriginalExtension();
            $path = $request->file('admin_profile')->move(public_path('images/adminProfile'), $fileName);
            $admin->admin_profile = 'images/adminProfile/' . $fileName;
        }

        // dd($admin);
        $admin->save();
        if (!empty($admin)) {
            return redirect()->route('admin.adminList')->with('success', 'Admin Added Successfully');
        } else {
            return redirect()->route('admin.adminList')->with('error', 'Admin Added Not Changed');
        }
    }

    public function editAdmin($admin_id)
    {
        $admin = Admin::find($admin_id);
        $roles = DB::table('roles')->where('is_active', 1)->get(['role_id', 'role_name']);

        return view('admin.admins.editAdmin', compact('admin', 'roles'));
    }

    public function updateAdmin(Request $request, $admin_id)
    {

        $admin = Admin::findOrFail($admin_id);
        $admin->admin_name = $request->input('admin_name');
        $admin->admin_email = $request->input('admin_email');
        $admin->admin_mobile = $request->input('admin_mobile');
        $admin->country_code = $request->input('country_code');
        $admin->admin_address = $request->input('admin_address');
        $admin->role_id = $request->input('role_id');

        if ($request->has('password')) {
            $admin->password = bcrypt($request->input('password'));
        }

        if ($request->hasFile('admin_profile')) {
            $fileName = "admin_profile_" . time() . '.' . $request->file('admin_profile')->getClientOriginalExtension();
            $path = $request->file('admin_profile')->move(public_path('images/adminProfile'), $fileName);
            $admin->admin_profile = 'images/adminProfile/' . $fileName;
        }

        $admin->save();
        return redirect()->route('admin.adminList')->with('success', 'Admin has been successfully updated.');
    }

    public function deleteAdmin($admin_id)
    {
        $admin = Admin::find($admin_id);

        if (!$admin) {
            return redirect()->back()->with('error', 'Admin not found.');
        }

        $admin->delete();

        return redirect()->route('admin.adminList')->with('success', 'Admin deleted successfully.');
    }

    public function checkUniqueAdminEmail(Request $request)
    {
        $email = $request->input('email');
        $existingEntry = Admin::where('admin_email', $email)->exists();

        if ($existingEntry) {
            return response(['is_exists' => 1], 200);
        } else {
            return response(['is_exists' => 0], 200);
        }
    }

    public function checkUniqueAdminMobile(Request $request)
    {
        $mobile = $request->input('mobile');
        $existingEntry = Admin::where('admin_mobile', $mobile)->exists();

        if ($existingEntry) {
            return response(['is_exists' => 1], 200);
        } else {
            return response(['is_exists' => 0], 200);
        }
    }
}
