<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User_request;
use Illuminate\Support\Facades\Config;
use App\Models\User;
use App\Models\WalletHistory;

class AdvRequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState", "addAdvertisement", "razorpayPayment"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }


    public function advertisement()
    {
        $advertisementData = DB::table('user_requests')->where('req_type', 'advertisement')->where('payment_transaction_status', 'success')->get();
        return view('admin.User-Request.advertisement', compact('advertisementData'));
    }


    public function commissionAgent(Request $request)
    {
        $advertisementData = DB::table('user_requests')->where('req_type', 'commissionAgent')->get();
        return view('admin.User-Request.commissionAgents', compact('advertisementData'));
    }

    public function advertisementApproved(Request $request)
    {
        $req_id = $request->req_id;
        $dataUpdate = [
            'status' => 1,
        ];

        $result = DB::table('user_requests')
            ->where('req_id', $req_id)
            ->update($dataUpdate);

        if (!empty($result)) {
            return redirect('advertisement')->with('success', 'Request Approved Successfully');
        } else {
            return redirect('advertisement')->with('error', 'Request Not Approved');
        }
    }
    public function commissionApproved(Request $request)
    {
        $req_id = $request->req_id;

        $dataUpdate = [
            'status' => 1,
        ];
        $result = DB::table('user_requests')
            ->where('req_id', $req_id)
            ->update($dataUpdate);
        if (!empty($result)) {
            return redirect('commissionAgent')->with('success', 'Request Approved Successfully');
        } else {
            return redirect('commissionAgent')->with('error', 'Request Not Approved');
        }
    }


    public function addAdvertisement(Request $request)
    {
        $advertisement = new User_request();
        $advertisement->package_id = $request->input('package_id', $advertisement->package_id);
        $advertisement->first_name = $request->input('first_name', $advertisement->first_name);
        $advertisement->last_name = $request->input('last_name', $advertisement->last_name);
        $advertisement->user_number = $request->input('user_number', $advertisement->user_number);
        $advertisement->user_email = $request->input('user_email', $advertisement->user_email);
        $advertisement->title = $request->input('title', $advertisement->title);
        $advertisement->description = $request->input('description', $advertisement->description);
        $advertisement->req_type = "advertisement";

        if ($request->hasFile('adv_document')) {
            $uploadedFile = $request->file('adv_document');
            $fileName = "adv_document" . time() . '.' . $uploadedFile->getClientOriginalExtension();
            $destinationPath = 'images/advDocument';
            $uploadedFile->move($destinationPath, $fileName);
            $advertisement->adv_document = 'images/advDocument/' . $fileName;
        }

        $advertisement->save();

        $userPackage = Package::where('package_id', $request->package_id)->first();
        return response()->json(['success' => "Success", 'userPackage' => $userPackage, 'advertisement' => $advertisement]);
    }

    public function razorpayPayment(Request $request)
    {
        $input = $request->all();
        $apiKey = ENV('RAZORPAY_KEY_ID');
        $apiSecret = ENV('RAZORPAY_KEY_SECRET');
        $paymentId = $input['razorpay_payment_id'];
        $apiUrl = "https://api.razorpay.com/v1/payments/$paymentId";
        $curl = curl_init($apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERPWD, "$apiKey:$apiSecret");
        $response = curl_exec($curl);
        if ($response === false) {
            $error = curl_error($curl);
            echo "Error fetching payment details: $error";
        } else {
            $paymentDetails = json_decode($response, true);
            if (isset($paymentDetails['error'])) {
                $userRequest = User_request::where('req_id', $input['req_id'])->first();
                $userRequest->razorpay_payment_json = json_encode($paymentDetails);
                $userRequest->payment_transaction_status = "failed";
                $userRequest->save();
                return redirect('advertisement')->with('error', 'Request not send');
            } else {
                $userRequest = User_request::where('req_id', $input['req_id'])->first();
                $userRequest->razorpay_payment_json = json_encode($paymentDetails);
                $userRequest->payment_transaction_status = "success";
                $userRequest->save();
                return redirect('advertisement')->with('success', 'Request send successfully');
            }
        }
        curl_close($curl);
    }

    public function addCommissionAgent(Request $request)
    {
        $advertisement = new User_request();
        $advertisement->req_type = 'commissionAgent';
        $advertisement->first_name = $request->input('first_name', $advertisement->first_name);
        $advertisement->last_name = $request->input('last_name', $advertisement->last_name);
        $advertisement->user_number = ($request->input('country_code') . $request->input('user_number', $advertisement->user_number));
        $advertisement->user_email = $request->input('user_email', $advertisement->user_email);
        $advertisement->save();
        return redirect()->back()->with('success', 'Successfully added commission agent');
    }

    public function approveCommissionAgent(Request $request)
    {
        $userData = [
            'email' => $request->input('user_email'),
            'user_number' => $request->input('user_number'),
            'first_name' => $request->input('first_name'),
        ];

        if ($request->has('referral_code')) {
            $userData['referral_code'] = $request->input('referral_code');
        }

        if (isset($userData['referral_code'])) {
            $referralCodeValid = User::where('referral_code', $userData['referral_code'])->exists();

            if (!$referralCodeValid) {
                return redirect()->back()->with('error', 'Invalid referral code');
            }
        }

        if (!empty($userData['email'])) {
            $emailMessage = 'Hello, this is an email notification!';
            if (isset($userData['referral_code'])) {
                $emailMessage .= ' Your referral code is: ' . $userData['referral_code'];
            }
            sendEmailNotification($userData, 'Join with us', $emailMessage);
        }

        // if (!empty($userData['user_number'])) {
        //     $smsMessage = 'Hello, this is an SMS notification!';
        //     if (isset($userData['referral_code'])) {
        //         $smsMessage .= ' Your referral code is: ' . $userData['referral_code'];
        //     }
        //     sendSMSNotification($userData, $smsMessage);
        // }

        return redirect()->back()->with('success', 'Email and SMS notifications sent successfully');
    }

    public function withdrawRequest()
    {
        $withdrawRequestArr = DB::table('withdraw_request')->get();
        return view('admin.User-Request.withdrawRequest', compact('withdrawRequestArr'));
    }

    public function deleteWithdrawRequest($withdraw_request_id)
    {
        $withdrawRequestArr = DB::table('withdraw_request')->where('withdraw_request_id', $withdraw_request_id)->first();

        // dd($withdrawRequestArr);
        $walletHistory = WalletHistory::where('wallet_history_id', $withdrawRequestArr->wallet_history_id)->first();

        if (!$withdrawRequestArr || !$walletHistory) {
            return redirect()->back()->with('error', 'Withdraw request not found');
        }

        $walletHistory->status = 2;
        $walletHistory->save();

        DB::table('withdraw_request')->where('withdraw_request_id', $withdraw_request_id)->delete();

        return view('admin.User-Request.withdrawRequest')->with('success', 'Withdraw request deleted successfully.');
    }

    public function approveWithdrawRequest(Request $request)
    {
        $withdraw_request_id = $request->withdraw_request_id;
        $withdrawRequestArr = DB::table('withdraw_request')->where('withdraw_request_id', $withdraw_request_id)->first();

        // dd($withdrawRequestArr);
        $walletHistory = WalletHistory::where('wallet_history_id', $withdrawRequestArr->wallet_history_id)->first();

        if (!$withdrawRequestArr || !$walletHistory) {
            return redirect()->back()->with('error', 'Withdraw request not found');
        }

        $walletHistory->status = 1;
        $walletHistory->transaction_id = $request->transaction_id;
        $walletHistory->save();

        DB::table('withdraw_request')->where('withdraw_request_id', $withdraw_request_id)->delete();

        return view('admin.User-Request.withdrawRequest')->with('success', 'Withdraw request approved successfully.');
    }
}
