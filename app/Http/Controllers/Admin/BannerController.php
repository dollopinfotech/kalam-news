<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class BannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }

    public function getBanner()
    {
        $banner = DB::table('banners')->get();
        return view('admin.banner-management.index', compact('banner'));
    }
    public function createBanner(Request $request)
    {
        return view('admin.banner-management.addBanner');
    }

    public function editBanner(Banner $banner, $banner_id)
    {
        $banner = Banner::find($banner_id);

        if (!empty($banner)) {
            return view('admin.banner-management.editBanner', compact('banner'));
        } else {
            return redirect('banner')->with('error', 'banner data not found');
        }
    }

    public function storeBanner(Request $request)
    {
        $banner_image = new Banner();

        if ($request->hasFile('banner_image')) {
            $fileName = "banner_image" . time() . '.' . $request->file('banner_image')->getClientOriginalExtension();
            $path = $request->file('banner_image')->move(public_path() . '/images/bannerImages', $fileName);
            $banner_image->banner_image = 'images/bannerImages/' . $fileName;
        }
        $banner_image->save();

        if ($banner_image) {
            return redirect('banner')->with('success', 'Banner updated successfully');
        } else {
            return redirect('banner')->with('error', 'Banner not updated');
        }
    }


    public function updateBanner(Request $request, $banner_id)
    {
        $banner = Banner::find($banner_id);
        $bannerImage = $banner->banner_image;

        if ($request->hasFile('banner_image')) {
            $fileName = "banner_image" . time() . '.' . $request->file('banner_image')->getClientOriginalExtension();
            $path = $request->file('banner_image')->move(public_path() . '/images/bannerImages', $fileName);
            $banner->banner_image = 'images/bannerImages/' . $fileName;

            if (file_exists($bannerImage)) {
                unlink($bannerImage);
            }
        }

        $banner->save();
        if (!empty($banner)) {
            return redirect('banner')->with('success', 'banner updated successfully');
        } else {
            return redirect('banner')->with('error', 'banner not updated');
        }
    }

    public function deleteBanner(Request $request, $banner_id)
    {
        $banner = Banner::find($banner_id);

        $banner->delete();
        return redirect('banner')->with('success', 'banner deleted successfully');
    }

    public function bannerStatus(Request $request, $banner_id)
    {
        $banner = Banner::find($banner_id);

        if (!$banner) {
            return response()->json(['error' => 'banner not found.'], 404);
        }

        $is_active = $request->input('is_active');
        $banner->is_active = $is_active ? 1 : 0;

        $banner->save();

        return response()->json(['success' => 'Status updated successfully.']);
    }
}
