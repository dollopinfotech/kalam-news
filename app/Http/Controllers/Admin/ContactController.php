<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contacts;
use Illuminate\Support\Facades\Config;


class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }
    public function getCustomerDetails()
    {
        $contacts = Contacts::all();
        return view('admin.customer-support.index', compact('contacts'));
    }

    public function createContact()
    {
        return view('admin.customer-support.addContact');
    }

    public function storeContact(Request $request)
    {
        $request->validate([
            'calling_number' => 'nullable|string',
            'whatsapp' => 'nullable|string',
            'telegram' => 'nullable|string',
            'email' => 'nullable|email',
        ]);

        Contacts::create($request->all());

        return redirect()->route('admin.getCustomerDetails')
            ->with('success', 'Contact created successfully.');
    }

    public function editContact($contact_id)
    {
        $contact = Contacts::findOrFail($contact_id);
        return view('admin.customer-support.editContact', compact('contact'));
    }

    public function updateContact(Request $request, $contact_id)
    {
        $request->validate([
            'calling_number' => 'nullable|string',
            'whatsapp' => 'nullable|string',
            'telegram' => 'nullable|string',
            'email' => 'nullable|email',
        ]);

        $contact = Contacts::findOrFail($contact_id);
        $contact->update($request->all());

        return redirect()->route('admin.getCustomerDetails')
            ->with('success', 'Contact updated successfully.');
    }

    public function deleteContact($contact_id)
    {
        $contact = Contacts::findOrFail($contact_id);
        $contact->delete();

        return redirect()->route('admin.getCustomerDetails')
            ->with('success', 'Contact deleted successfully.');
    }
}
