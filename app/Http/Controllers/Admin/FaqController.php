<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Faq;
use Illuminate\Support\Facades\Config;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }
    public function getFaq()
    {
        $faqs = DB::table('faqs')->get();
        return view('admin.faq-management.index', compact('faqs'));
    }

    public function createFaq()
    {
        return view('admin.faq-management.addFaq');
    }

    public function storeFaq(Request $request)
    {
        // print_r($request->faq_description); die;
        $request->validate([
            'faq_title' => 'required',
            'faq_description' => 'required'
        ]);

        $faq = new Faq();

        $faq->faq_title = $request->faq_title;
        $faq->faq_description = $request->faq_description;
        $faq->save();

        return redirect('faq');
    }

    public function editFaq($id)
    {
        $faq = Faq::find($id);

        return view('admin.faq-management.editFaq', compact('faq'));
    }

    public function updateFaq(Request $request, $id)
    {
        $request->validate([
            'faq_title' => 'required',
            'faq_description' => 'required'
        ]);

        $faq = Faq::find($id);
        $faq->faq_title = $request->faq_title;
        $faq->faq_description = $request->faq_description;
        $faq->save();

        return redirect('faq')->with('success', 'Faq updated successfully');
    }

    public function deleteFaq($id)
    {
        $faq = Faq::find($id);

        $faq->delete();
        return redirect('faq')->with('success', 'Faq deleted successfully');
    }
}
