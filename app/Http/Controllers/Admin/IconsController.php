<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Icons;
use Illuminate\Support\Facades\Config;

class IconsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }
    public function getIcons()
    {
        $icons = DB::table('icons')->get();
        return view('admin.icon-management.index',compact('icons'));
    }

    public function createIcon()
    {
        return view('admin.icon-management.addIcon');
    }

    public function storeIcon(Request $request)
    {
        $request->validate([
            'icon_name' => 'required',
        ]);

        $icons = new Icons();

        if ($request->hasFile('icon_image')) {
            $icon_image = $request->file('icon_image');
            $icon_name = $request->input('icon_name');
            $fileName = $icon_name . '.' . $request->file('icon_image')->getClientOriginalExtension();
            $icon_image->move('images/icons', $fileName);
            $icons->icon_image = $fileName;
        }

        $icons->icon_name = $request->icon_name;
        $icons->icon = $request->icon;
        $icons->save();

        return redirect('icons');
    }

    public function editIcon($icon_id)
    {
        $icons = Icons::find($icon_id);
        return view('admin.icon-management.editIcon', compact('icons'));
    }

    public function updateIcon(Request $request, $icon_id)
    {
        $request->validate([
            'icon_name' => 'required',
        ]);

        $icons = Icons::find($icon_id);

        if ($request->hasFile('icon_image')) {
            $icon_image = $request->file('icon_image');
            $icon_name = $request->input('icon_name');
            $fileName = $icon_name . '.' . $request->file('icon_image')->getClientOriginalExtension();
            $icon_image->move('images/icons', $fileName);
            $icons->icon_image = $fileName;
        }

        $icons->icon_name = $request->icon_name;
        $icons->icon = $request->icon;
        $icons->save();

        return redirect('icons')->with('success', 'Icons updated successfully');
    }

    public function deleteIcon($icon_id)
    {
        $icons = Icons::find($icon_id);

        $icons->delete();
        return redirect('icons')->with('success', 'Icons deleted successfully');
    }

    public function getIconsList()
    {
        $icons = Icons::all();
        return response()->json($icons);
    }
}
