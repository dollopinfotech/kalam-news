<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Icon;
use Illuminate\Support\Facades\Config;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }
    public function getMenu()
    {
        $menu = DB::table('menus')->select('menu_id', 'menu_name', 'menu_link', 'menu_icon', 'sub_menu', 'order_no', 'status')->where('parent_menu_id', 0)->get();
        return view('admin.menu-management.index', compact('menu'));
    }

    public function createMenu()
    {
        $data = array();

       $data['menu_icon'] = Icon :: all();

        return view('admin.menu-management.addMenu');
    }

    public function storeMenu(Request $request)
    {
        $request->validate([
            'menu_name' => 'required',
            'menu_link' => 'required',
            'menu_icon' => 'required',
            'sub_menu' => 'required',
            'order_no'   => 'required'
        ]);

        $menu = new Menu();

        $menu->menu_name = $request->menu_name;
        $menu->menu_link = $request->menu_link;
        $menu->menu_icon = $request->menu_icon;
        $menu->sub_menu = $request->sub_menu;
        $menu->order_no = $request->order_no;
        $menu->parent_menu_id = 0;
        $menu->save();

        if (!empty($menu)) {
            return redirect('menu')->with('success', 'Menu added successfully');
        } else {
            return redirect('menu')->with('error', 'Menu not added');
        }
    }

    public function editMenu($id)
    {
        $menu = Menu::find($id);

        return view('admin.menu-management.editMenu', compact('menu'));

        if (!empty($menu)) {
            return view('admin.menu-management.editMenu', compact('menu'));
        } else {
            return redirect('menu')->with('error', 'Menu data not found');
        }
    }

    public function updateMenu(Request $request, $id)
    {
        $request->validate([
            'menu_name' => 'required',
            'menu_link' => 'required',
            'menu_icon' => 'required',
            'sub_menu' => 'required',
            'order_no' => 'required'
        ]);

        $menu = Menu::find($id);
        $menu->menu_name = $request->menu_name;
        $menu->menu_link = $request->menu_link;
        $menu->menu_icon = $request->menu_icon;
        $menu->sub_menu = $request->sub_menu;
        $menu->order_no = $request->order_no;
        $menu->created_by = "";
        $menu->updated_by = "";

        $menu->save();

        if (!empty($menu)) {
            return redirect('menu')->with('success', 'Menu updated successfully');
        } else {
            return redirect('menu')->with('error', 'Menu not updated');
        }
    }

    public function deleteMenu($id)
    {
        $menu = Menu::find($id);

        $menu->delete();

        if (!empty($menu)) {
            return redirect('menu')->with('success', 'Menu deleted successfully');
        } else {
            return redirect('menu')->with('error', 'Menu not deleted');
        }
    }

    public function getSubMenu()
    {
        $subMenu = DB::table('menus as menu')
            ->select('sub_menu.menu_id', 'sub_menu.menu_link', 'sub_menu.status', 'menu.menu_name as parent_menu_name', 'sub_menu.menu_name as menu_name')
            ->join('menus as sub_menu', 'menu.menu_id', '=', 'sub_menu.parent_menu_id')
            ->where('sub_menu.parent_menu_id', '!=', 0)
            ->where('sub_menu.page_status', 0)
            ->get();
        return view('admin.menu-management.subMenuIndex', compact('subMenu'));
    }

    public function createSubMenu()
    {
        $menu = DB::table('menus')->select('menu_id', 'menu_name')->where('status', 1)->where('parent_menu_id', 0)->get();

        return view('admin.menu-management.addSubMenu', compact('menu'));
    }

    public function storeSubMenu(Request $request)
    {
        $request->validate([
            'menu_name' => 'required',
            'sub_menu_url' => 'required',
            'sub_menu_name' => 'required',
            'menu_order_no'   => 'required'
        ]);

        $menu = new Menu();
        $menu->menu_name = $request->sub_menu_name;
        $menu->menu_link = $request->sub_menu_url;
        $menu->parent_menu_id = $request->menu_name;
        $menu->order_no = $request->menu_order_no;
        $menu->menu_icon = "";
        $menu->save();

        if (!empty($menu)) {
            return redirect('subMenu')->with('success', 'Sub Menu added successfully');
        } else {
            return redirect('subMenu')->with('error', 'Sub Menu not added');
        }
    }

    public function editSubMenu($id)
    {
        $menu = DB::table('menus')->select('menu_id', 'menu_name')->where('status', 1)->where('parent_menu_id', 0)->get();
        $subMenu = Menu::find($id);

        if (!empty($menu) && !empty($subMenu)) {
            return view('admin.menu-management.editSubMenu', compact('subMenu'), compact('menu'));
        } else {
            return redirect('subMenu')->with('error', 'Sub Menu data not found');
        }
    }

    public function updateSubMenu(Request $request, $id)
    {
        $request->validate([
            'menu_name' => 'required',
            'sub_menu_url' => 'required',
            'sub_menu_name' => 'required',
            'menu_order_no'   => 'required'
        ]);

        $menu = Menu::find($id);
        $menu->menu_name = $request->sub_menu_name;
        $menu->menu_link = $request->sub_menu_url;
        $menu->parent_menu_id = $request->menu_name;
        $menu->order_no = $request->menu_order_no;
        $menu->menu_icon = "";
        $menu->sub_menu = 0;
        $menu->save();

        $menu->save();

        if (!empty($menu)) {
            return redirect('subMenu')->with('success', 'Sub Menu updated successfully');
        } else {
            return redirect('subMenu')->with('error', 'Sub Menu not updated');
        }
    }

    public function deleteSubMenu($menu_id)
    {
        $menu = Menu::find($menu_id);
        $menu->delete();

        if (!empty($menu)) {
            return redirect('subMenu')->with('success', 'Sub Menu deleted successfully');
        } else {
            return redirect('subMenu')->with('error', 'Sub Menu not deleted');
        }
    }

    public function getPages()
    {
        $pages = DB::table('menus as menu')
            ->select('pages.menu_id', 'pages.menu_link', 'pages.status', 'menu.menu_name as parent_menu_name', 'pages.menu_name as menu_name')
            ->join('menus as pages', 'menu.menu_id', '=', 'pages.parent_menu_id')
            ->where('pages.parent_menu_id', '!=', 0)
            ->where('pages.page_status', 1)
            ->get();
        return view('admin.menu-management.pagesIndex', compact('pages'));
    }

    public function createPages()
    {
        $menu = DB::table('menus')->select('menu_id', 'menu_name')->where('status', 1)->where('parent_menu_id', 0)->get();
        return view('admin.menu-management.addPages', compact('menu'));
    }

    public function storePages(Request $request)
    {
        $request->validate([
            'menu_name' => 'required',
            'page_url' => 'required',
            'page_name' => 'required',
            'page_order_no'   => 'required'
        ]);

        $menu = new Menu();
        $menu->menu_name = $request->page_name;
        $menu->menu_link = $request->page_url;
        $menu->parent_menu_id = $request->menu_name;
        $menu->order_no = $request->page_order_no;
        $menu->page_status = "1";
        $menu->menu_icon = "";
        $menu->save();

        if (!empty($menu)) {
            return redirect('pages')->with('success', 'Page added successfully');
        } else {
            return redirect('pages')->with('error', 'Page not added');
        }
    }

    public function editPages($id)
    {
        $menu = DB::table('menus')->select('menu_id', 'menu_name')->where('status', 1)->where('parent_menu_id', 0)->get();
        $pages = Menu::find($id);

        if (!empty($menu) && !empty($pages)) {
            return view('admin.menu-management.editPages', compact('pages'), compact('menu'));
        } else {
            return redirect('pages')->with('error', 'Page data not found');
        }
    }

    public function updatePages(Request $request, $id)
    {
        $request->validate([
            'menu_name' => 'required',
            'page_url' => 'required',
            'page_name' => 'required',
            'page_order_no'   => 'required'
        ]);

        $menu = Menu::find($id);
        $menu->menu_name = $request->page_name;
        $menu->menu_link = $request->page_url;
        $menu->parent_menu_id = $request->menu_name;
        $menu->order_no = $request->page_order_no;
        $menu->page_status = "1";
        $menu->menu_icon = "";
        $menu->save();

        $menu->save();
        if (!empty($menu)) {
            return redirect('pages')->with('success', 'Page updated successfully');
        } else {
            return redirect('pages')->with('error', 'Page not updated');
        }
    }

    public function deletePages($id)
    {
        $menu = Menu::find($id);
        $menu->delete();

        if (!empty($menu)) {
            return redirect('pages')->with('success', 'Page deleted successfully');
        } else {
            return redirect('pages')->with('error', 'Page not deleted');
        }
    }

    public function menuStatus(Request $request, $menu_id)
    {
        $menu = Menu::find($menu_id);

        if (!$menu) {
            return response()->json(['error' => 'Menu not found.'], 404);
        }

        $status = $request->input('status');
        $menu->status = $status ? 1 : 0;
        $menu->save();

        if (!empty($menu)) {
            return redirect('menu')->with('success', 'Menu Status Changed Successfully');
        } else {
            return redirect('menu')->with('error', 'Menu Status not Changed');
        }
        // return response()->json(['success' => 'Menu Status Changed Successfully.']);
    }
    public function subMenuStatus(Request $request, $menu_id)
    {
        $menu = Menu::find($menu_id);

        if (!$menu) {
            return response()->json(['error' => 'subMenu not found.'], 404);
        }

        $status = $request->input('status');
        $menu->status = $status ? 1 : 0;
        $menu->save();

        if (!empty($menu)) {
            return redirect('menu')->with('success', 'SubMenu Status Changed Successfully');
        } else {
            return redirect('menu')->with('error', 'SubMenu Status not Changed');
        }
        // return response()->json(['success' => 'SubMenu Status Changed Successfully.']);
    }
    public function pageStatus(Request $request, $menu_id)
    {
        $menu = Menu::find($menu_id);

        if (!$menu) {
            return response()->json(['error' => 'subMenu not found.'], 404);
        }

        $status = $request->input('status');
        $menu->status = $status ? 1 : 0;
        $menu->save();

        if (!empty($menu)) {
            return redirect('menu')->with('success', 'Page Status Changed Successfully');
        } else {
            return redirect('menu')->with('error', 'Page Status not Changed');
        }
        // return response()->json(['success' => 'Page Status Changed Successfully.']);
    }



}
