<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\SiteSettings;
use App\Models\Stage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }

    public function createPackages()
    {
        $totalStages = SiteSettings::first();
        return view('admin.packages.addPackage', ['totalStages' => $totalStages->number_of_stage_level]);
    }

    public function storePackages(Request $request)
    {
        DB::beginTransaction();
        $validatedData = $request->validate([
            'package_name' => 'required|string|max:255',
            'package_price' => 'required|numeric',
            'max_commission' => 'required|numeric',
            'package_desc' => 'required|string',
            'package_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'stages.*.commission' => 'required|numeric',
            'max_advertisement' => 'required|numeric',
            'stages.*.number_of_members' => 'required|integer',
            'stages.*.business_value' => 'required|numeric',
        ]);

        try {
            $package = new Package();
            $package->package_name = $request->input('package_name');
            $package->package_price = $request->input('package_price');
            $package->max_commission = $request->input('max_commission');
            $package->package_desc = $request->input('package_desc');
            $package->max_advertisement = $request->input('max_advertisement');

            if ($request->hasFile('package_image')) {
                $fileName = "package_image" . time() . '.' . $request->file('package_image')->getClientOriginalExtension();
                $path = $request->file('package_image')->move(public_path() . '/images/packageImages', $fileName);
                $package->package_image = 'images/packageImages/' . $fileName;

            }

            $package->save();

            foreach ($request->stages as $index => $stageData) {
                $stage = new Stage();
                $stage->package_id = $package->package_id;
                $stage->stage_number = $index + 1;
                $stage->commission = $stageData['commission'];
                $stage->number_of_members = $stageData['number_of_members'];
                $stage->business_value = $stageData['business_value'];
                $stage->save();
            }

            DB::commit();

            return redirect()->route('admin.show.packages')->with('success', 'Package created successfully.');
        }
         catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('admin.show.packages')->with('error', 'Failed to create Package.');
        }
    }

    public function updatePackage(Request $request, $package_id)
    {
        $validatedData = $request->validate([
            'package_name' => 'required|string|max:255',
            'package_price' => 'required|numeric',
            'max_commission' => 'required|numeric',
            'package_desc' => 'required|string',
            'max_advertisement' => 'required|numeric',
            'package_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'stages.*.commission' => 'required|numeric',
            'stages.*.number_of_members' => 'required|integer',
            'stages.*.business_value' => 'required|numeric',
        ]);

        $package = Package::findOrFail($package_id);
        $package->package_name = $request->input('package_name');
        $package->package_price = $request->input('package_price');
        $package->max_commission = $request->input('max_commission');
        $package->package_desc = $request->input('package_desc');
        $package->max_advertisement = $request->input('max_advertisement');
        $packageImage = $package->package_image;

        if ($request->hasFile('package_image')) {
            $fileName = "package_image" . time() . '.' . $request->file('package_image')->getClientOriginalExtension();
            $path = $request->file('package_image')->move(public_path() . '/images/packageImages', $fileName);
            $package->package_image = 'images/packageImages/' . $fileName;

            if (file_exists($packageImage)) {
                unlink($packageImage);
            }
        }
        $package->save();

        // Update stages for the package
        foreach ($request->stages as $index => $stageData) {
            $stage = Stage::where('package_id', $package_id)
                ->where('stage_number', $index + 1)
                ->first();

            if ($stage) {
                // Update existing stage
                $stage->commission = $stageData['commission'];
                $stage->number_of_members = $stageData['number_of_members'];
                // Recalculate business value based on new commission
                $stage->business_value = $stageData['commission'] / $stageData['number_of_members'];
                $stage->save();
            }
        }

        return redirect()->route('admin.show.packages')->with('success', 'Package updated successfully.');
    }

    public function showPackages()
    {
        $packages = Package::with('stages')->get();

        return view('admin.packages.index', ['packages' => $packages]);
    }

    public function editPackages($package_id)
    {
        // Retrieve the package with its stages
        $package = Package::with('stages')->findOrFail($package_id);

        return view('admin.packages.editPackage', compact('package'));
    }

    public function deletePackage($package_id)
    {
        $package = Package::find($package_id);

        if (!$package) {
            return redirect()->back()->with('error', 'Package not found.');
        }
        $package->delete();
        return redirect()->route('admin.show.packages')->with('success', 'Package deleted successfully.');
    }

    public function packageStatus(Request $request, $package_id)
    {
        $package = Package::find($package_id);

        if (!$package) {
            return response()->json(['error' => 'Package not found.'], 404);
        }

        $status = $request->input('is_active');
        $package->is_active = $status ? 1 : 0;
        $package->save();

        if (!empty($package)) {
            return redirect('packages')->with('success', 'Package Status Changed Successfully');
        } else {
            return redirect('packages')->with('error', 'Package Status Not Changed');
        }
    }

    public function showPackage($package_id)
    {
        $package = Package::find($package_id);
        $stages = DB::table('stages')->where('package_id', $package->package_id)->get();

        if (!$package) {
            abort(404); // Handle case where package is not found
        }
        return view('admin.packages.showPackage', compact('package', 'stages'));
    }

}

