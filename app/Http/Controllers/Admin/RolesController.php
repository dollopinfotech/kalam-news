<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Roles;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }
    public function addRoles()
    {
        return view('admin.roles-management.addRole');
    }

    public function getRoles()
    {
        $roles = DB::table('roles')->select('role_id', 'role_name', 'role_desc', 'page_privilage', 'is_active')->get();
        return view('admin.roles-management.rolesList', compact('roles'));
    }

    public function storeRoles(Request $request)
    {

        $request->validate([
            'role_name' => 'required',
            'role_desc' => 'required',
        ]);

        $role = new Roles();
        $role->role_name = $request->role_name;
        $role->role_desc = $request->role_desc;

        $role->save();
        return redirect()->route('admin.roles');
    }

    public function editRole($role_id)
    {
        $roles = Roles::find($role_id);
        return view('admin.roles-management.editRole', compact('roles'));
    }

    public function updateRole(Request $request, $role_id)
    {
        $request->validate([
            'role_name' => 'required',
            'role_desc' => 'required',
        ]);

        $role =  Roles::find($role_id);
        $role->role_name = $request->role_name;
        $role->role_desc = $request->role_desc;

        $role->save();
        return redirect()->route('admin.roles')->with('success', 'roles updated successfully');
    }
    public function destroyRole($role_id)
    {
        $role = Roles::find($role_id);
        $role->delete();
        return redirect()->route('admin.roles')->with('success', '');
    }

    public function rolePermission($id)
    {
        $roles = Roles::find($id);
        return view('admin.roles-management.rolePermission', compact('roles'));
    }

    public function addPermission(Request $request, $id)
    {

        $pagePrivilege = "";
        $menu_id = "";
        if (isset($request->menu_id)) {
            $menu_id = implode(",", $request->menu_id);
        }

        if (isset($request->pagePrivilege)) {
            $pagePrivilege = implode(",", $request->pagePrivilege);
        }

        $role =  Roles::find($id);
        $role->menu_id = $menu_id;
        $role->page_privilage = $pagePrivilege;

        $role->save();
        return redirect()->route('admin.roles')->with('success', 'roles permission updated successfully');
    }


    public function statusRole(Request $request, $role_id)
    {
        $roles = Roles::find($role_id);

        if (!$roles) {
            return response()->json(['error' => 'Roles not found.'], 404);
        }

        $status = $request->input('is_active');
        $roles->is_active = $status ? 1 : 0;
        $roles->save();

        if (!empty($roles)) {
            return redirect('roles')->with('success', 'Roles Status Changed Successfully');
        } else {
            return redirect('roles')->with('error', 'Roles Status not Changed');
        }
        // return response()->json(['success' => 'Roles Status Changed Successfully.']);
    }
}
