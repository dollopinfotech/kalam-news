<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Config;

class SMSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }
    public function viewPage(){
        return view('admin.sms.create');
    }
    public function sendSMS(Request $request)
    {
        // $sid    = env('TWILIO_SID');
        // $token  = env('TWILIO_AUTH_TOKEN');
        // $twilio = new Client($sid, $token);

        // $message = $twilio->messages->create(
        //     $request->input('to'),
        //     [
        //         'from' => env('TWILIO_PHONE_NUMBER'),
        //         'body' => $request->input('message')
        //     ]
        // );

        return "SMS sent successfully!";
    }
}
