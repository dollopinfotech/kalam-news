<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SocialMedia;
use Illuminate\Support\Facades\Config;

class SocialMediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }
    public function showLinks(){

        $socialMedia = SocialMedia::all();
        return view('admin.follow-us.index', compact('socialMedia'));
    }

    public function store(Request $request)
    {

        $platform = $request->input('platform');
        $links = $request->input('links');

        $socialMedia = new SocialMedia;
        $socialMedia->platform = $platform;
        $socialMedia->links = $links;

        if ($request->hasFile('icon')) {
            $fileName = "icon" . time() . '.' . $request->file('icon')->getClientOriginalExtension();
            $path = $request->file('icon')->move(public_path() . '/images/icons', $fileName);
            $socialMedia->icon = 'images/icons/' . $fileName;
        }
        $socialMedia->save();
        return redirect()->back()->with('success', 'Data has been successfully stored.');
    }

    public function editFollow($social_id)
    {
        $socialMedia = SocialMedia::findOrFail($social_id);
        return view('admin.follow-us.index', compact('socialMedia'));
    }


    public function updateFollow(Request $request)
    {
        $socialMedia = SocialMedia::findOrFail($request->social_id);

        $socialMedia->platform = $request->input('platform');
        $socialMedia->links = $request->input('links');
        $mediaIcon = $socialMedia->icon;

        if ($request->hasFile('icon')) {
            $fileName = "icon" . time() . '.' . $request->file('icon')->getClientOriginalExtension();
            $path = $request->file('icon')->move(public_path() . '/images/icons', $fileName);
            $socialMedia->icon = 'images/icons/' . $fileName;

            if (file_exists($mediaIcon)) {
                unlink($mediaIcon);
            }
        }

        $socialMedia->save();

        return redirect()->route('admin.showLinks')->with('success', 'Data has been successfully updated.');
    }


       public function deleteLinks($social_id)
    {
        $socialMedia = SocialMedia::findOrFail($social_id);
        $socialMedia->delete();

        return redirect()->route('admin.showLinks')->with('success', ' deleted successfully');
    }





}
