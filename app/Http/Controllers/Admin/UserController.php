<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BankDetail;
use App\Models\City;
use App\Models\Stage;
use App\Models\States;
use App\Models\User;
use App\Models\UserPackage;
use App\Models\UsersDocuments;
use App\Models\WalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web', ['except' => ['adminRegister', 'adminLogin', "getCitiesByState"]]);
        Config::set('auth.providers.users.model', \App\Models\Admin::class);
    }

    public function getUsers()
    {
        $users = User::where(function ($query) {
            $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
        })
            ->join('cities', 'users.city_id', '=', 'cities.city_id')
            ->join('states', 'users.state_id', '=', 'states.state_id')
            ->get();

        return view('admin.user-management.index', compact('users'));
    }

    public function getUser($id)
    {
        $user = User::find($id);

        $states = States::all();
        // $stateId = $request->state_id;

        $cities = City::all();

        $userDocs = DB::table('users_documents')->where('user_id', $id)->get();
        // dd($document);
        $bankAccounts = DB::table('bank_details')->where('user_id', $id)->get();

        // dd($bankAccounts);
        if (!empty($user)) {
            return view('admin.user-management.user', compact('user', 'bankAccounts', 'states', 'userDocs', 'cities'));
        } else {
            return redirect('users')->with('error', 'User data not found');
        }
    }

    public function updateUser(Request $request, $user_id)
    {
        $user = User::find($user_id);

        $user->first_name = $request->input('first_name', $user->first_name);
        $user->middle_name = $request->input('middle_name', $user->middle_name);
        $user->last_name = $request->input('last_name', $user->last_name);
        // $user->dob = $request->input('dob', $user->dob);
        $user->dob = date('Y-m-d', strtotime($request->input('dob', $user->dob)));
        $user->mobile_number = $request->input('mobile_number', $user->mobile_number);
        $user->whatsapp_number = $request->input('whatsapp_number', $user->whatsapp_number);
        $user->email = $request->input('email', $user->email);
        $user->pincode = $request->input('pincode', $user->pincode);
        $user->city_id = $request->input('city_id', $user->city_id);
        $user->state_id = $request->input('state_id', $user->state_id);
        $user->address = $request->input('address', $user->address);
        $userProfile = $user->profile;

        if ($request->hasFile('profile')) {
            $fileName = "profile" . time() . '.' . $request->file('profile')->getClientOriginalExtension();
            $path = $request->file('profile')->move(public_path() . '/images/usersImages', $fileName);
            $user->profile = 'images/usersImages/' . $fileName;

            if (file_exists($userProfile)) {
                unlink($userProfile);
            }
        }

        $user->save();
        if (!empty($user)) {
            return redirect('users')->with('success', 'User updated successfully');
        } else {
            return redirect('users')->with('error', 'User not updated');
        }
    }

    public function updateUserBank(Request $request, $bank_id)
    {
        // dd($request);
        $bankAccounts = BankDetail::find($bank_id);
        $bankAccounts->user_id = $request->input('user_id', $bankAccounts->user_id);
        $bankAccounts->customer_name = $request->input('customer_name', $bankAccounts->customer_name);

        $bankAccounts->account_no = $request->input('account_no', $bankAccounts->account_no);
        $bankAccounts->bank_name = $request->input('bank_name', $bankAccounts->bank_name);
        $bankAccounts->ifsc_code = $request->input('ifsc_code', $bankAccounts->ifsc_code);
        $bankAccounts->bank_branch = $request->input('bank_branch', $bankAccounts->bank_branch);

        $bankAccounts->save();

        if (!empty($bankAccounts)) {
            return redirect()->back()->with('success', 'User Bank updated successfully');
        } else {
            return redirect()->back()->with('error', 'User Bank not updated');
        }
    }

    public function updateDocument(Request $request, $docs_id)
    {
        $userDocs = UsersDocuments::find($docs_id);
        $userDocs->aadhar_card_no = $request->input('aadhar_card_no', $userDocs->aadhar_card_no);
        $userDocs->pan_card_no = $request->input('pan_card_no', $userDocs->pan_card_no);

        if ($request->hasFile('aadhar_card_photo_front')) {
            $aadhar_card_photo_front = $request->file('aadhar_card_photo_front');
            $fileName = "aadhar_card_photo_front" . time() . '.' . $request->file('aadhar_card_photo_front')->getClientOriginalExtension();
            $destinationPath = 'images/userDocuments';
            $aadhar_card_photo_front->move($destinationPath, $destinationPath . '/' . $fileName);
            $userDocs->aadhar_card_photo_front = $destinationPath . '/' . $fileName;
        }
        if ($request->hasFile('aadhar_card_photo_back')) {
            $aadhar_card_photo_back = $request->file('aadhar_card_photo_back');
            $fileName = "aadhar_card_photo_back" . time() . '.' . $request->file('aadhar_card_photo_back')->getClientOriginalExtension();
            $destinationPath = 'images/userDocuments';
            $aadhar_card_photo_back->move($destinationPath, $destinationPath . '/' . $fileName);
            $userDocs->aadhar_card_photo_back = $destinationPath . '/' . $fileName;
        }
        if ($request->hasFile('pan_card_photo')) {
            $pan_card_photo = $request->file('pan_card_photo');
            $fileName = "pan_card_photo" . time() . '.' . $request->file('pan_card_photo')->getClientOriginalExtension();
            $destinationPath = 'images/userDocuments';
            $pan_card_photo->move($destinationPath, $destinationPath . '/' . $fileName);
            $userDocs->pan_card_photo = $destinationPath . '/' . $fileName;
        }
        $userDocs->save();
        if (!empty($userDocs)) {
            return redirect('users')->with('success', 'User updated successfully');
        } else {
            return redirect('users')->with('error', 'User not updated');
        }
    }

    public function UserPackage($userPackage_id)
    {

        $userPackage = UserPackage::find($userPackage_id);

        if (!empty($userPackage)) {
            return view('admin.packages.showPackage', compact('userPackage', 'users'));
        } else {
            return redirect('users')->with('error', 'User data not found');
        }
    }

    public function userTree(Request $request)
    {
        if (isset($request->parent_user_id)) {
            $user = User::where('user_id', $request->parent_user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();

            // dd($user);
            $user['is_paid_user'] = getPaidUser($user->user_id);
            $user['user_level'] = 0;
            $userNodeCount = getUserNodeCount($user->user_id);
            $user['l_bv_count'] = $userNodeCount['l_user_count'];
            $user['r_bv_count'] = $userNodeCount['r_user_count'];
            $parentUserId = '';

            $user_id = $user->user_id;
        }
        // dd($user_id);
        if (isset($request->user_id) && $request->parent_user_id != $request->user_id) {
            $user = User::where('user_id', $request->user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();

            $user['is_paid_user'] = getPaidUser($request->user_id);
            getUserLevelForAdmin($request->parent_user_id, $request->user_id, 1);
            $user['user_level'] = Config::get('kalamnews.userLevel');
            $userNodeCount = getUserNodeCount($request->user_id);
            $user['l_bv_count'] = $userNodeCount['l_user_count'];
            $user['r_bv_count'] = $userNodeCount['r_user_count'];
            $parentUserId = User::select('user_id')->where('l_user_id', $request->user_id)->orWhere('r_user_id', $request->user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();
        }

        if (isset($user->l_user_id)) {
            $lUser = User::where('user_id', $user->l_user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();
            // dd($lUser);
            if (isset($lUser) && $lUser != '') {
                getUserLevelForAdmin($request->parent_user_id, $user->l_user_id, 1);
                $lUser['user_level'] = Config::get('kalamnews.userLevel');
                $lUser['is_paid_user'] = getPaidUser($user->l_user_id);
                $userNodeCount = getUserNodeCount($user->l_user_id);
                $lUser['l_bv_count'] = $userNodeCount['l_user_count'];
                $lUser['r_bv_count'] = $userNodeCount['r_user_count'];
            }
        }

        if (isset($user->r_user_id)) {
            $rUser = User::where('user_id', $user->r_user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();

            // dd($rUser);
            if (isset($rUser) && $rUser != '') {
                getUserLevelForAdmin($request->parent_user_id, $user->r_user_id, 1);
                $rUser['user_level'] = Config::get('kalamnews.userLevel');
                $rUser['is_paid_user'] = getPaidUser($user->r_user_id);
                $userNodeCount = getUserNodeCount($user->r_user_id);
                $rUser['l_bv_count'] = $userNodeCount['l_user_count'];
                $rUser['r_bv_count'] = $userNodeCount['r_user_count'];
            }
        }

        $data = [
            'success' => 'User tree fetched successfully',
            'parentUserId' => $parentUserId != '' ? $parentUserId->user_id : '',
            'user_details' => $user,
            'rUser' => isset($rUser) ? $rUser : array(),
            'lUser' => isset($lUser) ? $lUser : array(),
        ];

        // dd($data);
        if (isset($request->user_id)) {
            return response()->json($data);
        } else {
            return view('admin.user-management.userTree', compact("data"));
        }
    }

    public function adminRegisterUser()
    {
        $states = States::orderBy('state_name', 'asc')->get();
        $cities = []; // Initialize cities as an empty array

        return view('admin.user-management.addUser', compact('states', 'cities'));
    }

    public function storeUser(Request $request)
    {

        $user = new User();
        $user->initials = $request->input('initials', $user->initials);
        $user->first_name = $request->input('first_name', $user->first_name);
        $user->middle_name = $request->input('middle_name', $user->middle_name);
        $user->last_name = $request->input('last_name', $user->last_name);
        $user->dob = date('Y-m-d', strtotime($request->input('dob', $user->dob)));
        $user->mobile_number = $request->input('mobile_number', $user->mobile_number);
        $user->whatsapp_number = $request->input('whatsapp_number', $user->whatsapp_number);
        $user->email = $request->input('email', $user->email);
        $user->pincode = $request->input('pincode', $user->pincode);
        $user->state_id = $request->input('state_id', $user->state_id);
        $user->city_id = $request->input('city_id', $user->city_id);
        $user->address = $request->input('address', $user->address);
        $user->referred_by = $request->input('referred_by', $user->referred_by);

        if ($image = $request->file('profile')) {
            $fileName = 'profile' . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/usersImages', $fileName);
            $user->profile = 'images/usersImages/' . $fileName;
        }

        $firstNamePart = substr($request->input('first_name'), 0, 3);
        $randomStringPart = Str::random(8); // Generate an 8-character random string
        $password = $firstNamePart . $randomStringPart;
        $hashedPassword = Hash::make($password);

        $user->password = $hashedPassword;
        $firstNamePart = substr($request->input('first_name'), 0, 3);
        $randomStringPart = Str::random(5);
        $referralCode = strtoupper($firstNamePart . $randomStringPart);
        $user->referral_code = $referralCode;

        $user->is_mobile_verify = 1;
        $user->is_email_verify = 1;
        $user->save();
        if ($user) {
            $branch = $request->input('branch');
            $referred_by = $request->input('referred_by');
            $userReferrar = User::where('referral_code', $referred_by)->first();

            if ($userReferrar && $userReferrar->l_user_id == NULL && $branch == 'left') {
                $data['l_user_id'] = $user->user_id;
                User::where('referral_code', $referred_by)->update($data);
                return redirect('users')->with('success', 'User added successfully');
            } elseif ($userReferrar && $userReferrar->r_user_id == NULL && $branch == 'right') {
                $data['r_user_id'] = $user->user_id;
                User::where('referral_code', $referred_by)->update($data);
                return redirect('users')->with('success', 'User added successfully');
            } else {

                $userTree = array();

                if ($branch == 'left') {
                    getLastLeftUser($userReferrar->l_user_id, 1, $userTree);
                    $value =  Config::get('kalamnews.userTree');
                    $lUserDetails = end($value);
                    $data['l_user_id'] = $user->user_id;
                    User::where('user_id', $lUserDetails['user_details']['user_id'])->update($data);
                    return redirect('users')->with('success', 'User added successfully');
                } else if ($branch == 'right') {
                    getLastRightUser($userReferrar->r_user_id, 1, $userTree);
                    $value =  Config::get('kalamnews.userTree');
                    $rUserDetails = end($value);
                    $data['r_user_id'] = $user->user_id;
                    User::where('user_id', $rUserDetails['user_details']['user_id'])->update($data);
                    return redirect('users')->with('success', 'User added successfully');
                }
            }
        }
    }

    public function getUserTree($referred_by, $level_count, $userTree)
    {
        $userReferrar = User::where('referral_code', $referred_by)->where('is_mobile_verify', 1 || 'is_email_verify', 1)->first();

        if ($userReferrar != null && $level_count <= 15) {
            $arr = ['level_id' => $level_count, 'user_details' => $userReferrar];
            array_push($userTree, $arr);
            $this->getUserTree($userReferrar->referred_by, $level_count + 1, $userTree);
        }

        if ($userReferrar == null || $level_count >= 15) {
            Config::set('kalamnews.userTree', $userTree);
        }
    }

    public function getCommission($commissionByUserId)
    {
        $value = Config::get('kalamnews.userTree');

        foreach ($value as $user) {
            $level_id = $user['level_id'];
            $user_id = $user['user_details']['user_id'];
            $userPackage = UserPackage::where('user_id', $user_id)->first();

            if (!empty($userPackage) && isset($userPackage->package_id)) {
                $package_details = Stage::join('packages', 'stages.package_id', '=', 'packages.package_id')
                    ->select('stages.*', 'packages.package_name', 'packages.package_image')
                    ->where('stages.package_id', $userPackage->package_id)
                    ->get();

                foreach ($package_details as $packageArr) {
                    if ($packageArr['stage_number'] == $level_id) {
                        $walletHistory = new WalletHistory();
                        $walletHistory->user_id = $user_id;
                        $walletHistory->commission_by_user_id = $commissionByUserId;
                        $walletHistory->amount = $packageArr['business_value'];
                        $walletHistory->commission_level = $level_id;
                        $walletHistory->transaction_type = "income";
                        $walletHistory->payment_type = "wallet";
                        $walletHistory->save();
                    }
                }
            }
        }
    }

    public function checkReferralCode(Request $request)
    {
        $referral_code = $request->input('referral_code');

        $existingEntry = User::where('referral_code', $referral_code)->first();

        if ($existingEntry) {
            return true;
        } else {
            return false;
        }
    }
    public function checkUniqueMobileNumber(Request $request)
    {
        $mobile_number = $request->input('mobile_number');
        $existingEntry = User::where('mobile_number', $mobile_number)->exists();

        if ($existingEntry) {
            return response(['is_exists' => 1], 200);
        } else {
            return response(['is_exists' => 0], 200);
        }
    }
    public function checkUniqueWhatsAppNumber(Request $request)
    {
        $whatsapp_number = $request->input('whatsapp_number');
        $existingEntry = User::where('whatsapp_number', $whatsapp_number)->exists();

        if ($existingEntry) {
            return response(['is_exists' => 1], 200);
        } else {
            return response(['is_exists' => 0], 200);
        }
    }
    public function checkUniqueEmail(Request $request)
    {
        $email = $request->input('email');
        $existingEntry = User::where('email', $email)->exists();

        if ($existingEntry) {
            return response(['is_exists' => 1], 200);
        } else {
            return response(['is_exists' => 0], 200);
        }
    }

    public function showUserTransactions($user_id)
    {
        $transactions = WalletHistory::where('user_id', $user_id)->get();

        return view('admin.user-management.userTransaction', ['transactions' => $transactions]);
    }

    public function manageKyc()
    {
        $userDetails = DB::table('users')
            // ->select('users.*', 'bank_details.*', 'users_documents.*', 'users.user_id')
            // ->leftJoin('bank_details', 'users.user_id', '=', 'bank_details.user_id')
            // ->leftJoin('users_documents', 'users.user_id', '=', 'users_documents.user_id')
            ->where(function ($query) {
                $query->where('users.is_mobile_verify', 1)
                    ->orWhere('users.is_email_verify', 1);
            })
            ->get();

        if (!empty($userDetails)) {
            return view('admin.user-management.manageKyc', compact('userDetails'));
        } else {
            return redirect('manageKyc')->with('error', 'User KYC data not found');
        }
    }

    public function changeAdharVerifyStatus(Request $request)
    {
        $userDocs = UsersDocuments::find($request->docs_id);
        $userDocs->is_aadhar_verified = $request->input('is_aadhar_verified', $userDocs->is_aadhar_verified);
        $userDocs->is_pan_verified = $request->input('is_pan_verified', $userDocs->is_pan_verified);

        $userDocs->save();

        if (!empty($userDocs)) {
            return redirect('manageKyc')->with('success', 'KYC Status Changed Successfully');
        } else {
            return redirect('manageKyc')->with('error', 'KYC Status not Changed');
        }
    } 

    public function changeBankVerifyStatus(Request $request)
    {
        $userBank = BankDetail::find($request->bank_id);
        $userBank->is_bank_account_verify = $request->input('is_bank_account_verify', $userBank->is_bank_account_verify);

        $userBank->save();

        if (!empty($userBank)) {
            return redirect('manageKyc')->with('success', 'KYC Status Changed Successfully');
        } else {
            return redirect('manageKyc')->with('error', 'KYC Status not Changed');
        }
    }
}
