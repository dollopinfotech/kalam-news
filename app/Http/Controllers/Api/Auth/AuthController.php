<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\States;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;
use stdClass;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['checkReferral', 'registration', 'matchOtp', 'resendOtp', 'login', 'resetPassword', 'forgotPassword', 'register']]);
    }

    public function checkReferral(Request $request)
    {
        $referral_code = $request->input('referral_code');

        $user = User::select('first_name', 'middle_name', 'last_name', 'mobile_number', 'referral_code', 'cities.city_name', 'r_user_id', 'l_user_id')
            ->leftJoin('cities', 'users.city_id', 'cities.city_id')
            ->where('mobile_number', $referral_code)->orwhere('referral_code', $referral_code)->where('is_mobile_verify', 1 || 'is_email_verify', 1)->first();

        if ($user) {
            $data = [
                'name' => $user['first_name'],
                'mobile_number' => $user['mobile_number'],
                'referral_code' => $user['referral_code'],
                'city' => $user['city_name'],
                'left' => $user['l_user_id'] == '' || $user['l_user_id'] == null ? true : false,
                'right' => $user['r_user_id'] == '' || $user['r_user_id'] == null ? true : false,
            ];

            $lUserTree = array();
            if (isset($user['l_user_id']) && $user['l_user_id'] != "") {
                getLastLeftUser($user['l_user_id'], 1, $lUserTree);
                $value =  Config::get('kalamnews.userTree');
                $lUserDetails = end($value);
            }
            $rUserTree = array();
            if (isset($user['r_user_id']) && $user['r_user_id'] != "") {
                getLastRightUser($user['r_user_id'], 1, $rUserTree);
                $value =  Config::get('kalamnews.userTree');
                $rUserDetails = end($value);
            }

            return response()->json([
                "status" => 'Success',
                "message" => "Referral details fetched successfully",
                "referral_user_data" => $data,
                "l_user_data" => isset($lUserDetails) && $lUserDetails != "" ? $lUserDetails : new stdClass,
                "r_user_data" => isset($rUserDetails) && $rUserDetails != "" ? $rUserDetails : new stdClass,
            ], 200);
        } else {
            return response()->json(["status" => 'Failed', 'message' => 'Invalid referral code'], 400);
        }
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'initials' => 'required|in:Mr,Mrs,Ms,Mst',
            'first_name' => 'required|string',
            'middle_name' => 'string|nullable',
            'last_name' => 'required|string',
            'dob' => 'required|date|regex:/^\d{4}-\d{2}-\d{2}$/',
            'mobile_number' => 'required|numeric|digits:10',
            'whatsapp_number' => 'sometimes|numeric|digits:10|nullable',
            'email' => ['string', 'email',  'regex:/^.+@.+\..+$/i', 'nullable'],
            'password' => 'required|min:8',
            'address' => 'required|string',
            'city_id' => 'required|numeric|exists:cities,city_id',
            'state_id' => 'required|numeric|exists:states,state_id',
            'pincode' => 'required|numeric',
            'referred_by' => 'required|string|exists:users,referral_code',
            'branch' => 'string|required|in:left,right',
        ], [
            'email.email' => 'The email must be a valid email.',
            'email.regex' => 'The email must be a valid email.',
            'dob.regex' => 'The supported format for dob is yyyy-mm-dd.',
            'mobile_number.unique' => 'Mobile number already Exists.',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->first()], 400);
        }

        // $email = $request->input('email');
        $referred_by = $request->input('referred_by');
        $branch = $request->input('branch');
        $mobile_number = $request->input('mobile_number');

        $checkMobile = User::where('mobile_number', $mobile_number)
            ->where(function ($query) {
                $query->where('is_mobile_verify', 1)
                    ->orWhere('is_email_verify', 1);
            })
            ->first();

        // $checkEmail = User::where('email', $email)
        //     ->where(function ($query) {
        //         $query->where('is_email_verify', 1)
        //             ->orWhere('is_mobile_verify', 1);
        //     })
        //     ->first();


        if (!empty($checkMobile)) {
            return response()->json(['message' => "Mobile Number Already Exist"], 400);
        }

        // if (!empty($checkEmail)) {
        //     return response()->json(['message' => "Email Already Exist"], 400);
        // }

        $otp = rand(123456, 999999);
        $password = $request->input('password');
        $hashedPassword = Hash::make($password);
        $firstNamePart = substr($request->input('first_name'), 0, 3);
        $randomStringPart = Str::random(5);
        $referralCode = strtoupper($firstNamePart . $randomStringPart);
        $user = new User($request->all());
        $user->password = $hashedPassword;
        $user->referral_code = $referralCode;
        $user->otp = $otp;
        $user->save();

        if ($user) {
            $branch = $request->input('branch');
            $referred_by = $request->input('referred_by');
            $userReferrar = User::where('referral_code', $referred_by)->first();

            if ($userReferrar && $userReferrar->l_user_id == NULL && $branch == 'left') {
                $data['l_user_id'] = $user->user_id;
                User::where('referral_code', $referred_by)->update($data);
            } elseif ($userReferrar && $userReferrar->r_user_id == NULL && $branch == 'right') {
                $data['r_user_id'] = $user->user_id;
                User::where('referral_code', $referred_by)->update($data);
            } else {
                $userTree = array();
                if ($branch == 'left') {
                    getLastLeftUser($userReferrar->l_user_id, 1, $userTree);
                    $value =  Config::get('kalamnews.userTree');
                    $lUserDetails = end($value);
                    $data['l_user_id'] = $user->user_id;
                    User::where('user_id', $lUserDetails['user_details']['user_id'])->update($data);
                } else if ($branch == 'right') {
                    getLastRightUser($userReferrar->r_user_id, 1, $userTree);
                    $value =  Config::get('kalamnews.userTree');
                    $rUserDetails = end($value);
                    $data['r_user_id'] = $user->user_id;
                    User::where('user_id', $rUserDetails['user_details']['user_id'])->update($data);
                }
            }
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://msg.mtalkz.com/V2/http-api-post.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "apikey": "jeYIkPsFXzXggNjv",
                "senderid": "KALMNS",
                "number": ' . $mobile_number . ',
                "message": "Dear ' . $user->first_name . ' ' . $user->last_name . ', your OTP for Kalam News Publications is ' . $otp . '. Please use this code to complete your verification. Do not share this code with anyone. Thank you!",
                "format": "json",
                "templateid": "1107171923554124973"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;

        return response()->json([
            "status" => 'Success',
            "message" => "OTP send successfully",
            'otp' => $user->otp,
        ]);
    }

    public function matchOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required|numeric|digits:10|exists:users,mobile_number',
            'otp' => 'required|numeric|exists:users,otp',
            'otp_type' => 'required|in:registration,forgotPassword',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $mobile_number = $request->input('mobile_number');
        $otp = $request->input('otp');
        $otp_type = $request->input('otp_type');

        $user = User::where('mobile_number', $mobile_number)->orderBy('user_id', 'DESC')->first();
        if ($user->otp !=  $otp) {
            // dd($user->otp !=  $otp);
            return response()->json(['message' => 'Invalid OTP'], 402);
        }

        // $is_user_commission = $user->is_mobile_verify;

        if ($otp_type === 'registration') {
            try {

                $user->is_mobile_verify = 1;
                $user->save();
                $city = City::where('city_id', $user->city_id)->first();
                $state = States::where('state_id', $user->state_id)->first();
                $user['city_name'] = $city->city_name;
                $user['state_name'] = $state->state_name;
                $token = JWTAuth::fromUser($user);
                $percentageArr = getProfilePercentage($user->user_id);

                // if ($is_user_commission == 0) {
                //     $userTree = array();
                //     $this->getUserTree($user->referred_by, 1, $userTree);
                //     $this->getCommission($user->user_id);
                // }

                return response()->json(['message' => 'User registered successfully', 'access_token' => $token, 'user_details' => $user, 'profile_percentage' => $percentageArr[0], 'profile_percentage_arr' => $percentageArr[1]], 200);
            } catch (JWTException $e) {
                return response()->json(['message' => 'User could not be created'], 500);
            }
        } elseif ($otp_type === 'forgotPassword') {
            return response()->json(['message' => 'Otp verified successfully'], 200);
        }
    }

    public function resendOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required_without_all:email|numeric|digits:10',
            'email' => 'required_without_all:mobile_number|string',
        ], [
            'mobile_number.required_without_all' => 'Either mobile number or email is required.',
            'email.required_without_all' => 'Either mobile number or email is required.'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $email = $request->input('email');
        $mobile_number = $request->input('mobile_number');

        $user = User::select('user_id', 'mobile_number', 'email', 'otp', 'first_name', 'last_name')
            ->when($mobile_number !== null, function ($query) use ($mobile_number) {
                $query->where('mobile_number', $mobile_number);
            })
            ->when($email !== null, function ($query) use ($email) {
                $query->where('email', $email);
            })->orderBy('user_id', 'DESC')
            ->first();

        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $otp = rand(123456, 999999);

        $user->otp = $otp;
        $user->save();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://msg.mtalkz.com/V2/http-api-post.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "apikey": "jeYIkPsFXzXggNjv",
                "senderid": "KALMNS",
                "number": ' . $mobile_number . ',
                "message": "Dear ' . $user->first_name . ' ' . $user->last_name . ', your OTP for Kalam News Publications is ' . $otp . '. Please use this code to complete your verification. Do not share this code with anyone. Thank you!",
                "format": "json",
                "templateid": "1107171923554124973"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;

        return response()->json(['message' => 'OTP resend successfully', 'otp' => $otp], 200);
    }

    public function login(Request $request)
    {
        $mobile_number = $request->input('mobile_number');
        $password = $request->input('password');

        $user = User::select('user_id', 'mobile_number', 'email', 'password', 'is_mobile_verify', 'is_email_verify')
            ->where('mobile_number', $mobile_number)->orwhere('email', $mobile_number)->where('is_mobile_verify', 1 || 'is_email_verify', 1)->first();
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        if ($user->is_mobile_verify == 0 && $user->is_email_verify == 0) {
            return response()->json(['message' => 'User not found'], 404);
        }

        // $u = JWTAuth::attempt(['mobile_number' => $mobile_number, 'password' => $password]);

        if (Hash::check($password, $user['password'])) {
            try {
                $userModel = User::find($user['user_id']);
                $city = City::where('city_id', $userModel->city_id)->first();
                $state = States::where('state_id', $userModel->state_id)->first();
                $userModel['city_name'] = $city->city_name;
                $userModel['state_name'] = $state->state_name;
                $token = JWTAuth::fromUser($userModel);
                $percentageArr = getProfilePercentage($user['user_id']);
                return response()->json(['message' => 'Successfully', 'access_token' => (string)$token, 'user_details' => $userModel, 'profile_percentage' => $percentageArr[0], 'profile_percentage_arr' => $percentageArr[1]], 200);
            } catch (JWTException $e) {
                return response()->json(['message' => 'Could not create token'], 500);
            }
        } else {
            return response()->json(['message' => 'Invalid credentials'], 402);
        }
    }

    public function resetPassword(Request $request)
    {
        $validate = $request->validate([
            'mobile_number' => 'required_without_all:email|string|digits:10|exists:App\Models\User,mobile_number',
            'email' => 'required_without_all:mobile_number|string|exists:users,email',
            'password' => 'required|string|min:8|',
        ], [
            'mobile_number.required_without_all' => 'Either mobile number or email is required.',
            'email.required_without_all' => 'Either mobile number or email is required.'
        ]);

        $mobile_number = $request->input('mobile_number');
        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::select('user_id', 'mobile_number', 'email', 'password')
            ->when($mobile_number !== null, function ($query) use ($mobile_number) {
                $query->where('mobile_number', $mobile_number);
            })
            ->when($email !== null, function ($query) use ($email) {
                $query->where('email', $email);
            })
            ->first();

        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        } else {
            try {
                $user = User::find($user['user_id']);
                $user->password = Hash::make($password);
                $user->save();
                return response()->json(['message' => 'Password updated successfully'], 200);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Failed to update password'], 500);
            }
        }
    }

    public function forgotPassword(Request $request)
    {
        $validate = $request->validate([
            'mobile_number' => 'required_without_all:email|string|digits:10|exists:App\Models\User,mobile_number',
            'email' => 'required_without_all:mobile_number|string|exists:users,email',
        ], [
            'mobile_number.required_without_all' => 'Either mobile number or email is required.',
            'email.required_without_all' => 'Either mobile number or email is required.'
        ]);

        $mobile_number = $request->input('mobile_number');
        $email = $request->input('email');

        $user = User::select('user_id', 'mobile_number', 'email', 'otp', 'first_name', 'last_name')
            ->when($mobile_number !== null, function ($query) use ($mobile_number) {
                $query->where('mobile_number', $mobile_number);
            })
            ->when($email !== null, function ($query) use ($email) {
                $query->where('email', $email);
            })
            ->orderBy('user_id', 'DESC')
            ->first();

        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        } else {
            try {
                $otp = rand(123456, 999999);
                $user->otp = $otp;
                $user->save();

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://msg.mtalkz.com/V2/http-api-post.php',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => '{
                "apikey": "jeYIkPsFXzXggNjv",
                "senderid": "KALMNS",
                "number": ' . $mobile_number . ',
                "message": "Dear ' . $user->first_name . ' ' . $user->last_name . ', your OTP for Kalam News Publications is ' . $otp . '. Please use this code to complete your verification. Do not share this code with anyone. Thank you!",
                "format": "json",
                "templateid": "1107171923554124973"
            }',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                // echo $response;

                return response()->json(['message' => 'OTP send successfully', 'otp' => $otp], 200);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Failed to generate otp'], 500);
            }
        }
    }

    public function changePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:8',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }
        $old_password = $request->input('old_password');
        $password = $request->input('password');
        $user = FacadesAuth::user();

        if (!$user) {
            return response()->json(['message' => "User not found"], 404);
        }

        if (!Hash::check($old_password, $user->password)) {
            return response()->json(['message' => "Old password does not match."], 400);
        } elseif (Hash::check($password, $user->password)) {
            return response()->json(['message' => "Old password and new password cannot be same."], 400);
        } else {
            User::where('user_id', $user->user_id)->update(['password' => Hash::make($password)]);
        }
        return response()->json([
            "message" => "Password changed successfully!"
        ], 200);
    }
}
