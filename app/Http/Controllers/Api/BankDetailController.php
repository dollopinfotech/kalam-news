<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BankDetailsRequest;
use App\Models\BankDetail;
use App\Models\UserPackage;
use App\Models\WalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Razorpay\Api\Api;
use Illuminate\Support\Str;

class BankDetailController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['addBankDetails']]);
    }

    public function addBankDetails(BankDetailsRequest $request)
    {
        // Get File Name & Extension
        $fileNameFront = $request->file('adhar_card_photo_front');
        $fileNameBack = $request->file('adhar_card_photo_back');
        $fileNamePanCard = $request->file('pan_card_photo');
        $fileExtensionFront = $fileNameFront->getClientOriginalExtension();
        $fileExtensionBack = $fileNameBack->getClientOriginalExtension();
        $fileExtensionPanCard = $fileNamePanCard->getClientOriginalExtension();

        // Create New Name Uploaded File
        $newFileNameFront = 'adharfront' . '_' . time() . '_' . '.' . $fileExtensionFront;
        $newFileNameBack = 'adharback' . '_' . time() . '_' . '.' . $fileExtensionBack;
        $newFileNamePanCard = 'pancard' . '_' . time() . '_' . '.' . $fileExtensionPanCard;

        //Move Uploaded File
        $destinationPath = 'images/kycDocs';
        $fileNameFront->move($destinationPath, $newFileNameFront);
        $fileNameBack->move($destinationPath, $newFileNameBack);
        $fileNamePanCard->move($destinationPath, $newFileNamePanCard);

        $adharCardPhoto = array();
        array_push($adharCardPhoto, $newFileNameFront);
        array_push($adharCardPhoto, $newFileNameBack);

        $bank_details = new BankDetail($request->all());
        $bank_details->adhar_card_photo = json_encode($adharCardPhoto);
        $bank_details->pan_card_photo = $newFileNamePanCard;
        $bank_details->save();

        return response()->json(['message' => 'Successfully', 200]);
    }

    public function editBankDetails(Request $request)
    {
        $bank_id = $request->input('bank_id');
        $bank_details = BankDetail::find($bank_id);

        if (!$bank_details) {
            return response()->json(['message' => 'Data not found', 200]);
        }

        $adhar_card_photo = json_decode($bank_details->adhar_card_photo);

        $adhar_font = $adhar_card_photo[0];
        $adhar_back = $adhar_card_photo[1];
        $pan_card = $bank_details->pan_card_photo;

        $destinationPath = 'images/kycDocs';
        if ($request->file('adhar_card_photo_front') && $request->file('adhar_card_photo_back')) {
            $fileNameFront = $request->file('adhar_card_photo_front');
            $fileNameBack = $request->file('adhar_card_photo_back');

            $fileExtensionFront = $fileNameFront->getClientOriginalExtension();
            $fileExtensionBack = $fileNameBack->getClientOriginalExtension();

            $newFileNameFront = 'adharfront' . '_' . time() . '_' . '.' . $fileExtensionFront;
            $newFileNameBack = 'adharback' . '_' . time() . '_' . '.' . $fileExtensionBack;

            $fileNameFront->move($destinationPath, $newFileNameFront);
            $fileNameBack->move($destinationPath, $newFileNameBack);

            $adharCardPhoto = array();
            array_push($adharCardPhoto, $newFileNameFront);
            array_push($adharCardPhoto, $newFileNameBack);

            $bank_details->adhar_card_photo = json_encode($adharCardPhoto);

            $path_adhar_front = $destinationPath . '/' . $adhar_font;
            $path_adhar_back = $destinationPath . '/' . $adhar_back;

            if (file_exists($path_adhar_front) && file_exists($path_adhar_back)) {
                unlink($path_adhar_front);
                unlink($path_adhar_back);
            }
        }

        if ($request->file('pan_card_photo')) {
            $fileNamePanCard = $request->file('pan_card_photo');
            $fileExtensionPanCard = $fileNamePanCard->getClientOriginalExtension();
            $newFileNamePanCard = 'pancard' . '_' . time() . '_' . '.' . $fileExtensionPanCard;

            $fileNamePanCard->move($destinationPath, $newFileNamePanCard);
            $bank_details->pan_card_photo = $newFileNamePanCard;

            $path = $destinationPath . '/' . $pan_card;

            if (file_exists($path)) {
                unlink($path);
            }
        }

        $bank_details->update(array('user_id' => $request->input('user_id'), 'customer_name' => $request->input('customer_name'), 'bank_name' => $request->input('bank_name'), 'account_no' => $request->input('account_no'), 'ifsc_code' => $request->input('ifsc_code'), 'mobile_no' => $request->input('mobile_no'), 'bank_branch' => $request->input('bank_branch'), 'adhar_card_no' => $request->input('adhar_card_no'), 'pan_card_no' => $request->input('pan_card_no')));
        return response()->json(['message' => 'Successfully', 200]);
    }

    public function getBankDetails($id)
    {
        $bank_details = BankDetail::find($id);
        return response()->json(['message' => 'Success', 'bank_details' =>  $bank_details, 200]);
    }

    public function addBankAccount(Request $request)
    {
        $request->validate([
            'ifsc_code' => 'required|string|max:255',
            'bank_name' => 'required|string|max:255',
            'bank_branch' => 'required|string|max:255',
            'account_no' => 'required|string|max:255',
            'customer_name' => 'required|string|max:255',
            'account_type' => 'required|in:current,saving',
            'bank_passbook' => 'requeired'
        ]);

        $user = auth()->user();
        $user_id = $user->user_id;

        $primaryBankAccount = BankDetail::where('user_id', $user_id)->where('primary_bank', true)->first();

        if ($primaryBankAccount) {
            $primaryBankAccount->primary_bank = false;
            $primaryBankAccount->save();
        }

        $bank_details = new BankDetail();

        $bank_details->ifsc_code = $request->input('ifsc_code');
        $bank_details->bank_name = $request->input('bank_name');
        $bank_details->bank_branch = $request->input('bank_branch');
        $bank_details->account_no = $request->input('account_no');
        $bank_details->customer_name = $request->input('customer_name');
        $bank_details->account_type = $request->input('account_type');
        $bank_details->user_id = $user_id;
        $bank_details->primary_bank = true;

        if ($imageBank = $request->file('bank_passbook')) {
            $fileNameBank = 'bankPhoto' . '_' . time() . '.' . $imageBank->getClientOriginalExtension();
            $imageBank->move(public_path() . '/images/kycDocs', $fileNameBank);
            $bank_details->bank_passbook = 'images/kycDocs/' . $fileNameBank;
        }

        $bank_details->save();

        return response()->json(['message' => 'Bank account details added successfully'], 200);
    }

    public function getBankAccounts()
    {
        $user = auth()->user();
        $user_id = $user->user_id;
        $bankAccounts = BankDetail::where('user_id', $user_id)->orderBy('bank_id', 'ASC')->get();

        return response()->json(['message' => 'Bank accounts fetched successfully', 'bank_accounts' => $bankAccounts], 200);
    }


    public function deleteBankAccount(Request $request)
    {
        $request->validate([
            'bank_id' => 'required|exists:bank_details,bank_id',
        ]);

        $bank_id = $request->input('bank_id');

        $bankAccount = BankDetail::find($bank_id);

        if ($bankAccount) {
            $bankAccount->delete();
            return response()->json(['message' => 'Bank account deleted successfully'], 200);
        } else {
            return response()->json(['error' => 'Bank account not found'], 404);
        }
    }

    public function updateBankAccounts(Request $request)
    {
        $request->validate([
            'bank_id' => 'required|exists:bank_details,bank_id,user_id,' . auth()->id(),
            'ifsc_code' => 'required|string|max:255',
            'bank_name' => 'required|string|max:255',
            'bank_branch' => 'required|string|max:255',
            'account_no' => 'required|string|max:255',
            'customer_name' => 'required|string|max:255',
            'account_type' => 'required|in:current,saving',
        ]);

        $bank_id = $request->input('bank_id');

        $bank_details = BankDetail::where('bank_id', $bank_id)
            ->where('user_id', auth()->id())
            ->first();

        if (!$bank_details) {
            return response()->json(['error' => 'Bank account not found'], 404);
        }

        $bank_details->ifsc_code = $request->input('ifsc_code');
        $bank_details->bank_name = $request->input('bank_name');
        $bank_details->bank_branch = $request->input('bank_branch');
        $bank_details->account_no = $request->input('account_no');
        $bank_details->customer_name = $request->input('customer_name');
        $bank_details->account_type = $request->input('account_type');
        $bank_photo = $bank_details->bank_passbook;
        $bank_details->is_bank_account_verify = 0;

        if ($imageBank = $request->file('bank_passbook')) {
            $fileNameBank = 'bankPhoto' . '_' . time() . '.' . $imageBank->getClientOriginalExtension();
            $imageBank->move(public_path() . '/images/kycDocs', $fileNameBank);
            $bank_details->bank_passbook = 'images/kycDocs/' . $fileNameBank;

            if (file_exists($bank_photo)) {
                unlink($bank_photo);
            }
        }

        $bank_details->save();

        return response()->json(['message' => 'Bank account details updated successfully'], 200);
    }

    public function changePrimaryBankAccount(Request $request)
    {
        $user = auth()->user();
        $user_id = $user->user_id;

        $bank_id = $request->input('bank_id');

        $bank_account = BankDetail::where('user_id', $user_id)
            ->where('bank_id', $bank_id)
            ->first();

        if (!$bank_account) {
            return response()->json(['error' => 'Bank account not found'], 404);
        }

        $bank_account->primary_bank = 1;

        BankDetail::where('user_id', $user_id)
            ->where('bank_id', '<>', $bank_id)
            ->update(['primary_bank' => 0]);

        $bank_account->save();

        return response()->json(['message' => 'Primary bank account changed successfully'], 200);
    }

    public function addUserKyc(BankDetailsRequest $request)
    {
        // Get File Name & Extension
        $fileNameFront = $request->file('adhar_card_photo_front');
        $fileNameBack = $request->file('adhar_card_photo_back');
        $fileNamePanCard = $request->file('pan_card_photo');
        $fileExtensionFront = $fileNameFront->getClientOriginalExtension();
        $fileExtensionBack = $fileNameBack->getClientOriginalExtension();
        $fileExtensionPanCard = $fileNamePanCard->getClientOriginalExtension();

        // Create New Name Uploaded File
        $newFileNameFront = 'adharfront' . '_' . time() . '_' . '.' . $fileExtensionFront;
        $newFileNameBack = 'adharback' . '_' . time() . '_' . '.' . $fileExtensionBack;
        $newFileNamePanCard = 'pancard' . '_' . time() . '_' . '.' . $fileExtensionPanCard;

        //Move Uploaded File
        $destinationPath = 'images/kycDocs';
        $fileNameFront->move($destinationPath, $newFileNameFront);
        $fileNameBack->move($destinationPath, $newFileNameBack);
        $fileNamePanCard->move($destinationPath, $newFileNamePanCard);

        $adharCardPhoto = array();
        array_push($adharCardPhoto, $newFileNameFront);
        array_push($adharCardPhoto, $newFileNameBack);

        $bank_details = new BankDetail($request->all());
        $bank_details->adhar_card_photo = json_encode($adharCardPhoto);
        $bank_details->pan_card_photo = $newFileNamePanCard;
        $bank_details->save();

        return response()->json(['message' => 'Successfully', 200]);
    }

    // public function withdrawCommission(Request $request)
    // {
    //     $user = auth()->user();
    //     $userBank = BankDetail::where('user_id', $user->user_id)->where('primary_bank', 1)->where('is_bank_account_verify', 1)->first();
    //     $userPackage = UserPackage::where('user_id', $user->user_id)->first();

    //     if (isset($userBank) && isset($userPackage)) {
    //         $api_key = env('RAZORPAY_KEY_ID');
    //         $api_secret = env('RAZORPAY_KEY_SECRET');
    //         $firstNamePart = substr($user->first_name, 0, 3);
    //         $randomStringPart = Str::random(11);
    //         $reference_id = strtoupper($firstNamePart . $randomStringPart);

    //         $ch = curl_init();

    //         curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/contacts');
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_POST, 1);
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
    //             'name' => $userBank->customer_name,
    //             'email' => $user->email,
    //             'contact' => $user->mobile_number,
    //             'type' => 'employee',
    //             'reference_id' => $reference_id,
    //             'notes' => [
    //                 'notes_key_1' => 'Tea, Earl Grey, Hot',
    //                 'notes_key_2' => 'Tea, Earl Grey… decaf.'
    //             ]
    //         ]));

    //         curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . $api_secret);

    //         $headers = [];
    //         $headers[] = 'Content-Type: application/json';
    //         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //         $result = curl_exec($ch);
    //         if (curl_errno($ch)) {
    //             echo 'Error:' . curl_error($ch);
    //         }
    //         curl_close($ch);

    //         $contact_data = json_decode($result, true);
    //         // dd($contact_data);
    //         $contact_id = $contact_data['id'];

    //         if (isset($contact_id) && $contact_id != "") {
    //             $ch = curl_init();

    //             curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/fund_accounts');
    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //             curl_setopt($ch, CURLOPT_POST, 1);
    //             curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
    //                 'contact_id' => $contact_id,
    //                 'account_type' => 'bank_account',
    //                 'bank_account' => [
    //                     'name' => $userBank->customer_name,
    //                     'ifsc' => $userBank->ifsc_code,
    //                     'account_number' => $userBank->account_no,
    //                 ]
    //             ]));

    //             curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . $api_secret);

    //             $headers = [];
    //             $headers[] = 'Content-Type: application/json';
    //             curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //             $result = curl_exec($ch);
    //             if (curl_errno($ch)) {
    //                 echo 'Error:' . curl_error($ch);
    //             }
    //             curl_close($ch);

    //             $fund_account_data = json_decode($result, true);
    //             $fund_account_id = $fund_account_data['id'];

    //             if (isset($fund_account_id) && $fund_account_id != "") {
    //                 $ch = curl_init();

    //                 curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/payouts');
    //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //                 curl_setopt($ch, CURLOPT_POST, 1);
    //                 curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
    //                     'account_number' => $userBank->account_no, // Your virtual account number
    //                     'fund_account_id' => $fund_account_id,
    //                     'amount' => $request->amount*100, // Amount in paise (e.g., 10000 paise = INR 100)
    //                     'currency' => 'INR',
    //                     'mode' => 'IMPS',
    //                     'purpose' => 'payout',
    //                     'queue_if_low_balance' => true,
    //                     'reference_id' => $reference_id,
    //                     'narration' => 'Commission Payout',
    //                     'notes' => [
    //                         'notes_key_1' => 'Salary',
    //                         'notes_key_2' => 'June'
    //                     ]
    //                 ]));

    //                 curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . $api_secret);

    //                 $headers = [];
    //                 $headers[] = 'Content-Type: application/json';
    //                 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    //                 $result = curl_exec($ch);
    //                 if (curl_errno($ch)) {
    //                     echo 'Error:' . curl_error($ch);
    //                 }
    //                 curl_close($ch);

    //                 $payout_data = json_decode($result, true);

    //                 dd($payout_data);
    //                 return response([
    //                     'message' => 'money withdraw success',
    //                     'payout_id' => $payout_data['id']
    //                 ]);
    //             }
    //         }
    //     } else {
    //         return response([
    //             'message' => 'You are not eligible for withdraw'
    //         ]);
    //     }
    // }

    // public function withdrawCommission(Request $request)
    // {
    //     $user = auth()->user();
    //     $userBank = BankDetail::where('user_id', $user->user_id)->where('primary_bank', 1)->where('is_bank_account_verify', 1)->first();
    //     $userPackage = UserPackage::where('user_id', $user->user_id)->first();
    //     $profilePercentage = getProfilePercentage($user->user_id);

    //     $amount = $request->input('amount');

    //     // Define the charges
    //     $tds_percentage = 6;
    //     $service_percentage = 6;

    //     // Calculate charges
    //     $tds_charge = ($tds_percentage / 100) * $amount;
    //     $service_charge = ($service_percentage / 100) * $amount;

    //     // Calculate total withdrawable amount
    //     $total_withdrawable_amount = $amount - $tds_charge - $service_charge;

    //     if (isset($profilePercentage) && $profilePercentage[0] == 100) {

    //         $walletHistory = new WalletHistory();
    //         $walletHistory->user_id = $user->user_id;
    //         $walletHistory->amount = $amount;
    //         $walletHistory->payment_type = "online";
    //         $walletHistory->transaction_type = "withdrawal";
    //         $walletHistory->save();

    //         if(isset($walletHistory) && $walletHistory->wallet_history_id != ""){
    //            $withdrawRequest = DB::table('withdraw_request')->insert([
    //                 'user_id' => $user->user_id,
    //                 'wallet_history_id' => $walletHistory->wallet_history_id,
    //                 'amount' => $amount,
    //                 'account_no' => $userBank->account_no,
    //                 'bank_name' => $userBank->bank_name,
    //                 'customer_name' => $userBank->customer_name,
    //                 'ifsc_code' => $userBank->ifsc_code,
    //                 'bank_branch' => $userBank->bank_branch,
    //                 'account_type' => $userBank->account_type,
    //                 'tds_charge_amount' => $tds_charge,
    //                 'service_charge_amount' => $service_charge,
    //                 'withdrawable_amount' => $total_withdrawable_amount,
    //             ]);

    //             if(isset($withdrawRequest) && $withdrawRequest != ""){

    //             }else{

    //                 return response([
    //                     'message' => 'Withdraw request failed',
    //                 ]);  
    //             }
    //         }else{
    //             return response([
    //                 'message' => 'Withdraw request failed',
    //             ]);  
    //         }
    //     } else {
    //         return response([
    //             'message' => 'Your profile is not complete',
    //         ]);
    //     }
    // }

    public function withdrawCommission(Request $request)
    {
        // Begin a database transaction
        DB::beginTransaction();

        try {
            $user = auth()->user();
            $userBank = BankDetail::where('user_id', $user->user_id)->where('primary_bank', 1)->where('is_bank_account_verify', 1)->first();
            $userPackage = UserPackage::where('user_id', $user->user_id)->first();
            $profilePercentage = getProfilePercentage($user->user_id);

            $amount = $request->input('amount');

            // Define the charges
            $tds_percentage = 6;
            $service_percentage = 6;

            // Calculate charges
            $tds_charge = ($tds_percentage / 100) * $amount;
            $service_charge = ($service_percentage / 100) * $amount;

            // Calculate total withdrawable amount
            $total_withdrawable_amount = $amount - $tds_charge - $service_charge;

            // if (isset($profilePercentage) && $profilePercentage[0] == 100) {
                $walletHistory = new WalletHistory();
                $walletHistory->user_id = $user->user_id;
                $walletHistory->amount = $request->input('amount');
                $walletHistory->payment_type = "online";
                $walletHistory->transaction_type = "withdrawal";
                $walletHistory->save();

                if (isset($walletHistory) && $walletHistory->wallet_history_id != "") {
                    $withdrawRequest = DB::table('withdraw_request')->insert([
                        'user_id' => $user->user_id,
                        'wallet_history_id' => $walletHistory->wallet_history_id,
                        'amount' => $request->input('amount'),
                        'account_no' => $userBank->account_no,
                        'bank_name' => $userBank->bank_name,
                        'customer_name' => $userBank->customer_name,
                        'ifsc_code' => $userBank->ifsc_code,
                        'bank_branch' => $userBank->bank_branch,
                        'account_type' => $userBank->account_type,
                        'tds_charge_amount' => $tds_charge,
                        'service_charge_amount' => $service_charge,
                        'withdrawable_amount' => $total_withdrawable_amount,
                    ]);

                    if (isset($withdrawRequest) && $withdrawRequest != "") {
                        // Commit the transaction if everything is successful
                        DB::commit();

                        return response([
                            'message' => 'Withdraw request successful',
                        ]);
                    } else {
                        // Roll back the transaction if the withdrawal request failed
                        DB::rollBack();

                        return response([
                            'message' => 'Withdraw request failed',
                        ]);
                    }
                } else {
                    // Roll back the transaction if the wallet history could not be saved
                    DB::rollBack();

                    return response([
                        'message' => 'Withdraw request failed',
                    ]);
                }
            // } else {
            //     // Roll back the transaction if the profile is not complete
            //     DB::rollBack();

            //     return response([
            //         'message' => 'Your profile is not complete',
            //     ]);
            // }
        } catch (\Exception $e) {
            // Roll back the transaction on any exception
            DB::rollBack();

            // Handle the exception or return an error response
            return response([
                'message' => 'An error occurred while processing the withdrawal request',
                'error' => $e->getMessage(), // Optionally, include the error message for debugging
            ]);
        }
    }
}
