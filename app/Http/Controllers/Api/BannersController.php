<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;

class BannersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['']]);
    }

    public function getBannerList()
    {
        $banner = Banner::all();
        return response()->json(['message' => 'Success', 'banner' =>  $banner]);
    }
}
