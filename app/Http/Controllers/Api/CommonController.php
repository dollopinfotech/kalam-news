<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Package;
use App\Models\SiteSettings;
use App\Models\SocialMedia;
use App\Models\States;
use App\Models\UserPackage;
use App\Models\UsersDocuments;
use App\Models\WalletHistory;
use Illuminate\Http\Request;
use stdClass;

class CommonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['getStateList', 'getCityList']]);
    }

    public function getStateList(Request $request)
    {

        $limit = $request->input('limit', 10);
        $offset = $request->input('offset', 0);
        $result = States::select('state_id', 'state_name', 'flag')
            ->where('country_id', '101')
            ->skip($offset * $limit)
            ->take($limit)
            ->get();

        if ($result) {
            return response([
                'message' => 'Success',
                'states' => $result,
            ], 200);
        } else {
            return response([
                'message' => 'No data found',
            ], 200);
        }
    }

    public function getCityList(Request $request)
    {
        $limit = $request->input('limit', 10);
        $offset = $request->input('offset', 0);
        $result = City::select('city_id', 'city_name', 'state_id')
            ->where('state_id', $request->state_id)
            ->skip($offset * $limit)
            ->take($limit)
            ->get();

        if ($result) {
            return response([
                'message' => 'Success',
                'cities' => $result,
            ], 200);
        } else {
            return response([
                'message' => 'No data found',
            ], 404);
        }
    }

    public function getAppSetting()
    {
        $siteSetting = SiteSettings::first();
        $followUs = SocialMedia::get();
        $user = auth()->user();
        $userKycDoc = UsersDocuments::where('user_id', $user->user_id)->first();

        $contacts = ['calling_number' => $siteSetting->calling_number, 'whatsapp' => $siteSetting->whatsapp, 'telegram' => $siteSetting->telegram, 'email' => $siteSetting->email];

        return response([
            'message' => 'Success',
            'kyc_add_or_not' => (string)(isset($userKycDoc) && $userKycDoc != '' ? 1 : 0),
            'contacts' => $contacts,
            'followUs' => $followUs,
            'kycDocument' => isset($userKycDoc) && $userKycDoc != '' ? $userKycDoc : new stdClass,
            'termCondition' => $siteSetting->term_condition,
            'welcomeMessage' => $siteSetting->welcome_message,
            'inviteMessage' => str_replace("REFERRAL_CODE", $user->referral_code, $siteSetting->invite_message),
        ], 200);
    }

    public function getUserDashboardBVCount()
    {
        $user = auth()->user();
        $userNodeCount = getUserNodeCount($user->user_id);
        $userDashboardBVCount = new stdClass;
        $wallet_history = WalletHistory::where('user_id', $user->user_id)->get();

        $left_commission = $wallet_history->where('commission_node', 'left')->sum('amount');
        $right_commission = $wallet_history->where('commission_node', 'right')->sum('amount');
        $total_withdrawal = $wallet_history->where('transaction_type', 'withdrawal')->sum('amount');
        $total_wallet_balance = ($left_commission + $right_commission) - $total_withdrawal;

        $userDashboardBVCount->left_commission = (string)$left_commission;
        $userDashboardBVCount->right_commission = (string)$right_commission;
        $userDashboardBVCount->total_withdrawal = (string)$total_withdrawal;
        $userDashboardBVCount->total_wallet_balance = (string)$total_wallet_balance;
        $userDashboardBVCount->l_user_node_count = (string)$userNodeCount['l_user_count'];
        $userDashboardBVCount->r_user_node_count = (string)$userNodeCount['r_user_count'];

        return response([
            'message' => 'Success',
            'userDashboardBVCount' => $userDashboardBVCount,
        ], 200);
    }

    public function getWalletHistory()
    {
        $user = auth()->user();

        // Fetch wallet history and user package
        $wallet_history = WalletHistory::where('user_id', $user->user_id)->orderBy('created_at', 'desc')->get();
        $userPackage = UserPackage::select('package_id')->where('user_id', $user->user_id)->orderBy('userPackage_id', 'DESC')->first();

        // Determine the maximum commission
        $max_commission = $userPackage ? Package::select('max_commission')->where('package_id', $userPackage->package_id)->first() : (object)['max_commission' => 0];

        // Filter wallet history based on status
        $valid_wallet_history = $wallet_history->filter(function ($item) {
            return in_array($item->status, [0, 1]);
        });

        // Calculate totals
        $total_withdrawal = $valid_wallet_history->where('transaction_type', 'withdrawal')->sum('amount');
        $total_commission = $valid_wallet_history->where('transaction_type', 'income')->sum('amount');

        // Node commissions
        $total_left_node_commission = $valid_wallet_history->where('transaction_type', 'income')->where('commission_node', 'left');
        $total_right_node_commission = $valid_wallet_history->where('transaction_type', 'income')->where('commission_node', 'right');

        // Level wise node count
        $leftNodeCount = $this->getLevelWiseNodeCount($valid_wallet_history, 'left');
        $rightNodeCount = $this->getLevelWiseNodeCount($valid_wallet_history, 'right');

        // Node pairs
        $nodePairArr = $this->getNodePairs($leftNodeCount, $rightNodeCount);

        // Node commission arrays
        $leftNodeCommissionArr = $this->getNodeCommissionArray($total_left_node_commission, 'left');
        $rightNodeCommissionArr = $this->getNodeCommissionArray($total_right_node_commission, 'right');

        // Sorting commission arrays
        $rightNodeCommissionArr = collect($rightNodeCommissionArr)->sortBy('wallet_history_id')->toArray();
        $leftNodeCommissionArr = collect($leftNodeCommissionArr)->sortBy('wallet_history_id')->toArray();

        // Calculate total commission balance
        $totalCommissionBalanceArr = $this->getTotalCommissionBalance($nodePairArr, $leftNodeCommissionArr, $rightNodeCommissionArr);

        // Calculate available balance
        $available_balance = array_reduce($totalCommissionBalanceArr, function ($carry, $item) {
            return $carry + $item['amount'];
        }, 0);

        // Calculate percentage
        $percentage = $max_commission->max_commission > 0 ? ($total_withdrawal / $max_commission->max_commission) * 100 : 0;

        // Return JSON response
        return response()->json([
            'success' => true,
            'total_withdrawal' => (string)$total_withdrawal,
            'total_commission' => (string)$total_commission,
            'available_balance' => (string)($available_balance - $total_withdrawal),
            'max_earning' => (string)$max_commission->max_commission,
            'wallet_balance' => (string)($total_commission - $total_withdrawal),
            'wallet_history' => $wallet_history,
            'limit_balance' => (string)($max_commission->max_commission - $total_withdrawal),
            'percentage' => (int)$percentage,
        ]);
    }

    private function getLevelWiseNodeCount($wallet_history, $node)
    {
        $nodeCount = [];
        for ($level = 1; $level <= 15; $level++) {
            $level_vise_node_count = $wallet_history->where('transaction_type', 'income')->where('commission_node', $node)->where('commission_level', $level)->count('amount');
            array_push($nodeCount, $level_vise_node_count);
        }
        return $nodeCount;
    }

    private function getNodePairs($leftNodeCount, $rightNodeCount)
    {
        $nodePairArr = [];
        foreach ($leftNodeCount as $key => $value) {
            $nodePairArr[$key] = min($value, $rightNodeCount[$key]);
        }
        return $nodePairArr;
    }

    private function getNodeCommissionArray($node_commission, $node)
    {
        $nodeCommissionArr = [];
        foreach ($node_commission as $commission) {
            $arr = [
                'wallet_history_id' => $commission->wallet_history_id,
                'child_level' => $commission->commission_level,
                'commission_node' => $node,
                'amount' => $commission->amount,
            ];
            array_push($nodeCommissionArr, $arr);
        }
        return $nodeCommissionArr;
    }

    private function getTotalCommissionBalance($nodePairArr, $leftNodeCommissionArr, $rightNodeCommissionArr)
    {
        $totalCommissionBalanceArr = [];
        foreach ($nodePairArr as $key => $node_pair_count) {
            $leftPairCommissionArr = $this->getPairCommissionArray($leftNodeCommissionArr, $key + 1);
            $rightPairCommissionArr = $this->getPairCommissionArray($rightNodeCommissionArr, $key + 1);

            if ($key + 1 === 2 && $node_pair_count > 2) {
                continue;
            }

            $pair_count = min(count($leftPairCommissionArr), count($rightPairCommissionArr));

            for ($i = 0; $i < $pair_count; $i++) {
                if ($i < $node_pair_count) {
                    array_push($totalCommissionBalanceArr, $leftPairCommissionArr[$i]);
                    array_push($totalCommissionBalanceArr, $rightPairCommissionArr[$i]);
                }
            }
        }

        return $totalCommissionBalanceArr;
    }

    private function getPairCommissionArray($nodeCommissionArr, $level)
    {
        return array_values(array_filter($nodeCommissionArr, function ($commission) use ($level) {
            return isset($commission['child_level']) && $commission['child_level'] === $level;
        }));
    }
}
