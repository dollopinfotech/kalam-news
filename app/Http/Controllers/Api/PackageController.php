<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Stage;
use App\Models\User;
use App\Models\UserCommission;
use App\Models\UserPackage;
use App\Models\WalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    public function __construct()
    {
        # By default we are using here auth:api middleware
        $this->middleware('auth:api', ['except' => ['']]);
    }

    public function getPackage(Request $request)
    {
        $userPackage = UserPackage::where('user_id', auth()->user()->user_id)->orderBy('userPackage_id', 'DESC')->first();
        if(isset($userPackage) && $userPackage->package_id != ''){
            $userPackageDetails = Package::select('package_id', 'package_name', 'package_price', 'package_image')->where('package_id', $userPackage->package_id)->first();
            $result = Package::select('package_id', 'package_name', 'package_price', 'package_image')->where('package_price', '>=', $userPackageDetails->package_price)->orderBy('package_price', 'ASC')->get();
        }else{
            $result = Package::select('package_id', 'package_name', 'package_price', 'package_image')->orderBy('package_price', 'ASC')->get();
        }
 
        $package_list  = array();
        foreach($result as $package){
            $arr = ['package_id' => $package->package_id, 'package_name' => $package->package_name, 'package_price' => $package->package_price, 'package_image' => $package->package_image, 'is_package_purchage' => (isset($userPackageDetails->package_id) && $package->package_id == $userPackageDetails->package_id ? 1 : 0)];
            array_push($package_list, $arr);
        }

        if ($package_list) {
            return response([
                'message' => 'Success',
                'is_user_package' => $userPackage != '' ? 1 : 0,
                'package_list' => $package_list,
            ], 200);
        } else {
            return response([
                'message' => 'No data found',
            ], 200);
        }
    }

    public function getPackageDetail(Request $request)
    {

        $userPackage = UserPackage::where('user_id', auth()->user()->user_id)->first();
        $package_id = $request->input('package_id');

        $package_details = Stage::join('packages', 'stages.package_id', '=', 'packages.package_id')
            ->select('stages.*', 'packages.package_name', 'packages.package_image', 'packages.package_price', 'packages.package_desc')
            ->where('stages.package_id', $package_id)
            ->get();
        if ($package_details->isNotEmpty()) {
            return response()->json([
                'message' => 'Success',
                'package_name' => $package_details->first()->package_name,
                'package_image' => $package_details->first()->package_image,
                'package_amount' => $package_details->first()->package_price,
                'package_description' => $package_details->first()->package_desc,
                'package_details' => $package_details,
                'is_user_package' => $userPackage != '' ? 1 : 0,
            ], 200);
        } else {
            return response()->json(['message' => 'Package not found'], 200);
        }
    }

    public function purchasePackage(Request $request)
    {
        if ($request->input('is_tc_accept') == 1) {
            $user = auth()->user();
            $user_id = $user->user_id;
            $userPackage = new UserPackage();
            $userPackage->package_id = $request->input('package_id');
            $userPackage->user_id = $user_id;
            $userPackage->transaction_id = $request->input('transaction_id');
            $userPackage->transaction_type = $request->input('transaction_type');
            $userPackage->payment_status = $request->input('payment_status');
            $userPackage->is_tc_accept = $request->input('is_tc_accept');

            $userPackage->save();
            $userTree = array();
            getUserTree($user_id, 1, $userTree);
            $userTree =  Config::get('kalamnews.userTree');
            if (isset($userPackage) && !empty($userPackage) && isset($userPackage->package_id)) {
                $package_details = Stage::join('packages', 'stages.package_id', '=', 'packages.package_id')
                    ->select('stages.*', 'packages.package_name', 'packages.package_image')
                    ->where('stages.package_id', $userPackage->package_id)
                    ->get();
                if (isset($userTree) && !empty($userTree)) {
                    foreach ($userTree as $user) {
                        // $total_income = $user['total_income'];
                        // $max_commission = $user['max_commission'];
                        // if($total_income < $max_commission){
                            foreach ($package_details as $packageArr) {
                                if ($packageArr['stage_number'] == $user['user_level']) {
                                    $walletHistory = new WalletHistory();
                                    $walletHistory->user_id = $user['user_details']['user_id'];
                                    $walletHistory->commission_by_user_id = $user_id;
                                    $walletHistory->amount = $packageArr['business_value'];
                                    $walletHistory->commission_level = $user['user_level'];
                                    $walletHistory->transaction_type = "income";
                                    $walletHistory->payment_type = "wallet";
                                    $walletHistory->status = 1;
                                    $walletHistory->commission_node = $user['user_details']['commission_node'];
                                    $walletHistory->save();
                                }
                            }
                        // }                        
                    }
                }
                return response()->json(['message' => 'Package Purchase Successfully'], 200);
            }
        } else {
            return response()->json(['message' => 'Please Accept Terms & Condition'], 201);
        }
    }
}
