<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\User;
use App\Models\BankDetail;
use App\Models\City;
use App\Models\States;
use App\Models\UserPackage;
use App\Models\UsersDocuments;
use App\Models\WalletHistory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use stdClass;

class UserController extends Controller
{
    public function __construct()
    {
        # By default we are using here auth:api middleware
        $this->middleware('auth:api', ['except' => ['']]);
    }

    public function getPaymentHistory(Request $request)
    {
        // Get the authenticated user
        $user = auth()->user();

        $paymentHistory = Payment::where('user_id', 1)->orderBy('created_at', 'desc')->get();

        return response()->json(['success' => true, 'payment_history' => $paymentHistory]);
    }

    public function transferToBank(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric|min:0',
        ]);

        $user = auth()->user();

        $walletBalance = $user->wallet_balance;

        if ($walletBalance >= $request->amount) {
            $user->wallet_balance -= $request->amount;
            $user->save();

            $payment = new Payment();
            $payment->payment_type = 'withdrawal';
            $payment->amount = $request->amount;
            $payment->user_id = $user->id;
            $payment->save();

            return response()->json(['success' => true, 'message' => 'Amount transferred successfully']);
        } else {
            return response()->json(['success' => false, 'message' => 'Insufficient balance']);
        }
    }

    public function getProfile(Request $request)
    {
        $user = auth()->user();
        $city = City::where('city_id', $user->city_id)->first();
        $state = States::where('state_id', $user->state_id)->first();
        $user['city_name'] = $city->city_name;
        $user['state_name'] = $state->state_name;

        $percentageArr = getProfilePercentage($user->user_id);

        $data = [
            'message' => 'User profile fetched successfully',
            'user_details' => $user,
            'profile_percentage' => $percentageArr[0],
            'profile_percentage_arr' => $percentageArr[1],
        ];
        return response()->json($data, 200);
    }

    public function updateProfile(Request $request)
    {
        $user = auth()->user();
        $user_id = $user->user_id;

        $validator = Validator::make($request->all(), [
            'initials' => 'required|in:Mr,Mrs,Ms,Mst',
            'first_name' => 'required|string',
            'middle_name' => 'string|nullable',
            'last_name' => 'required|string',
            'whatsapp_number' => 'sometimes|numeric|digits:10|nullable',
            'address' => 'required|string',
            'pincode' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->first()], 400);
        }

        $userData = array(
            'first_name' => $request->input('first_name'),
            'initials' => $request->input('initials'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'whatsapp_number' => $request->input('whatsapp_number'),
            'pincode' => $request->input('pincode'),
            'address' => $request->input('address'),
        );

        $userProfile = $user->profile;

        if ($image = $request->file('profile')) {
            $fileName = 'profile' . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/usersImages', $fileName);
            $userData['profile'] = 'images/usersImages/' . $fileName;

            if (file_exists($userProfile)) {
                unlink($userProfile);
            }
        }

        $data1 = User::where('user_id', $user_id)->update($userData);
        if ($data1) {
            return response()->json([
                "status" => 'Success',
                "message" => "Profile updated successfully.",

            ], 200);
        }
    }

    public function userKycDocument(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'aadhar_card_no' => 'required',
            'pan_card_no' => 'required',
            'aadhar_card_photo_front' => 'required',
            'aadhar_card_photo_back' => 'required',
            'pan_card_photo'   => 'required',
            'user_kyc_photo'   => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $user = auth()->user();
        $userKycDoc = new UsersDocuments();

        $userKycDoc->user_id = $user->user_id;
        $userKycDoc->aadhar_card_no = $request->input('aadhar_card_no');
        $userKycDoc->pan_card_no = $request->input('pan_card_no');

        if ($imageFront = $request->file('aadhar_card_photo_front')) {
            $fileNameFront = 'adharfront' . '_' . time() . '.' . $imageFront->getClientOriginalExtension();
            $imageFront->move(public_path() . '/images/kycDocs', $fileNameFront);
            $userKycDoc->aadhar_card_photo_front = 'images/kycDocs/' . $fileNameFront;
        }

        if ($imageBack = $request->file('aadhar_card_photo_back')) {
            $fileNameBack = 'adharback' . '_' . time() . '.' . $imageBack->getClientOriginalExtension();
            $imageBack->move(public_path() . '/images/kycDocs', $fileNameBack);
            $userKycDoc->aadhar_card_photo_back = 'images/kycDocs/' . $fileNameBack;
        }

        if ($imagePan = $request->file('pan_card_photo')) {
            $fileNamePan = 'panCard' . '_' . time() . '.' . $imagePan->getClientOriginalExtension();
            $imagePan->move(public_path() . '/images/kycDocs', $fileNamePan);
            $userKycDoc->pan_card_photo = 'images/kycDocs/' . $fileNamePan;
        }

        if ($imageUser = $request->file('user_kyc_photo')) {
            $fileNameUser = 'kycPhoto' . '_' . time() . '.' . $imageUser->getClientOriginalExtension();
            $imageUser->move(public_path() . '/images/kycDocs', $fileNameUser);
            $userKycDoc->user_kyc_photo = 'images/kycDocs/' . $fileNameUser;
        }

        $userKycDoc->save();

        if ($userKycDoc) {
            return response()->json([
                "status" => 'Success',
                "message" => "User kyc documents added successfully.",
            ], 200);
        } else {
            return response()->json([
                "status" => 'error',
                "message" => "User kyc documents not added.",
            ], 200);
        }
    }


    public function updatePanCard(Request $request)
    {
        $user = auth()->user();

        $pan_card_no = $request->input('pan_card_no');
        $pan_card_photo = $request->file('pan_card_photo');
        $bank_id = $request->input('bank_id');

        $bankDetail = BankDetail::find($bank_id);

        if ($bankDetail) {
            $bankDetail->pan_card_no = $pan_card_no;

            if ($image = $request->file('pan_card_photo')) {
                $fileName = 'pan-card' . time() . '.' . $image->getClientOriginalExtension();
                $path = $image->move(public_path() . '/images/pan-card', $fileName);
                $bankDetail['pan_card_photo'] = 'pan-card/' . $fileName;
            }

            $bankDetail->save();

            return response()->json([
                "status" => 'Success',
                "message" => "Pan card details updated successfully.",
            ], 200);
        }
    }

    public function getUserTree(Request $request)
    {
        $user = auth()->user();
        $user['is_paid_user'] = getPaidUser($user->user_id);
        $user['user_level'] = 0;
        $userNodeCount = getUserNodeCount($user->user_id);
        $user['l_bv_count'] = (string)$userNodeCount['l_user_count'];
        $user['r_bv_count'] = (string)$userNodeCount['r_user_count'];
        $parentUserData = '';

        $wallet_history = WalletHistory::where('user_id', $user->user_id)->get();

        $left_commission = $wallet_history->where('commission_node', 'left')->sum('amount');
        $right_commission = $wallet_history->where('commission_node', 'right')->sum('amount');

        $count = new StdClass();
        $count->total_bv_count = (string)($right_commission + $left_commission);
        $count->l_bv_count = (string)$left_commission;
        $count->r_bv_count = (string)$right_commission;

        $user_id = $user->user_id;
        if (isset($request->user_id)) {
            $user = User::where('user_id', $request->user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();

            $user['is_paid_user'] = getPaidUser($request->user_id);
            getUserLevel($request->user_id, 1);
            $user['user_level'] = Config::get('kalamnews.userLevel');
            $userNodeCount = getUserNodeCount($request->user_id);
            $user['l_bv_count'] = (string)$userNodeCount['l_user_count'];
            $user['r_bv_count'] = (string)$userNodeCount['r_user_count'];
            $parentUserData = User::where('l_user_id', $request->user_id)->orWhere('r_user_id', $request->user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();
        }

        if (isset($user->l_user_id)) {
            $lUser = User::where('user_id', $user->l_user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();
            if (isset($lUser) && $lUser != '') {
                getUserLevel($user->l_user_id, 1);
                $lUser['user_level'] = Config::get('kalamnews.userLevel');
                $lUser['is_paid_user'] = getPaidUser($user->l_user_id);
                $userNodeCount = getUserNodeCount($user->l_user_id);
                $lUser['l_bv_count'] = (string)$userNodeCount['l_user_count'];
                $lUser['r_bv_count'] = (string)$userNodeCount['r_user_count'];
            }
        }

        if (isset($user->r_user_id)) {
            $rUser = User::where('user_id', $user->r_user_id)->where(function ($query) {
                $query->where('is_mobile_verify', 1)->orWhere('is_email_verify', 1);
            })->first();

            if (isset($rUser) && $rUser != '') {
                getUserLevel($user->r_user_id, 1);
                $rUser['user_level'] = Config::get('kalamnews.userLevel');
                $rUser['is_paid_user'] = getPaidUser($user->r_user_id);
                $userNodeCount = getUserNodeCount($user->r_user_id);
                $rUser['l_bv_count'] = (string)$userNodeCount['l_user_count'];
                $rUser['r_bv_count'] = (string)$userNodeCount['r_user_count'];
            }
        }

        return response()->json([
            'message' => 'User tree fetched successfully',
            'user_details' => $user,
            'rUser' => isset($rUser) ? $rUser : new stdClass,
            'lUser' => isset($lUser) ? $lUser : new stdClass,
            'parentUserData' => $parentUserData != '' ? $parentUserData : new stdClass,
            'userBVCount' => $count,
        ]);
    }

    public function updateKycDocument(Request $request)
    {
        $docs_id = $request->input('docs_id');

        $userKycDoc = UsersDocuments::where('docs_id', $docs_id)
            ->where('user_id', auth()->id())
            ->first();

        $aadhar_photo_front = $userKycDoc->aadhar_card_photo_front;
        $aadhar_photo_back = $userKycDoc->aadhar_card_photo_back;
        $pan_card_photo = $userKycDoc->pan_card_photo;
        $user_kyc_photo = $userKycDoc->user_kyc_photo;

        if ($request->kyc_update_type == 0) {
            $userKycDoc->aadhar_card_no = $request->input('aadhar_card_no');
            $userKycDoc->is_aadhar_verified = 0;

            if ($imageFront = $request->file('aadhar_card_photo_front')) {
                $fileNameFront = 'adharfront' . '_' . time() . '.' . $imageFront->getClientOriginalExtension();
                $imageFront->move(public_path() . '/images/kycDocs', $fileNameFront);
                $userKycDoc->aadhar_card_photo_front = 'images/kycDocs/' . $fileNameFront;

                if (file_exists($aadhar_photo_front)) {
                    unlink($aadhar_photo_front);
                }
            }

            if ($imageBack = $request->file('aadhar_card_photo_back')) {
                $fileNameBack = 'adharback' . '_' . time() . '.' . $imageBack->getClientOriginalExtension();
                $imageBack->move(public_path() . '/images/kycDocs', $fileNameBack);
                $userKycDoc->aadhar_card_photo_back = 'images/kycDocs/' . $fileNameBack;

                if (file_exists($aadhar_photo_back)) {
                    unlink($aadhar_photo_back);
                }
            }

            if ($imageUser = $request->file('user_kyc_photo')) {
                $fileNameUser = 'kycPhoto' . '_' . time() . '.' . $imageUser->getClientOriginalExtension();
                $imageUser->move(public_path() . '/images/kycDocs', $fileNameUser);
                $userKycDoc->user_kyc_photo = 'images/kycDocs/' . $fileNameUser;

                if (file_exists($user_kyc_photo)) {
                    unlink($user_kyc_photo);
                }
            }
        } else if ($request->kyc_update_type == 1) {
            $userKycDoc->pan_card_no = $request->input('pan_card_no');
            $userKycDoc->is_pan_verified = 0;
            if ($imagePan = $request->file('pan_card_photo')) {
                $fileNamePan = 'panCard' . '_' . time() . '.' . $imagePan->getClientOriginalExtension();
                $imagePan->move(public_path() . '/images/kycDocs', $fileNamePan);
                $userKycDoc->pan_card_photo = 'images/kycDocs/' . $fileNamePan;

                if (file_exists($pan_card_photo)) {
                    unlink($pan_card_photo);
                }
            }
        }else if($request->kyc_update_type == 2){
            $userKycDoc->aadhar_card_no = $request->input('aadhar_card_no');
            $userKycDoc->pan_card_no = $request->input('pan_card_no');
            $userKycDoc->is_aadhar_verified = 0;
            $userKycDoc->is_pan_verified = 0;

            if ($imageFront = $request->file('aadhar_card_photo_front')) {
                $fileNameFront = 'adharfront' . '_' . time() . '.' . $imageFront->getClientOriginalExtension();
                $imageFront->move(public_path() . '/images/kycDocs', $fileNameFront);
                $userKycDoc->aadhar_card_photo_front = 'images/kycDocs/' . $fileNameFront;

                if (file_exists($aadhar_photo_front)) {
                    unlink($aadhar_photo_front);
                }
            }

            if ($imageBack = $request->file('aadhar_card_photo_back')) {
                $fileNameBack = 'adharback' . '_' . time() . '.' . $imageBack->getClientOriginalExtension();
                $imageBack->move(public_path() . '/images/kycDocs', $fileNameBack);
                $userKycDoc->aadhar_card_photo_back = 'images/kycDocs/' . $fileNameBack;

                if (file_exists($aadhar_photo_back)) {
                    unlink($aadhar_photo_back);
                }
            }

            if ($imageUser = $request->file('user_kyc_photo')) {
                $fileNameUser = 'kycPhoto' . '_' . time() . '.' . $imageUser->getClientOriginalExtension();
                $imageUser->move(public_path() . '/images/kycDocs', $fileNameUser);
                $userKycDoc->user_kyc_photo = 'images/kycDocs/' . $fileNameUser;

                if (file_exists($user_kyc_photo)) {
                    unlink($user_kyc_photo);
                }
            }

            if ($imagePan = $request->file('pan_card_photo')) {
                $fileNamePan = 'panCard' . '_' . time() . '.' . $imagePan->getClientOriginalExtension();
                $imagePan->move(public_path() . '/images/kycDocs', $fileNamePan);
                $userKycDoc->pan_card_photo = 'images/kycDocs/' . $fileNamePan;

                if (file_exists($pan_card_photo)) {
                    unlink($pan_card_photo);
                }
            }
        }

        $userKycDoc->save();

        if ($userKycDoc) {
            return response()->json([
                "status" => 'Success',
                "message" => "User kyc documents updated.",
            ], 200);
        } else {
            return response()->json([
                "status" => 'error',
                "message" => "User kyc documents not updated.",
            ], 200);
        }
    }
}
