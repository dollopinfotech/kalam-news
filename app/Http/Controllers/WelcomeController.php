<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\SiteSettings;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function agent()
    {
        $siteSetting = SiteSettings::first();
        return view('guest.agent', compact('siteSetting'));
    }

    public function advertisement()
    {
        $packageDetails = Package::get();
        $siteSetting = SiteSettings::first();

        return view('guest.advertisement', compact('packageDetails', 'siteSetting'));
    }

    public function contactUs()
    {
        $siteSetting = SiteSettings::first();
        return view('guest.contactUs', compact('siteSetting'));
    }

    public function cooperation()
    {
        return view('guest.cooperation');
    }

    public function editorPen()
    {
        return view('guest.editorPen');
    }

    public function joinUs()
    {
        return view('guest.joinUs');
    }

    public function ourTeam()
    {
        return view('guest.ourTeam');
    }

    public function ourWork()
    {
        return view('guest.ourWork');
    }

    public function privacyPolicy()
    {
        return view('guest.privacyPolicy');
    }

    public function responsibilities()
    {
        return view('guest.responsibilities');
    }

    public function termConditions()
    {
        return view('guest.termConditions');
    }

    public function returnPolicy()
    {
        return view('guest.returnPolicy');
    }

    public function shippingPolicy()
    {
        $siteSetting = SiteSettings::first();
        return view('guest.shippingPolicy', compact('siteSetting'));
    }
}
