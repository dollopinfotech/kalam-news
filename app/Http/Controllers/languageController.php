<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
class languageController extends Controller
{

    public function langSwitch($locale)
    {
    if (isset($locale) && in_array($locale, config('app.available_locales'))) {
        session(['locale' => $locale]);
        App::setLocale($locale);
    } else {
        session(['locale' => config('app.fallback_locale')]);
        App::setLocale(config('app.fallback_locale'));
    }

    return redirect()->back()->with('locale_changed', true);
   }

}
