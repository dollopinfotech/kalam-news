<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use App\Models\SiteSettings;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ShareSiteSettings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $siteSettings = SiteSettings::first(); // Fetch site settings from the database
        // dd($_COOKIE);
        $adminProfile = Admin::first();
        $data = [
            'siteSettings' => $siteSettings,
            'adminProfile' => $adminProfile
        ];
        view()->share($data);
        return $next($request);
    }
}


