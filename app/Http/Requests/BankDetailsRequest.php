<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankDetailsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,user_id',
            'customer_name' => 'required|string',
            'bank_name' => 'required|string',
            'account_no' => 'required|string',
            'ifsc_code' => 'required',
            'mobile_no' => 'required|string|max:10',
            'bank_branch' => 'required|string',
            'adhar_card_no' => 'required|numeric|digits:12',
            'adhar_card_photo_front' => 'required|max:10000|mimes:jpg,png,pdf,heic,jpeg',
            'adhar_card_photo_back' => 'required|max:10000|mimes:jpg,png,pdf,heic,jpeg',
            'pan_card_no' => 'required|string',
            'pan_card_photo' => 'required|max:10000|mimes:jpg,png,pdf,heic,jpeg'
        ];
    }
}
