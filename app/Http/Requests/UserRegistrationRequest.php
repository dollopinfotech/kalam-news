<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    // public function authorize(): bool
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'initials' => 'required|in:Mr,Mrs,Ms,Mst',
            'first_name' => 'required|string',
            'middle_name' => 'string|nullable',
            'last_name' => 'required|string',
            'dob' => 'required|date|regex:/^\d{4}-\d{2}-\d{2}$/',
            'mobile_number' => 'required|string|max:10|unique:users,mobile_number',
            'whatsapp_number' => 'sometimes|string|max:10|unique:users,whatsapp_number|nullable',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email', 'regex:/^.+@.+\..+$/i'],
            'password' => 'required|min:8',
            'address' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'pincode' => 'required|numeric',
            'referred_by' => 'string|nullable',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Email already Exists.',
            'email.email' => 'The email must be a valid email.',
            'email.regex' => 'The email must be a valid email.',
            'mobile_number.unique' => 'Mobile number already Exists.',
        ];
    }
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // Generate OTP here
            $this->merge(['otp' => rand(123456, 999999)]);
        });
    }
}
