<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray($request): array
    {
        return [
            'status' => 'success',
            'message' => 'User Created successfully',
            'data' => [
                'user_id' => $this->user_id,
                'uuid' => $this->uuid,
                'referral_code' => $this->referral_code,
                'initials' => $this->initials,
                'first_name' => $this->first_name,
                'middle_name' => $this->middle_name,
                'last_name' => $this->last_name,
                'dob' => $this->dob,
                'mobile_number' => $this->mobile_number,
                'whatsapp_number' => $this->whatsapp_number,
                'email_address' => $this->email_address,
                'is_mobile_verified' => $this->is_mobile_verified,
                'is_email_verified' => $this->is_email_verified,
                'pincode' => $this->pincode,
                'city' => $this->city,
                'state' => $this->state,
                'address' => $this->address,
                'otp' => $this->otp,
                'referred_by' => $this->referred_by,
            ],
        ];

    }
}
