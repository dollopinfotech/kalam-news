<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankDetail extends Model
{
    use HasFactory;

    protected $primaryKey = 'bank_id';

    protected $fillable = [
        'user_id',
        'customer_name',
        'bank_name',
        'account_no',
        'ifsc_code',
        'bank_branch',
        'account_type',
        'primary_bank'
    ];
}

