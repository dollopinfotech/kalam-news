<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryType extends Model
{
    use HasFactory;
    protected $table = "tbl_category_types";

    protected $fillable = [
        'catTypeId','uuid','catTypeName','catTypeImg'
    ];

    protected $primaryKey = 'catTypeId';
    public $timestamps = false;
}
