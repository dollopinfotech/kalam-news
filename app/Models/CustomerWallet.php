<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerWallet extends Model
{
    use HasFactory;
    protected $table = "tbl_customer_wallet";

    protected $fillable = [
        'walletId',
        'uuid',
        'customerId',
        'couponId',
        'orderId',
        'ordItmId',
        'transId',
        'inAmt',
        'outAmt',
        'actionType',
        'referralById',
        'transType',
        'createdAt',
    ];

    protected $primaryKey = 'walletId';
    public $timestamps = false;
}
