<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icon extends Model
{
    use HasFactory;
    protected $table = 'icons';

    protected $primaryKey = 'icon_id';
    protected $fillable = [
        'icon_name',
        'icon_class',
        'status',
        'created_by',
        'updated_by'
    ];
}
