<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Icons extends Model
{
    use HasFactory;
    protected $primaryKey = 'icon_id';
    protected $fillable = [
       'icon_id', 'icon_name','icon', 'icon_image'
    ] ;
}
