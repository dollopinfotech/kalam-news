<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $primaryKey = 'menu_id';

    public $timestamps = false;

    protected $fillable = [
        'parent_menu_id','menu_name','menu_link','menu_icon','sub_menu','status','page_status','order_no','created_at','updated_at'
    ];
}
