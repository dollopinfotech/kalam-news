<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $primaryKey = 'package_id';

    public $incrementing = true;

    public $timestamps = true; // Corrected property name and access level

    protected $fillable = ['uuid', 'package_name', 'package_price', 'package_image', 'is_active','package_desc','max_commission'];

    public function stages()
    {
        return $this->hasMany(Stage::class, 'package_id'); // Assuming 'package_id' is the foreign key
    }

    public function stages_join()
    {
        return $this->hasOne(Stage::class);

    }

    public function userRequests()
    {
        return $this->hasMany(User_request::class);
    }
}
