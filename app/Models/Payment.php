<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        "payment_id",
        "uuid",
        "payment_type",
        "amount",
        "user_id",
        "transaction_id"
    ];

    protected $primaryKey = 'payment_id';

    protected $guarded = [];

    // Payment.php
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
