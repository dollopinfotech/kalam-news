<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;

    protected $fillable = ['role_name', 'role_desc', 'is_active', 'page_privilage'];
    protected $primaryKey = 'role_id';
    protected $guarded = [];
}
