<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteSettings extends Model
{
    use HasFactory;

    protected $primaryKey = 'site_setting_id';

    protected $fillable = ['site_name','site_email','site_address','contact_number','logo','favicon','number_of_stage_level'];

}

