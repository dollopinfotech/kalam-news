<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    use HasFactory;

    protected $table = 'social_links';

    protected $primaryKey = 'social_id';
    public $timestamps = true;
    protected $fillable = [

        'platform',
        'links',
        'icon'
    ];
}
