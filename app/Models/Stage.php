<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    use HasFactory;
    protected $primaryKey = 'stage_id';
    // public $incrementing = false;
    protected $fillable = ['package_id', 'commission', 'number_of_members', 'business_value','stage_number'];


    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id'); // Assuming 'package_id' is the foreign key
    }

    public function packages()
    {
        return $this->hasOne(Package::class);
    }
}
