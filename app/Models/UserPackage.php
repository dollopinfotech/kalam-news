<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model
{
    use HasFactory;

    // protected $table = 'tbl_user_package';
    protected $fillable = [
    'userPackage_id', 'package_id', 'user_id', 'payment_status', 'transaction_id'
];
}
