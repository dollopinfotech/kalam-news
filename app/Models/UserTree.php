<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTree extends Model
{
    use HasFactory;

    protected $primaryKey = 'user_tree_id';

    protected $fillable = [
        'user_id',
        'referred_by',
        'node',
    ];
}
