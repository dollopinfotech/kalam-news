<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_request extends Model
{
    use HasFactory;

    protected $primaryKey = 'req_id';

    protected $fillable = [
      'req_type','user_name','user_number','user_email','title','description','status',
      'package_id','first_name','last_name'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

}
