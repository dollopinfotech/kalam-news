<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersDocuments extends Model
{
    use HasFactory;

    protected $primaryKey = 'docs_id';

    protected $fillable = [
        'user_id',

        'adhar_card_no',
        'adhar_card_photo',
        'pan_card_no',
        'pan_card_photo'
    ];

}
