<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletHistory extends Model
{
    use HasFactory;

    protected $primaryKey = 'wallet_history_id';

    protected $guarded = [];

    // Payment.php
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
