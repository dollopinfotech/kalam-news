<?php

namespace App\Providers;

use App\Repository\Interface\AdminInterface;
use App\Repository\Interface\CommonInterface;
use App\Repository\Interface\CustomerInterface;
use App\Repository\Interface\DriverInterface;
use App\Repository\Repository\AdminRepository;
use App\Repository\Repository\CommonRepository;
use App\Repository\Repository\CustomerRepository;
use App\Repository\Repository\DriverRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(AdminInterface::class, AdminRepository::class);
        $this->app->bind(CustomerInterface::class, CustomerRepository::class);
        $this->app->bind(DriverInterface::class, DriverRepository::class);
        $this->app->bind(CommonInterface::class, CommonRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
