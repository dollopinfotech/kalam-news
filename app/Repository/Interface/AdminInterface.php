<?php

namespace App\Repository\Interface;


interface AdminInterface
{

    public function getUsersListing($key="",$order="");

    public function getUserDetails($uuid);





    //--------------------------------------------------------------//
    public function adminRegister(array $data);

    public function adminLogin(array $data);

    public function adminLogout();

    public function getAdminProfile();

    public function updateAdminProfile(array $data);


    public function getAllProducts($key="",$order="");

    public function getProductDetails($uuid);

    public function addProduct(array $data, array $fileArray);

    public function updateProduct($id, array $data, array $fileArray);

    public function deleteProduct($id);

    public function addBrand(array $data);

    public function getBrandListing($key="",$order="");

    public function deleteBrand($id);

    public function addCategory(array $data);

    public function getCategoryListing($key="",$order="");

    public function deleteCategory($id);

    public function addCategoriesType(array $data);

    public function getCategoriesTypeListing($key="",$order="");

    public function deleteCategoriesType($id);

    public function addSubCategory(array $data);

    public function getSubCategoryListing($categoryId,$key="",$order="");

    public function deleteSubCategory($id);

    public function addFaq(array $data);

    public function getFaqListing($key = "");

    public function addJobs(array $data);

    public function addNewCoupon(array $data);

    public function getAllCoupons();

    public function deleteCoupons($uuid);

    public function updateCoupon($uuid, array $data);

    public function driverPayout($driverId, array $data);

    public function approveDriverPayment($drvWalletId);

    public function vendorListing();

    public function vendorDetail($uuid);

    public function addVendor(array $data);

    public function updateVendor($uuid, array $data);

    public function getCategoryDetail($catId);





    public function deleteCustomer($customerId);

    public function getUpcomingDeliveries($uuid, array $data);

    public function getOrdersListing($uuid);

    public function getLastOrderDetail($customerId);

    public function getOrdersByCustomerCustomerId($customerId);

    public function getOrderDetail($uuid);

    public function getItemsSold($uuid, array $data);

    public function getCancelledOrders($uuid, array $data);

    public function getCompletedOrders($uuid, array $data);














}
