<?php

namespace App\Repository\Interface;


interface CustomerInterface
{

    public function customerRegistration(array $data);

    public function customerLogin(array $data);

    public function customerLogout();

    public function getCustomerProfile();

    public function updateCustomerProfile($uuid, array $data);

    public function addAddress(array $data);

    public function getAddressList($uuid);

    public function getProductListing($filter,$key='');

    public function getProductDetail($uuid);

    public function getBrandListing($key='');

    public function getCategoryListing($key='');

    public function getCategoryTypeListing($key='');

    public function getSubCategoryListing($categoryId,$key='');

    public function addToCart(array $data);

    public function getCartDetails($key="");

    public function removeFromCart($id);

    public function addToWishlist(array $data);

    public function getWishlistData($filter,$key="");

    public function removeFromWishlist($productId);

    public function getCustomerFromUuid($uuid);

    public function getCustomerFromToken();

    public function addReview(array $data);

    public function getReviewList($productId,$key="");

    public function getMyReviewList($key="");

    public function getFaqListing($key="");

    public function getAllOrder($key="");

    public function getOrderDetail($uuid);

    public function resetCustomerPassword($password);

    public function changeCustomerPassword($oldPassword, $password);

    public function customerForgotPassword($email, $password);

    public function getAccountDetail($uuid);

    public function addAccountDetail($uuid, array $data);

    public function getSearchResult(array $data);

    public function getSizes($data,$key='');

    public function managePreferences(array $data);

    public function getLanguageListing();

    public function sendOtp($email,$otp);

    public function deactivateMyAccount();

    public function calcWalletAmount();

    public function getWalletHistory();

    public function referralPayout($referredBy,$referredTo);

    public function WalletToBankTrans($amount);

    public function addToWallet(array $data);

    public function orderSummary($orderId);
}

















