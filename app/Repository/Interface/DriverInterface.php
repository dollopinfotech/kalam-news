<?php

namespace App\Repository\Interface;


interface DriverInterface
{
    public function driverRegistration(array $data);

    public function driverLogin(array $data);

    public function driverDocument(array $driverData);

    public function driverBankDetails(array $data);

    public function driverLogout();

    public function getDriverProfile();

    public function getActiveOrders($driverId);

    public function getAcceptedOrders($driverId);

    public function driverEarnings();

    public function calcDriverWalletAmount();

    public function driverWalletToBankTrans($amount);
}
