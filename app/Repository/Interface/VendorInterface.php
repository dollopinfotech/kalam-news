<?php

namespace App\Repository\Interface;


interface VendorInterface
{

    public function vendorLogin(array $data);

    public function vendorLogout();

    public function getVendorProfile();



}
