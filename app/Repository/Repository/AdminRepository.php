<?php

namespace App\Repository\Repository;

use App\Models\Admin;
use App\Models\Brand;
use App\Models\Category;
use App\Models\CategoryType;
use App\Models\Coupon;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\DriverJob;
use App\Models\DriverWallet;
use App\Models\Faq;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Vendor;
use App\Repository\Interface\AdminInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminRepository implements AdminInterface
{

    public function adminRegister(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $query = Admin::create($data);

        if($query){
            $token = Auth::login($query);
            return response()->json([
                'status' => 'success',
                'message' => 'Admin created successfully!',
                'admin' => $query,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function adminLogin(array $data)
    {
        $emailOrMobile = $data['emailOrMobile'];
        if(preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/', $emailOrMobile)){
            $credentials['adminEmail'] = $emailOrMobile;
            $credentials['password'] = $data['password'];
            $query = admin::select('uuid','adminId','isActive')->where('adminEmail',$emailOrMobile)->first();
            if(!$query && $query==null){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Email not registered!',
                ], 401);
            }
        }else if(preg_match('/^[6-9][0-9]{9}$/', $emailOrMobile)){
            $credentials['adminMobile'] = $emailOrMobile;
            $credentials['password'] = $data['password'];
            $query = admin::select('uuid','adminId','isActive')->where('adminMobile',$emailOrMobile)->first();
            if(!$query && $query==null){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Mobile not registered!',
                ], 401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid credentials',
            ], 400);
        }
        // if(isset($data['adminEmail'])){
        //     $query = Admin::select('uuid','adminId','password')->where('adminEmail',$data['adminEmail'])->first();
        // }else if(isset($data['adminMobile'])){
        //     $query = Admin::select('uuid','adminId','password')->where('adminMobile',$data['adminMobile'])->first();
        // }
        // if(!$query && $query==null){
        //     return response()->json([
        //         'status' => 'error',
        //         'message' => 'Mobile or Email not registered!',
        //     ], 401);
        // }
        $customClaims = [
            'adminUuid' => $query['uuid'],
            'adminId' => $query['adminId'],
        ];
        $token = Auth::claims($customClaims)->attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Credentials did not match!',
            ], 401);
        }
        $adminData = Auth::user();
        return response()->json([
                'status' => 'success',
                'adminData' => $adminData,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
        ]);
    }

    public function adminLogout()
    {
        Auth::logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out!',
        ]);
    }

    public function getAdminProfile()
    {
        // from token
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $query = Admin::select('adminId','adminProfilePicture','adminName','adminEmail',DB::raw("DATE_FORMAT(dob, '%d-%m-%Y') as dob"),'adminMobile')->where('uuid', $token['adminUuid'])->first();

        // from auth
        // $admin = Auth::user();

        // $query = Admin::select('adminId','adminProfilePicture','adminName','adminEmail','dob','adminMobile')->where('uuid', $uuid)->first();

        if($query){
            return $query;
        }else{
            return 0;
        }
    }

    public function updateAdminProfile(array $data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);

        if(!sizeof($data)){
            return response()->json([
                'status' => 'error',
                'message' => 'Nothing to update!',
            ],200);
        }else{
            $query = Admin::where('uuid', $token['adminUuid'])->update($data);
            if($query){
                return $query;
            }else{
                return 0;
            }
        }
    }

    public function getUsersListing($key="",$order="")
    {
        $cond = [
            'isActive' => 1,
            'isDeleted' => 0
        ];
        $query = Customer::select('customerId','uuid AS customerUuid','fullName','profilePicture','email','mobile',DB::raw("DATE_FORMAT(dob, '%d-%m-%Y') as dob"),'gender','referralCode','referredBy','createdAt')->where($cond);
        if (isset($key) && !empty($key)) {
            $order = (isset($order) && !empty($order))?$order:"desc";
            $query = $query->orderBy($key, $order)->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return $query;
        }else{
            return 0;
        }
    }

    public function getUserDetails($uuid)
    {
        $query = Customer::select('customerId','fullName','profilePicture','email','mobile',DB::raw("DATE_FORMAT(dob, '%d-%m-%Y') as dob"),'gender','referralCode','referredBy','createdAt')->where('uuid', $uuid)->first();
        if($query){
            return $query;
        }
    }

    public function getAllProducts($key='',$order="")
    {
        $sql= "SELECT tbl_products.productId,tbl_products.uuid AS productUuid,tbl_products.productName,tbl_products.productDesc,tbl_products.unit,tbl_products.productQty,tbl_products.productMrp,tbl_products.productMsp,tbl_products.productMfd,tbl_products.productExp,tbl_products.createdAt,tbl_brands.brandName,tbl_brands.brandImg,cat.catName,cat.catImg,sub_cat.catName AS subCatName,sub_cat.catImg AS subCatImg,tbl_category_types.catTypeName,tbl_category_types.catTypeImg,GROUP_CONCAT((tbl_product_images.productImage)) AS productImages
        FROM tbl_products
        LEFT JOIN tbl_product_images ON tbl_product_images.productId=tbl_products.productId
        LEFT JOIN tbl_brands ON tbl_brands.brandId=tbl_products.brandId
        LEFT JOIN tbl_category_types ON tbl_category_types.catTypeId=tbl_products.catTypeId
        LEFT JOIN tbl_categories as cat ON tbl_products.catId=cat.catId
        LEFT JOIN tbl_categories as sub_cat ON tbl_products.subCatId=sub_cat.catId
        WHERE tbl_products.isActive=1 AND tbl_products.isDeleted=0
        GROUP BY tbl_products.productId";
        // dd($sql);
        $query = DB::select( DB::raw($sql) );
        /* $con = [
            'tbl_products.isActive' => 1,
            'tbl_products.isDeleted' => 0,
        ];
        $query = Product::select('tbl_products.productId','tbl_products.uuid','tbl_products.productName','tbl_products.productDesc','tbl_products.unit','tbl_products.productQty','tbl_products.productMrp','tbl_products.productMsp','tbl_products.productMfd','tbl_products.productExp','tbl_product_images.productImage','tbl_brands.brandName','tbl_brands.brandImg','cat.catName','cat.catImg','sub_cat.catName AS subCatName','sub_cat.catImg AS subCatImg','tbl_category_types.catTypeName','tbl_category_types.catTypeImg')
                        ->leftJoin('tbl_brands', 'tbl_products.brandId', 'tbl_brands.brandId')
                        ->leftJoin('tbl_categories AS cat', function($join){
                            $join->on('tbl_products.catId', 'cat.catId')
                            ->where('cat.parentId', 1 );
                        })
                        ->leftJoin('tbl_categories AS sub_cat', function($join){
                            $join->on('tbl_products.subCatId', 'sub_cat.catId')
                            ->where('sub_cat.parentId', 0 );
                        })
                        ->leftJoin('tbl_category_types', 'tbl_products.catTypeId', 'tbl_category_types.catTypeId')
                        ->leftJoin('tbl_product_images', 'tbl_products.productId', 'tbl_product_images.productId')
                        ->where($con); */

        /* if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        } */
        $result = [];
        foreach($query as $product){
            $product->productImages = explode(',', $product->productImages);
            array_push($result,$product);
        }
        if ($query) {
            return $result;
        } else {
            return 0;
        }
    }

    public function getProductDetails($uuid)
    {
        $sql= "SELECT tbl_products.productId,tbl_products.uuid AS productUuid,tbl_products.productName,tbl_products.productDesc,tbl_products.unit,tbl_products.productQty,tbl_products.productMrp,tbl_products.productMsp,tbl_products.productMfd,tbl_products.productExp,tbl_products.manufacturingDesc,tbl_brands.brandId,tbl_brands.brandName,tbl_brands.brandImg,cat.catName,cat.catId,cat.catImg,sub_cat.catName AS subCatName,sub_cat.catImg AS subCatImg,tbl_category_types.catTypeId,tbl_category_types.catTypeName,tbl_category_types.catTypeImg,GROUP_CONCAT((tbl_product_images.productImage)) AS productImages,GROUP_CONCAT((tbl_product_images.order)) AS imageOrder,tbl_vendors.vendorName,tbl_vendors.vendorId
        FROM tbl_products
        LEFT JOIN tbl_product_images ON tbl_product_images.productId=tbl_products.productId
        LEFT JOIN tbl_brands ON tbl_brands.brandId=tbl_products.brandId
        LEFT JOIN tbl_category_types ON tbl_category_types.catTypeId=tbl_products.catTypeId
        LEFT JOIN tbl_categories as cat ON tbl_products.catId=cat.catId
        LEFT JOIN tbl_categories as sub_cat ON tbl_products.subCatId=sub_cat.catId
        LEFT JOIN tbl_vendors ON tbl_products.vendorId=tbl_vendors.vendorId
        WHERE tbl_products.uuid='$uuid' AND tbl_products.isActive=1 AND tbl_products.isDeleted=0
        GROUP BY tbl_products.productId";
        // dd($sql);
        $query = DB::select( DB::raw($sql) );
        /* $con = [
            'tbl_products.isActive' => 1,
            'tbl_products.isDeleted' => 0,
            'tbl_products.uuid' => $uuid,
        ]; */
        /* $query = Product::select('tbl_products.productId','tbl_products.uuid AS productUuid','tbl_products.productName','tbl_products.productDesc','tbl_products.unit','tbl_products.productQty','tbl_products.productMrp','tbl_products.productMsp','tbl_products.productMfd','tbl_products.productExp','tbl_brands.brandName','tbl_brands.brandImg','cat.catName','cat.catImg','sub_cat.catName AS subCatName','sub_cat.catImg AS subCatImg','tbl_category_types.catTypeName','tbl_category_types.catTypeImg','tbl_product_images.productImage')
                        ->leftJoin('tbl_brands', 'tbl_products.brandId', 'tbl_brands.brandId')
                        ->leftJoin('tbl_categories AS cat', 'tbl_products.catId', 'cat.catId')
                        ->leftJoin('tbl_categories AS sub_cat', 'tbl_products.subCatId', 'sub_cat.catId')
                        ->leftJoin('tbl_category_types', 'tbl_products.catTypeId', 'tbl_category_types.catTypeId')
                        ->leftJoin('tbl_product_images', 'tbl_products.productId', 'tbl_product_images.productId')
                        ->where($con)->first(); */
        $check = Product::where('uuid',$uuid)->first();
        if($check){
            if($query){
                $query[0]->productImages = explode(',', $query[0]->productImages);
                $query[0]->imageOrder = explode(',', $query[0]->imageOrder);
                return $query[0];
            }else{
                return 0;
            }
        }else{
            return 0;

        }
    }

    public function addProduct(array $data, array $fileArray)
    {
        $data['uuid'] = (string)Str::uuid();
        $query = Product::insertGetId($data);
        if($query){
            if(sizeof($fileArray)){
                $i=1;
                foreach($fileArray as $image){
                    $imageData = [
                        'productId' => $query,
                        'productImage' => $image,
                        'order' => $i++
                    ];
                    $query2 = ProductImage::insert($imageData);
                }
            }
            return 1;
        }else{
            return 0;
        }
    }

    public function updateProduct($uuid, array $data, array $fileArray)
    {
        if(!sizeof($data)){
            return response()->json([
                'status' => 'success',
                'message' => 'Nothing to update!',
            ],200);
        }else{
            $check = Product::where('uuid',$uuid)->first();
            if($check){
                $data['updateAt'] = Carbon::now()->toDateTimeString();
                $query = Product::where('uuid',$uuid)->update($data);
                if($query){
                    if(sizeof($fileArray)){
                        $productId = Product::where('uuid',$uuid)->first();
                        foreach($fileArray as $image){
                            $imageData = [
                                'productId' => $productId,
                                'productImage' => $image
                            ];
                            $query2 = ProductImage::insert($imageData);
                        }
                    }
                    return 1;
                }else{
                    return 0;
                }
            }else{
                return 0;

            }
        }
    }

    public function deleteProduct($productId)
    {
        $product = Product::where('productId',$productId)->first();
        if($product){
            $data = [
                'deletedAt' => Carbon::now()->toDateTimeString(),
                'isDeleted'=>1
                ];
            $query = Product::where('productId',$productId)->update($data);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Product not found!',
            ],404);
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product deleted successfully!',
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function addBrand(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $con = ['brandName' => $data['brandName']];
        $check = Brand::select('brandName')->where($con)->first();
        if (!$check) {
            $query = Brand::insertGetId($data);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'Brand already exist!',
            ], 200);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Brand added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getBrandListing($key="",$order="")
    {
        $con = [
            'tbl_brands.isActive' => 1,
            'tbl_brands.isDeleted' => 0
        ];
        $query = Brand::select('tbl_brands.brandId','tbl_brands.uuid','tbl_brands.brandName','tbl_brands.brandImg',)
            ->where($con);

        if (isset($key) && !empty($key)) {
            $order = (isset($order) && !empty($order))?$order:"desc";
            $query = $query->orderBy($key, $order)->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return $query;
        }else{
            return 0;
        }
    }

    public function deleteBrand($brandId)
    {
        $brand = Brand::where('brandId', $brandId)->first();
        if ($brand) {
            $data = [
                'deletedAt' => Carbon::now()->toDateTimeString(),
                'isDeleted' => 1
            ];
            $query = Brand::where('brandId', $brandId)->update($data);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Brand not found!',
            ], 404);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Brand deleted successfully!',
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getOrdersListing($uuid)
    {
        $query = Order::select('tbl_customer.fullName', 'tbl_customer.mobile', 'tbl_order.orderNo', 'tbl_order.uuid', 'tbl_order.transId', 'tbl_order.totalAmt', 'tbl_order.ordAddress', 'tbl_order.paymentStatus', 'tbl_order.ordStatus')
                        ->leftJoin('tbl_customer', 'tbl_order.customerId', 'tbl_customer.customerId');
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return $query;
        }else{
            return 0;
        }
    }

    public function getOrdersByCustomerCustomerId($customerId)
    {
        $query = Order::select('tbl_orders.orderId','tbl_orders.uuid AS orderUuid','tbl_orders.orderCode','tbl_orders.couponCode','tbl_orders.orderAddress','tbl_orders.totalAmt','tbl_orders.discountedAmt','tbl_orders.paymentMode','tbl_orders.transId','tbl_orders.paymentStatus','tbl_orders.orderStatus',DB::raw("DATE_FORMAT(createdAt, '%M %d,%Y') as createdAt"),'tbl_orders.itemCount')->where('tbl_orders.customerId',$customerId)->take(5);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        $totalAmtSpt=0;
        foreach($query as $orderData){
            $totalAmtSpt += $orderData['discountedAmt'];
        }
        $data['orderData'] = $query;
        $data['totalCount'] = Order::where('tbl_orders.customerId',$customerId)->count();
        $data['totalAmtSpt'] = $totalAmtSpt;
        // $createdAt = date('M d, Y', strtotime($query[0]->createdAt));
        if($query){
            return $data;
        }else{
            return 0;
        }
    }

    public function getOrderDetail($uuid)
    {
        $query = Order::select('tbl_customer.fullName', 'tbl_customer.mobile', 'tbl_order.orderNo', 'tbl_order.transId', 'tbl_order.totalAmt', 'tbl_order.ordAddress', 'tbl_order.paymentStatus', 'tbl_order.ordStatus')
            ->leftJoin('tbl_customer', 'tbl_order.customerId', 'tbl_customer.customerId')
            ->where('tbl_order.uuid', $uuid)->first();

        if($query){
            return $query;
        }else{
            return 0;
        }
    }

    public function addCategory(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $con = ['catTypeId' => $data['catTypeId'], 'catName' => $data['catName']];
        $check = Category::select('catName')->where($con)->first();
        if (!$check) {
            $query = Category::insertGetId($data);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'Category  already exist!',
            ], 200);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Category added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getCategoryListing($key="",$order="")
    {

        $con = [
            'tbl_categories.isActive' => 1,
            'tbl_categories.isDeleted' => 0,
            'tbl_categories.parentId' => 0,
        ];
        $query = Category::select('tbl_categories.catId', 'tbl_categories.uuid', 'tbl_categories.catName', 'tbl_categories.catImg', 'tbl_category_types.catTypeName', 'tbl_category_types.catTypeImg')
            ->leftJoin('tbl_category_types', 'tbl_categories.catTypeId', 'tbl_category_types.catTypeId')
            ->where($con);
        if (isset($key) && !empty($key)) {
            $order = (isset($order) && !empty($order))?$order:"desc";
            $query = $query->orderBy($key, $order)->get();
        } else {
            $query = $query->get();
        }
        if ($query) {
            return $query;
        } else {
            return 0;
        }
    }

    public function deleteCategory($id)
    {
        $query = Category::where('catId',$id)->delete();
        return $query;
    }

    public function addCategoriesType(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $query = CategoryType::insertGetId($data);
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Category type added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getCategoriesTypeListing($key="",$order="")
    {
        $cond = [
            'tbl_category_types.isActive' => 1
        ];
        $query = CategoryType::select('catTypeId', 'uuid AS CategoriesTypeUuid', 'catTypeName', 'catTypeImg', 'isActive', 'createdAt', 'updateAt')->where($cond);
        if (isset($key) && !empty($key)) {
            $order = (isset($order) && !empty($order))?$order:"desc";
            $query = $query->orderBy($key, $order)->get();
        } else {
            $query = $query->get();
        }
        if ($query) {
            return $query;
        } else {
            return 0;
        }
    }

    public function deleteCategoriesType($id)
    {
        $query = CategoryType::where('catTypeId', $id)->first();
        if ($query) {
            $data = [
                'deletedAt' => Carbon::now()->toDateTimeString(),
                'isDeleted' => 1
            ];
            $query1 = CategoryType::where('catTypeId', $id)->update($data);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'SubCategoryType not found!',
            ], 404);
        }
        if ($query1) {
            return response()->json([
                'status' => 'success',
                'message' => 'SubCategoryType deleted successfully!',
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function addSubCategory(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $data['parentId'] = 1;
        $query = Category::insertGetId($data);
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'SubCategory added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getSubCategoryListing($categoryId='',$key="",$order="")
    {
        $con = [
            'tbl_categories.isActive' => 1,
            'tbl_categories.isDeleted' => 0,
            'tbl_categories.parentId' => 1,
        ];
        if($categoryId!=""){
            $con['tbl_categories.parentCatId'] = $categoryId;
        }
        $query = Category::select('tbl_categories.catId AS subCatId', 'tbl_categories.uuid AS subCatUuid', 'tbl_categories.catName AS subCatName', 'tbl_categories.catImg AS subCatImg', 'tbl_category_types.catTypeName', 'tbl_category_types.catTypeImg')
        ->leftJoin('tbl_category_types', 'tbl_categories.catTypeId', 'tbl_category_types.catTypeId')
        ->where($con);
        if (isset($key) && !empty($key)) {
            $order = (isset($order) && !empty($order))?$order:"desc";
            $query = $query->orderBy($key, $order)->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return $query;
        }else{
            return 0;
        }
    }

    public function deleteSubCategory($id)
    {
        $query = Category::where('catId', $id)->first();
        if ($query) {
            $data = [
                'deletedAt' => Carbon::now()->toDateTimeString(),
                'isDeleted' => 1
            ];
            $query1 = Category::where('catId', $id)->update($data);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'SubCategory not found!',
            ], 404);
        }
        if ($query1) {
            return response()->json([
                'status' => 'success',
                'message' => 'SubCategory deleted successfully!',
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function addFaq(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
       // $query = Faq::insertGetId($data);
        $con = ['faqTitle' => $data['faqTitle'], 'faqTitle' => $data['faqTitle']];
        $check = Faq::select('faqTitle')->where($con)->first();
        if(!$check) {
            $query = Faq::insertGetId($data);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'Faq already exist!',
            ], 200);
        }
        if ($query) {
             return response()->json([
                'status' => 'success',
                'message' => 'Faq added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getFaqListing($key = "")
    {
        $cond = [
            'isActive' => 1,
        ];
        $query = Faq::select('faqId', 'uuid AS faqUuid', 'faqTitle', 'faqDesc')->where($cond);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if ($query) {
            if ($query->count() > 0) {
                return $query;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function addJobs(array $data)
    {
        $check = DriverJob::select('jobId')->where('orderId',$data['orderId'])->first();
        if($check){
            return response()->json([
                'status' => 'error',
                'message' => 'Job already assigned!',
            ],400);
        }
        $query = DriverJob::insertGetId($data);
        if ($query) {
             return response()->json([
                'status' => 'success',
                'message' => 'Driver job added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function addNewCoupon($data)
    {
        $data['uuid'] = (string)Str::uuid();
        return Coupon::create($data);

    }

    public function getAllCoupons()
    {
        return Coupon::where(['isActive'=>1,'isDeleted'=>0])->get();
    }

    public function deleteCoupons($uuid)
    {
        $check = Coupon::where('uuid',$uuid)->first();
        if($check){
            $query = Coupon::where('uuid',$uuid)->update(['isDeleted'=>1]);
            if($query){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Coupon deleted successfully',
                ],200);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Coupon not fount!',
            ],400);
        }
    }

    public function updateCoupon($uuid, $data)
    {
        $check = Coupon::where('uuid',$uuid)->first();
        if($check){
            $query = Coupon::where('uuid',$uuid)->update($data);
            if($query){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Coupon updated successfully',
                ],200);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Coupon not fount!',
            ],400);
        }

    }

    public function driverPayout($driverId, $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $check = Driver::where('driverId',$driverId)->first();
        if(!$check){
            return response()->json([
                'status' => 'error',
                'message' => 'Driver not found',
            ],404);
        }

        $query = DriverWallet::create($data);
        if ($query) {
            return response()->json([
               'status' => 'success',
               'message' => 'Payout successful!',
           ], 200);
       } else {
           return response()->json([
               'status' => 'error',
               'message' => 'Something went wrong!',
           ], 401);
       }
    }

    public function approveDriverPayment($drvWalletId)
    {
        $query = DriverWallet::where(['drvWalletId'=>$drvWalletId,'actionType'=>'withdrawal'])->update(['isApproved'=>1]);
        if ($query) {
            return response()->json([
               'status' => 'success',
               'message' => 'Payment approved',
           ], 200);
       } else {
           return response()->json([
               'status' => 'error',
               'message' => 'Something went wrong!',
           ], 401);
       }
    }

    public function vendorListing($key='')
    {
        $con = [
            'isActive'=>1,
            'isDeleted'=>0,
        ];
        $query = Vendor::select('vendorId','uuid','vendorName','vendorMobile','vendorEmail','vendorAddress','vendorLatitude','vendorLongitude','cityName','stateName','countryName')
                ->leftJoin('tbl_cities','tbl_cities.cityId','tbl_vendors.cityId')
                ->leftJoin('tbl_states','tbl_states.stateId','tbl_vendors.stateId')
                ->leftJoin('tbl_countries','tbl_countries.countryId','tbl_vendors.countryId')
                ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return $query;
        }else{
            return 0;
        }

    }

    public function vendorDetail($uuid)
    {
        $con = [
            'isActive'=>1,
            'isDeleted'=>0,
            'uuid'=>$uuid,
        ];
        $query = Vendor::select('vendorId','vendorName','vendorMobile','vendorEmail','vendorAddress','vendorLatitude','vendorLongitude','cityName','stateName','countryName')
                ->leftJoin('tbl_cities','tbl_cities.cityId','tbl_vendors.cityId')
                ->leftJoin('tbl_states','tbl_states.stateId','tbl_vendors.stateId')
                ->leftJoin('tbl_countries','tbl_countries.countryId','tbl_vendors.countryId')
                ->where($con)->first();

        if($query){
            return $query;
        }else{
            return 0;
        }
    }

    public function addVendor(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $query = Vendor::create($data);

        if($query){
            return $query;
        }else{
            return 0;
        }
    }

    public function updateVendor($uuid, array $data)
    {
        if(!sizeof($data)){
            return response()->json([
                'status' => 'success',
                'message' => 'Nothing to update!',
            ],200);
        }else{
            $data['updatedAt'] = Carbon::now()->toDateTimeString();
            $query = Vendor::where('uuid',$uuid)->update($data);
            if($query){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Vendor updated successfully',
                ],201);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }
    }

    public function getCategoryDetail($catId)
    {
        $con = [
            'tbl_categories.isActive' => 1,
            'tbl_categories.isDeleted' => 0,
            'tbl_categories.parentId' => 0,
            'tbl_categories.catId' => $catId,
        ];
        $query = Category::select('tbl_categories.catId', 'tbl_categories.uuid', 'tbl_categories.catName', 'tbl_categories.catImg', 'tbl_category_types.catTypeName', 'tbl_category_types.catTypeImg')
            ->leftJoin('tbl_category_types', 'tbl_categories.catTypeId', 'tbl_category_types.catTypeId')
            ->where($con)->first();
        if ($query) {
            return $query;
        } else {
            return 0;
        }
    }

    public function getLastOrderDetail($customerId)
    {
        $order = Order::select('tbl_orders.orderCode','tbl_orders.createdAt')->where('tbl_orders.customerId',$customerId)->first();
        $data['lastOrderNo'] = $order->orderCode;
        $to = Carbon::now();
        $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $order->createdAt);
        $data['lastOrderDays'] = $to->diffInDays($from);
        $customer = Customer::select('createdAt')->where('customerId',$customerId)->first();
        $to = Carbon::now();
        $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $customer->createdAt);
        $data['registered'] = $to->diffInDays($from);
        if($data){
            return $data;
        }else{
            return 0;
        }
    }


    public function deleteCustomer($customerId)
    {
        $data = [
            'deletedAt' => Carbon::now()->toDateTimeString(),
            'isDeleted'=>1
        ];
        return Product::where('customerId',$customerId)->update($data);

    }

    public function getItemsSold($uuid, array $data)
    {

    }

    public function getCancelledOrders($uuid, array $data)
    {

    }

    public function getCompletedOrders($uuid, array $data)
    {

    }

    public function getUpcomingDeliveries($uuid, array $data)
    {

    }

}
