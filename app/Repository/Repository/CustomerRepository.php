<?php

namespace App\Repository\Repository;

use App\Mail\MailTemplate;
use App\Models\Address;
use App\Models\BankDetail;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\Category;
use App\Models\CategoryType;
use App\Models\Customer;
use App\Models\CustomerWallet;
use App\Models\Faq;
use App\Models\Languages;
use App\Models\Order;
use App\Models\Preference;
use App\Models\Product;
use App\Models\Review;
use App\Models\Size;
use App\Repository\Interface\CustomerInterface;
use App\Models\Wishlist;
use App\Repository\Interface\CommonInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class CustomerRepository implements CustomerInterface
{
    public $common;
    public function __construct(CommonInterface $common)
    {
        $this->common = $common;
    }

    public function customerRegistration(array $data)
    {
        $query = Customer::create($data);
        if($query){
            Preference::create(['customerId'=>$query['customerId']]);
            if(isset($data['referredBy']) && $data['referredBy']!=""){
                $this->referralPayout($data['referredBy'],$query['customerId']);
            }
            $token = Auth::login($query);
            return response()->json([
                'status' => 'success',
                'message' => 'Customer created successfully!',
                'customer' => $query,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function customerLogin(array $data)
    {
        $emailOrMobile = $data['emailOrMobile'];
        if(preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/', $emailOrMobile)){
            $credentials['email'] = $emailOrMobile;
            $credentials['password'] = $data['password'];
            $query = Customer::select('uuid','customerId','isActive')->where('email',$emailOrMobile)->first();
            if(!$query && $query==null){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Email not registered!',
                ], 401);
            }
        }else if(preg_match('/^[6-9][0-9]{9}$/', $emailOrMobile)){
            $credentials['mobile'] = $emailOrMobile;
            $credentials['password'] = $data['password'];
            $query = Customer::select('uuid','customerId','isActive')->where('mobile',$emailOrMobile)->first();
            if(!$query && $query==null){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Mobile not registered!',
                ], 401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid credentials',
            ], 400);
        }

        // if(isset($data['email'])){
        //     $query = Customer::select('uuid','customerId','isActive')->where('email',$data['email'])->first();
        // }else if(isset($data['mobile'])){
        //     $query = Customer::select('uuid','customerId','isActive')->where('mobile',$data['mobile'])->first();
        // }

        if($query['isActive']==0){
            Customer::where('customerId',$query->customerId)->update(['isActive'=>1]);
        }
        $customClaims = [
            'customerUuid' => $query['uuid'],
            'customerId' => $query['customerId'],
        ];
        $token = Auth::claims($customClaims)->attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => "These credentials do not match our records.",
            ], 401);
        }
        $customerData = Auth::user();
        return response()->json([
                'status' => 'success',
                'customerData' => $customerData,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
        ]);
    }

    public function customerLogout()
    {
        Auth::logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out!',
        ]);
    }

    public function getCustomerProfile()
    {
        // from token
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $query = Customer::select('customerId','profilePicture','fullName','email','dob','mobile','walletAmount','deviceType')->where('uuid', $token['customerUuid'])->first();

        // from auth
        // $customer = Auth::user();
        // $query = Customer::select('customerId','profilePicture','fullName','email','dob','mobile','walletAmount','deviceType')->where(['uuid'=>$uuid, 'isActive'=>1])->first();

        if($query){
            return response()->json([
                'status' => 'success',
                'customer' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function updateCustomerProfile($uuid, array $data)
    {
        $query = Customer::where('uuid',$uuid)->update($data);

        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Customer updated successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function addAddress(array $data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $query = Customer::where('uuid',$token['customerUuid'])->first();

        if($query){
            $data['uuid'] = (string)Str::uuid();
            $data['customerId'] = $query['customerId'];
            $result = Address::create($data);
            if($result){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Address added successfully!',
                ],201);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Customer not found!',
            ],404);
        }

    }

    public function getAddressList($uuid)
    {
        $customerId = Customer::select('customerId')->where('uuid', $uuid)->first();
        if($customerId){
            $query = Address::select('tbl_customers.fullName','tbl_addresses.addressId','tbl_cities.cityName','tbl_cities.latitude','tbl_cities.longitude','tbl_states.stateName','tbl_countries.countryName','tbl_addresses.pinCode','tbl_addresses.isDefault','tbl_addresses.landmark','tbl_addresses.street','tbl_addresses.colony','tbl_addresses.houseNo','tbl_addresses.addressType')->where('tbl_addresses.customerId', $customerId['customerId'])
                            ->leftJoin('tbl_cities', 'tbl_cities.cityId', 'tbl_addresses.cityId')
                            ->leftJoin('tbl_states', 'tbl_states.stateId', 'tbl_addresses.stateId')
                            ->leftJoin('tbl_countries', 'tbl_countries.countryId', 'tbl_addresses.countryId')
                            ->leftJoin('tbl_customers', 'tbl_customers.customerId', 'tbl_addresses.customerId')
                            ->get();
            if($query){
                return $query;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    public function addToCart(array $data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data['customerId'] = $token['customerId'];
        $data['uuid'] = (string)Str::uuid();
        $con= ['customerId'=>$data['customerId'],'productId'=>$data['productId'],'size'=>$data['size']];
        $check = Cart::select('cartId','cartQty')->where($con)->first();
        if(!$check){
            $query = Cart::insertGetId($data);
        }else{
            $query = Cart::where('cartId',$check['cartId'])->update(['cartQty'=>$check['cartQty']+1]);
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product added to cart successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCartDetails($key = "")
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $cond = [
            'tbl_carts.customerId' => $customerId,
        ];
        $query = Cart::select('tbl_carts.cartId','tbl_carts.uuid AS cartUuid','tbl_carts.cartQty','tbl_carts.size','tbl_carts.isChecked','tbl_products.productId','tbl_products.uuid AS productUuid','tbl_products.productDesc','tbl_products.productMrp','tbl_product_images.productImage')
                        ->leftJoin('tbl_products','tbl_products.productId' ,'tbl_carts.productId')
                        ->leftJoin('tbl_product_images','tbl_products.productId' ,'tbl_product_images.productId')
                        ->where($cond);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
                ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'cartData' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function removeFromCart($productId)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $con = [
            'customerId' => $customerId,
            'productId' => $productId,
        ];
        $query = Cart::where($con)->delete();
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product removed from cart successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getAllOrder($key = "")
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $query = Order::select('tbl_orders.orderId','tbl_orders.uuid AS orderUuid','tbl_orders.orderCode','tbl_orders.couponCode','tbl_orders.orderAddress','tbl_orders.totalAmt','tbl_orders.discountedAmt','tbl_orders.paymentMode','tbl_orders.transId','tbl_orders.paymentStatus','tbl_orders.orderStatus')->where('customerId',$customerId);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'orders' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getOrderDetail($uuid)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $con=['tbl_orders.uuid' => $uuid, 'customerId' => $customerId];
        $query = Order::select('tbl_orders.orderId','tbl_orders.uuid AS orderUuid','tbl_orders.orderCode','tbl_orders.couponCode','tbl_orders.orderAddress','tbl_orders.totalAmt','tbl_orders.discountedAmt','tbl_orders.paymentMode','tbl_orders.transId','tbl_orders.paymentStatus','tbl_orders.orderStatus')
                        ->where($con)->first();
        if($query){
            return response()->json([
                'status' => 'success',
                'order' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getProductListing($filter,$key='')
    {

        $sql= "SELECT tbl_products.productId,tbl_products.uuid AS productUuid,tbl_products.productName,tbl_products.productDesc,tbl_products.unit,tbl_products.productQty,tbl_products.productMrp,tbl_products.productMsp,tbl_products.productMfd,tbl_products.productExp,tbl_brands.brandName,tbl_brands.brandImg,cat.catName,cat.catImg,sub_cat.catName AS subCatName,sub_cat.catImg AS subCatImg,tbl_category_types.catTypeName,tbl_category_types.catTypeImg,GROUP_CONCAT((tbl_product_images.productImage)) AS productImage
        FROM tbl_products
        LEFT JOIN tbl_product_images ON tbl_product_images.productId=tbl_products.productId
        LEFT JOIN tbl_brands ON tbl_brands.brandId=tbl_products.brandId
        LEFT JOIN tbl_category_types ON tbl_category_types.catTypeId=tbl_products.catTypeId
        LEFT JOIN tbl_categories as cat ON tbl_products.catId=cat.catId
        LEFT JOIN tbl_categories as sub_cat ON tbl_products.subCatId=sub_cat.catId
        WHERE tbl_products.isActive=1 AND tbl_products.isDeleted=0
        GROUP BY tbl_products.productId";
        $query = DB::select( DB::raw($sql) );

        // $con = [
        //     'tbl_products.isActive' => 1,
        //     'tbl_products.isDeleted' => 0,
        // ];
        // $query = Product::select('tbl_products.productId','tbl_products.uuid','tbl_products.productName','tbl_products.productDesc','tbl_products.unit','tbl_products.productQty','tbl_products.productMrp','tbl_products.productMsp','tbl_products.productMfd','tbl_products.productExp','tbl_product_images.productImage','tbl_brands.brandName','tbl_brands.brandImg','cat.catName','cat.catImg','sub_cat.catName AS subCatName','sub_cat.catImg AS subCatImg','tbl_category_types.catTypeName','tbl_category_types.catTypeImg')
        //                 ->leftJoin('tbl_brands', 'tbl_products.brandId', 'tbl_brands.brandId')
        //                 ->leftJoin('tbl_categories AS cat', function($join){
        //                     $join->on('tbl_products.catId', 'cat.catId')
        //                     ->where('cat.parentId', 1 );
        //                 })
        //                 ->leftJoin('tbl_categories AS sub_cat', function($join){
        //                     $join->on('tbl_products.subCatId', 'sub_cat.catId')
        //                     ->where('sub_cat.parentId', 0 );
        //                 })
        //                 ->leftJoin('tbl_category_types', 'tbl_products.catTypeId', 'tbl_category_types.catTypeId')
        //                 ->leftJoin('tbl_product_images', 'tbl_products.productId', 'tbl_product_images.productId')
        //                 ->where($con);

        // if(isset($filter) && $filter!=""){
        //     $filters = preg_split ("/\,/", $filter);
        //     $query = $query->whereIn('tbl_wishlist.category',$filters)
        //                     ->orWhereIn('tbl_wishlist.categoryType',$filters);
        // }

        // if (isset($key) && !empty($key)) {
        //     $query = $query->orderBy($key, "desc")->get();
        // } else {
        //     $query = $query->get();
        // }

        if($query){
            return response()->json([
                'status' => 'success',
                'productList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getProductDetail($uuid)
    {
        $sql= "SELECT tbl_products.productId,tbl_products.uuid AS productUuid,tbl_products.productName,tbl_products.productDesc,tbl_products.unit,tbl_products.productQty,tbl_products.productMrp,tbl_products.productMsp,tbl_products.productMfd,tbl_products.productExp,tbl_brands.brandName,tbl_brands.brandImg,cat.catName,cat.catImg,sub_cat.catName AS subCatName,sub_cat.catImg AS subCatImg,tbl_category_types.catTypeName,tbl_category_types.catTypeImg,GROUP_CONCAT((tbl_product_images.productImage)) AS productImage
        FROM tbl_products
        LEFT JOIN tbl_product_images ON tbl_product_images.productId=tbl_products.productId
        LEFT JOIN tbl_brands ON tbl_brands.brandId=tbl_products.brandId
        LEFT JOIN tbl_category_types ON tbl_category_types.catTypeId=tbl_products.catTypeId
        LEFT JOIN tbl_categories as cat ON tbl_products.catId=cat.catId
        LEFT JOIN tbl_categories as sub_cat ON tbl_products.subCatId=sub_cat.catId;
        WHERE tbl_products.uuid='$uuid' AND tbl_products.isActive=1 AND tbl_products.isDeleted=0
        GROUP BY tbl_products.productId";
        // dd($sql);
        $query = DB::select( DB::raw($sql) );

        // $con = [
        //     'tbl_products.isActive' => 1,
        //     'tbl_products.isDeleted' => 0,
        //     'tbl_products.uuid' => $uuid,
        // ];
        // $query = Product::select('tbl_products.productId','tbl_products.uuid','tbl_products.productName','tbl_products.productDesc','tbl_products.unit','tbl_products.productQty','tbl_products.productMrp','tbl_products.productMsp','tbl_products.productMfd','tbl_products.productExp','tbl_product_images.productImage','tbl_brands.brandName','tbl_brands.brandImg','cat.catName','cat.catImg','sub_cat.catName AS subCatName','sub_cat.catImg AS subCatImg','tbl_category_types.catTypeName','tbl_category_types.catTypeImg')
        //                 ->leftJoin('tbl_brands', 'tbl_products.brandId', 'tbl_brands.brandId')
        //                 ->leftJoin('tbl_categories AS cat', function($join){
        //                     $join->on('tbl_products.catId', 'cat.catId')
        //                     ->where('cat.parentId', 1 );
        //                 })
        //                 ->leftJoin('tbl_categories AS sub_cat', function($join){
        //                     $join->on('tbl_products.subCatId', 'sub_cat.catId')
        //                     ->where('sub_cat.parentId', 0 );
        //                 })
        //                 ->leftJoin('tbl_category_types', 'tbl_products.catTypeId', 'tbl_category_types.catTypeId')
        //                 ->leftJoin('tbl_product_images', 'tbl_products.productId', 'tbl_product_images.productId')
        //                 ->where($con)->first();

        if($query){
            if (count($query) > 0){
                return response([
                    'status' => 'success',
                    'product' => $query,
                ],200);
            }else{
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ],200);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getBrandListing($key='')
    {
        $con = [
            'isActive' => 1,
            'isDeleted' => 0
        ];
        $query = Brand::select('brandId','uuid','brandName','brandImg',)
                        ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }

        if($query){
            return response()->json([
                'status' => 'success',
                'brandList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCategoryListing($key='')
    {

        $con = [
            'tbl_categories.isActive' => 1,
            'tbl_categories.isDeleted' => 0,
            'tbl_categories.parentId' => 1,
        ];
        $query = Category::select('tbl_categories.catId','tbl_categories.uuid','tbl_categories.catName','tbl_categories.catImg', 'tbl_category_types.catTypeName', 'tbl_category_types.catTypeImg')
                        ->leftJoin('tbl_category_types', 'tbl_categories.catTypeId', 'tbl_category_types.catTypeId')
                        ->where($con);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'categoryList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCategoryTypeListing($key='')
    {
        $con = [
            'isActive' => 1,
            'isDeleted' => 0
        ];
        $query = CategoryType::select('catTypeId','uuid','catTypeName','catTypeImg',)
                        ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'categoryTypeList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getSubCategoryListing($categoryId,$key='')
    {
        $con = [
            'tbl_categories.isActive' => 1,
            'tbl_categories.isDeleted' => 0,
            'tbl_categories.parentId' => 0,
            'tbl_categories.parentCatId' => $categoryId,
        ];
        $query = Category::select('tbl_categories.catId AS subCatId','tbl_categories.uuid AS subCatUuid','tbl_categories.catName AS subCatName','tbl_categories.catImg AS subCatImg', 'tbl_category_types.catTypeName', 'tbl_category_types.catTypeImg')
                        ->leftJoin('tbl_category_types', 'tbl_categories.catTypeId', 'tbl_category_types.catTypeId')
                        ->where($con);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'subCategoryList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function addReview(array $data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data['customerId'] = $token['customerId'];
        $data['uuid'] = (string)Str::uuid();
        $check_exist = Product::select('productId')->where('productId',$data['productId'])->first();
        if(!$check_exist){
            return response()->json([
                'status' => 'error',
                'message' => 'Product not found!',
            ],404);
        }
        $con= ['customerId'=>$data['customerId'],'productId'=>$data['productId']];
        $check = Review::select('reviewId')->where($con)->first();
        if(!$check){
            $image=$data['reviewFile'];
            if(isset($image) && $image!="")
            {
                $data['reviewFile'] = $this->common->insertBulkFiles($image,'customerReview','/images/customerReview');
                // $fileName = "customerReview" . time() . '.' . $image->getClientOriginalExtension();
                // $path = $image->move(public_path() . '/images/customerReview', $fileName);
                // $data['reviewFile'] = $fileName;
            }
            $query = Review::insertGetId($data);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Review already exist!',
            ],400);
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Review added successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getReviewList($productId,$key = "")
    {
       $query = Review::select('tbl_reviews.reviewId','tbl_reviews.uuid AS reviewUuid','tbl_reviews.rating', 'tbl_reviews.review', 'tbl_reviews.reviewFile AS images','tbl_customers.fullName', 'tbl_products.productName', 'tbl_reviews.createdAt')
                        ->leftJoin('tbl_customers', 'tbl_reviews.customerId', 'tbl_customers.customerId')
                        ->leftJoin('tbl_products', 'tbl_reviews.productId', 'tbl_products.productId')
                        ->where('tbl_reviews.productId',$productId);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'reviewList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'No reviews available',
            ],404);
        }
    }

    public function getMyReviewList($key = "")
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data = ['tbl_reviews.customerId' => $token['customerId']];
        $query = Review::select('tbl_reviews.reviewId','tbl_reviews.uuid AS reviewUuid','tbl_reviews.rating', 'tbl_reviews.review', 'tbl_reviews.reviewFile AS images','tbl_customers.fullName', 'tbl_products.productName', 'tbl_reviews.createdAt')
                        ->leftJoin('tbl_customers', 'tbl_reviews.customerId', 'tbl_customers.customerId')
                        ->leftJoin('tbl_products', 'tbl_reviews.productId', 'tbl_products.productId')
                        ->where($data);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'reviewList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'No reviews available',
            ],404);
        }
    }

    public function getFaqListing($key = "")
    {
        $cond = [
            'isActive' => 1,
        ];
        $query = Faq::where($cond);
            if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'faqs' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'No faqs available',
            ],404);
        }
    }

    public function addToWishlist(array $data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data['customerId'] = $token['customerId'];
        $data['uuid'] = (string)Str::uuid();
        $con= ['customerId'=>$data['customerId'],'productId'=>$data['productId']];
        $check = Wishlist::select('wishlistId')->where($con)->first();
        if(!$check){
            $query = Wishlist::insertGetId($data);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Product already exist!',
            ],400);
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product added to wishlist successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getWishlistData($filter,$key = "")
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $cond = [
            'tbl_wishlist.customerId' => $customerId,
        ];


        // $catList = Wishlist::select('tbl_categories.catName','tbl_category_types.catTypeName')
        //                     ->leftJoin('tbl_categories','tbl_wishlist.categoryId','tbl_categories.catId')
        //                     ->leftJoin('tbl_category_types','tbl_wishlist.categoryTypeId','tbl_category_types.catTypeId')
        //                     ->where($cond)
        //                     ->get();
        // dd($catList->toArray());
        $query = Wishlist::select('tbl_wishlist.wishlistId', 'tbl_wishlist.uuid AS wishlistUuid','tbl_wishlist.category','tbl_wishlist.categoryType','tbl_products.productId','tbl_products.uuid AS productUuid','tbl_products.productDesc','tbl_products.productMrp','tbl_product_images.productImage')
                            ->leftJoin('tbl_products','tbl_products.productId' ,'tbl_wishlist.productId')
                            ->leftJoin('tbl_product_images','tbl_products.productId' ,'tbl_product_images.productId')
                            ->where($cond);

        if(isset($filter) && $filter!=""){
            $filters = preg_split ("/\,/", $filter);
            $query = $query->whereIn('tbl_wishlist.category',$filters)
                            ->orWhereIn('tbl_wishlist.categoryType',$filters);
        }

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }

        if($query){
            return response()->json([
                'status' => 'success',
                'wishlist' => $query->toArray(),
                // 'catList' => $catList->toArray(),
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function removeFromWishlist($productId)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $con = [
            'customerId' => $customerId,
            'productId' => $productId,
        ];
        $query = Wishlist::where($con)->delete();
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product successfully removed from wishlist!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCustomerFromUuid($uuid)
    {
        $query = Customer::select('customerId','profilePicture','fullName','email','dob','mobile','walletAmount','deviceType')->where('uuid', $uuid)->first();
        if($query){
            return response()->json([
                'status' => 'success',
                'customer' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCustomerFromToken()
    {
        // from token
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $query = Customer::select('customerId','profilePicture','fullName','email','dob','mobile','walletAmount','deviceType')->where('uuid', $token['customerUuid'])->first();
        // return $query;
        // from auth
        // $customer = Auth::user();

        if($query){
            return response()->json([
                'status' => 'success',
                'customer' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function resetCustomerPassword($password)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];

        $customer = Customer::where('customerId', $customerId)->first();

        if(!$customer){
            return response([
                'message' => 'Bad credentials'
            ], 401);
        }
        $data = [
            'password' => Hash::make($password),
        ];

        $result = Customer::where('customerId', $customerId)->update($data);
        if($result){
            return response([
                'message' => 'Success',
            ],201);
        }else{
            return response([
                'status' => 'error',
                'message' => 'Something went wrong!'
            ], 401);
        }
    }

    public function changeCustomerPassword($oldPassword, $password)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];

        $customer = Customer::where('customerId', $customerId)->first();

        if(!$customer || !Hash::check($oldPassword, $customer->password)){
            return response([
                'status' => 'error',
                'message' => 'Bad credentials'
            ], 401);
        }
        $data = [
            // 'password' => bcrypt($request['password']),
            'password' => Hash::make($password),
        ];

        $result = Customer::where('customerId', $customerId)->update($data);
        if($result){
            return response([
                'status' => 'Success',
                'message' => 'Password changed successfully!'
            ],201);
        }else{
            return response([
                'status' => 'error',
                'message' => 'Something went wrong!'
            ], 401);
        }
    }

    public function customerForgotPassword($email, $password)
    {
        $customer = Customer::where('email', $email)->first();

        if(!$customer){
            return response([
                'message' => 'Bad credentials'
            ], 401);
        }
        $data = [
            'password' => Hash::make($password),
        ];

        $result = Customer::where('email', $email)->update($data);
        if($result){
            return response([
                'status' => 'Success',
                'message' => 'Password changed successfully!'
            ],201);
        }else{
            return response([
                'status' => 'error',
                'message' => 'Something went wrong!'
            ], 401);
        }
    }

    public function getSizes($data,$key='')
    {
        $con = [
            'isActive' => 1,
            'brandId' => $data['brandId'],
            'gender' => $data['gender'],
        ];
        $query = Size::select('sizeId','categoryType', 'measurements')
                        ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
            $query = $query->toArray();
        }
        $sizeList =[];
        foreach($query as $size){
            $list['sizeId'] = $size['sizeId'];
            $list['categoryType'] = $size['categoryType'];
            $list['measurements'] = json_decode($size['measurements'] ,true);
            array_push($sizeList, $list);
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'sizeList' => $sizeList,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getAccountDetail($uuid)
    {
        $customerId = Customer::select('customerId')->where('uuid', $uuid)->first();
        if($customerId){
            $query = BankDetail::select('bankDetailId','customerName','bankName','accountNumber','ifscCode')->where('customerId', $customerId['customerId'])->get();
            if($query){
                return response()->json([
                    'status' => 'success',
                    'bankDetail' => $query,
                ],200);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Customer not found',
            ],404);
        }
    }

    public function addAccountDetail($uuid, array $data)
    {
        // $token = JWTAuth::getToken();
        // $token = JWTAuth::decode($token);
        // $data['customerId'] = $token['customerId'];
        $customerId = Customer::select('customerId')->where('uuid', $uuid)->first();
        if($customerId){
            $data['customerId'] = $customerId['customerId'];
            $check = BankDetail::where(['customerId'=>$data['customerId']])->first();
            if($check){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Account already exist!',
                ],400);
            }

            $query = BankDetail::insertGetId($data);
            if($query){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Bank details added successfully!',
                ],201);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Customer not found',
            ],404);
        }
    }

    public function getSearchResult($data)
    {
        $finalArray = [];
        $product = Product::where('productName','LIKE','%'.$data['searchKey'].'%')->take(3)->get();
        $brand = Brand::where('brandName','LIKE','%'.$data['searchKey'].'%')->take(3)->get();
        $category = Category::where('catName','LIKE','%'.$data['searchKey'].'%')->take(3)->get();
        $categoryType = CategoryType::where('catTypeName','LIKE','%'.$data['searchKey'].'%')->take(3)->get();
        if($product){
            $finalArray['product'] = $product->toArray();
        }
        if($brand){
            $finalArray['brand'] = $brand->toArray();
        }
        if($category){
            $finalArray['category'] = $category->toArray();
        }
        if($categoryType){
            $finalArray['categoryType'] = $categoryType->toArray();
        }
        if($finalArray){
            return response()->json([
                'status' => 'success',
                'searchResults' => $finalArray,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function managePreferences($data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];

        $customer = Customer::where('customerId', $customerId)->first();

        if($customer){
            $result = Preference::where('customerId', $customerId)->update($data);
            if($result){
                return response([
                    'status' => 'Success',
                    'message' => 'Preferences changed successfully!'
                ],201);
            }else{
                return response([
                    'status' => 'error',
                    'message' => 'Something went wrong!'
                ], 401);
            }
        }else{
            return response([
                'status' => 'error',
                'message' => 'Invalid token!'
            ], 400);
        }


    }

    public function getLanguageListing()
    {
        $query = Languages::all();
        if($query){
            return response()->json([
                'status' => 'success',
                'languages' => $query->toArray(),
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function sendOtp($email,$otp)
    {
        $customer = Customer::where('email', $email)->first();
        if($customer){
            $mail_details = [
                'title' => 'Laravel',
                'subject' => 'OTP for Laravel',
                'body' => 'Your OTP is : '. $otp
            ];

            Mail::to($email)->send(new MailTemplate($mail_details));

             return response()->json([
                'status' => 'success',
                'message' => 'Otp sent successfully!',
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Customer not found!',
            ],404);
        }

    }

    public function deactivateMyAccount()
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);

        $query = Customer::select('customerId','isActive')->where('uuid', $token['customerUuid'])->first();
        if(isset($query) && $query->isActive==1){
            Customer::where('customerId',$query->customerId)->update(['isActive'=>0]);
            Auth::logout();
            return response()->json([
                'status' => 'success',
                'message' => 'Account deactivated!',
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function referralPayout($referredBy, $referredTo)
    {
        $referredByQuery = Customer::select('customerId')->where(['referralCode'=>$referredBy,'isActive'=>1])->first();
        if($referredByQuery){
            $data1 = [
                'uuid' => (string)Str::uuid(),
                'customerId' => $referredByQuery['customerId'],
                'inAmt' => 100,
                'actionType' => 'referral',
                'referralById' => $referredTo,
                'transType' => 'credit'
            ];
            CustomerWallet::create($data1);
            $data2 = [
                'uuid' => (string)Str::uuid(),
                'customerId' => $referredTo,
                'inAmt' => 100,
                'actionType' => 'referral',
                'transType' => 'credit'
            ];
            CustomerWallet::create($data2);
            $this->calcWalletAmount();
            return 1;
        }else{
            return 0;
        }
    }

    public function calcWalletAmount()
    {
        $customerData = Customer::select('customerId')->get();
        foreach($customerData as $customer){
            $wallet=0;
            $customerId = $customer['customerId'];
            $customerWalletData = CustomerWallet::select('inAmt','outAmt')->where('customerId',$customerId)->get();
            if($customerWalletData){
                foreach($customerWalletData as $customerWallet){
                    if($customerWallet['inAmt']){
                        $wallet += $customerWallet['inAmt'];
                    }else{
                        $wallet -= $customerWallet['outAmt'];
                    }
                }
                Customer::where('customerId',$customerId)->update(['walletAmount'=>$wallet]);
            }
        }
        return 1;
    }

    public function getWalletHistory()
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customer = Customer::select('customerId')->where('uuid', $token['customerUuid'])->first();
        $customerId = $customer['customerId'];
        $query = CustomerWallet::select('tbl_customer_wallet.walletId','tbl_customer_wallet.uuid','tbl_coupons.couponCode','tbl_coupons.couponTitle','tbl_orders.orderCode','tbl_orders.transId AS odrTransId','tbl_orders.orderStatus','tbl_customer_wallet.transId','tbl_customer_wallet.ordItmId','tbl_customer_wallet.inAmt','tbl_customer_wallet.outAmt','tbl_customer_wallet.actionType','tbl_customer_wallet.transType','tbl_customer_wallet.createdAt')
                                ->leftJoin('tbl_coupons','tbl_coupons.couponId','tbl_customer_wallet.couponId')
                                ->leftJoin('tbl_orders','tbl_orders.orderId','tbl_customer_wallet.orderId')
                                ->where('tbl_customer_wallet.customerId',$customerId)
                                ->get();

        if($query){
            return response()->json([
                'status' => 'success',
                'walletHistory' => $query->toArray(),
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function WalletToBankTrans($amount)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $query = Customer::select('customerId','walletAmount')->where('uuid', $token['customerUuid'])->first();
        $walletAmount = $query['walletAmount'];
        if($walletAmount<100){
            return response()->json([
                'status' => 'error',
                'message' => 'Wallet amount should be minimum Rs 100 to transfer into bank account',
            ],400);
        }
        if($walletAmount>=$amount){
            $data = [
                'uuid' => (string)Str::uuid(),
                'customerId' => $query['customerId'],
                'outAmt' => $amount,
                'actionType' => 'bank transfer',
                'transId' => "TXN-".rand(1234,9999),
                'transType' => 'debit'
            ];
            $walletQuery = CustomerWallet::create($data);
            $this->calcWalletAmount();
            if($walletQuery){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Transaction successfully!',
                ],200);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Amount should be less than or equals to total wallet account',
            ],400);
        }
    }

    public function addToWallet($data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data['customerId'] = $token['customerId'];
        $data['uuid'] = (string)Str::uuid();
        if($data['actionType']=="bank transfer"){
            $data['transType'] = "debit";
        }else{
            $data['transType'] = "credit";
        }
        $query = CustomerWallet::create($data);
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Transaction successfully!',
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function orderSummary($orderId)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $orderDetail = Order::select('tbl_orders.orderId','tbl_orders.uuid AS orderUuid','tbl_orders.orderCode','tbl_orders.couponCode','tbl_orders.orderAddress','tbl_orders.totalAmt','tbl_orders.discountedAmt','tbl_orders.paymentMode','tbl_orders.transId','tbl_orders.paymentStatus','tbl_orders.orderStatus')->where(['customerId'=>$customerId,'orderId'=>$orderId])->first();

        $sql= "SELECT tbl_orders.orderNo, tbl_orders.orderCode, tbl_products.`productName`, tbl_order_item.`productQty`, tbl_order_item.`finalPrice`, tbl_order_item.`deliveryCharge`,
        tbl_vendors.`vendorName`, tbl_vendors.`vendorEmail`, tbl_vendors.`vendorAddress`,
        tbl_order_item.`ordStatus`
        FROM tbl_order_item
        LEFT JOIN tbl_orders ON tbl_order_item.orderId=tbl_orders.orderId
        LEFT JOIN tbl_products ON tbl_products.productId=tbl_order_item.productId
        LEFT JOIN tbl_vendors ON tbl_vendors.vendorId=tbl_order_item.vendorId
        WHERE tbl_order_item.orderId=(SELECT tbl_orders.orderId FROM tbl_orders WHERE tbl_orders.orderId='$orderId' AND tbl_orders.customerId='$customerId')";
        $query = DB::select( DB::raw($sql) );

        if($query){
            if (count($query) > 0){
                return response([
                    'status' => 'success',
                    'orderDetail' => $orderDetail,
                    'orderSummary' => $query,
                ],200);
            }else{
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ],200);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }


}
