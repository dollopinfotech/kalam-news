<?php

namespace App\Repository\Repository;

use App\Models\Driver;
use App\Models\DriverBankAccount;
use App\Models\DriverDocument;
use App\Models\DriverJob;
use App\Models\DriverWallet;
use App\Repository\Interface\DriverInterface;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Str;

class DriverRepository implements DriverInterface
{

    public function driverRegistration(array $data)
    {
        $query = Driver::create($data);
        if($query){
             $token = Auth::login($query);
            return response()->json([
                'status' => 'success',
                'message' => 'driver created successfully!',
                'driver' => $query,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function driverLogin(array $data)
    {
        $emailOrMobile = $data['emailOrMobile'];
        if(preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/', $emailOrMobile)){
            $credentials['driverEmail'] = $emailOrMobile;
            $credentials['password'] = $data['password'];
            $query = Driver::select('driverId')->where('driverEmail',$emailOrMobile)->first();
            if(!$query && $query==null){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Email not registered!',
                ], 401);
            }
        }else if(preg_match('/^[6-9][0-9]{9}$/', $emailOrMobile)){
            $credentials['driverMobile'] = $emailOrMobile;
            $credentials['password'] = $data['password'];
            $query = Driver::select('driverId')->where('driverMobile',$emailOrMobile)->first();
            if(!$query && $query==null){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Mobile not registered!',
                ], 401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid credentials',
            ], 400);
        }
        // if(isset($data['driverEmail'])){
        //     $query = Driver::select('driverId')->where('driverEmail',$data['driverEmail'])->first();
        // }else if(isset($data['driverMobile'])){
        //     $query = Driver::select('driverId')->where('driverMobile',$data['driverMobile'])->first();
        // }
        // if(!$query && $query==null){
        //     return response()->json([
        //         'status' => 'error',
        //         'message' => 'Mobile or Email not registered!',
        //     ], 401);
        // }
        $driverClaims = [
            'driverId' => $query['driverId'],
        ];
        $token = Auth::claims($driverClaims)->attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Credentials did not match!',
            ], 401);
        }
                $driverData = Auth::user();
        return response()->json([
                'status' => 'success',
                'driverData' => $driverData,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
        ]);
    }

    public function driverBankDetails($data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data['driverId'] = $token['driverId'];
        $query = DriverBankAccount::create($data);
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Bank Details added successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function driverDocument(array $driverData)
    {
        // $token = JWTAuth::getToken();
        // $token = JWTAuth::decode($token);
        // $driverData['driverId'] = $token['driverId'];
        $query = DriverDocument::create($driverData);
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Driver documents inserted successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function driverLogout()
    {
        Auth::logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out!',
        ]);

    }

    public function getDriverProfile()
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);

        $query = Driver::select('tbl_drivers.driverId','driverName','gender','dob','driverEmail', 'driverMobile','driverAddress','aadhar','drivingLicense','vehiclePicture','vehicleDocuments')
                            ->leftJoin('tbl_driver_docs', 'tbl_driver_docs.driverId', 'tbl_drivers.driverId')
                            ->where('tbl_drivers.driverId', $token['driverId'])->first();
        if($query){
            return response()->json([
                'status' => 'success',
                'driver' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getActiveOrders($driverId)
    {
        $con = [
            'jobStatus' => 'Active',
            'driverId' => $driverId,
        ];
        $query = DriverJob::select('tbl_orders.orderNo','tbl_orders.uuid','tbl_orders.orderAddress','tbl_orders.orderStatus')
                        ->leftJoin("tbl_orders","tbl_orders.orderId","=","tbl_driver_jobs.orderId")
        ->where($con)->get();
        if($query){
            return response()->json([
                'status' => 'success',
                'driver' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }

    }

    public function getAcceptedOrders($driverId)
    {
        $con = [
            'jobStatus' => 'Accepted',
            'driverId' => $driverId,
        ];
        $query = DriverJob::select('tbl_orders.orderNo','tbl_orders.uuid','tbl_orders.orderAddress','tbl_orders.orderStatus')
                        ->leftJoin("tbl_orders","tbl_orders.orderId","=","tbl_driver_jobs.orderId")
        ->where($con)->get();
        if($query){
            return response()->json([
                'status' => 'success',
                'driver' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }

    }

    public function driverEarnings()
    {
        $driverData = Driver::select('driverId')->get();
        foreach($driverData as $driver){
            $earning=0;
            $driverId = $driver['driverId'];
            $driverJobData = DriverJob::select('earning')->where('driverId',$driverId)->get();
            if($driverJobData){
                foreach($driverJobData as $driverJob){
                    $earning += $driverJob['earning'];
                }
                Driver::where('driverId',$driverId)->update(['totalEarning'=>$earning]);
            }
        }
        return 1;
    }

    public function calcDriverWalletAmount()
    {
        $driverData = Driver::select('driverId')->get();
        foreach($driverData as $driver){
            $wallet=0;
            $driverId = $driver['driverId'];
            $driverWalletData = DriverWallet::select('inAmt','outAmt','isApproved')->where('dPtnrId',$driverId)->get();
            if($driverWalletData){
                foreach($driverWalletData as $driverWallet){
                    if($driverWallet['inAmt']){
                        $wallet += $driverWallet['inAmt'];
                    }else{
                        if($driverWallet['isApproved']==1){
                            $wallet -= $driverWallet['outAmt'];
                        }
                    }
                }
                Driver::where('driverId',$driverId)->update(['walletAmount'=>$wallet]);
            }
        }
        return 1;
    }

    public function driverWalletToBankTrans($amount)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $query = Driver::select('driverId','walletAmount')->where('driverId', $token['driverId'])->first();
        $walletAmount = $query['walletAmount'];
        if($walletAmount<100){
            return response()->json([
                'status' => 'error',
                'message' => 'Wallet amount should be minimum Rs 100 to transfer into bank account',
            ],400);
        }
        if($walletAmount>=$amount){
            $data = [
                'uuid' => (string)Str::uuid(),
                'dPtnrId' => $query['driverId'],
                'outAmt' => $amount,
                'actionType' => 'withdrawal',
                // 'transId' => "TXN-".rand(1234,9999),
                'transType' => 'debit'
            ];
            $walletQuery = DriverWallet::create($data);
            $this->calcDriverWalletAmount();
            if($walletQuery){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Transaction successfully!',
                ],200);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Amount should be less than or equals to total wallet account',
            ],400);
        }
    }
}
