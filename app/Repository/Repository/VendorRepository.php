<?php

namespace App\Repository\Repository;

use App\Models\Vendor;
use App\Models\Product;
use App\Models\ProductImage;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Repository\Interface\VendorInterface;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class VendorRepository implements VendorInterface
{


    public function vendorLogin(array $data)
    {
        $emailOrMobile = $data['emailOrMobile'];
        if(preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/', $emailOrMobile)){
            $credentials['vendorEmail'] = $emailOrMobile;
            $credentials['password'] = $data['password'];
            $query = Vendor::select('uuid','vendorId','isActive')->where('vendorEmail',$emailOrMobile)->first();
            if(!$query && $query==null){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Email not registered!',
                ], 401);
            }
        }else if(preg_match('/^[6-9][0-9]{9}$/', $emailOrMobile)){
            $credentials['vendorMobile'] = $emailOrMobile;
            $credentials['password'] = $data['password'];
            $query = Vendor::select('uuid','vendorId','isActive')->where('vendorMobile',$emailOrMobile)->first();
            if(!$query && $query==null){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Mobile not registered!',
                ], 401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid credentials',
            ], 400);
        }
        if($query['isActive']==0){
            Vendor::where('vendorId',$query->vendorId)->update(['isActive'=>1]);
        }
        $customClaims = [
            'vendorUuid' => $query['uuid'],
            'vendorId' => $query['vendorId'],
        ];
        $token = Auth::claims($customClaims)->attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => "These credentials do not match our records.",
            ], 401);
        }
        $customerData = Auth::user();
        return response()->json([
                'status' => 'success',
                'customerData' => $customerData,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
        ]);

    }

    public function vendorLogout()
    {
        Auth::logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out!',
        ]);
    }

    public function getVendorProfile()
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $con = [
            'isActive'=>1,
            'isDeleted'=>0,
            'uuid'=>$token['vendorUuid'],
        ];
        $query = Vendor::select('vendorId','vendorName','vendorMobile','vendorEmail','vendorAddress','vendorLatitude','vendorLongitude','cityName','stateName','countryName')
                        ->leftJoin('tbl_cities','tbl_cities.cityId','tbl_vendors.cityId')
                        ->leftJoin('tbl_states','tbl_states.stateId','tbl_vendors.stateId')
                        ->leftJoin('tbl_countries','tbl_countries.countryId','tbl_vendors.countryId')
                        ->where($con)->first();

        if($query){
            if ($query){
                return response([
                    'status' => 'success',
                    'adminData' => $query,
                ],200);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }



}
