<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->integer('admin_id')->autoIncrement();
            $table->uuid('uuid')->default(DB::raw('(UUID())'))->unique();
            $table->string('role_id')->nullable();
            $table->string('admin_name');
            $table->string('admin_profile', 100)->nullable();
            $table->string('admin_mobile', 20)->unique();
            $table->string('country_code',10)->default('+91');
            $table->string('admin_email')->unique();
            $table->string('admin_address')->nullable();
            $table->string('password');
            $table->integer('otp')->nullable();
            $table->tinyInteger('is_active')->default(1)->comment('0: Not Active, 1: Active');
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('admins');
    }
};
