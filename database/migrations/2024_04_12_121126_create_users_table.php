<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('user_id')->autoIncrement();
            $table->uuid('uuid')->default(DB::raw('(UUID())'))->unique();
            $table->string('referral_code')->nullable();
            $table->string('initials')->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->date('dob');
            $table->string('password');
            $table->string('mobile_number')->unique();
            $table->string('whatsapp_number')->nullable();
            $table->string('email')->unique();
            $table->string('pincode');
            $table->string('city');
            $table->string('state');
            $table->text('address');
            $table->string('profile')->nullable();
            $table->integer('status')->default('1');
            $table->string('otp')->nullable();
            $table->integer('is_mobile_verify')->default('0');
            $table->integer('is_email_verify')->default('0');
            $table->string('referred_by')->nullable();
            $table->integer('l_user_id',false)->nullable();
            $table->integer('r_user_id',false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
