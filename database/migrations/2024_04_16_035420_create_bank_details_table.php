<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_details', function (Blueprint $table) {
            $table->integer('bank_id')->autoIncrement();
            $table->integer('user_id');
            $table->string('customer_name');
            $table->string('mobile_no');
            $table->string('bank_name');
            $table->string('account_no');
            $table->string('ifsc_code');
            $table->string('bank_branch');
            $table->string('adhar_card_no');
            $table->string('adhar_card_photo');
            $table->string('pan_card_no');
            $table->string('pan_card_photo');
            $table->integer('is_adhar_card_verify')->default('0');
            $table->integer('is_pan_card_verify')->default('0');
            $table->integer('is_bank_account_verify')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_details');
    }
};
