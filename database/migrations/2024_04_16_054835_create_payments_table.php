<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->integer('payment_id')->autoIncrement();
            $table->uuid('uuid')->default(DB::raw('(UUID())'))->unique();
            $table->string('payment_type');
            $table->string('amount');
            $table->integer('user_id');
            $table->integer('transaction_id');
            $table->tinyInteger('is_active')->default(1)->comment('0: Not Active, 1: Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payments');
    }
};
