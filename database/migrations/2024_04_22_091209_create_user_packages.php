<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_packages', function (Blueprint $table) {          
                $table->integer('userPackage_id')->autoIncrement();
                $table->unsignedBigInteger('package_id');
                $table->unsignedBigInteger('user_id');
                $table->string('transaction_id')->nullable();
                $table->string('transaction_type')->default('online');
                $table->enum('payment_status', ['success', 'pending', 'canceled', 'failed'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_package');
    }
};
