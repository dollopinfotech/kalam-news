<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users_documents', function (Blueprint $table) {
            $table->integer('docs_id')->autoIncrement();
            $table->integer('user_id');
            $table->string('aadhar_card_no');
            $table->string('aadhar_card_photo');
            $table->string('pan_card_no');
            $table->string('pan_card_photo');
            $table->integer('is_adhar_card_verify')->default('0');
            $table->integer('is_pan_card_verify')->default('0');
            $table->integer('is_bank_account_verify')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users_documents');
    }
};
