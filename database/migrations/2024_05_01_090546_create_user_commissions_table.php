<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_commissions', function (Blueprint $table) {
            $table->integer('user_commission_id')->autoIncrement();
            $table->integer('user_id', false); 
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->integer('commission_by_user_id', false); 
            $table->foreign('commission_by_user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->decimal('commission_amount', 10, 2);
            $table->integer('commission_level', false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_commissions');
    }
};
