<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('adv_request', function (Blueprint $table) {
            $table->integer('adv_request_id')->autoIncrement();
            $table->integer('user_id', false); 
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->string('adv_title');
            $table->string('adv_desc');
            $table->string('adv_doc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('adv_request');
    }
};
