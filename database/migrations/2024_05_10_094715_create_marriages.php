<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_marriage', function (Blueprint $table) {
            $table->integer('merriage_id')->autoIncrement();
            $table->string('name');
            $table->date('dob');
            $table->integer('height');
            $table->string('qualification');
            $table->string('business_or_job');
            $table->string('father_name');
            $table->string('father_business_or_job');
            $table->string('mother_name');
            $table->string('mother_business_or_job');
            $table->string('address');
            $table->string('mobile_number');
            $table->string('bio_data_profile');
            $table->string('profile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('marriage');
    }
};
