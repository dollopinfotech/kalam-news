<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
    }

    private function getSeedData()
    {
        return [
            'Menus' => [
                [
                    'name' => 'Dashboard',
                    'menu_link' => 'dashboard',
                    'menu_icon' => 'fa fa-dashboard',
                ],
                [
                    'name' => 'Menu Management',
                    'menu_link' => '#',
                    'menu_icon' => 'fa fa-bars',
                    'sub_menu' => '1',
                ],
                [
                    'name' => 'Role & Permission',
                    'menu_link' => '#',
                    'menu_icon' => 'fa fa-tasks',
                    'sub_menu' => '1',
                ],
                [
                    'name' => 'Customer Management',
                    'menu_link' => '#',
                    'menu_icon' => 'fa fa-users',
                    'sub_menu' => '1',
                ],
                [
                    'name' => 'Site Setting',
                    'menu_link' => 'site-settings',
                    'menu_icon' => 'fas fa-cogs',
                ],
                [
                    'name' => 'Packages',
                    'menu_link' => 'packages',
                    'menu_icon' => 'fas fa-cube',
                ],
            ],

            'SubMenus' => [
                [
                    'parent_menu_id' => '2',
                    'name' => 'Menu',
                    'menu_link' => 'menu',
                ],
                [
                    'parent_menu_id' => '2',
                    'name' => 'Sub Menu',
                    'menu_link' => 'subMenu',
                ],
                [
                    'parent_menu_id' => '3',
                    'name' => 'Permissions',
                    'menu_link' => '#',
                ],
                [
                    'parent_menu_id' => '3',
                    'name' => 'Roles',
                    'menu_link' => '#',
                ],
                [
                    'parent_menu_id' => '4',
                    'name' => 'Customers',
                    'menu_link' => 'users',
                ],
            ],

        ];
    }
}
