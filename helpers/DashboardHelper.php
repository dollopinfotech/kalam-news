<?php

namespace App\Helpers;

use App\Models\User;
use App\Models\UserPackage;
use App\Models\Package;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class DashboardHelper
{
    public static function getDashboardMetrics()
    {
        $users = User::where(function ($query) {
            $query->where('is_mobile_verify', 1)
                  ->orWhere('is_email_verify', 1);
        })->get();

        $packages = UserPackage::where('payment_status', 'success')->get();

        $packageIds = $packages->pluck('package_id')->toArray();
        $packageData = Package::whereIn('package_id', $packageIds)->get()->keyBy('package_id');

        $totalPackageValue = 0;
        foreach ($packages as $package) {
            if (isset($packageData[$package->package_id])) {
                $totalPackageValue += $packageData[$package->package_id]->package_price;
            }
        }

        $userCount = $users->count();
        $packageSoldCount = $packages->count();
        $avgPackageSoldVal = $packageSoldCount ? $totalPackageValue / $packageSoldCount : 0;

        $lastMonthStart = Carbon::now()->subMonth()->startOfMonth();
        $lastMonthEnd = Carbon::now()->subMonth()->endOfMonth();
        $monthBeforeLastStart = Carbon::now()->subMonths(2)->startOfMonth();
        $monthBeforeLastEnd = Carbon::now()->subMonths(2)->endOfMonth();

        $lastMonthUserCount = User::where(function ($query) {
            $query->where('is_mobile_verify', 1)
                  ->orWhere('is_email_verify', 1);
        })
        ->whereBetween('created_at', [$lastMonthStart, $lastMonthEnd])
        ->count();

        $monthBeforeLastUserCount = User::where(function ($query) {
            $query->where('is_mobile_verify', 1)
                  ->orWhere('is_email_verify', 1);
        })
        ->whereBetween('created_at', [$monthBeforeLastStart, $monthBeforeLastEnd])
        ->count();

        $lastMonthPackages = UserPackage::where('payment_status', 'success')
                                        ->whereBetween('created_at', [$lastMonthStart, $lastMonthEnd])
                                        ->get();

        $monthBeforeLastPackages = UserPackage::where('payment_status', 'success')
                                              ->whereBetween('created_at', [$monthBeforeLastStart, $monthBeforeLastEnd])
                                              ->get();

        $lastMonthValue = 0;
        foreach ($lastMonthPackages as $package) {
            if (isset($packageData[$package->package_id])) {
                $lastMonthValue += $packageData[$package->package_id]->package_price;
            }
        }

        $monthBeforeLastValue = 0;
        foreach ($monthBeforeLastPackages as $package) {
            if (isset($packageData[$package->package_id])) {
                $monthBeforeLastValue += $packageData[$package->package_id]->package_price;
            }
        }

        $userHikePercentage = 0;
        if ($monthBeforeLastUserCount > 0) {
            $userHikePercentage = (($lastMonthUserCount - $monthBeforeLastUserCount) / $monthBeforeLastUserCount) * 100;
        }

        $packageSoldHikePercentage = 0;
        if ($monthBeforeLastPackages->count() > 0) {
            $packageSoldHikePercentage = (($lastMonthPackages->count() - $monthBeforeLastPackages->count()) / $monthBeforeLastPackages->count()) * 100;
        }

        $avgHikePercentage = 0;
        if ($monthBeforeLastValue > 0) {
            $avgHikePercentage = (($lastMonthValue - $monthBeforeLastValue) / $monthBeforeLastValue) * 100;
        }

        $lastMonthName = Carbon::now()->subMonth()->format('F Y');

        return [
            'users' => $users,
            'userCount' => $userCount,
            'packageSoldCount' => $packageSoldCount,
            'totalPackageValue' => $totalPackageValue,
            'avgPackageSoldVal' => $avgPackageSoldVal,
            'avgHikePercentage' => $avgHikePercentage,
            'userHikePercentage' => $userHikePercentage,
            'packageSoldHikePercentage' => $packageSoldHikePercentage,
            'lastMonthName' => $lastMonthName
        ];
    }

    

    public static function getDailyUserRegistrations()
    {
        $dateThirtyDaysAgo = Carbon::now()->subDays(7)->toDateString();
        
        $dailyRegistrations = User::selectRaw('DATE(created_at) as date, COUNT(*) as count')
            ->where('created_at', '>=', $dateThirtyDaysAgo)
            ->where(function ($query) {
                $query->where('is_mobile_verify', 1)
                      ->orWhere('is_email_verify', 1);
            })
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get();
            
        // Fill in missing dates with zero counts
        $startDate = Carbon::parse($dateThirtyDaysAgo);
        $endDate = Carbon::now();
        $dates = [];
        $currentDate = $startDate->copy();
        while ($currentDate <= $endDate) {
            $dates[] = $currentDate->toDateString();
            $currentDate->addDay();
        }
        
        $registeredDates = $dailyRegistrations->pluck('date')->toArray();
        $missingDates = array_diff($dates, $registeredDates);
        
        foreach ($missingDates as $missingDate) {
            $dailyRegistrations->push([
                'date' => $missingDate,
                'count' => 0
            ]);
        }
        
        $dailyRegistrations = $dailyRegistrations->sortBy('date')->values();
        
        return $dailyRegistrations;
    }
    


    // public static function getPackageSalesData($interval)
    // {
    //     $query = UserPackage::selectRaw('DATE(created_at) as date, COUNT(*) as count')
    //                         ->groupBy('date')
    //                         ->orderBy('date', 'ASC');
                            
    //     if ($interval == 'weekly') {
    //         $query->whereDate('created_at', '>=', now()->subWeeks(1));
    //         $query->orWhereDate('updated_at', '>=', now()->subWeeks(1));
    //     } else if ($interval == 'monthly') {
    //         $query->whereDate('created_at', '>=', now()->subMonths(1));
    //         $query->orWhereDate('updated_at', '>=', now()->subMonths(1));
    //     }
    
    //     return $query->get();
    // }
    

    public static function getPackageSalesData($interval)
{
    $query = UserPackage::selectRaw('DATE(created_at) as date, COUNT(*) as count')
                        ->groupBy('date')
                        ->orderBy('date', 'ASC');
// dd($query);
    if ($interval == 'weekly') {
        $query->where('created_at', '>=', now()->subWeeks(1));
    } else if ($interval == 'monthly') {
        $query->where('created_at', '>=', now()->subMonths(1));
    }
    return $query->get();
}

}
