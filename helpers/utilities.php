<?php

use App\Models\BankDetail;
use App\Models\Package;
use App\Models\User;
use App\Models\UserPackage;
use App\Models\UsersDocuments;
use App\Models\WalletHistory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\CommissionAgent;
use App\Services\SMSService;


if (!function_exists('generateReferralCode')) {
    // function generateReferralCode()
    // {
    //     return strtoupper(Str::random(8));
    // }

    // function checkNull($data)
    // {
    //     foreach ($data as $value) {
    //         if (is_null($value)) {
    //             return true; // Returns true if any value is null
    //         }
    //     }
    //     return false; // Returns false if no value is null
    // }

    function generateReferralCode($firstName)
    {
        // Extract the first 4 characters of the first name
        $firstNamePart = substr($firstName, 0, 3);

        // Generate a random string of length 4
        $randomStringPart = Str::random(5);

        // Concatenate the first name part and the random string part
        $referralCode = strtoupper($firstNamePart . $randomStringPart);
        $referCodeCheck =  User::where('referral_code', $referralCode)->first();
        if ($referCodeCheck != '') {
            return $referralCode;
        } else {
            generateReferralCode($firstName);
        }
    }
}


if (!function_exists('alerts_show')) {
    /**
     * To show multipal messages
     * @todo
     */
    // function alerts_show($errors = null, $theme = 'backend')
    // {


    //     if(is_array(session('msg'))){
    //         $res = '';
    //         foreach (session('msg') as $msg){
    //             $res .= $msg;
    //         }
    //         return $res;
    //     }

    //     if (session('msg')) {
    //         return session('msg');
    //     }

    //     if (! is_null($errors) && count($errors) > 0) {
    //         $data = compact('errors');

    //         return view("$theme::misc.errors", $data)->render();
    //     }
    // }



    function alerts_show($errors = null)
    {
        // Check if there are any messages in the cookie
        $cookieMsg = request()->cookie('msg');

        if (!empty($cookieMsg)) {
            return $cookieMsg;
        }

        // Display errors passed as parameter
        if (!is_null($errors) && count($errors) > 0) {
            $data = compact('errors');
            return view("$theme::misc.errors", $data)->render();
        }
    }

    if (!function_exists('sendSMS')) {
        function sendSMS($to, $message)
        {
            $sid = env('TWILIO_SID');
            $token = env('TWILIO_TOKEN');
            $from = env('TWILIO_FROM');

            $client = new Client($sid, $token);

            try {
                $client->messages->create(
                    $to,
                    [
                        'from' => $from,
                        'body' => $message,
                    ]
                );
                return true;
            } catch (\Exception $e) {
                \Log::error('Failed to send SMS: ' . $e->getMessage());
                return false;
            }
        }
    }

    if (!function_exists('sendEmail')) {
        function sendEmail($to, $subject, $body)
        {
            Mail::raw($body, function ($message) use ($to, $subject) {
                $message->to($to)
                    ->subject($subject);
            });
        }
    }
}


if (!function_exists('getProfilePercentage')) {
    function getProfilePercentage($user_id)
    {
        $user = User::where('user_id', $user_id)->first();
        $bankDetail = BankDetail::where('user_id', $user->user_id)->where('primary_bank', 1)->first();
        $usersDocuments = UsersDocuments::where('user_id', $user->user_id)->first();
        $userPackage = UserPackage::where('user_id', $user->user_id)->first();

        $maximumPoints  = 100;
        $profilePercentage = 10;
        $profilePercentageArr = array();

        $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Name', 1);

        if ($user->mobile_number != '') {
            $profilePercentage += 2;
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Mobile Number', 1);
        } else {
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Mobile Number', 0);
        }

        // if ($user->whatsapp_number != '') {
        //     $profilePercentage += 1;
        //     $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Whatsapp Number', 1);
        // } else {
        //     $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Whatsapp Number', 0);
        // }

        if ($user->profile != '') {
            $profilePercentage += 1;
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Profile Photo', 1);
        } else {
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Profile Photo', 0);
        }

        if ($user->r_user_id != '') {
            $profilePercentage += 5;
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Right Node Add', 1);
        } else {
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Right Node Add', 0);
        }

        if ($user->l_user_id != '') {
            $profilePercentage += 5;
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Left Node Add', 1);
        } else {
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Left Node Add', 0);
        }

        if ($user->is_mobile_verify != '') {
            $profilePercentage += 2;
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Mobile Number Verify', 1);
        } else {
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Mobile Number Verify', 0);
        }

        // if ($user->is_email_verify != '') {
        //     $profilePercentage += 1;
        //     $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Email Verify', 1);
        // } else {
        //     $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Email Verify', 0);
        // }

        if (!empty($bankDetail) && isset($bankDetail->user_id)) {
            $profilePercentage += 20;
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Bank Details Add', 1);

            if ($bankDetail->is_bank_account_verify != 0) {
                $profilePercentage += 5;
                $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Bank Details Verify', 1);
            } else {
                $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Bank Details Verify', 0);
            }
        } else {
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Bank Details Add', 0);
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Bank Details Verify', 0);
        }

        if (!empty($usersDocuments) && isset($usersDocuments->user_id)) {
            $profilePercentage += 15;
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'KYC Document Add', 1);

            if ($usersDocuments->is_aadhar_verified != 0) {
                $profilePercentage += 5;
                $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Adhar Card Verify', 1);
            } else {
                $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Adhar Card Verify', 0);
            }

            if ($usersDocuments->is_pan_verified != 0) {
                $profilePercentage += 5;
                $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Pan Card Verify', 1);
            } else {
                $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Pan Card Verify', 0);
            }
        } else {
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'KYC Document Add', 0);
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Adhar Card Verify', 0);
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Pan Card Verify', 0);
        }

        if (!empty($userPackage) && isset($userPackage->user_id)) {
            if ($userPackage->payment_status == 'success') {
                $profilePercentage += 25;
                $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Package Purchage', 1);
            }
        } else {
            $profilePercentageArr = setProfilePercentageData($profilePercentageArr, 'Package Purchage', 0);
        }

        return [$profilePercentage * $maximumPoints / 100, $profilePercentageArr];
    }
}

if (!function_exists('setProfilePercentageData')) {
    function setProfilePercentageData($profilePercentageArr, $title, $isEmpty)
    {
        $arr = ['title' => $title, 'isEmpty' => $isEmpty];
        array_push($profilePercentageArr, $arr);
        return $profilePercentageArr;
    }
}

if (!function_exists('getUserLevel')) {
    function getUserLevel($user_id, $level_count)
    {
        $user = auth()->user();
        $userReferrar = User::where('l_user_id', $user_id)->orwhere('r_user_id', $user_id)->where('is_mobile_verify', 1 || 'is_email_verify', 1)->first();

        if (isset($userReferrar->user_id) && $userReferrar->user_id != $user->user_id) {
            getUserLevel($userReferrar->user_id, $level_count + 1);
        } else if (isset($userReferrar->user_id) && $userReferrar->user_id == $user->user_id) {
            Config::set('kalamnews.userLevel', $level_count);
        }
    }
}

if (!function_exists('getPaidUser')) {
    function getPaidUser($user_id)
    {
        $userPackage = UserPackage::where('user_id', $user_id)->first();
        return $userPackage != '' ? 1 : 0;
    }
}

if (!function_exists('getUserNodeCount')) {
    function getUserNodeCount($user_id)
    {
        // $userNodeCount = DB::select('CALL sp_nodeCounts(' . $user_id . ')');

        // foreach ($userNodeCount as $userCount) {
        //     return ['l_user_count' => $userCount->left_childs, 'r_user_count' => $userCount->right_childs];
        // }

        $wallet_history = WalletHistory::where('user_id', $user_id)->get();

        $left_childs = $wallet_history->where('commission_node', 'left')->count('amount');
        $right_childs = $wallet_history->where('commission_node', 'right')->count('amount');

        return ['l_user_count' => $left_childs, 'r_user_count' => $right_childs];
    }
}


if (!function_exists('getUserBVCount')) {
    function getUserBVCount($user_id)
    {
        $userBVCount = DB::select('CALL sp_nodesBvCounts(' . $user_id . ')');

        foreach ($userBVCount as $bvCount) {
            return ['total_bv_count' => $bvCount->left_commission + $bvCount->right_commission, 'r_bv_count' => $bvCount->right_commission, 'l_bv_count' => $bvCount->left_commission];
        }
    }
}

if (!function_exists('htmlToPlaneText')) {
    function htmlToPlainText($str)
    {
        // $str = str_replace('&nbsp;', ' ', $str);
        // $str = html_entity_decode($str, ENT_QUOTES | ENT_COMPAT, 'UTF-8');
        // $str = html_entity_decode($str, ENT_HTML5, 'UTF-8');
        // $str = html_entity_decode($str);
        // $str = htmlspecialchars_decode($str);
        $str = html_entity_decode($str);

        return $str;
    }
}


if (!function_exists('getLastRightUser')) {
    function getLastRightUser($user_id, $level_count, $userTree)
    {
        $userReferrar = User::where('user_id', $user_id)->where('is_mobile_verify', 1 || 'is_email_verify', 1)->first();

        if ($userReferrar != null) {
            $arr = ['level_id' => $level_count, 'user_details' => $userReferrar];
            array_push($userTree, $arr);
            getLastRightUser($userReferrar->r_user_id, $level_count + 1, $userTree);
        }

        if ($userReferrar == null) {
            Config::set('kalamnews.userTree', $userTree);
        }
    }
}


if (!function_exists('getLastLeftUser')) {
    function getLastLeftUser($user_id, $level_count, $userTree)
    {
        $userReferrar = User::where('user_id', $user_id)->where('is_mobile_verify', 1 || 'is_email_verify', 1)->first();

        if ($userReferrar != null) {
            $arr = ['level_id' => $level_count, 'user_details' => $userReferrar];
            array_push($userTree, $arr);
            getLastLeftUser($userReferrar->l_user_id, $level_count + 1, $userTree);
        }

        if ($userReferrar == null) {
            Config::set('kalamnews.userTree', $userTree);
        }
    }
}


if (!function_exists('getUserTree')) {
    function getUserTree($user_id, $level_count, $userTree)
    {
        $userReferrar = User::where('l_user_id', $user_id)->orwhere('r_user_id', $user_id)->where('is_mobile_verify', 1 || 'is_email_verify', 1)->first();

        if ($userReferrar != null) {
            if (isset($userReferrar->l_user_id) && $userReferrar->l_user_id == $user_id) {
                $commission_node = 'left';
            } else if (isset($userReferrar->r_user_id) && $userReferrar->r_user_id == $user_id) {
                $commission_node = 'right';
            }
            $wallet_history = WalletHistory::where('user_id', $userReferrar->user_id)->get();
            $total_income = $wallet_history->where('transaction_type', 'income')->sum('amount');
            $userPackage = UserPackage::select('package_id')
                ->where('user_id', $user_id)
                ->orderBy('userPackage_id', 'DESC')
                ->first();
            if (isset($userPackage->package_id)) {
                $max_commission = Package::select('max_commission')
                    ->where('package_id', $userPackage->package_id)
                    ->first();
            } else {
                $max_commission = new stdClass;
                $max_commission->max_commission = 0;
            }
            $userReferrar->commission_node = $commission_node;
            $arr = ['user_level' => $level_count, 'user_details' => $userReferrar, 'total_income' => $total_income, 'max_commission' => $max_commission->max_commission];
            array_push($userTree, $arr);
            getUserTree($userReferrar->user_id, $level_count + 1, $userTree);
        }

        if ($userReferrar == null) {
            Config::set('kalamnews.userTree', $userTree);
        }
    }
}


if (!function_exists('getUserLevelForAdmin')) {
    function getUserLevelForAdmin($parent_user_id, $user_id, $level_count)
    {
        $user = User::where('user_id', $parent_user_id)->first();
        $userReferrar = User::where('l_user_id', $user_id)->orwhere('r_user_id', $user_id)->where('is_mobile_verify', 1 || 'is_email_verify', 1)->first();

        if (isset($userReferrar->user_id) && $userReferrar->user_id != $user->user_id) {
            getUserLevelForAdmin($user->user_id, $userReferrar->user_id, $level_count + 1);
        } else if (isset($userReferrar->user_id) && $userReferrar->user_id == $user->user_id) {
            Config::set('kalamnews.userLevel', $level_count);
        }
    }
}

if (!function_exists('file_url')) {
    function file_url($path, $secure = null)
    {
        if (env('APP_ENV') == 'local') {
            return app('url')->asset(ltrim($path, '/'), $secure);
        } else {
            return app('url')->asset('public/' . ltrim($path, '/'), $secure);
        }
    }
}


if (!function_exists('sendEmailNotification')) {
    function sendEmailNotification($userData, $subject, $message)
    {
        // Check if the user data array has the email key
        if (isset($userData['email']) && !empty($userData['email'])) {
            $userData['subject'] = $subject;
            $mail = new CommissionAgent([$userData]);
            Mail::to($userData['email'])->send($mail);
        } else {
            // Handle the case where email is not available
            throw new Exception('User email not found');
        }
    }
}

if (!function_exists('sendSMSNotification')) {
    function sendSMSNotification($userData, $message)
    {
        $smsService = new SMSService();
        $smsService->sendSMS(('+91'+$userData['user_number']), $message);
    }
}

if (!function_exists('pushNotificationAndroid')) {
    function pushNotificationAndroid($title, $body, $fcmIds, $image, $activity, $activityJson)
    {
        $secret_key = env('PUSH_NOTI_API_ACCESS_KEY');
        $title = html_entity_decode($title);
        $body = html_entity_decode($body);

        if (is_array($fcmIds)) {
            $fcmIds = array_unique($fcmIds);
            $fcmIds = array_values($fcmIds);
        } else {
            $fcmIds = array($fcmIds);
        }
        if ($image == '') {
            $image = "https://master.my-company.app/img/logo.png";
        }

        $msg_id = (int) date("YmdHis");

        $data = array(
            "image" => $image,
            'body' => $body,
            'title' => $title,
            'click_action' => $activity,
            'click_json' => $activityJson,
            'msg_id' => $msg_id,
        );

        $filteredFcmIds = array_chunk($fcmIds, 1000);
        foreach ($filteredFcmIds as $fcmIdArray) {

            $fields = array(
                'registration_ids' => $fcmIdArray,
                'data' => $data
            );
            $headers = array(
                'Authorization: key=' . $secret_key,
                'Content-Type: application/json'
            );

            // print_r($fields); die;
            #Send Reponse To FireBase Server    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result, true);
            // print_r($result); die;
            // $re = $result['success'];
        }
    }
}


if (!function_exists('getFcmIds')) {
    function getFcmIds()
    {
        $user = auth()->user();
        return $user->fcm_id;
    }
}

if (!function_exists('array_sort')) {
    function array_sort($array, $on, $order = SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
}
