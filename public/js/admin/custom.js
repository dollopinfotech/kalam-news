function deleteData(id) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this data !",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $("form.deleteForm" + id).submit();
            // swal("Successfully Deleted !", {
            //     icon: "success",
            // });
        } else {
            // swal("Your imaginary file is safe!");
        }
    });
}

function EditFollowUs(social_id, platform, links, icon) {
    console.log(platform);
    $("#FollowEditModal").modal("show");
    $("#platform").val(platform);
    $("#social_id").val(social_id);
    $("#links").val(links);

    var iconId = document.getElementById("editpreview_icon");

    if (icon) {
        iconId.src = "{{ asset('images/icons/') }}" + icon;
        iconId.style.display = "block";
    }
    $("label.error").hide();
    $(".error").removeClass("error");
}
function getCities(stateId) {
    $.ajax({
        url: "getCitiesByState",
        type: "GET",
        dataType: "json",
        data: {
            state_id: stateId,
        },
        success: function (response) {
            console.log(response);
            var options = '<option value="">Select City</option>';
            $.each(response.cities, function (index, city) {
                options +=
                    '<option value="' +
                    city.city_id +
                    '">' +
                    city.city_name +
                    "</option>";
            });
            $("#city_id").html(options);
        },
        error: function (xhr) {
            console.log(xhr.responseText); // Handle error
        },
    });
}

function EditFollowUs(social_id, platform, links, icon) {
    // console.log(platform);
    $("#FollowEditModal").modal("show");
    $("#platform").val(platform);
    $("#social_id").val(social_id);
    $("#links").val(links);

    var iconId = document.getElementById("editpreview_icon");

    console.log(icon);
    if (icon) {
        iconId.src = icon;
        iconId.style.display = "block";
    }
    $("label.error").hide();
    $(".error").removeClass("error");
}

// Check Valid Referral Code
function checkReferralCode() {
    var referral_code = document.getElementById("referred_by").value;
    $.ajax({
        url: "checkReferralCode",
        type: "GET",
        dataType: "json",
        data: {
            referral_code: referral_code,
        },
        success: function (response) {
            document.getElementById("referral_code_success").innerText =
                "Referral code " + referral_code + " is valid";
            document.getElementById("referral_code_error").innerText = "";
        },
        error: function (xhr) {
            document.getElementById("referral_code_error").innerText =
                "Referral code " + referral_code + " is invalid";
            document.getElementById("referral_code_success").innerText = "";
        },
    });
}

function checkUniqueMobileNumber(mobile_number) {
    if (mobile_number.length !== 10 || !/^\d+$/.test(mobile_number)) {
        // If mobile number doesn't meet the criteria, show error message and return
        $("#mobile_number_message").text(
            "Mobile number must be exactly 10 digits and contain only numbers."
        );
        return;
    } else {
        $("#mobile_number_message").text("");
    }

    $.ajax({
        url: "checkUniqueMobileNumber",
        type: "GET",
        dataType: "json",
        data: {
            mobile_number: mobile_number,
        },
        success: function (response) {
            if (response.is_exists == 1) {
                $("#mobile_number_message").text(
                    "Mobile number already exists!"
                );
                $("#submit").prop("disabled", true);
            } else {
                $("#mobile_number_message").text("");
                $("#submit").prop("disabled", false);
            }
        },
    });
}

$(document).ready(function () {
    $("#whatsapp_number").on("change", function () {
        var whatsapp_number = $(this).val();
        if (whatsapp_number.length !== 10 || !/^\d+$/.test(whatsapp_number)) {
            // If mobile number doesn't meet the criteria, show error message and return
            $("#whatsapp_number_message").text(
                "Mobile number must be exactly 10 digits and contain only numbers."
            );
            return;
        }

        $.ajax({
            url: "checkUniqueWhatsAppNumber",
            type: "GET",
            dataType: "json",
            data: {
                whatsapp_number: whatsapp_number,
            },
            success: function (response) {
                $("#whatsapp_number_message").text("User already exists!");
                $("#submit").prop("disabled", true);
            },
        });
    });
});

function checkUniqueEmail(email) {
    $.ajax({
        url: "checkUniqueEmail",
        type: "GET",
        dataType: "json",
        data: {
            email: email,
        },
        success: function (response) {
            if (response.is_exists == 1) {
                $("#email_message").text("Email already exists!");
                $("#submit").prop("disabled", true);
            } else {
                $("#email_message").text("");
                $("#submit").prop("disabled", false);
            }
        },
    });
}

function advertisementRequest(id) {
    swal({
        title: "Are you sure?",
        text: "You won't be able to approve this!",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((willAccept) => {
        if (willAccept) {
            $("form.advertisementRequest" + id).submit();
            // swal("Successfully Deleted !", {
            //     icon: "success",
            // });
        } else {
            // swal("Your imaginary file is safe!");
        }
    });
}

function commissionRequest(id) {
    swal({
        title: "Are you sure?",
        text: "You won't be able to approve this!",
        icon: "warning",
        buttons: true,
        dangerMode: false,
    }).then((willAccept) => {
        if (willAccept) {
            $("form.commissionRequest" + id).submit();
            // swal("Successfully Deleted !", {
            //     icon: "success",
            // });
        } else {
            // swal("Your imaginary file is safe!");
        }
    });
}

function checkUniqueAdminEmail(email) {
    $.ajax({
        url: "checkUniqueAdminEmail",
        type: "GET",
        dataType: "json",
        data: {
            email: email,
        },
        success: function (response) {
            if (response.is_exists == 1) {
                $("#email_message").text("Email already exists!");
                $("#submit").prop("disabled", true);
            } else {
                $("#email_message").text("");
                $("#submit").prop("disabled", false);
            }
        },
    });
}

function checkUniqueAdminMobile(mobile) {
    $.ajax({
        url: "checkUniqueAdminMobile",
        type: "GET",
        dataType: "json",
        data: {
            mobile: mobile, // Pass 'mobile' instead of 'email'
        },
        success: function (response) {
            if (response.is_exists == 1) {
                $("#mobile_message").text("Mobile number already exists!");
                $("#submit").prop("disabled", true);
            } else {
                $("#mobile_message").text("");
                $("#submit").prop("disabled", false);
            }
        },
    });
}

function adharPanKyc(
    docs_id,
    aadhar_card_no,
    aadhar_card_photo_front,
    aadhar_card_photo_back,
    pan_card_no,
    pan_card_photo,
    is_aadhar_verified,
    is_pan_verified,
    user_kyc_photo
) {
    $("#adharPanKycModal").modal("show");
    document.getElementById("aadhar_card_no").innerText = aadhar_card_no;
    document.getElementById("pan_card_no").innerText = pan_card_no;
    $("#docs_id").val(docs_id);
    $("#is_aadhar_verified").val(is_aadhar_verified).select();
    $("#is_pan_verified").val(is_pan_verified).select();

    if (is_aadhar_verified == 1 || is_aadhar_verified == 2) {
        $('#is_aadhar_verified option[value="0"]').attr("disabled", "disabled");
        $('#is_aadhar_verified option[value="1"]').attr("disabled", "disabled");
        $('#is_aadhar_verified option[value="2"]').attr("disabled", "disabled");
    }

    if (is_pan_verified == 1 || is_pan_verified == 2) {
        $('#is_pan_verified option[value="0"]').attr("disabled", "disabled");
        $('#is_pan_verified option[value="1"]').attr("disabled", "disabled");
        $('#is_pan_verified option[value="2"]').attr("disabled", "disabled");
    }

    var aadharCardPhotoFront = document.getElementById(
        "aadhar_card_photo_front"
    );
    var aadharCardPhotoBack = document.getElementById("aadhar_card_photo_back");
    var panCardPhoto = document.getElementById("pan_card_photo");
    var userKycPhoto = document.getElementById("user_kyc_photo");

    // console.log(aadhar_card_photo_front);

    if (aadhar_card_photo_front) {
        aadharCardPhotoFront.src = aadhar_card_photo_front;
        aadharCardPhotoFront.style.display = "block";
    }

    if (aadhar_card_photo_back) {
        aadharCardPhotoBack.src = aadhar_card_photo_back;
        aadharCardPhotoBack.style.display = "block";
    }

    if (pan_card_photo) {
        panCardPhoto.src = pan_card_photo;
        panCardPhoto.style.display = "block";
    }

    if (user_kyc_photo) {
        userKycPhoto.src = user_kyc_photo;
        userKycPhoto.style.display = "block";
    }

    $("label.error").hide();
    $(".error").removeClass("error");
    $(".preview").hide();
    $(".cursor-overlay").hide();
}

function bankKyc(
    bank_id,
    customer_name,
    bank_name,
    account_no,
    ifsc_code,
    bank_branch,
    bank_passbook,
    is_bank_account_verify,
    account_type,
    primary_bank
) {
    $("#bankKycModal").modal("show");
    document.getElementById("customer_name").innerText = customer_name;
    document.getElementById("bank_name").innerText = bank_name;
    document.getElementById("account_no").innerText = account_no;
    document.getElementById("ifsc_code").innerText = ifsc_code;
    document.getElementById("bank_branch").innerText = bank_branch;
    document.getElementById("account_type").innerText = account_type;
    // document.getElementById("primary_bank").innerText = primary_bank;
    $("#bank_id").val(bank_id);
    $("#is_bank_account_verify").val(is_bank_account_verify).select();

    if (is_bank_account_verify == 1 || is_bank_account_verify == 2) {
        $('#is_bank_account_verify option[value="0"]').attr("disabled", "disabled");
        $('#is_bank_account_verify option[value="1"]').attr("disabled", "disabled");
        $('#is_bank_account_verify option[value="2"]').attr("disabled", "disabled");
    }

    var bankPassbook = document.getElementById(
        "bank_passbook"
    );

    if (bank_passbook) {
        bankPassbook.src = bank_passbook;
        bankPassbook.style.display = "block";
    }

    $("label.error").hide();
    $(".error").removeClass("error");
    $(".preview").hide();
    $(".cursor-overlay").hide();
}
