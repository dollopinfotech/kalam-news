// Add/Update Menu Form Validation
$("#addMenuForm ").validate({
    rules: {
        menu_name: {
            required: true,
        },
        menu_link: {
            required: true,
        },
        menu_icon: {
            required: true,
        },
        order_no: {
            required: true,
        },
    },
    messages: {
        menu_name: {
            required: "Please Enter Menu Name",
        },
        menu_link: {
            required: "Please Enter Menu Link",
        },
        menu_icon: {
            required: "Please Enter Menu Icon",
        },
        order_no: {
            required: "Please Enter Menu Order No",
        },
    },
});

// Add/Update Pages Form Validation
$("#addPagesForm").validate({
    rules: {
        page_name: {
            required: true,
        },
        page_url: {
            required: true,
        },
        page_order_no: {
            required: true,
        },
    },
    messages: {
        page_name: {
            required: "Please Enter Page Name",
        },
        page_url: {
            required: "Please Enter Page Link",
        },
        page_order_no: {
            required: "Please Enter Order Number",
        },
    },
});

// Add/Update Sub Menu Form Validation
$("#addSubMenuForm").validate({
    rules: {
        sub_menu_name: {
            required: true,
        },
        sub_menu_url: {
            required: true,
        },
        menu_order_no: {
            required: true,
        },
    },
    messages: {
        sub_menu_name: {
            required: "Please Enter Sub Menu Name",
        },
        sub_menu_url: {
            required: "Please Enter Sub Menu Link",
        },
        menu_order_no: {
            required: "Please Enter Order Number",
        },
    },
});

// Add/Update Faq Form Validation
$("#addFaqForm").validate({
    rules: {
        faq_title: {
            required: true,
        },
        faq_description: {
            required: true,
        },
    },
    messages: {
        faq_title: {
            required: "Please Enter Faq Title",
        },
        faq_description: {
            required: "Please Enter Faq Description",
        },
    },
});

// Add/Update Role Form Validation
$("#addRoleForm").validate({
    rules: {
        role_name: {
            required: true,
        },
        role_desc: {
            required: true,
        },
    },
    messages: {
        role_name: {
            required: "Please Enter Role Name",
        },
        role_desc: {
            required: "Please Enter Role Description",
        },
    },
});

// Add User Form Validation
$("#addUserForm").validate({
    rules: {
        initials: {
            required: true,
        },
        first_name: {
            required: true,
        },
        last_name: {
            required: true,
        },
        mobile_number: {
            required: true,
            digits: true,
            matches: "[0-9]+", // <-- no such method called "matches"!
            minlength: 10,
            regex: /^[+-]{1}[0-9]{1,3}\-[0-9]{10}$/,
            maxlength: 10,
        },
        email: {
            required: true,
            email: true,
            regex: /^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/,
        },
        referred_by: {
            required: true,
        },
        // node: {
        //     required: true,
        // },
        dob: {
            required: true,
        },
        address: {
            required: true,
        },
        pincode: {
            required: true,
        },
        profile: {
            required: true,
        },
        state_id: {
            required: true,
        },
        city_id: {
            required: true,
        },
    },
    messages: {
        initials: {
            required: "Please select initials",
        },
        first_name: {
            required: "Please enter first name",
        },
        last_name: {
            required: "Please enter last name",
        },
        mobile_number: {
            required: "Please enter mobile number",
        },
        email: {
            required: "Please enter email address",
            email: "Please enter a valid email address",
        },
        referred_by: {
            required: "Please Enter referral code",
        },
        node: {
            required: "Please enter node",
        },
        dob: {
            required: "Please Enter Dob",
        },
        address: {
            required: "Please Enter address",
        },
        pincode: {
            required: "Please Enter pincode",
        },
        profile: {
            required: "Please select profile picture",
        },
        state_id: {
            required: "Please select state.",
        },
        city_id: {
            required: "Please select city.",
        },
    },
});

// Admin Login Form Validation
$("#loginForm").validate({
    rules: {
        emailOrMobile: {
            required: true,
            email: true,
        },
        password: {
            required: true,
        },
    },
    messages: {
        emailOrMobile: {
            required: "Please Enter Email",
            email: "Please Enter Valid Email",
        },
        password: {
            required: "Please Enter Password",
        },
    },
});

//user validation form
$("#updateEmpPersonalInfo").validate({
    rules: {
        initials: {
            required: true,
        },
        first_name: {
            required: true,
        },
        last_name: {
            required: true,
        },

        dob: {
            required: true,
        },
    },
    messages: {
        initials: {
            required: "Please Enter Initials",
        },
        first_name: {
            required: "Please Enter First name",
        },
        last_name: {
            required: "Please Enter Last name",
        },
        dob: {
            required: "Please Enter Dob",
        },
    },
});

//Update User Contact Form Validation
$("#updateEmpContactDetailForm").validate({
    rules: {
        mobile_number: {
            required: true,
            digits: true,
            matches: "[0-9]+", // <-- no such method called "matches"!
            minlength: 10,
            regex: /^[+-]{1}[0-9]{1,3}\-[0-9]{10}$/,
            maxlength: 10,
        },
        whatsapp_number: {
            required: false,
            digits: true,
            matches: "[0-9]+", // <-- no such method called "matches"!
            minlength: 10,
            regex: /^[+-]{1}[0-9]{1,3}\-[0-9]{10}$/,
            maxlength: 10,
        },
        email: {
            required: true,
            email: true,
            regex: /^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i,
        },
        pincode: {
            required: true,
        },
        state_id: {
            required: true,
        },
        city_id: {
            required: true,
        },
        address: {
            required: true,
        },
    },
    messages: {
        mobile_number: {
            required: "Please Enter Mobile Number",
        },
        email: {
            required: "Please enter email address.",
            email: "Please enter a valid email address.",
        },
        pincode: {
            required: "Please enter pincode.",
        },
        state_id: {
            required: "Please select state.",
        },
        city_id: {
            required: "Please select city.",
        },
        address: {
            required: "Please enter address.",
        },
    },
});

//Update User Bank form validation
$("#updateEmpBankInfo").validate({
    rules: {
        customer_name: {
            required: true,
        },
        bank_name: {
            required: true,
        },
        account_no: {
            required: true,
            digits: true,
        },
        ifsc_code: {
            required: true,
            ifsc: true,
        },
        bank_branch: {
            required: true,
        },
    },
    messages: {
        customer_name: {
            required: "Please Enter customer name",
        },
        bank_name: {
            required: "Please Enter bank name",
        },
        account_no: {
            required: "Please Enter account no",
        },
        ifsc_code: {
            required: "Please Enter ifsc code",
        },
        bank_branch: {
            required: "Please Enter bank branch",
        },
    },
});

//Update User Documents form validation
$("#updateEmpDocument").validate({
    rules: {
        aadhar_card_no: {
            required: true,
            digits: true,
            minlength: 12,
            maxlength: 12,
        },
        aadhar_card_photo_front: {
            required: true,
        },
        aadhar_card_photo_back: {
            required: true,
        },
        pan_card_no: {
            required: true,
            alphanumeric: true,
            minlength: 10,
            maxlength: 10,
        },
        pan_card_photo: {
            required: true,
        },
    },
    messages: {
        aadhar_card_no: {
            required: "Please enter Aadhar card number",
            digits: "Please enter only digits",
            minlength: "Aadhar card number must be 12 digits",
            maxlength: "Aadhar card number must be 12 digits",
        },
        aadhar_card_photo_front: {
            required: "Please upload front photo of Aadhar card",
        },
        aadhar_card_photo_back: {
            required: "Please upload back photo of Aadhar card",
        },
        pan_card_no: {
            required: "Please enter PAN card number",
            alphanumeric: "Please enter alphanumeric characters only",
            minlength: "PAN card number must be 10 characters",
            maxlength: "PAN card number must be 10 characters",
        },
        pan_card_photo: {
            required: "Please upload PAN card photo",
        },
    },
});

// Add Package validation form
$("#package-form").validate({
    rules: {
        package_name: {
            required: true,
        },
        package_price: {
            required: true,
        },
        package_image: {
            required: true,
        },
        package_desc: {
            required: true,
        },
        max_commission: {
            required: true,
        },
        max_advertisement: {
            required: true,
        },
    },
    messages: {
        package_name: {
            required: "Please Enter Package Name",
        },
        package_price: {
            required: "Please Enter Package price ",
        },
        package_image: {
            required: "Please Select Package Image",
        },
        max_advertisement: {
            required: "Please Select Package Advertisement",
        },
        max_commission: {
            required: "Please Enter Max Commission",
        },
        package_desc: {
            required: "Please Enter Package Description",
        },
    },
});

// Edit Package validation form
$("#editPackage-form").validate({
    rules: {
        package_name: {
            required: true,
        },
        package_price: {
            required: true,
        },
        package_desc: {
            required: true,
        },
        max_commission: {
            required: true,
        },
        max_advertisement: {
            required: true,
        },
    },
    messages: {
        package_name: {
            required: "Please Enter Package Name",
        },
        package_price: {
            required: "Please Enter Package price ",
        },
        max_advertisement: {
            required: "Please Select Package Advertisement",
        },
        max_commission: {
            required: "Please Enter Max Commission",
        },
        package_desc: {
            required: "Please Enter Package Description",
        },
    },
});

$(document).ready(function () {
    $("#followAddForm").validate({
        rules: {
            platform: {
                required: true,
            },
            links: {
                required: true,
            },
            icon: {
                required: true,
            },
        },
        messages: {
            platform: {
                required: "Please enter platform",
            },
            links: {
                required: "Please enter links ",
            },
            icon: {
                required: "Please enter icone",
            },
        },
    });
});

$(document).ready(function () {
    $("#followEditForm").validate({
        rules: {
            platform: {
                required: true,
            },
            links: {
                required: true,
            },
        },
        messages: {
            platform: {
                required: "Please enter platform",
            },
            links: {
                required: "Please enter links ",
            }
        },
    });
});


$(document).ready(function () {
    $("#editAdminForm").validate({
        rules: {
            admin_name: {
                required: true,
            },
            admin_email: {
                required: true,
                email: true,
            },
            admin_mobile: {
                required: true,
            },
            admin_address: {
                required: true,
            },
            password: {
                required: true,
            },
        },
        messages: {
            admin_name: {
                required: "Please enter admin name",
            },
            admin_email: {
                required: "Please enter admin email",
                email: "Please enter a valid email address",
            },
            admin_mobile: {
                required: "Please enter admin mobile",
            },
            admin_address: {
                required: "Please enter admin address",
            },
            password: {
                required: "Please enter a password",
            },
        },
    });
});
