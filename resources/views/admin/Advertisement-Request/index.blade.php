@extends('layouts.app')
@section('content')
    <div id="top" class="sa-app__body">
        <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
            <div class="container">
                <div class="py-5">
                    <div class="row g-4 align-items-center">
                        <div class="col">
                            <nav class="mb-2" aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-sa-simple">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Advertisement</li>
                                </ol>
                            </nav>
                            <h1 class="h3 m-0">Advertisement</h1>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="p-4">
                        <input type="text" placeholder="Start typing to search for users"
                            class="form-control form-control--search mx-auto" id="table-search" />
                    </div>
                    <div class="sa-divider"></div>
                    <table class="sa-datatables-init users-data-table nowrap" data-sa-search-input="#table-search">
                        <thead>
                            <tr>
                                <th>Sr</th>
                                <th>Request Type</th>
                                <th>User Name</th>
                                <th>User Number</th>
                                <th>User Email</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($manage_request as $manage_requests)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $manage_requests->req_type }}</td>
                                    <td>{{ $manage_requests->user_name }}</td>
                                    <td>{{ $manage_requests->user_number }}</td>
                                    <td>{{ $manage_requests->user_email }}</td>
                                    <td>{{ $manage_requests->title }}</td>
                                    <td>{{ $manage_requests->description }}</td>
                                    <td>
                                        @if ($manage_requests->status == 1)
                                            <button type="button" disabled class="btn btn-success btn-sm fs-12 fw-500">Accept</button>
                                        @elseif($manage_requests->status == 0)
                                            <button type="button" disabled class="btn btn-warning btn-sm fs-12 fw-500">Pending</button>
                                        @elseif($manage_requests->status == 2)
                                            <button type="button" disabled class="btn btn-danger btn-sm fs-12 fw-500">Reject</button>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($manage_requests->status == 1)
                                            <button type="button" disabled class="btn btn-success btn-sm">Approved</button>
                                        @else
                                            <form class="acceptRequest<?php echo $manage_requests->req_id; ?>"
                                                action="{{ route('admin.requestApproved', $manage_requests->req_id) }}"
                                                method="post">
                                                @csrf
                                                @method('POST')
                                                <input type="hidden" name="req_id" id="req_id"
                                                    value="{{ $manage_requests->req_id }}">
                                                <button class="btn btn-success btn-sm"
                                                    onclick="acceptRequest('<?php echo $manage_requests->req_id; ?>');"
                                                    type="button">Approve</button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
