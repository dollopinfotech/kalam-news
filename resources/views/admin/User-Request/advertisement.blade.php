@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Advertisement Request</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Advertisement Request</h1>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for users" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init users-data-table nowrap" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Name</th>
                            <th>Mobile No</th>
                            <th>Email</th>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($advertisementData as $advertisement)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $advertisement->first_name . '' .$advertisement->last_name ?? ''}}</td>
                            <td>{{ $advertisement->user_number ?? ''}}</td>
                            <td>{{ $advertisement->user_email ?? ''}}</td>
                            <td>{{ $advertisement->title ?? ''}}</td>
                            <td><a href="javascript:void(0);" title="View Detail" onclick="viewDetails('{{ $advertisement->title }}','{{ urlencode($advertisement->description) }}','{{ $advertisement->adv_document }}')"><i class="fa fa-eye"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="viewDetailsModal" tabindex="-1" aria-labelledby="exampleModalNlLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalNlLabel">Advertisement Request Details</h5>
            </div>
            <div class="modal-body">
                <div class="col-md-12 m-2">
                    <div class="text-center">
                        <strong><span id="title"></span></strong>
                        <span id="preview_adv_document"></span>
                    </div>
                    <div class="mt-4">
                        <span class="h6 fw-normal text-justify" id="description"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<script>
    function viewDetails(title, description, adv_document) {
        $("#viewDetailsModal").modal("show");
        var inputString = description;
        var splitArray = inputString.split("+");
        var joinedString = splitArray.join(" ");
        $("#description").html(decodeURIComponent(joinedString));
        $("#title").html(title);
        $("#preview_adv_document").empty();
        if (adv_document) {
            var fileExtension = adv_document.split('.').pop().toLowerCase();
            var previewElement = document.getElementById("preview_adv_document");
            if (fileExtension === 'jpg' || fileExtension === 'jpeg' || fileExtension === 'png') {
                const img = document.createElement("img");
                img.src = adv_document;
                img.alt = "document";
                img.classList.add("mt-4");
                img.style.width = "100%";
                previewElement.appendChild(img);
            } else if (fileExtension === 'mp4' || fileExtension === 'webm' || fileExtension === 'ogg') {
                const video = document.createElement("video");
                video.width = 400;
                video.controls = true;
                const source = document.createElement("source");
                source.src = adv_document;
                source.type = "video/" + fileExtension;
                video.appendChild(source);
                previewElement.appendChild(video);
            } else if (fileExtension === 'pdf') {
                const iframe = document.createElement("iframe");
                iframe.src = adv_document;
                iframe.width = "100%";
                iframe.height = "500px";
                previewElement.appendChild(iframe);
            } else if (fileExtension === 'doc' || fileExtension === 'docx') {
                const downloadLink = document.createElement("a");
                downloadLink.href = adv_document;
                downloadLink.textContent = "Download Document";
                previewElement.appendChild(downloadLink);
            } else {
                const unsupportedMessage = document.createElement("p");
                unsupportedMessage.textContent = "Unsupported file format";
                previewElement.appendChild(unsupportedMessage);
            }
        }
        $("label.error").hide();
        $(".error").removeClass("error");
    }
</script>
@endsection