@extends('layouts.app')
@section('content')
    <div id="top" class="sa-app__body">
        <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
            <div class="container">
                <div class="py-5">
                    <div class="row g-4 align-items-center">
                        <div class="col">
                            <nav class="mb-2" aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-sa-simple">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Commission Agents</li>
                                </ol>
                            </nav>
                            <h1 class="h3 m-0">Commission Agents</h1>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="p-4">
                        <input type="text" placeholder="Start typing to search for users"
                            class="form-control form-control--search mx-auto" id="table-search" />
                    </div>
                    <div class="sa-divider"></div>
                    <table class="sa-datatables-init users-data-table nowrap" data-sa-search-input="#table-search">
                        <thead>
                            <tr>
                                <th>Sr</th>
                                <th>User Name</th>
                                <th>User Number</th>
                                <th>User Email</th>
                                <th>status</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($advertisementData as $commissionAgent)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $commissionAgent->first_name . ' ' . $commissionAgent->last_name ?? '' }}</td>
                                    <td>{{ $commissionAgent->user_number ?? '' }}</td>
                                    <td>{{ $commissionAgent->user_email ?? '' }}</td>
                                    <td>
                                        @if ($commissionAgent->status == 1)
                                            <button type="button" disabled
                                                class="btn btn-success btn-sm fs-12 fw-500">Approved</button>
                                        @elseif($commissionAgent->status == 0)
                                            <button type="button" disabled
                                                class="btn btn-warning btn-sm fs-12 fw-500">Pending</button>
                                        @elseif($commissionAgent->status == 2)
                                            <button type="button" disabled
                                                class="btn btn-danger btn-sm fs-12 fw-500">Rejected</button>
                                        @endif
                                    </td>
                                    <td><button type="button" id="approveBtn" class="btn btn-primary"
                                            data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="showDetails('{{ $commissionAgent->first_name }}','{{ $commissionAgent->user_number }}','{{ $commissionAgent->user_email }}')">Approve</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="{{ route('admin.approveCA') }}" id="approveCAForm" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Approve Commission Agent</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-auto">
                                <label for="refferal_code" class="form-label">Referral Code<span style="color: red;">
                                        *</span></label>
                                <input type="text" class="refferal_code form-control" name="referral_code"
                                    id="refferal_code">
                                <input type="hidden" name="user_number" id="user_number">
                                <input type="hidden" name="user_email" id="user_email">
                                <input type="hidden" name="first_name" id="first_name">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" value="submit" id="submit" class="btn btn-primary">Approve</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/validation/jquery.validate.min.js"></script>
    <script src="../vendor/select2/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#approveCAForm").validate({
                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.hasClass('select2-hidden-accessible')) {
                        error.insertAfter(element.next('span'));
                        element.next('span').addClass('error').removeClass('valid');
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    refferal_code: {
                        required: true,
                    },
                },
                messages: {
                    refferal_code: {
                        required: "Please Enter Refferal Code",
                    },
                },
            });
        });

        function showDetails(first_name, user_number, user_email){
            document.getElementById("first_name").value = first_name;
            document.getElementById("user_number").value = user_number;
            document.getElementById("user_email").value = user_email;
        }
    </script>
@endsection
