@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Withdraw Request</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Withdraw Request</h1>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for withdraw request" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init users-data-table nowrap" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Customer Name</th>
                            <th>Bank Name</th>
                            <th>Account No</th>
                            <th>Ifsc Code</th>
                            <th>Bank Branch</th>
                            <th>Amount</th>
                            <th>TDS Charge</th>
                            <th>Service Charge</th>
                            <th>Withdrawable Amount</th>
                            <th>Account Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($withdrawRequestArr as $withdrawRequest)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $withdrawRequest->customer_name ?? ''}}</td>
                            <td>{{ $withdrawRequest->bank_name ?? ''}}</td>
                            <td>{{ $withdrawRequest->account_no ?? ''}}</td>
                            <td>{{ $withdrawRequest->ifsc_code ?? ''}}</td>
                            <td>{{ $withdrawRequest->bank_branch ?? ''}}</td>
                            <td>{{ $withdrawRequest->amount ?? ''}}</td>
                            <td>{{ $withdrawRequest->tds_charge_amount ?? ''}}</td>
                            <td>{{ $withdrawRequest->service_charge_amount ?? ''}}</td>
                            <td>{{ $withdrawRequest->withdrawable_amount ?? ''}}</td>
                            <td>{{ $withdrawRequest->account_type ?? ''}}</td>
                            <td>
                                <div class="d-flex">
                                    <button class="btn btn-success btn-sm me-1" onclick="approveWithdrawRequest('{{ $withdrawRequest->withdraw_request_id }}')">Approve</button>
                                    <form class="deleteForm<?php echo $withdrawRequest->withdraw_request_id; ?>" action="{{ route('admin.deleteWithdrawRequest', $withdrawRequest->withdraw_request_id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $withdrawRequest->withdraw_request_id ?>');" type="button">Reject</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="approveWithdrawRequestModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="{{ route('admin.approveWithdrawRequest') }}" id="approveWithdrawRequestForm" method="post" enctype="multipart/form-data" onsubmit="return ValidateTransactionId()">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Approve Withdraw Request</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-auto">
                            <label for="transaction_id" class="form-label">Transaction Id<span style="color: red;">
                                    *</span></label>
                            <input type="text" class="transaction_id form-control" name="transaction_id" id="transaction_id">
                            <span id="transaction_id_error" class="error_message" style="color: red;"></span>
                            <input type="hidden" name="withdraw_request_id" id="withdraw_request_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" value="submit" id="submit" class="btn btn-primary">Approve</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/validation/jquery.validate.min.js"></script>
<script src="../vendor/select2/js/select2.min.js"></script>
<script>
    function ValidateTransactionId() {
        var transactionId = $('#transaction_id').val();

        var isValid = true;

        // Perform validation for each field
        if (transactionId === "") {
            document.getElementById("transaction_id_error").innerText = "Please Enter Transaction Id";
            isValid = false;
        }

        return isValid;
    }

    function approveWithdrawRequest(withdraw_request_id) {
        $("#approveWithdrawRequestModal").modal("show");
        $("#withdraw_request_id").val(withdraw_request_id);
        $("label.error").hide();
        $(".error").removeClass("error");
    }
</script>
@endsection