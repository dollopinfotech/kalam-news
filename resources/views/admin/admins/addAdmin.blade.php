@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css">
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <style>
        .error-message {
            color: red;
        }
    </style>
    <div id="top" class="sa-app__body">
        <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
            <div class="container">
                <div class="py-5">
                    <div class="row g-4 align-items-center">
                        <div class="col">
                            <nav class="mb-2" aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-sa-simple">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.adminList') }}">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Admin</li>
                                </ol>
                            </nav>
                            <h1 class="h3 m-0">Add Admin</h1>
                        </div>
                        <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <div class="sa-entity-layout"
                    data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                    <div class="sa-entity-layout__body">
                        <div class="sa-entity-layout__main">
                            {{-- <form id="" method="POST" action="{{ route('admin.storeAdmin') }}" enctype="multipart/form-data" onsubmit="return validateForm()">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" /> --}}
                                <form id="addAdminForm" method="POST" action="{{ route('admin.storeAdmin') }}" enctype="multipart/form-data">
                                    @csrf
                                <div class="card">
                                    <div class="card-body p-5">
                                        <div class="row g-6 mb-4">
                                            <div class="col-md-4">
                                                <label for="admin_name" class="form-label">Admin Name <span
                                                        style="color:red">*</span></label>
                                                <input type="text" class="form-control" name="admin_name" id="admin_name"
                                                    value="" />
                                                <div id="admin_name_error" style="color:red"></div>
                                            </div>

                                            <div class="col-md-4">
                                                <label for="admin_email" class="form-label">Email <span
                                                        style="color:red">*</span></label>
                                                <input type="email" class="form-control" name="admin_email"
                                                    id="admin_email" onchange="checkUniqueAdminEmail(this.value)"/>
                                                    <div id="email_message" style="color:red;"></div>
                                                <div id="admin_email_error" style="color:red"></div>

                                            </div>
                                            <div class="col-md-4">
                                                <label for="admin_mobile" class="form-label">Mobile <span style="color:red">*</span></label>
                                                <div class="input-group">
                                                    <select class="form-select form-select-sm" id="country_code" name="country_code" style="flex: 1;">
                                                        <option value="+1">USA (+1)</option>
                                                        <option value="+44">UK (+44)</option>
                                                        <option value="+91">India (+91)</option>
                                                    </select>
                                                    <input type="text" class="form-control" id="admin_mobile" name="admin_mobile" placeholder="Enter your phone number" style="flex: 2;" onchange="checkUniqueAdminMobile(this.value)"/>
                                                </div>
                                                <div id="mobile_message" style="color:red; margin-top: 5px;"></div>
                                                <div id="admin_mobile_error" class="error-message"></div>
                                            </div>

                                            <div class="col-md-4">
                                                <label for="role_id" class="form-label">Role <span
                                                        style="color:red">*</span></label>
                                                <select class="form-select" id="role_id" name="role_id">
                                                    <option value="" disabled selected>Select a role</option>
                                                    @foreach ($roles as $role)
                                                        <option value="{{ $role->role_id }}">{{ $role->role_name }}</option>
                                                    @endforeach
                                                </select>
                                                <div id="role_id_error" style="color:red"></div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="password" class="form-label">Password <span
                                                        style="color:red">*</span></label>
                                                <div class="input-group">
                                                    <input type="password" class="form-control" name="password"
                                                        id="password" aria-describedby="password-toggle">
                                                    <button class="btn btn-outline-secondary" type="button"
                                                        id="password-toggle" aria-label="Toggle password visibility"
                                                        style="color: #000;" onclick="togglePasswordVisibility()">
                                                        <i id="password-icon" class="fas fa-eye"></i>
                                                    </button>
                                                </div>
                                                <div id="password_error" class="error-message"></div>
                                            </div>

                                            <div class="col-md-4">
                                                <label for="admin_profile" class="col-form-label">Image<span
                                                        style="color:red">*</span></label>
                                                <input type="file" class="form-control" id="admin_profile"
                                                    name="admin_profile" accept="image/*">
                                                <img id="preview_admin_profile"
                                                    src="@if (isset($admin->admin_profile)) {{ $admin->admin_profile }} @endif"
                                                    alt="Icon Preview" class="mt-4" style="width: 25%; display: none;">
                                                <div id="admin_profile_error" style="color:red"></div>
                                            </div>


                                            <div class="col-md-4">
                                                <label for="admin_address" class="form-label"> Address</label>
                                                <textarea name="admin_address" id="admin_address" class="form-control" cols="30" rows="5"></textarea>
                                                <div id="admin_address_error" style="color:red"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center mb-4">
                                        <button type="submit" id="submit" value="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    document.addEventListener("DOMContentLoaded", function() {
        const admin_profile = document.getElementById('admin_profile');
        const preview_admin_profile = document.getElementById('preview_admin_profile');

        admin_profile.onchange = evt => {
            const file = admin_profile.files[0];
            if (file) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    preview_admin_profile.src = e.target.result;
                    preview_admin_profile.style.display = 'block';
                }
                reader.readAsDataURL(file);
            }
        };
    });
</script>
<script>
    function togglePasswordVisibility() {
        var passwordInput = document.getElementById("password");
        var passwordIcon = document.getElementById("password-icon");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            passwordIcon.classList.remove("fa-eye");
            passwordIcon.classList.add("fa-eye-slash");
        } else {
            passwordInput.type = "password";
            passwordIcon.classList.remove("fa-eye-slash");
            passwordIcon.classList.add("fa-eye");
        }
    }
</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

    $(document).ready(function() {
        $('#addAdminForm').submit(function(e) {
            e.preventDefault();
            if (validateForm()) {
                $(this).unbind('submit').submit();
            }
        });
    });
    function validateForm() {
        var isValid = true;
        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        var adminName = $('#admin_name').val().trim();
        if (adminName === '') {
            $('#admin_name_error').text('Please Enter Admin Name');
            isValid = false;
        } else if (!isValidName(adminName)) {
            $('#admin_name_error').text('Please Enter a valid Admin Name (only text characters)');
            isValid = false;
        }
        var adminEmail = $('#admin_email').val().trim();
        if (adminEmail === '') {
            $('#admin_email_error').text(' Please Enter Email ');
            isValid = false;
        } else if (!emailRegex.test(adminEmail)) {
            $('#admin_email_error').text(' Please Enter a valid Email Address ');
            isValid = false;
        }

        var adminMobile = $('#admin_mobile').val().trim();
        if (adminMobile === '') {
            $('#admin_mobile_error').text('Please Enter Mobile');
            isValid = false;
        } else if (!isValidMobile(adminMobile)) {
            $('#admin_mobile_error').text('Please Enter a valid Mobile Number');
            isValid = false;
        }

        var password = $('#password').val().trim();
        if (password === '') {
            $('#password_error').text(' Please Enter Password ');
            isValid = false;
        } else if (!/^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W])/.test(password)) {
            $('#password_error').text(' Password must be at least 8 characters long and contain at least one uppercase letter, one lowercase letter, one number, and one special character.');
            isValid = false;
        }

        if ($('#admin_profile').get(0).files.length === 0) {
            $('#admin_profile_error').text(' Please Enter Profile Image ');
            isValid = false;
        }
        var roleId = $('#role_id').val();
        if (!roleId) {
            $('#role_id_error').text('Please select a role');
            isValid = false;
        }


        return isValid;
    }

    function isValidName(name) {
        var nameRegex = /^[a-zA-Z\s]+$/;
        return nameRegex.test(name);
    }
    function isValidMobile(mobile) {
        var mobileRegex = /^\d{10}$/;
        return mobileRegex.test(mobile);
    }
</script>
