@extends('layouts.app')
@section('content')
    <div id="top" class="sa-app__body">
        <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
            <div class="container">
                <div class="py-5">
                    <div class="row g-4 align-items-center">
                        <div class="col">
                            <nav class="mb-2" aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-sa-simple">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.adminList') }}">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit Admin</li>
                                </ol>
                            </nav>
                            <h1 class="h3 m-0">Edit Admin</h1>
                        </div>
                        <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <div class="sa-entity-layout"
                    data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                    <div class="sa-entity-layout__body">
                        <div class="sa-entity-layout__main">
                            <form method="POST" action="{{ route('admin.updateAdmin', ['admin_id' => $admin->admin_id]) }}"
                                id="editAdminForm" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="card">
                                    <div class="card-body p-5">
                                        <div class="row g-6 mb-4">
                                            <div class="col-md-4">
                                                <label for="admin_name" class="form-label">Admin Name <span
                                                        style="color:red">*</span></label>
                                                <input type="text" class="form-control" name="admin_name" id="admin_name"
                                                    value="{{ $admin->admin_name }}" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="admin_email" class="form-label">Admin Email <span
                                                        style="color:red">*</span></label>
                                                <input type="email" class="form-control" name="admin_email"
                                                    id="admin_email" value="{{ $admin->admin_email }}" />
                                            </div>

                                            <div class="col-md-4">
                                                <label for="admin_mobile" class="form-label">Mobile <span
                                                        style="color:red">*</span></label>
                                                <div class="input-group">
                                                    <select class="form-select form-select-sm" id="country_code"
                                                        name="country_code">
                                                        <option value="+1"
                                                            {{ $admin->country_code == '+1' ? 'selected' : '' }}>USA (+1)
                                                        </option>
                                                        <option value="+44"
                                                            {{ $admin->country_code == '+44' ? 'selected' : '' }}>UK (+44)
                                                        </option>
                                                        <option value="+91"
                                                            {{ $admin->country_code == '+91' ? 'selected' : '' }}>India
                                                            (+91)</option>
                                                    </select>
                                                    <input type="text" class="form-control" id="admin_mobile"
                                                        name="admin_mobile" value="{{ $admin->admin_mobile }}"
                                                        placeholder="Enter your phone number" style="flex: 3;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row g-6 mb-4">
                                            <div class="col-md-4">
                                                <label for="admin_profile">Admin Image<span style="color: red;">*</span></label>
                                                <input type="file" class="form-control" id="admin_profile" name="admin_profile" accept="image/*" value="{{ $admin->admin_profile ?? '' }}">
                                                <img id="preview_editadmin_profile" src="{{ asset($admin->admin_profile) }}" alt="Current Image" class="mt-4" style="width: 25%" />
                                            </div>

                                            <div class="col-md-4">
                                                <label for="role_id" class="form-label">Role <span
                                                        style="color:red">*</span></label>
                                                <select class="form-select" id="role_id" name="role_id" required>
                                                    <option value="" disabled selected>Select a role</option>
                                                    @foreach ($roles as $role)
                                                        <option value="{{ $role->role_id }}"
                                                            {{ $admin->role_id == $role->role_id ? 'selected' : '' }}>
                                                            {{ $role->role_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="admin_address" class="form-label">Address</label>
                                                <textarea name="admin_address" id="admin_address" class="form-control" cols="30" rows="5">{{ $admin->admin_address }}</textarea>
                                                <div id="admin_address_error" style="color:red"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 text-center mb-4">
                                        <button type="submit" value="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    document.addEventListener("DOMContentLoaded", function() {
        const admin_profile = document.getElementById('admin_profile');
        const preview_editadmin_profile = document.getElementById('preview_editadmin_profile');

        admin_profile.onchange = evt => {
            const [file] = admin_profile.files;
            if (file) {
                preview_editadmin_profile.src = URL.createObjectURL(file);
            }
        };
    });
</script>

