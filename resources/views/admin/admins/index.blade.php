@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Admin</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Admin</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ route('admin.addAdmin') }}" class="btn btn-primary">New Admin</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for Admin" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init menu-data-table" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Role</th>
                            <th>Admin Profile</th>
                            <th>Admin Name</th>
                            <th>Admin Email</th>
                            <th>Admin Mobile</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($admin as $adminData)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                @php
                                    $role = \App\Models\Roles::find($adminData->role_id);
                                @endphp
                                {{ $role ? $role->role_name : 'Not Assigned' }}
                            </td>
                            <td>
                                <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
                                    <img src="{{ file_url($adminData->admin_profile) }}"
                                    width="40" height="40" alt="">
                                </div>
                            </td>
                            <td>{{ $adminData->admin_name}}</td>
                            <td>{{ $adminData->admin_email}}</td>
                            <td>{{ $adminData->admin_mobile}}</td>
                            <td>
                                <label class="form-check form-switch">
                                    <input type="checkbox" class="form-check-input is-valid" name="is_active" {{ $adminData->is_active ? 'checked' : '' }} onchange="toggleAdminStatus(event, {{ $adminData->admin_id }})">
                                </label>
                            </td>
                            <td>
                                <div class="d-flex">

                                    <form action="{{ route('admin.editAdmin', $adminData->admin_id) }}" method="GET">
                                        <button class="btn btn-primary btn-sm me-1" type="submit">Edit</button>
                                    </form>
                                    <form class="deleteForm<?php echo $adminData->admin_id; ?>" action="{{ route('admin.deleteAdmin', $adminData->admin_id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $adminData->admin_id ?>');" type="button">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
    function toggleAdminStatus(event, admin_id) {
        var is_activeInput = event.target;
        var is_active = is_activeInput.checked;
        $.ajax({
            url: '/statusAdmin/' + admin_id,
            type: 'POST',
            data: {
                is_active: is_active ? '1' : '0', // Send '1' if checked, '0' if not
                _token: '{{ csrf_token() }}'
            },

            success: function(response) {
            }
        });
        document.location.reload(true);
    }
</script>
