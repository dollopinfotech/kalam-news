<!DOCTYPE html>
<html lang="en" dir="ltr" data-scompiler-id="0">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <title>Stroyka Admin - eCommerce Dashboard Template</title>
    <!-- icon -->
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <!-- fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" />
    <!-- css -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.ltr.css" />
    <link rel="stylesheet" href="vendor/highlight.js/styles/github.css" />
    <link rel="stylesheet" href="vendor/simplebar/simplebar.min.css" />
    <link rel="stylesheet" href="vendor/quill/quill.snow.css" />
    <link rel="stylesheet" href="vendor/air-datepicker/css/datepicker.min.css" />
    <link rel="stylesheet" href="vendor/select2/css/select2.min.css" />
    <link rel="stylesheet" href="vendor/datatables/css/dataTables.bootstrap5.min.css" />
    <link rel="stylesheet" href="vendor/nouislider/nouislider.min.css" />
    <link rel="stylesheet" href="vendor/fullcalendar/main.min.css" />
    <link rel="stylesheet" href="css/style.css" />
</head>

<body>
    <form id="" action="{{ route('admin.adminResetPassword') }}" method="post" onsubmit="return validateForm()">
        @csrf
        @method('post')
        <div class="min-h-100 p-0 p-sm-6 d-flex align-items-stretch">
            <div class="card w-25x flex-grow-1 flex-sm-grow-0 m-sm-auto">
                <div class="card-body p-sm-5 m-sm-3 flex-grow-0">
                    <h1 class="mb-0 fs-3">Reset Password</h1>
                    <div class="fs-exact-14 text-muted mt-2 pt-1 mb-5 pb-2">Please enter your new password.</div>
                    <div class="mb-4">
                        <label class="form-label">Email</label>
                        <input type="text" name="admin_email" id="admin_email" value="{{ $adminData->admin_email }}" class="form-control form-control-lg" readonly />
                    </div>
                    <div class="mb-4">
                        <label class="form-label">Password</label>
                        <input type="password" name="password" id="password" class="form-control form-control-lg" onchange="checkPassword()" />
                        <span id="password_error" class="error_message" style="color: red;"></span>
                    </div>
                    <div class="mb-4 pb-2">
                        <label class="form-label">Confirm password</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control form-control-lg" onchange="checkConfirmPassword()" />
                        <span id="password_confirmation_error" class="error_message" style="color: red;"></span>
                    </div>
                    <div class="pt-3"><button type="submit" class="btn btn-primary btn-lg w-100">Reset</button></div>
                    <div class="form-group mb-0 mt-4 pt-2 text-center text-muted">
                        Remember your password?
                        <a href="{{route('admin.auth.login')}}">Sign in</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- scripts -->
        <script>
            function checkPassword() {
                var pattern = /^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W])/;

                var checkval = pattern.test($("#password").val());

                if (!checkval) {
                    document.getElementById("password_error").innerText = "Must contain at least one number and one uppercase and lowercase letter and at least one special symbol, and at least 8 or more characters";
                } else {
                    document.getElementById("password_error").innerText = "";
                }
            }

            function checkConfirmPassword() {
                var pattern = /^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W])/;
                var password = document.getElementById("password").value;
                var password_confirmation = document.getElementById("password_confirmation").value;
                var checkval = pattern.test($("#password_confirmation").val());

                if (!checkval) {
                    document.getElementById("password_confirmation_error").innerText = "Must contain at least one number and one uppercase and lowercase letter and at least one special symbol, and at least 8 or more characters";
                } else if (password !== password_confirmation) {
                    document.getElementById("password_confirmation_error").innerText = "Confirm password does not match";
                } else {
                    document.getElementById("password_confirmation_error").innerText = "";
                }
            }

            function validateForm() {
                var pattern = /^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W])/;
                var password = document.getElementById("password").value;
                var password_confirmation = document.getElementById("password_confirmation").value;

                var isValid = true;

                if (password === "") {
                    document.getElementById("password_error").innerText = "Please enter password";
                    isValid = false;
                } else if (!pattern.test(password)) {
                    document.getElementById("password_error").innerText = "Must contain at least one number and one uppercase and lowercase letter and at least one special symbol, and at least 8 or more characters";
                    isValid = false;
                } else if (password_confirmation === "") {
                    document.getElementById("password_confirmation_error").innerText = "Please enter confirm password";
                    isValid = false;
                } else if (!pattern.test(password_confirmation)) {
                    document.getElementById("password_confirmation_error").innerText = "Must contain at least one number and one uppercase and lowercase letter and at least one special symbol, and at least 8 or more characters";
                    isValid = false;
                } else if (password !== password_confirmation) {
                    document.getElementById("password_confirmation_error").innerText = "Confirm password does not match";
                    isValid = false;
                } else {
                    document.getElementById("password_error").innerText = "";
                    document.getElementById("password_confirmation_error").innerText = "";
                }

                return isValid;
            }
        </script>
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/feather-icons/feather.min.js"></script>
        <script src="vendor/simplebar/simplebar.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/highlight.js/highlight.pack.js"></script>
        <script src="vendor/quill/quill.min.js"></script>
        <script src="vendor/air-datepicker/js/datepicker.min.js"></script>
        <script src="vendor/air-datepicker/js/i18n/datepicker.en.js"></script>
        <script src="vendor/select2/js/select2.min.js"></script>
        <script src="vendor/fontawesome/js/all.min.js" data-auto-replace-svg="" async=""></script>
        <script src="vendor/chart.js/chart.min.js"></script>
        <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="vendor/datatables/js/dataTables.bootstrap5.min.js"></script>
        <script src="vendor/nouislider/nouislider.min.js"></script>
        <script src="vendor/fullcalendar/main.min.js"></script>
        <script src="js/stroyka.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/calendar.js"></script>
        <script src="js/demo.js"></script>
        <script src="js/demo-chart-js.js"></script>
</body>

</html>