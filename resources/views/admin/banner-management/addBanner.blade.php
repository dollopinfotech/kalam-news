@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.getBanner') }}">Banner</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add Banner</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Add Banner</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ route('admin.getBanner') }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                <div class="sa-entity-layout__body">
                    <div class="sa-entity-layout__main">
                        <form method="POST" action="{{ route('admin.storeBanner') }}" id="addBannerForm" enctype="multipart/form-data" onsubmit="return validateBannerForm()">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="card">
                                <div class="card-body p-5">
                                    <div class="mb-4">
                                        <div class="">
                                            <label class="form-label" for="banner_image">Banner</label>
                                            <input type="file" class="form-control" id="banner_image" name="banner_image" accept="image/png, image/jpg, image/jpeg" />

                                            <img id="preview_banner_photo" src="" width="12%" height="60" alt="" alt="your image" class="mt-4" style="width: 12%; display: none;" />
                                            <span id="banner_image_error" class="error_message" style="color: red;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-4">
                                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.getElementById('banner_image').onchange = evt => {
            const [file] = evt.target.files;
            const preview_banner_photo = document.getElementById('preview_banner_photo');
            const banner_image_error = document.getElementById('banner_image_error');

            if (file) {
                preview_banner_photo.src = URL.createObjectURL(file);
                preview_banner_photo.style.display = 'block'; // Show the preview image
                banner_image_error.textContent = ""; // Clear any previous error message
            } else {
                preview_banner_photo.src = ""; // Clear the preview if no file is selected
                preview_banner_photo.style.display = 'none'; // Hide the preview image
            }
        };
    });
</script>
<script>
    function validateBannerForm() {
        var banner_image = $('#banner_image').val();

        var isValid = true;

        // Perform validation for each field
        if (banner_image === "") {
            document.getElementById("banner_image_error").innerText = "Please Enter banner";
            isValid = false;
        }

        return isValid;
    }
</script>

@endsection