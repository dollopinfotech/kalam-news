@extends('layouts.app')
@section('content')

<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Banner</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Banner</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ route('admin.createBanner') }}" class="btn btn-primary">New Banner</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for menu" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init menu-data-table" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Banner</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($banner as $bannerData)
                        <tr>
                            <td>{{ $loop->iteration}}</td>
                            <td>

                                <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
                                    <img src="{{ file_url($bannerData->banner_image) }}"
                                        width="40" height="40" alt="">
                                </div>
                            </td>
                            <td>
                                <label class="form-check form-switch">
                                    <input type="checkbox" class="form-check-input is-valid" name="is_active" {{ $bannerData->is_active ? 'checked' : '' }} onchange="togglebannerDataStatus(event, {{ $bannerData->banner_id }})">
                                </label>
                                <div id="statusMessage" class="status-message">
                                    {{-- <i class="fa fa-check">Banner</i>
                                    <span id="successText"></span> --}}
                                </div>
                            </td>
                            <td>
                                <div class="d-flex">
                                    <form action="{{ route('admin.editBanner', $bannerData->banner_id) }}" method="GET">
                                        <button class="btn btn-primary btn-sm me-1" type="submit">Edit</button>
                                    </form>
                                    <form action="{{ route('admin.deleteBanner', $bannerData->banner_id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

{{-- <script>
    function togglebannerDataStatus(event, banner_id) {
        var is_activeInput = event.target;
        var is_active = is_activeInput.checked;

        $.ajax({
            url: '/bannerStatus/' + banner_id,
            type: 'POST',
            data: {
            is_active: is_active ? '1' : '0', // Send '1' if checked, '0' if not
            _token: '{{ csrf_token() }}'
        },

            success: function(response) {
                // Handle success
                console.log(response);
            }
        });
    }
</script> --}}

<script>
    function togglebannerDataStatus(event, banner_id) {
        var is_activeInput = event.target;
        var is_active = is_activeInput.checked;
        // Send AJAX request
        $.ajax({
            url: '/bannerStatus/' + banner_id, // Corrected URL
            type: 'POST',
            data: {
                is_active: is_active ? '1' : '0', // Send '1' if checked, '0' if not
                _token: '{{ csrf_token() }}'
            },

            success: function(response) {
                $('#successText').text(response.success);
                $('#statusMessage').fadeIn().delay(2000).fadeOut();
            }
        });
    }
</script>
