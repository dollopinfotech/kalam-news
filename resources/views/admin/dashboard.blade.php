@extends('layouts.app')

@section('content')

<style>
    .saw-chart-circle__container {
    position: absolute;
    left: 0;
    top: -26px;
    width: 100%;
    height: 100%;
    padding: 1.25rem;
}
    </style>
    <!-- sa-app__body -->
    <div id="top" class="sa-app__body px-2 px-lg-4">
        <div class="container pb-6">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <h1 class="h3 m-0">Dashboard</h1>
                    </div>
                    <div class="col-auto d-flex">
                    </div>
                </div>
            </div>
            <div class="row g-4 g-xl-5">
                <div class="col-12 col-md-4 d-flex">
                    <div class="card saw-indicator flex-grow-1" data-sa-container-query='{"340":"saw-indicator--size--lg"}'>
                        <div class="sa-widget-header saw-indicator__header">
                            <h2 class="sa-widget-header__title">Total Users</h2>
                        </div>
                        <div class="saw-indicator__body">
                            <div class="saw-indicator__value">{{ $metrics['userCount'] }}</div>
                            <div class="saw-indicator__delta saw-indicator__delta--rise">
                                <div class="saw-indicator__delta-direction">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"
                                        fill="currentColor">
                                        <path d="M9,0L8,6.1L2.8,1L9,0z"></path>
                                        <circle cx="1" cy="8" r="1"></circle>
                                        <rect x="0" y="4.5" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -2.864 4.0858)"
                                            width="7.1" height="2"></rect>
                                    </svg>
                                </div>
                                <div class="saw-indicator__delta-value">
                                    {{ number_format($metrics['userHikePercentage'], 2) }}%</div>
                            </div>
                            <div class="saw-indicator__caption">Compared to {{ $metrics['lastMonthName'] }}</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 d-flex">
                    <div class="card saw-indicator flex-grow-1" data-sa-container-query='{"340":"saw-indicator--size--lg"}'>
                        <div class="sa-widget-header saw-indicator__header">
                            <h2 class="sa-widget-header__title">Total Package Sold Value</h2>
                        </div>
                        <div class="saw-indicator__body">
                            <div class="saw-indicator__value">&#8377; {{ number_format($metrics['totalPackageValue'], 2) }}
                            </div>
                            <div class="saw-indicator__delta saw-indicator__delta--rise">
                                <div class="saw-indicator__delta-direction">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"
                                        fill="currentColor">
                                        <path d="M9,0L8,6.1L2.8,1L9,0z"></path>
                                        <circle cx="1" cy="8" r="1"></circle>
                                        <rect x="0" y="4.5" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -2.864 4.0858)"
                                            width="7.1" height="2"></rect>
                                    </svg>
                                </div>
                                <div class="saw-indicator__delta-value">
                                    {{ number_format($metrics['avgHikePercentage'], 2) }}%</div>
                            </div>
                            <div class="saw-indicator__caption">Compared to {{ $metrics['lastMonthName'] }}</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 d-flex">
                    <div class="card saw-indicator flex-grow-1" data-sa-container-query='{"340":"saw-indicator--size--lg"}'>
                        <div class="sa-widget-header saw-indicator__header">
                            <h2 class="sa-widget-header__title">Total Packages Sold</h2>
                            <div class="sa-widget-header__actions"></div>
                        </div>
                        <div class="saw-indicator__body">
                            <div class="saw-indicator__value">{{ $metrics['packageSoldCount'] }}</div>
                            <div class="saw-indicator__delta saw-indicator__delta--rise">
                                <div class="saw-indicator__delta-direction">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" viewBox="0 0 9 9"
                                        fill="currentColor">
                                        <path d="M9,0L8,6.1L2.8,1L9,0z"></path>
                                        <circle cx="1" cy="8" r="1"></circle>
                                        <rect x="0" y="4.5" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -2.864 4.0858)"
                                            width="7.1" height="2"></rect>
                                    </svg>
                                </div>
                                <div class="saw-indicator__delta-value">
                                    {{ number_format($metrics['packageSoldHikePercentage'], 2) }}%</div>
                            </div>
                            <div class="saw-indicator__caption">Compared to {{ $metrics['lastMonthName'] }}</div>
                        </div>
                    </div>
                </div>


                <div class="col-12 col-md-4 d-flex">
                    <div class="card saw-indicator flex-grow-1" data-sa-container-query='{"340":"saw-indicator--size--lg"}'>
                        <div class="sa-widget-header saw-indicator__header">
                            <h2 class="sa-widget-header__title">Total Package Sold Value</h2>
                            <div class="sa-widget-header__actions">
                                <select id="intervalSelect" onchange="updateChart()">
                                    <option value="weekly" {{ $interval == 'weekly' ? 'selected' : '' }}>Weekly</option>
                                    <option value="monthly" {{ $interval == 'monthly' ? 'selected' : '' }}>Monthly</option>
                                </select>
                            </div>
                        </div>
                        <div class="saw-indicator__body">
                            <div class="saw-indicator__delta--rise">
                                <div class="saw-indicator__delta-direction">
                                    <div class="saw-chart-circle__body">
                                        <div class="saw-chart-circle__container">
                                           
                                            <canvas id="packageSoldChart" width="1013px" height="325px"
                                                style="display: block;box-sizing: border-box;height: 228px;width: 250px;"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-8 col-xxl-9 d-flex">
                    <div class="card flex-grow-1 saw-chart">
                        <div class="sa-widget-header saw-chart__header">
                            <h2 class="sa-widget-header__title"> Users </h2>
                        </div>
                        <div class="saw-chart__body">
                            <div class="saw-chart__container"><canvas id="userChart" width="1013px" height="325px"
                                    style="display: block;box-sizing: border-box;height: 160px;width: 447px;"></canvas>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- sa-app__body / end -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
    <script>
        var ctx = document.getElementById('userChart').getContext('2d');
        var userChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: @json($users->pluck('date')),
                datasets: [{
                    label: 'Users Joined',
                    data: @json($users->pluck('count')),
                    fill: 'origin',
                    borderWidth: 0,
                    backgroundColor: '#ffd333',
                    borderColor: 'transparent',
                }]
            },
            options: {
                scales: {
                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                    x: {
                        type: 'time',
                        time: {
                            unit: 'day',
                            tooltipFormat: 'DD MMM YYYY'
                        },
                        title: {
                            display: true,
                            text: 'Date'
                        },
                        ticks: {
                            fontFamily: 'Roboto',
                            fontSize: 17,
                            fontColor: '#828f99',
                        },
                        gridLines: {
                            display: false,
                        },
                    },
                    y: {
                        beginAtZero: true,
                        title: {
                            display: true,
                            text: 'Number of Users'
                        },
                        ticks: {
                            fontFamily: 'Roboto',
                            fontSize: 13,
                            fontColor: '#828f99',
                            fontWeight: 'bold',
                        },

                        // gridLines: {
                        //     lineWidth: 1,
                        //     color: 'rgba(0, 0, 0, 0.1)',
                        //     zeroLineWidth: 1,
                        //     zeroLineColor: 'rgba(0, 0, 0, 0.1)',
                        //     drawBorder: false,
                        // },
                    }
                }
            }
        });

        var packageSoldChart;
        function updateChart() {
            // alert('tgfv')
            var interval = document.getElementById('intervalSelect').value;
            fetch(`/admin/packageSoldData?interval=${interval}`)
                .then(response => response.json())
                .then(data => {
                    packageSoldChart.data.labels = data.labels;
                    packageSoldChart.data.datasets[0].data = data.counts;
                    packageSoldChart.update();
                });
        }

        document.addEventListener('DOMContentLoaded', function() {
            var ctxPackage = document.getElementById('packageSoldChart').getContext('2d');
            packageSoldChart = new Chart(ctxPackage, {
                type: 'doughnut',
                data: {
                    labels: @json($packages->pluck('date')),
                    datasets: [{
                        label: 'Packages Sold',
                        data: @json($packages->pluck('count')),
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                             display: false, 
                        },
                        title: {
                            display: true,
                            text: 'Package Sales Distribution'
                        }
                    }
                }
            });
            updateChart();
        });
  

    </script>
@endsection
