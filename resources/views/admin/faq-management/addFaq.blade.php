@extends('layouts.app')
@section('content')
<style>
    .ck-editor__editable{
        height: 200px !important;
    }
</style>
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.getFaq') }}">Faq</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add Faq</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Add Faq</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                <div class="sa-entity-layout__body">
                    <div class="sa-entity-layout__main">
                        <form method="POST" action="{{ route('admin.storeFaq') }}" id="addFaqForm" enctype="multipart/form-data" onsubmit="return validateFaqForm()">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="card">
                                <div class="card-body p-5">
                                    <div class="mb-4">
                                        <div class="col mb-4">
                                            <label for="faq_title" class="form-label">Faq Title <span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="faq_title" id="faq_title" value="" />
                                        </div>
                                        <div class="col-md-12">
                                            <label for="faq_description" class="form-label">Faq Description <span style="color:red">*</span></label>
                                            <textarea class="form-control" rows="8" cols="80" id="faq_description" name="faq_description"></textarea>
                                            <span id="faq_description_error" class="error_message" style="color: red;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-4">
                                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'faq_description' );
</script>
<script>
    function validateFaqForm() {
        var faqDescription =  $('#faq_description').val();

        var isValid = true;

        // Perform validation for each field
        if (faqDescription === "") {
            document.getElementById("faq_description_error").innerText = "Please Enter Faq Description";
            isValid = false;
        }

        return isValid;
    }
</script>

@endsection
