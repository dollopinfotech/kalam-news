@extends('layouts.app')
@section('content')
<style>
    p{
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        font-weight:normal;
    }
</style>
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Faq</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Faq</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ route('admin.createFaq') }}" class="btn btn-primary">New Faq</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for faq" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init menu-data-table" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Faq Title</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($faqs as $faqData)
                        <tr>
                            <td>{{ $loop->iteration}}</td>
                            <td>{{ $faqData->faq_title }}</td>
                            <td><?php echo htmlToPlainText($faqData->faq_description) ?></td>
                            <td>
                                <div class="d-flex">
                                    <form action="{{ route('admin.editFaq', $faqData->faq_id) }}" method="GET">
                                        <button class="btn btn-primary btn-sm me-1" type="submit">Edit</button>
                                    </form>
                                    <form class="deleteForm<?php echo $faqData->faq_id; ?>" action="{{ route('admin.deleteFaq', $faqData->faq_id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $faqData->faq_id ?>');" type="button" >Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection