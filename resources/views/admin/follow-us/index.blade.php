@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="">Follow us</a></li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Follow us</h1>
                    </div>
                    <div class="col-auto d-flex">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#FollowModal">New Follow</button>
                    </div>
                </div>
            </div>



            <div style="margin-left: 290px">
                <div id="FollowModal" class="modal fade pullDown" role="dialog" id="large">
                    <div class="modal-dialog modal-lg" role="document">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header bg-primary white">
                                <h4 class="modal-title" id="myModalLabel8">Follow Us</h4>
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="followAddForm" method="POST" action="{{ route('admin.store') }}" enctype="multipart/form-data">
                                    <div class="card mb-3" style="border-radius: 15px; " id="cards">
                                        @csrf
                                        <div class="card-body" id='card-copy'>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <label for="platform" class="col-form-label">Platform Name<span style="color:red">*</span></label>
                                                    <input type="text" class="form-control" name="platform" value="">
                                                    <span id="platform_error" class=" error_message" style="color: red;"></span>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="links" class="col-form-label">Link <span style="color:red">*</span></label>
                                                    <input type="text" class="form-control" name="links" value="">
                                                    <span id="links_error" class="error_message" style="color: red;"></span>

                                                </div>


                                                <div class="col-md-4">
                                                    <label for="icon" class="col-form-label">Icon<span style="color:red">*</span></label>
                                                    <input type="file" class="form-control" id="icon" name="icon" accept="image/*" onchange="previewImage(event)">
                                                    <img id="preview_icon" src="" alt="Icon Preview" class="mt-4" style="width: 25%; display: none;">
                                                    <span id="icon_error" class="error_message" style="color: red;"></span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <!-- Move buttons below the card -->
                                    <div class="col-md-10 offset-md-1">
                                        <div class="mb-3 d-flex justify-content-center">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div id="myform"></div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="p-4">
                <input type="text" placeholder="Start typing to search for Follow-us" class="form-control form-control--search mx-auto" id="table-search" />
            </div>
            <div class="sa-divider"></div>
            <table class="sa-datatables-init menu-data-table" data-sa-search-input="#table-search">
                <thead>
                    <tr>
                        <th>Sr</th>
                        <th>Icon</th>
                        <th>Platform Name</th>
                        <th>Links</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($socialMedia as $social)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>
                            <div class="d-flex align-items-center">
                                <div class="product_icon mr-2">
                                    <img src="{{ file_url($social->icon) ?? '' }}" style="height: 40px; width: 40px; border-radius: 50%;" alt="Icon">
                                </div>
                            </div>
                        </td>
                        <td>{{ $social->platform }} </td>
                        <td>{{ $social->links }}</td>
                        <td>
                            <div class="d-flex">

                                <button class="btn btn-primary btn-sm me-1" onclick="EditFollowUs('{{ $social->social_id }}', '{{ $social->platform }}', '{{ $social->links }}', '{{ $social->icon }}')">Edit</button>

                                <form action="{{ route('delete.links', $social->social_id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                </form>

                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div style="margin-left: 290px">
            <div id="FollowEditModal" class="modal fade pullDown" role="dialog" id="large">
                <div class="modal-dialog modal-lg" role="document">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header bg-primary white">
                            <h4 class="modal-title" id="myModalLabel8">Follow Us</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="followEditForm" method="POST" action=" {{ route('admin.updateFollow') }}" enctype="multipart/form-data">
                                @csrf
                                <div>
                                    <input type="hidden" name="social_id" id="social_id" value="">
                                </div>
                                <div class="card mb-3" style="border-radius: 15px; " id="cards">
                                    <div class="card-body" id='card-copy'>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="platform" class="col-form-label">Platform Name<span style="color:red">*</span></label>
                                                <input type="text" class="form-control" name="platform" value="" id="platform">
                                                <span id="platform_error" class=" error_message" style="color: red;"></span>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="links" class="col-form-label">Link <span style="color:red">*</span></label>
                                                <input type="text" class="form-control" name="links" value="" id="links">
                                                <span id="links_error" class="error_message" style="color: red;"></span>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="icon" class="col-form-label">Icon<span style="color:red">*</span></label>
                                                <input type="file" class="form-control" id="icon" name="icon" accept="image/*" onchange="previewEditImage(event, 'editpreview_icon')">
                                                <img id="editpreview_icon" src="{{ file_url($social->icon) ?? '' }}" alt="Icon Preview" class="mt-4" style="width: 25%">
                                                <span id="icon_error" class="error_message" style="color: red;"></span>
                                            </div><br>

                                        </div>
                                    </div>
                                    <!-- Move buttons below the card -->
                                    <div class="col-md-10 offset-md-1">
                                        <div class="mb-3 d-flex justify-content-center">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
                <div id="myform"></div>
            </div>
        </div>
    </div>
</div>

<div class="card mt-5">
</div>
</div>
</div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        const icon = document.getElementById('icon');
        const preview_icon = document.getElementById('preview_icon');

        icon.onchange = evt => {
            const file = icon.files[0];
            if (file) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    preview_icon.src = e.target.result;
                    preview_icon.style.display = 'block';
                }
                reader.readAsDataURL(file);
            }
        };
    });
</script>

<script>
    function previewEditImage(event, previewId) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById(previewId);
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>
@endsection