@extends('layouts.app')
@section('content')
    <div id="top" class="sa-app__body">
        <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
            <div class="container">
                <div class="py-5">
                    <div class="row g-4 align-items-center">
                        <div class="col">
                            <nav class="mb-2" aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-sa-simple">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.getIcons') }}">Icon</a></li>
                                </ol>
                            </nav>
                            <h1 class="h3 m-0">Add Icon</h1>
                        </div>
                    </div>
                </div>
                <div class="sa-entity-layout"
                    data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                    <div class="sa-entity-layout__body">
                        <div class="sa-entity-layout__main">
                            <form method="POST" action="{{ route('admin.storeIcon') }}" id="addIconForm"
                                enctype="multipart/form-data" onsubmit="return validateIconsForm()">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="card">
                                    <div class="card-body p-5">
                                        <div class="row mb-4">
                                            <div class="col-md-6">
                                                <label for="icon_name" class="form-label"> Icon Name <span
                                                        style="color:red">*</span></label>
                                                <input type="text" class="form-control" name="icon_name" id="icon_name"
                                                    value="" />
                                            </div>
                                            <div class="col-md-6">
                                                <label for="icon_image" class="form-label"> Icon Image </label>
                                                <input type="file" class="form-control" name="icon_image" id="icon_image"
                                                    value="" accept="image/*" />
                                                <img id="preview_icon_image" src="" alt="icon image"
                                                    class="mt-4" style="width: 25%; display: none;" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <label for="icon" class="form-label"> Icon </label>
                                            <textarea class="form-control" id="icon" name="icon" rows="5" cols="33"></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center mb-4">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function validateIconsForm() {
            var icon = $('#icon_name').val();
            var isValid = true;

            // Perform validation for each field
            if (icon === "") {
                alert(icon);
                document.getElementById("icon_name").innerText = "Please Enter Icon Name";
                isValid = false;
            }

            return isValid;
        }
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const icon_image = document.getElementById('icon_image');
            const preview_icon_image = document.getElementById('preview_icon_image');

            icon_image.onchange = evt => {
                const [file] = icon_image.files;
                if (file) {
                    preview_icon_image.src = URL.createObjectURL(file);
                    preview_icon_image.style.display = 'block';
                }
            };
        });
    </script>
@endsection
