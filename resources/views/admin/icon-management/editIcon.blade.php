@extends('layouts.app')
@section('content')
    <div id="top" class="sa-app__body">
        <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
            <div class="container">
                <div class="py-5">
                    <div class="row g-4 align-items-center">
                        <div class="col">
                            <nav class="mb-2" aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-sa-simple">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.getIcons') }}">Icon</a></li>
                                </ol>
                            </nav>
                            <h1 class="h3 m-0">Edit Icon</h1>
                        </div>
                    </div>
                </div>
                <div class="sa-entity-layout"
                    data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                    <div class="sa-entity-layout__body">
                        <div class="sa-entity-layout__main">
                            <form method="POST" action="{{ route('admin.updateIcon', $icons->icon_id) }}" id="addIconForm"
                                enctype="multipart/form-data" onsubmit="return validateIconFaqForm()">
                                @method('PUT')
                                @csrf
                                <div class="card">
                                    <div class="card-body p-5">
                                        <div class="mb-4 row">
                                            <div class="col-md-6">
                                                <input type="hidden" id="icon_id" name="icon_id">
                                                <label for="icon_name" class="form-label">Icon Name <span
                                                        style="color:red">*</span></label>
                                                <input type="text" class="form-control" name="icon_name" id="icon_name"
                                                    value="{{ $icons->icon_name }}" />
                                            </div>
                                            <div class="col-md-6">
                                                <label for="icon_image" class="form-label"> Icon Image </label>
                                                <input type="file" class="form-control" name="icon_image" id="icon_image"
                                                    value="" accept="image/*" />
                                                <img id="preview_icon_image" src="{{ file_url('images/icons/'.$icons->icon_image )}}" alt="icon image"
                                                    class="mt-4" style="width: 25%;" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <label for="icon" class="form-label"> Icon </label>
                                            <textarea class="form-control" id="icon" name="icon" rows="5" cols="33">{{ $icons->icon }}</textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-center mb-4">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function validateIconsForm() {
            var icon = $('#icon_name').val();

            var isValid = true;

            // Perform validation for each field
            if (icon === "") {
                document.getElementById("icon_name").innerText = "Please Enter Icon Name";
                isValid = false;
            }

            return isValid;
        }
    </script>
@endsection
