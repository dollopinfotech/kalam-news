<?php 
use App\Models\Menu;
use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;

$menuData = DB::table('menus')
    ->where(['page_status' => 0, 'status' => 1])
    ->get();
	// main menu
	$path = request()->path();
    $pathName = explode("/",$path);
    $pageName = $pathName[0];

    // echo $pageName; die;
    $adminData = auth() ->user();
    if(isset($adminData->admin_id) && $adminData->admin_id != ''){
        $adminProfile = Admin::where('admin_id', $adminData->admin_id)->first();
        $access = DB::table('roles')
        ->where(['role_id' => $adminProfile->role_id])
        ->get();
        // print_r($accessData);die;
        $menuPrivilege = array();
        $pagePrivilege = array();
        foreach ($access as $privilegeData) {
            $menuPrivilege = $privilegeData->menu_id;
            $pagePrivilege = $privilegeData->page_privilage;
        }
        $accessMenuIdArr = explode(",", $menuPrivilege);
        $pagePrivilegeArr = explode(",", $pagePrivilege);
        
        $accessPage= DB::table('menus')
        ->where(['menu_link' => $pageName, 'status' => 1])
        ->get();
        
        $pageMenuId = "";
        foreach ($accessPage as $accessPageData) {
            $pageMenuId = $accessPageData->menu_id;
        }
        //  print_r($accessPageData);die;
        //  print_r($accessPageDat);die;
        
        //  print_r($pageName);die;
        
        if(in_array($pageMenuId, $accessMenuIdArr) || in_array($pageMenuId, $pagePrivilegeArr)){ 	} else {
            ?>
<script language="javascript">
    window.location = "../404";
</script>
<?php  } 
} else { ?>
<script language="javascript">
    window.location = "../404";
</script>
<?php } ?>
