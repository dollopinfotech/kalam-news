<?php

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;

$adminData = auth() ->user();
if(isset($adminData->admin_id) && $adminData->admin_id != ''){
    $adminProfile = Admin::where('admin_id', $adminData->admin_id)->first();
    $access = DB::table('roles')
        ->where(['role_id' => $adminProfile->role_id])
        ->get();
    // print_r($accessData);die;
    $menuPrivilege = [];
    $pagePrivilege = [];
    foreach ($access as $privilegeData) {
        $menuPrivilege = $privilegeData->menu_id;
        $pagePrivilege = $privilegeData->page_privilage;
    }
    $accessMenuIdArr = explode(',', $menuPrivilege);
    $pagePrivilegeArr = explode(',', $pagePrivilege);

    $menuData = DB::table('menus')
        ->where(['page_status' => 0, 'status' => 1])
        ->get();

    // echo $menuData ; die;
    $menuArray = [];
    foreach ($menuData as $data) {
        $menu_id = $data->menu_id;
        if ($data->parent_menu_id == 0 && $data->sub_menu == 0) {
            if (in_array($menu_id, $accessMenuIdArr)) {
                $menu = [
                    'menu_id' => $data->menu_id,
                    'menu_link' => $data->menu_link,
                    'menu_icon' => $data->menu_icon,
                    'menu_name' => $data->menu_name,
                    'subMenu' => '',
                ];
                array_push($menuArray, $menu);
            }
        } elseif ($data->parent_menu_id == 0 && $data->sub_menu == 1) {
            $subMenuArray = [];
            $subMenuData = DB::table('menus')
                ->where(['parent_menu_id' => $menu_id, 'page_status' => 0])
                ->get();
            $subMenuContent = '';
            $subMenuCount = 0;
            foreach ($subMenuData as $temData) {
                if (in_array($temData->menu_id, $accessMenuIdArr)) {
                    $submenu = [
                        'menu_id' => $temData->menu_id,
                        'menu_link' => $temData->menu_link,
                        'menu_name' => $temData->menu_name,
                        'menu_icon' => $temData->menu_icon,
                    ];
                    // print_r($submenu);die;
                    array_push($subMenuArray, $submenu);
                    $subMenuCount++;
                }
                if (in_array($temData->menu_id, $pagePrivilegeArr)) {
                    $submenu = [
                        'menu_id' => $temData->menu_id,
                        'menu_link' => $temData->menu_link,
                        'menu_name' => $temData->menu_name,
                        'menu_icon' => $temData->menu_icon,
                    ];
                    // print_r($submenu);die;
                    array_push($subMenuArray, $submenu);
                }
            }
            if (!empty($subMenuArray) && $subMenuCount > 0) {
                $menu = [
                    'menu_id' => $data->menu_id,
                    'menu_link' => $data->menu_link,
                    'menu_icon' => $data->menu_icon,
                    'menu_name' => $data->menu_name,
                    'subMenu' => $subMenuArray,
                ];
                array_push($menuArray, $menu);
            }
        }
    }

    $sidebareData = [];
    $sidebareData = $menuArray;

    ?>
<style>
    p {
        font-family: "Damion", cursive;
        color: #000;
        font-weight: bold;
        display: flex;
        font-size: 16px;
        text-align: center;
        height: 23px;
    }

    .hide-bg {
        mix-blend-mode: multiply;
        filter: contrast(1);
    }
</style>
{{-- @if (auth()->check() && auth()->user()->role === 'admin')

    @endif --}}
<div class="sa-app sa-app--desktop-sidebar-shown sa-app--mobile-sidebar-hidden sa-app--toolbar-fixed">
    <!-- sa-app__sidebar -->
    <div class="sa-app__sidebar">
        <div class="sa-sidebar">
            <div class="sa-sidebar__header">
                <a class="sa-sidebar__logo" href="{{ route('admin.dashboard') }}">
                    <!-- logo -->
                    <div class="sa-sidebar-logo" style="gap:12px;">
                        {{-- <span style="font-weight: bold;">Kalam News</span> --}}
                        @if (isset($siteSettings->logo))
                            <img src="{{ file_url($siteSettings->logo) ?? '' }}" alt="Site Logo" class="hide-bg"
                                style="height: 54px; width:105px;  float: left;">
                        @endif
                        <span style="font-weight: bold;">{{ $siteSettings->site_name ?? '' }}</span>
                        {{-- <div class="sa-sidebar-logo__caption">Admin</div> --}}
                    </div>
                    <!-- logo / end -->
                </a>
            </div>
            <div class="sa-sidebar__body" data-simplebar="">
                <ul class="sa-nav sa-nav--sidebar" data-sa-collapse="">
                    <li class="sa-nav__section">
                        <ul class="sa-nav__menu sa-nav__menu--root">
                            <?php
                                foreach ($sidebareData as $data) {
                                    $menu_id = $data['menu_id'];
                                    if ($data['subMenu'] == "") {
                                        if (in_array($menu_id, $accessMenuIdArr)) { ?>
                            <li class="sa-nav__menu-item sa-nav__menu-item--has-icon"
                                data-sa-collapse-item="sa-nav__menu-item--open">
                                <a href="../<?php echo $data['menu_link']; ?>" class="sa-nav__link">
                                    <span class="sa-nav__icon"><i class="<?php echo $data['menu_icon'];
                                    ?> menu-icon"></i></span>
                                    {{-- <span class="sa-nav__icon"><img src="{{ file_url('images/icons/'.$data['menu_icon']) }}" style="width: 25px;" /></span> --}}
                                    <span class="sa-nav__title"><?php echo $data['menu_name']; ?></span>
                                    <!-- <span class="sa-nav__arrow">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="6" height="9" viewBox="0 0 6 9" fill="currentColor">
                                                        <path d="M5.605,0.213 C6.007,0.613 6.107,1.212 5.706,1.612 L2.696,4.511 L5.706,7.409 C6.107,7.809 6.107,8.509 5.605,8.808 C5.204,9.108 4.702,9.108 4.301,8.709 L-0.013,4.511 L4.401,0.313 C4.702,-0.087 5.304,-0.087 5.605,0.213 Z"></path>
                                                    </svg>
                                                </span> -->
                                </a>
                            </li>
                            <?php }
                                    } else if ($data['subMenu'] != "") {
                                        $parentActive = "";
                                        foreach ($data['subMenu'] as $temData) {
                                            $temData['menu_link'] . ',';
                                        }
                                        ?>

                            <li class="sa-nav__menu-item sa-nav__menu-item--has-icon"
                                data-sa-collapse-item="sa-nav__menu-item--open">
                                <a href="javaScript:void();" class="sa-nav__link" data-sa-collapse-trigger="">
                                    <span class="sa-nav__icon"><i class="<?php echo $data['menu_icon']; ?> menu-icon"></i></span>
                                    <span class="sa-nav__title"><?php echo $data['menu_name']; ?></span>
                                    <span class="sa-nav__arrow">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="6" height="9"
                                            viewBox="0 0 6 9" fill="currentColor">
                                            <path
                                                d="M5.605,0.213 C6.007,0.613 6.107,1.212 5.706,1.612 L2.696,4.511 L5.706,7.409 C6.107,7.809 6.107,8.509 5.605,8.808 C5.204,9.108 4.702,9.108 4.301,8.709 L-0.013,4.511 L4.401,0.313 C4.702,-0.087 5.304,-0.087 5.605,0.213 Z">
                                            </path>
                                        </svg>
                                    </span>
                                </a>
                                <ul class="sa-nav__menu sa-nav__menu--sub" data-sa-collapse-content="">
                                    <?php
                                                foreach ($data['subMenu'] as $temData) {
                                                    $menu_id1 = $temData['menu_id'];
                                                    if (in_array($temData['menu_id'], $accessMenuIdArr)) {
                                                        $active = "";
                                                        $menu_name = $temData['menu_name'];
                                                ?>
                                    <li class="sa-nav__menu-item">
                                        <a href="../<?php echo $temData['menu_link']; ?>" class="sa-nav__link">
                                            <span class="sa-nav__menu-item-padding"></span>
                                            <span class="sa-nav__title"><?php echo $menu_name; ?></span>
                                        </a>
                                    </li>
                                    <?php
                                                    }
                                                } ?>
                                </ul>
                            </li>
                            <?php }
                                }
                                ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="sa-app__sidebar-shadow"></div>
        <div class="sa-app__sidebar-backdrop" data-sa-close-sidebar=""></div>
    </div>
    <!-- sa-app__sidebar / end -->
    <!-- sa-app__content -->
    <div class="sa-app__content">
        <!-- sa-app__toolbar -->
        <div class="sa-toolbar sa-toolbar--search-hidden sa-app__toolbar">
            <div class="sa-toolbar__body">
                <div class="sa-toolbar__item">
                    <button class="sa-toolbar__button" type="button" aria-label="Menu" data-sa-toggle-sidebar="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"
                            fill="currentColor">
                            <path d="M1,11V9h18v2H1z M1,3h18v2H1V3z M15,17H1v-2h14V17z"></path>
                        </svg>
                    </button>
                </div>
                <div class="sa-toolbar__item sa-toolbar__item--search">
                    <form class="sa-search sa-search--state--pending">
                        <div class="sa-search__body">
                            <label class="visually-hidden" for="input-search">Search for:</label>
                            <div class="sa-search__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"
                                    viewBox="0 0 16 16" fill="currentColor">
                                    <path
                                        d="M16.243 14.828C16.243 14.828 16.047 15.308 15.701 15.654C15.34 16.015 14.828 16.242 14.828 16.242L10.321 11.736C9.247 12.522 7.933 13 6.5 13C2.91 13 0 10.09 0 6.5C0 2.91 2.91 0 6.5 0C10.09 0 13 2.91 13 6.5C13 7.933 12.522 9.247 11.736 10.321L16.243 14.828ZM6.5 2C4.015 2 2 4.015 2 6.5C2 8.985 4.015 11 6.5 11C8.985 11 11 8.985 11 6.5C11 4.015 8.985 2 6.5 2Z">
                                    </path>
                                </svg>
                            </div>
                            <input type="text" id="input-search" class="sa-search__input"
                                placeholder="Search for the truth" autocomplete="off" />
                            <button class="sa-search__cancel d-sm-none" type="button" aria-label="Close search">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                    viewBox="0 0 12 12" fill="currentColor">
                                    <path
                                        d="M10.8,10.8L10.8,10.8c-0.4,0.4-1,0.4-1.4,0L6,7.4l-3.4,3.4c-0.4,0.4-1,0.4-1.4,0l0,0c-0.4-0.4-0.4-1,0-1.4L4.6,6L1.2,2.6 c-0.4-0.4-0.4-1,0-1.4l0,0c0.4-0.4,1-0.4,1.4,0L6,4.6l3.4-3.4c0.4-0.4,1-0.4,1.4,0l0,0c0.4,0.4,0.4,1,0,1.4L7.4,6l3.4,3.4 C11.2,9.8,11.2,10.4,10.8,10.8z">
                                    </path>
                                </svg>
                            </button>
                            <div class="sa-search__field"></div>
                        </div>
                        <div class="sa-search__dropdown">
                            <div class="sa-search__dropdown-loader"></div>
                            <div class="sa-search__dropdown-wrapper">
                                <div class="sa-search__suggestions sa-suggestions"></div>
                                <div class="sa-search__help sa-search__help--type--no-results">
                                    <div class="sa-search__help-title">
                                        No results for &quot;
                                        <span class="sa-search__query"></span>
                                        &quot;
                                    </div>
                                    <div class="sa-search__help-subtitle">Make sure that all words are spelled
                                        correctly.</div>
                                </div>
                                <div class="sa-search__help sa-search__help--type--greeting">
                                    <div class="sa-search__help-title">Start typing to search for</div>
                                    <div class="sa-search__help-subtitle">Products, orders, customers, actions, etc.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sa-search__backdrop"></div>
                    </form>
                </div>
                <div class="mx-auto"></div>
                <div class="sa-toolbar__item d-sm-none">
                    <button class="sa-toolbar__button" type="button" aria-label="Show search"
                        data-sa-action="show-search">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16"
                            fill="currentColor">
                            <path
                                d="M16.243 14.828C16.243 14.828 16.047 15.308 15.701 15.654C15.34 16.015 14.828 16.242 14.828 16.242L10.321 11.736C9.247 12.522 7.933 13 6.5 13C2.91 13 0 10.09 0 6.5C0 2.91 2.91 0 6.5 0C10.09 0 13 2.91 13 6.5C13 7.933 12.522 9.247 11.736 10.321L16.243 14.828ZM6.5 2C4.015 2 2 4.015 2 6.5C2 8.985 4.015 11 6.5 11C8.985 11 11 8.985 11 6.5C11 4.015 8.985 2 6.5 2Z">
                            </path>
                        </svg>
                    </button>
                </div>

                <div class="sa-toolbar__item dropdown">
                    <button class="sa-toolbar__button" type="button" id="dropdownMenuButton3" data-bs-toggle="dropdown" data-bs-reference="parent" data-bs-offset="0,1" aria-expanded="false">
                        @php
                            $currentLocale = app()->getLocale();
                            $currentFlag = $currentLocale == 'hi' ? 'IN' : 'GB';
                        @endphp
                        <img src="vendor/flag-icons/24/{{ $currentFlag }}.png" class="sa-language-icon" alt="">
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end" id="language" aria-labelledby="dropdownMenuButton3">
                        @foreach(config('app.available_locales') as $name => $locale)
                            @php
                                $flag = $locale == 'Hindi' ? 'IN' : 'GB';
                            @endphp
                            <li>

                                <a class="dropdown-item d-flex align-items-center" href="{{ route("admin.langSwitch", ['locale' => $locale]) }}" data-language="{{ $locale }}">
                                    <img src="vendor/flag-icons/24/{{ $flag }}.png" class="sa-language-icon me-3" alt="">
                                    <span class="ps-2">{{ $name }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>



                <div class="dropdown sa-toolbar__item">
                    <button class="sa-toolbar-user" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown"
                        data-bs-offset="0,1" aria-expanded="false">
                        <span class="sa-toolbar-user__avatar sa-symbol sa-symbol--shape--rounded">
                            <img src="{{ file_url($adminProfile->admin_profile) ?? '' }}"
                                width="64" height="64" alt="" />
                        </span>
                        <span class="sa-toolbar-user__info">
                            <span class="sa-toolbar-user__title">{{ $adminProfile->admin_name ?? '' }}</span>
                            <span class="sa-toolbar-user__subtitle">{{ $adminProfile->admin_email ?? '' }}</span>
                        </span>
                    </button>
                    <ul class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                        <li><a class="dropdown-item" href="/admin-profile">Profile</a></li>
                        <li><a class="dropdown-item" href="">Settings</a></li>
                        <li>
                            <hr class="dropdown-divider" />
                        </li>
                        <li><a class="dropdown-item" href="{{ route('admin.logout') }}">Sign Out</a></li>
                    </ul>
                </div>
            </div>
            <div class="sa-toolbar__shadow"></div>
        </div>

        <!-- sa-app__toolbar / end -->
        {{-- <main class="py-4" style="padding-left: 18rem;"> --}}
        @yield('content')
        {{-- </main> --}}
        <!-- sa-app__footer -->
        <div class="sa-app__footer d-block d-md-flex">
            <!-- copyright -->
            Admin — Kalam News Publication © 2024
            <div class="m-auto"></div>
            <div>
                Powered by HTML — Design by Dollop Infotech Pvt. Ltd.
            </div>
            <!-- copyright / end -->
        </div>
        <!-- sa-app__footer / end -->
    </div>
    <!-- sa-app__content / end -->
    <!-- sa-app__toasts -->
    <div class="sa-app__toasts toast-container bottom-0 end-0"></div>
    <!-- sa-app__toasts / end -->
</div>
<?php } else { ?>
<script language="javascript">
    window.location = "../404";
</script>
<?php } ?>


{{-- <script>
    document.querySelectorAll('#language li a').forEach(element => {
        element.addEventListener('click', function(event) {
            event.preventDefault();

            var selectedLanguage = event.currentTarget.getAttribute('data-language');
            console.log( selectedLanguage);
            $.ajax({
                type: 'GET',
                url: '{{ route('admin.langSwitch') }}',
                data: {locale: locale},
                success: function(data) {
                    // Update the page content with the new translations
                    $.each(data, function(key, value) {
                        $('[data-translate="' + key + '"]').text(value);
                    });
                }
            });

        });
    });
</script> --}}

