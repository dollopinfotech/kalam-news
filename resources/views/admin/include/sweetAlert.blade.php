@if(Session::has('success'))
<script type="text/javascript">
    swal({
        title: "Success!",
        icon: "success",
        text: "{{ Session::get('success') }}",
        // type: "success",
        timer: 3000
    });
</script>
@elseif(Session::has('error'))
<script type="text/javascript">
    swal({
        title: "Error!",
        icon: "error",
        text: "{{ Session::get('error') }}",
        // type: "error",
        timer: 4000
    });
</script>
@elseif(Session::has('warning'))
<script type="text/javascript">
    swal({
        title: "Warning!",
        icon: "warning",
        text: "{{ Session::get('warning') }}",
        // type: "error",
        timer: 4000
    });
</script>
@endif