@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.menu') }}">Menu</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add Menu</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Add Menu</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                <div class="sa-entity-layout__body">
                    <div class="sa-entity-layout__main">
                        <form id="addMenuForm" method="POST" action="{{ route('admin.storeMenu') }}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="card">
                                <div class="card-body p-5">
                                    <div class="row g-6 mb-4">
                                        <div class="col">
                                            <label for="menu_name" class="form-label">Menu Name <span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="menu_name" id="menu_name" value="" required/>
                                        </div>
                                        <div class="col">
                                            <label for="menu_link" class="form-label">Page Url <span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="menu_link" id="menu_link" required/>
                                        </div>
                                        <div class="col">
                                            <label for="menu_icon" class="form-label">Menu Icon <span style="color:red">*</span></label>
                                            <input id="getclass" type="text" class="form-control" name="menu_icon" value="" required="" data-toggle="modal" data-target="#iconModal">
                                        </div>
                                    </div>
                                    <div class="row g-6 mb-4">
                                        <div class="col">
                                            <label for="sub_menu" class="form-label">Sub Menu <span style="color:red">*</span></label>
                                            <select name="sub_menu" id="sub_menu" class="form-control">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label for="order_no" class="form-label">Order No <span style="color:red">*</span></label>
                                            <input type="number" class="form-control" name="order_no" id="order_no" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-4">
                                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="margin-left: 290px">
    <div id="iconModal" class="modal fade pullDown" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-primary white">
                    <h4 class="modal-title" id="myModalLabel8">Select Icon</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="list-inline">
                        @php
                            $data = new App\Models\Icon;
                            $data = $data->all();
                        @endphp
                        @foreach ($data as $data)
                            <li title="{{$data['icon_name']}}" style="display: inline; font-size: 22px; padding: 10px; cursor: pointer;">
                                <i id="{{$data['icon_class']}}" onclick="get_class(this);" class="fa {{$data['icon_class']}}"></i>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
            <div id="myform"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function get_class(x) {
        var element = $(x);
        var class_name = element.attr("class");
        document.getElementById('getclass').value = class_name;
        $('#iconModal').modal('toggle');
    }
</script>
<script>
    document.getElementById('getclass').addEventListener('click', function() {
        $('#iconModal').modal('show');
    });
</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        fetch('/getIcons')
            .then(response => response.json())
            .then(data => {
                const select = document.getElementById('menu_icon');
                data.forEach(icon => {
                    const option = document.createElement('option');
                    option.value = icon.icon_image;
                    option.textContent = icon.icon_name;
                    select.appendChild(option);
                });
            })
            .catch(error => console.error('Error fetching icons:', error));
    });
</script>

@endsection
