@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.subMenu') }}">Sub Menu</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add Sub Menu</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Add Sub Menu</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                <div class="sa-entity-layout__body">
                    <div class="sa-entity-layout__main">
                        <form method="POST" action="{{ route('admin.storeSubMenu') }}" id="addSubMenuForm" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="card">
                                <div class="card-body p-5">
                                    <div class="row mb-4">
                                        <div class="col-md-4">
                                            <label for="menu_name" class="form-label">Menu Name <span style="color:red">*</span></label>
                                            <select class="form-select sa-select2" name="menu_name" id="menu_name">
                                                @foreach ($menu as $menuData)
                                                <option value="{{ $menuData->menu_id }}">{{ $menuData->menu_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="sub_menu_name" class="form-label">Sub Menu Name<span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="sub_menu_name" id="sub_menu_name" />
                                        </div>
                                        <div class="col-md-4">
                                            <label for="sub_menu_url" class="form-label">Sub Menu Url<span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="sub_menu_url" id="sub_menu_url" />
                                        </div>
                                    </div>
                                    <div class="row mb-4 col-md-4">
                                        <div class="col">
                                            <label for="menu_order_no" class="form-label">Menu Order No <span style="color:red">*</span></label>
                                            <input type="number" class="form-control" name="menu_order_no" id="menu_order_no" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-4">
                                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection