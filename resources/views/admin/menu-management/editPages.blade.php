@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.pages') }}">Pages</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit Pages</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Edit Pages</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                <div class="sa-entity-layout__body">
                    <div class="sa-entity-layout__main">
                        <form method="POST" action="{{ route('admin.updatePages',$pages->menu_id) }}" id="addPagesForm" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="card">
                                <div class="card-body p-5">
                                    <div class="row mb-4">
                                        <div class="col-md-4">
                                            <label for="menu_name" class="form-label">Menu Name <span style="color:red">*</span></label>
                                            <select class="form-select" name="menu_name" id="menu_name">
                                                <option value="">Select Menu</option>
                                                @foreach ($menu as $menuData)
                                                <option @if( $pages->parent_menu_id == $menuData->menu_id)
                                                    selected="selected" @endif value="{{ $menuData->menu_id }}">{{ $menuData->menu_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="page_name" class="form-label">Page Name<span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="page_name" id="page_name" value="{{ $pages->menu_name }}" required/>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="page_url" class="form-label">Page Url<span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="page_url" id="page_url" value="{{ $pages->menu_link }}" required/>
                                        </div>
                                    </div>
                                    <div class="row mb-4 col-md-4">
                                        <div class="col">
                                            <label for="page_order_no" class="form-label">Menu Order No <span style="color:red">*</span></label>
                                            <input type="number" class="form-control" name="page_order_no" id="page_order_no" value="{{ $pages->order_no }}" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-4">
                                    <button type="submit" value="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection