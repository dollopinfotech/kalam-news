@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Pages</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Pages</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ route('admin.createPages') }}" class="btn btn-primary">New Pages</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for page" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init menu-data-table" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Page Name</th>
                            <th>Menu Name</th>
                            <th>Page Url</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pages as $pagesData)
                        <tr>
                            <td>{{ $loop->iteration}}</td>
                            <td>{{ $pagesData->menu_name }}</td>
                            <td>{{ $pagesData->parent_menu_name }}</td>
                            <td>{{ $pagesData->menu_link }}</td>
                            <td>
                                <label class="form-check form-switch">
                                    <input type="checkbox" class="form-check-input is-valid" name="status" {{ $pagesData->status ? 'checked' : '' }} onchange="togglePageStatus(event, {{ $pagesData->menu_id }})">
                                </label>
                            </td>
                            <td>
                                <div class="d-flex">
                                    <form action="{{ route('admin.editPages', $pagesData->menu_id) }}" method="GET">
                                        <button class="btn btn-primary btn-sm me-1" type="submit">Edit</button>
                                    </form>
                                    <form class="deleteForm<?php echo $pagesData->menu_id; ?>" action="{{ route('admin.deletePages', $pagesData->menu_id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $pagesData->menu_id ?>');" type="button" >Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function togglePageStatus(event, menu_id) {
        var is_activeInput = event.target;
        var status = is_activeInput.checked;
        // Send AJAX request
        $.ajax({
            url: '/pageStatus/' + menu_id, // Corrected URL
            type: 'POST',
            data: {
                status: status ? '1' : '0', // Send '1' if checked, '0' if not
                _token: '{{ csrf_token() }}'
            },

            success: function(response) {
                // Handle success
                // $('#successText').text(response.success);
                // $('#statusMessage').fadeIn().delay(2000).fadeOut();
            }
        });
        document.location.reload(true);
    }
</script>
