@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Sub Menu</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Sub Menu</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ route('admin.createSubMenu') }}" class="btn btn-primary">New Sub Menu</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for sub menu" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init menu-data-table" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Sub Menu</th>
                            <th>Menu Name</th>
                            <th>Menu Url</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subMenu as $subMenuData)
                        <tr>
                            <td>{{ $loop->iteration}}</td>
                            <td>{{ $subMenuData->menu_name }}</td>
                            <td>{{ $subMenuData->parent_menu_name }}</td>
                            <td>{{ $subMenuData->menu_link }}</td>
                            <td>
                                <label class="form-check form-switch">
                                    <input type="checkbox" class="form-check-input is-valid" name="status" {{ $subMenuData->status ? 'checked' : '' }} onchange="togglesubMenuStatus(event, {{ $subMenuData->menu_id }})">
                                </label>
                            </td>
                            <td>
                                <div class="d-flex">
                                    <form action="{{ route('admin.editSubMenu', $subMenuData->menu_id) }}" method="GET">
                                        <button class="btn btn-primary btn-sm me-1" type="submit">Edit</button>
                                    </form>
                                    <form class="deleteForm<?php echo $subMenuData->menu_id; ?>" action="{{ route('admin.deleteSubMenu', $subMenuData->menu_id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $subMenuData->menu_id ?>');" type="button" >Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function togglesubMenuStatus(event, menu_id) {
        var is_activeInput = event.target;
        var status = is_activeInput.checked;
        // Send AJAX request
        $.ajax({
            url: '/subMenuStatus/' + menu_id, // Corrected URL
            type: 'POST',
            data: {
                status: status ? '1' : '0', // Send '1' if checked, '0' if not
                _token: '{{ csrf_token() }}'
            },

            success: function(response) {
                // $('#successText').text(response.success);
                // $('#statusMessage').fadeIn().delay(2000).fadeOut();
            }
        });
        document.location.reload(true);
    }
</script>
