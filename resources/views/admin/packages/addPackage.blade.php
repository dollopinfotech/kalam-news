@extends('layouts.app')
@section('content')
<style>
    /* Some basic styling, you can customize further */
    .form-group {
        margin-bottom: 20px;
    }

    .form-group label {
        display: block;
        margin-bottom: 5px;
    }

    .form-group input {
        width: 100%;
        padding: 8px;
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th,
    td {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    .ck-editor__editable {
        height: 200px !important;
    }
</style>

<!-- sa-app__body -->
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.show.packages') }}">Packages</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Add Packages</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Add Packages</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card mt-5">
                    <div class="card-body p-5">
                        <form id="package-form" action="{{ route('admin.store.packages') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-4 row">
                                <div class="col-md-4">
                                    <label for="package_name" class="form-label">Package Name<span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" id="package_name" name="package_name">
                                    <span id="package_name_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-md-4">
                                    <label for="package_price" class="form-label">Package Price<span style="color: red;">*</span></label>
                                    <input type="number" class="form-control" id="package_price" name="package_price" min="0">
                                    <span id="package_price_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-md-4">
                                    <label for="max_commission" class="form-label"> Max Commission<span style="color: red;">*</span></label>
                                    <input type="number" class="form-control" id="max_commission" name="max_commission" min="0">
                                    <span id="max_commission_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-md-4">
                                    <label for="package_image">Package Image<span style="color: red;">*</span></label>
                                    <input type="file" class="form-control" id="package_image" name="package_image" accept="image/*">
                                    <img id="preview_package_image" src="" alt="your image" class="mt-4" style="width: 25%; display:  none;" />
                                </div>

                                <div class="col-md-4">
                                    <label for="max_advertisement" class="form-label">Package Advertisement<span style="color: red;">*</span></label>
                                    <input type="number" class="form-control" id="max_advertisement" name="max_advertisement" min="0">
                                    <span id="max_advertisement_error" class="error_message" style="color: red;"></span>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="package_desc" class="form-label">Package Description <span style="color:red">*</span></label>
                                <textarea class="form-control" rows="8" cols="80" id="package_desc" name="package_desc"></textarea>
                                <span id="package_desc_error" class="error_message" style="color: red;"></span>
                            </div><br>


                            <h3>Package Commission Stages</h3>
                            <table id="commission-table">
                                <thead>
                                    <tr>
                                        <th>Stage</th>
                                        <th>Commission</th>
                                        <th>Number of Members</th>
                                        <th>Business Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <br>
                            <div class="col-md-12 text-center mb-4">
                                <button type="submit" value="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card mt-5">
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'package_desc' );
</script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        var totalStages = {{ $totalStages ?? 0 }};
        var tableBody = document.querySelector("#commission-table tbody");

        for (var i = 0; i < totalStages; i++) {
            var stage = i + 1;
            var row = tableBody.insertRow();

            var stageCell = row.insertCell(0);
            var commissionCell = row.insertCell(1);
            var membersCell = row.insertCell(2);
            var businessValueCell = row.insertCell(3);

            stageCell.innerHTML = '<input type="number" name="stages[' + i +
                '][stage_number]" class="stage_number" value="' + stage + '"  readonly>';

            commissionCell.innerHTML = '<input type="number" name="stages[' + i +
                '][commission]" class="commission" value="" >';


            var members = Math.pow(2, stage); // Dynamic member limit calculation
            membersCell.innerHTML = '<input type="number" name="stages[' + i +
                '][number_of_members]" class="number_of_members" value="' + members + '"  readonly>';

            businessValueCell.innerHTML = '<input type="number" disabled name="stages[' +
                i + '][business_value]" class="business_value" value="" >';


            // Event listener to calculate and update business value based on commission
            var commissionInput = commissionCell.querySelector('input');
            commissionInput.addEventListener('input', function(businessValueCell, members, stageIndex) {
                return function() {
                    var newCommission = parseFloat(this.value);
                    if (!isNaN(newCommission)) {
                        var business_value = (newCommission / members).toFixed(2);
                        businessValueCell.innerHTML = '<input type="number" name="stages[' +
                            stageIndex + '][business_value]" class="business_value" value="' +
                            business_value + '"  readonly>';
                    }
                };
            }(businessValueCell, members, i));

            commissionInput.addEventListener('keydown', function(event) {
                if (event.key === 'Tab') {
                    event.preventDefault();

                    var rowIndex = this.closest('tr').rowIndex; // Get row index

                    if (!event.shiftKey) {
                        var nextRow = tableBody.rows[rowIndex + 0]; // Get next row
                        if (nextRow) {
                            var nextCommissionInput = nextRow.cells[1].querySelector('input');
                            if (nextCommissionInput) {
                                nextCommissionInput.focus();
                            }
                        }
                    } else { // Shift+Tab pressed
                        var previousRow = tableBody.rows[rowIndex - 2]; // Get previous row
                        if (previousRow && rowIndex > 0) {
                            var previousCommissionInput = previousRow.cells[1].querySelector('input');
                            if (previousCommissionInput) {
                                previousCommissionInput.focus();
                            }
                        }
                    }
                }
            });

        }
        document.getElementById("package-form").addEventListener("submit", function(event) {
            // Handle form submission
        });
    });

    document.addEventListener("DOMContentLoaded", function() {
        const package_image = document.getElementById('package_image');
        const preview_package_image = document.getElementById('preview_package_image');

        package_image.onchange = evt => {
            const [file] = package_image.files;
            if (file) {
                preview_package_image.src = URL.createObjectURL(file);
                preview_package_image.style.display = 'block';
            }
        };
    });
</script>

@endsection
