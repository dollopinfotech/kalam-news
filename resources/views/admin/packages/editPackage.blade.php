@extends('layouts.app')
@section('content')
<style>
    /* Some basic styling, you can customize further */
    .form-group {
        margin-bottom: 20px;
    }

    .form-group label {
        display: block;
        margin-bottom: 5px;
    }

    .form-group input {
        width: 100%;
        padding: 8px;
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th,
    td {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    .ck-editor__editable {
        height: 200px !important;
    }
</style>
<!-- sa-app__body -->
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.show.packages') }}">Packages</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit Packages</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Edit Packages</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card mt-5">
                    <div class="card-body p-5">
                        <form id="editPackage-form" action="{{ route('admin.update.package', ['package_id' => $package->package_id]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-4 row">
                                <div class="col-md-4">
                                    <label for="package_name" class="form-label">Package Name<span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" id="package_name" name="package_name" value="{{ $package->package_name }}" required>
                                    <span id="package_name_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-md-4">
                                    <label for="package_price" class="form-label">Package Price<span style="color: red;">*</span></label>
                                    <input type="number" class="form-control" id="package_price" name="package_price" min="0" value="{{ $package->package_price }}" required>
                                    <span id="package_price_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-md-4">
                                    <label for="max_commission" class="form-label">Max Commission<span style="color: red;">*</span></label>
                                    <input type="number" class="form-control" id="max_commission" name="max_commission" min="0"
                                    value="{{ $package->max_commission }}">
                                    <span id="max_commission_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-md-4">
                                    <label for="package_image">Package Image<span style="color: red;">*</span></label>
                                    <input type="file" class="form-control" id="package_image" name="package_image" accept="image/*">
                                    <img id="preview_editpackage_image" src="{{ file_url($package->package_image) ?? '' }}" alt="Current Image" class="mt-4" style="width: 25%" />
                                </div>

                                <div class="col-md-4">
                                    <label for="max_advertisement" class="form-label">Package Advertisement<span style="color: red;">*</span></label>
                                    <input type="number" class="form-control" id="max_advertisement" name="max_advertisement" min="0" value="{{ $package->max_advertisement }}">
                                    <span id="max_advertisement_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-md-12">
                                    <label for="package_desc" class="form-label">Package Description <span style="color: red;">*</span></label>
                                    <textarea type="text" id="package_desc" name="package_desc" class="form-control" rows="4" cols="50">{{ $package->package_desc }}
                                    </textarea>
                                    <span id="package_desc_error" class="error_message" style="color: red;"></span>
                                </div>
                            </div>
                            <h3>Package Commission Stages</h3>
                            <table id="commission-table">
                                <thead>
                                    <tr>
                                        <th>Stage</th>
                                        <th>Commission</th>
                                        <th>Number of Members</th>
                                        <th>Business Value</th>
                                    </tr>
                                    @foreach($package->stages as $stage)
                                    <tr>
                                        <td><input type="number" name="stages[{{ $loop->index }}][stage_number]" class="stage_number" value="{{ $stage->stage_number }}" required readonly></td>
                                        <td><input type="number" name="stages[{{ $loop->index }}][commission]" class="commission" value="{{ $stage->commission }}" required></td>
                                        <td><input type="number" name="stages[{{ $loop->index }}][number_of_members]" class="number_of_members" value="{{ $stage->number_of_members }}" required readonly></td>
                                        <td><input type="number" name="stages[{{ $loop->index }}][business_value]" class="business_value" value="{{ $stage->business_value }}" required readonly></td>
                                    </tr>
                                    @endforeach
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary me-2">Submit</button>
                                <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card mt-5">
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'package_desc' );
</script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        var tableBody = document.querySelector("#commission-table tbody");

        // Function to calculate business value
        function calculateBusinessValue(commission, membersCell, businessValueCell) {
            var newCommission = parseFloat(commission);
            var members = parseFloat(membersCell.querySelector('input').value);
            if (!isNaN(newCommission)) {
                var businessValue = (newCommission / members).toFixed(2);
                businessValueCell.querySelector('input').value = businessValue;
            }
        }

        // Add event listener to each commission input field
        var commissionInputs = document.querySelectorAll('.commission');
        commissionInputs.forEach(function(commissionInput) {
            var businessValueCell = commissionInput.parentNode.nextElementSibling.nextElementSibling;
            var membersCell = businessValueCell.previousElementSibling;
            commissionInput.addEventListener('input', function() {
                calculateBusinessValue(commissionInput.value, membersCell, businessValueCell);
            });

            commissionInput.addEventListener('keydown', function(event) {
                if (event.key === 'Tab' && !event.shiftKey) {
                    event.preventDefault();
                    var nextCommissionInput = commissionInput.closest('tr').nextElementSibling.querySelector('.commission');
                    if (nextCommissionInput) {
                        nextCommissionInput.focus();
                    }
                } else if (event.key === 'Tab' && event.shiftKey) {
                    event.preventDefault();
                    var prevCommissionInput = commissionInput.closest('tr').previousElementSibling.querySelector('.commission');
                    if (prevCommissionInput) {
                        prevCommissionInput.focus();
                    }
                }
            });
        });

        document.getElementById("package-form").addEventListener("submit", function(event) {
            // Calculate business value for all commission inputs before form submission
            commissionInputs.forEach(function(commissionInput) {
                var businessValueCell = commissionInput.parentNode.nextElementSibling.nextElementSibling;
                var membersCell = businessValueCell.previousElementSibling;
                calculateBusinessValue(commissionInput.value, membersCell, businessValueCell);
            });

            // Form submission
            console.log("Form submitted!");
        });
    });
</script>







<script>
    package_image.onchange = evt => {
        const [file] = package_image.files
        if (file) {
            preview_editpackage_image.src = URL.createObjectURL(file)
        }
    }
</script>




@endsection
