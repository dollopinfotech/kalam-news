@extends('layouts.app')
@section('content')
<style>
    .status-message {
        position: fixed;
        top: 20px;
        right: 20px;
        z-index: 9999;
        color: #fff;
        background-color: rgba(55, 199, 86, 0.8);
        padding: 10px 20px;
        border-radius: 5px;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
        display: none;
        font-family: Arial, sans-serif;
    }

    .status-message i {
        margin-right: 10px;
    }

    .status-message i.fa-check {
        color: #000;
        /* Black color for the check icon */
    }

    .status-message.show {
        display: block;
        animation: fadeInAndOut 4s ease-in-out forwards;
    }

    @keyframes fadeInAndOut {

        0%,
        100% {
            opacity: 1;
        }

        50% {
            opacity: 0;
        }
    }
</style>
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Packages</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Packages</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ route('admin.create.packages') }}" class="btn btn-primary">New Package</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for package" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <div class="table-responsive" style="overflow-x: auto;">
                    <table id="menu-data-table" class="sa-datatables-init" data-sa-search-input="#table-search" style="min-width: 100%;">
                        <thead>
                            <tr>
                                <th>Sr.No.</th>
                                <th>Package Name</th>
                                <th>Price (<i class="fa fa-inr" aria-hidden="true"></i>)</th>
                                <th>Advertisement</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($packages as $package)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $package->package_name }}</td>
                                <td>{{ $package->package_price }}</td>
                                <td>{{ $package->max_advertisement }}</td>
                                <!-- HTML -->
                                <td>
                                    <label class="form-check form-switch">
                                        <input type="checkbox" class="form-check-input is-valid" name="is_active" {{ $package->is_active ? 'checked' : '' }} onchange="togglePackageStatus(event, {{ $package->package_id }})">
                                    </label>
                                    <div id="statusMessage" class="status-message">
                                        {{-- <i class="fa fa-check">Packages</i>
                                            <span id="successText"></span> --}}
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex">
                                        <a class="btn-info btn-sm me-1" href="{{ route('admin.showpackage', $package->package_id) }}" style="text-decoration:none">Show</a>
                                        <form action="{{ route('admin.edit.package', $package->package_id) }}" method="GET">
                                            <button class="btn btn-primary btn-sm me-1" type="submit">Edit</button>
                                        </form>
                                        <form class="deleteForm<?php echo $package->package_id; ?>" action="{{ route('admin.delete.package', $package->package_id) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $package->package_id ?>');" type="button">Delete</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    function togglePackageStatus(event, package_id) {
        var is_activeInput = event.target;
        var is_active = is_activeInput.checked;
        // Send AJAX request
        $.ajax({
            url: '/packageStatus/' + package_id, // Corrected URL
            type: 'POST',
            data: {
                is_active: is_active ? '1' : '0', // Send '1' if checked, '0' if not
                _token: '{{ csrf_token() }}'
            },

            success: function(response) {
                // $('#successText').text(response.success);
                // $('#statusMessage').fadeIn().delay(2000).fadeOut();
            }
        });
        document.location.reload(true);
    }
</script>
@endsection
