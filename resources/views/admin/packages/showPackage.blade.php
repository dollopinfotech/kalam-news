
@extends('layouts.app')
@section('content')
<style>
    p{
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        font-weight:normal;
    }
</style>
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.show.packages') }}">Packages</a></li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Show Packages</h1>
                    </div>
                    <div class="col-auto d-flex">
                        <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body p-5">
                    <div class="mb-4 row border">
                        <div class="col-md-4 text-center border">
                            <img src="{{ file_url($package->package_image) ?? '' }}" width="300px" height="300px" alt="Package Image">
                            <div class="mt-6">
                                <h4>{{ $package->package_name }}</h4>
                                <h4>Package price <i class="fa fa-inr" aria-hidden="true"></i> {{ $package->package_price }}</h4>
                                <h4>Max Commission <i class="fa fa-inr" aria-hidden="true"></i> {{ $package->max_commission }}</h4>
                            </div>
                        </div>
                        <div class="col-md-8 border">
                            <!-- <h4 class="text-center">Package Description :</h4> -->
                            <span class="h6 fw-normal text-justify"><?php echo htmlToPlainText($package->package_desc); ?></span>
                        </div>
                    </div>
                    <h3>Package Commission Stages</h3>
                    <table id="commission-table" class="table">
                        <thead>
                            <tr>
                                <th>Stage</th>
                                <th>Commission</th>
                                <th>Number of Members</th>
                                <th>Business Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($package->stages as $stage)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $stage->commission }}</td>
                                <td>{{ $stage->number_of_members }}</td>
                                <td>{{ $stage->business_value }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
