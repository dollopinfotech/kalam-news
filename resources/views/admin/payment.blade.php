@extends('layouts.app')
@section('content')
    <div id="top" class="sa-app__body">
        <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
            <div class="container">
                <div class="py-5">
                    <div class="row g-4 align-items-center">
                        <div class="col">
                            <nav class="mb-2" aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-sa-simple">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.users') }}">Users</a></li>

                                    <li class="breadcrumb-item active" aria-current="page">Payment History</li>
                                </ol>
                            </nav>
                            <h1 class="h3 m-0">Payment History</h1>
                        </div>
                        <div class="col-auto d-flex"><a href="{{ route('admin.users') }}" class="btn btn-primary">Back</a>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="p-4">
                        <input type="text" placeholder="Start typing to search for PaymentHistory"
                            class="form-control form-control--search mx-auto" id="table-search" />
                    </div>
                    <div class="sa-divider"></div>
                    <table class="sa-datatables-init users-data-table nowrap" data-sa-search-input="#table-search">
                        <thead>
                            <tr>
                                <th>Sr</th>
                                <th>User Name</th>
                                <th>Payment Type</th>
                                <th>Amount</th>
                                <th>Transaction Type</th>
                                <th>Transaction Id</th>
                                <th>Date</th>

                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($payments as $paymentData)
                            <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ optional($paymentData->user)->first_name }}</td>
                                <td>{{ $paymentData->payment_type }}</td>
                                <td>{{ $paymentData->amount }}</td>
                                <td>{{ $paymentData->transaction_type }}</td>
                                <td>{{ $paymentData->transaction_id }}</td>
                                <td>{{ $paymentData->created_at->format('Y-m-d') }}</td>

                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
