@extends('layouts.app')
@section('content')
<!-- sa-app__body -->
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container container-lg">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a>Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Profile</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
            <div class="row">

                <div class="col-md-6">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="user-avatar-section">
                                <div class="d-flex align-items-center flex-column">
                                    <a href="javascript:void(0);" data-fancybox="images" data-caption="Photo Name : User Photo">
                                        <img id="adminProfile" class="img-fluid rounded mb-3 pt-1 mt-4" src="{{ file_url($adminProfile->admin_profile) ?? '' }}" height="300" width="300" alt="Admin avatar">
                                    </a>
                                    <div class="user-info text-center">
                                        <h3 class="mb-2 mt-4">{{ $adminProfile->admin_name ?? 'Admin Name' }}</h3>
                                        <span class="badge bg-label-secondary mt-1">{{ $adminProfile->role ?? 'Admin Role' }}</span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="info-container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="list-unstyled">
                                            <li class="mb-2 pt-1">
                                                <span class="fw-semibold me-1"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                                <span> {{ $adminProfile->admin_email ?? 'Email not available' }}</span>
                                            </li>
                                            <hr>
                                            <li class="mb-2 pt-1">
                                                <span class="fw-semibold me-1"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                                <span> {{ $adminProfile->admin_mobile ?? 'Number not available' }}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="nav-align-top nav-tabs-shadow mb-4">
                                <div style=" margin-bottom: 10px; text-align: center;">
                                    <h3>
                                        Personal Info
                                    </h3>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="PersonalInfo" role="tabpanel">
                                        <form id="updateAdminPersonalInfo" action="{{ route('admin.update.profile') }}" method="post" enctype="multipart/form-data" onsubmit="return validateadminForm()">
                                            @csrf
                                            <div class="row g-3">
                                                <div class="">
                                                    <label class="form-label" for="admin_name">Name</label>
                                                    <input type="text" maxlength="40" name="admin_name" id="admin_name" class="form-control" value="{{ $adminProfile->admin_name ?? '' }}">
                                                    <span id="name_error" class="error_message" style="color: red;"></span>

                                                </div>
                                                <div class="">
                                                    <label class="form-label" for="admin_mobile">Mobile No</label>
                                                    <div class="">
                                                        <input type="text" maxlength="15" class="form-control onlyNumber" id="admin_mobile" name="admin_mobile" value="{{ $adminProfile->admin_mobile ?? '' }}">
                                                        <span id="number_error" class="error_message" style="color: red;"></span>

                                                    </div>
                                                </div>
                                                <div class="">
                                                    <label class="form-label" for="admin_email">Email</label>
                                                    <input type="email" maxlength="40" name="admin_email" id="admin_email" class="form-control" value="{{ $adminProfile->admin_email ?? '' }}">
                                                    <span id="email_error" class="error_message" style="color: red;"></span>

                                                </div>
                                                <div class="">
                                                    <label class="form-label" for="admin_profile">Profile</label>
                                                    <input type="file" class="form-control" id="admin_profile" name="admin_profile" accept="image/*" />
                                                    <img id="preview_profile_photo" src="{{ file_url($adminProfile->admin_profile) ?? '' }}" alt="your image" class="mt-4" style="width: 25%" />
                                                    <span id="profile_photo_error" class="error_message" style="color: red;"></span>

                                                </div>
                                                <hr>
                                                <div class="col md-12 text-center">
                                                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="card mt-5">
    </div>
</div>
<!-- sa-app__body / end -->

<script>
    admin_profile.onchange = evt => {
        const [file] = admin_profile.files
        if (file) {
            preview_profile_photo.src = URL.createObjectURL(file)
        }
    }
</script>
<script>
    function validateadminForm() {
        var name = document.getElementById("admin_name").value.trim();
        var email = document.getElementById("admin_email").value.trim();
        var number = document.getElementById("admin_mobile").value.trim();



        var isValid = true;

        // Perform validation for each field
        if (name === "") {
            document.getElementById("name_error").innerText = "Name is required.";
            isValid = false;
        }

        if (email === "") {
            document.getElementById("email_error").innerText = "Site email is required.";
            isValid = false;
        } else if (!/\S+@\S+\.\S+/.test(email)) {
            document.getElementById("email_error").innerText = "Please enter a valid email address.";
            isValid = false;
        }

        if (number === "") {
            document.getElementById("number_error").innerText = "Contact number is required.";
            isValid = false;
        } else if (!/^\d+$/.test(number)) {
            document.getElementById("number_error").innerText = "Contact number should contain only numbers.";
            isValid = false;
        }


        return isValid;
    }
</script>

@endsection
