@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.roles') }}">Roles</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add Roles</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Add Role</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                <div class="sa-entity-layout__body">
                    <div class="sa-entity-layout__main">
                        <form method="POST" action="{{ route('admin.storeRoles') }}" id="addRoleForm" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="card">
                                <div class="card-body p-5">
                                    <div class="row g-6 mb-4">
                                        <div class="col">
                                            <label for="role_name" class="form-label">Role Name <span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="role_name" id="role_name" value="" />
                                        </div>
                                        <div class="col">
                                            <label for="role_desc" class="form-label">Role Description <span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="role_desc" id="role_desc" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-4">
                                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection