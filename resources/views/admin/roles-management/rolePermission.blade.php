<?php

use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

?>
@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('admin.roles') }}">Roles</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Role Permission </li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Role Permission</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                <div class="sa-entity-layout__body">
                    <div class="sa-entity-layout__main">
                        <form method="POST" action="{{ route('admin.addPermission',$roles->role_id) }}" id="editRole" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="card">
                                <div class="card-body p-5">
                                    <div class="row g-6 mb-4">
                                        <div class="col">
                                            <label for="role_name" class="form-label">Role Name <span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="role_name" id="role_name" value="{{ $roles->role_name }}" readonly />
                                        </div>
                                        <div class="col">
                                            <label for="role_desc" class="form-label">Role Description<span style="color:red">*</span></label>
                                            <input type="text" class="form-control" name="role_desc" id="role_desc" value="{{ $roles->role_desc }}" readonly />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 offset-3">
                                        <div class="controls">
                                            <div class="text-center">
                                                <h6>
                                                    main_menu/permissions
                                                </h6>
                                                <input type="checkbox" id="user-checkbox" class="chk_boxes pagePrivilege" />
                                                <label for="user-checkbox">check/uncheck </label>
                                            </div>

                                            <?php
                                            $roleData = DB::table('roles')->where('role_id', '=', $roles->role_id)->get();
                                            foreach ($roleData as $privilegeData) {
                                                $menuPrivilege = $privilegeData->menu_id;
                                                $pagePrivilege = $privilegeData->page_privilage;
                                            }
                                            $menuPrivilege = explode(",", $menuPrivilege);
                                            $pagePrivilege = explode(",", $pagePrivilege);

                                            $menuId = $roles->menu_id;
                                            $menuId = explode(",", $menuId);
                                            $pagePrivilegeId = $roles->page_privilage;
                                            $pagePrivilegeId = explode(",", $pagePrivilegeId);

                                            $i = 1;
                                            $menuData = DB::table('menus')->where(['page_status' => 0, 'status' => 1])->get();
                                            foreach ($menuData as $dataMenu) {

                                                $menu_id = $dataMenu->menu_id;
                                                if ($dataMenu->sub_menu == 0) {
                                                    if ($dataMenu->parent_menu_id == 0 && $dataMenu->page_status == 0) { ?>
                                                        <div class="card-body">
                                                            <ul class="list-group ul_group">
                                                                <?php
                                                                if (in_array($menu_id, $menuPrivilege)) { ?>
                                                                    <!-- Menu Name that do not have any sub menu -->
                                                                    <li class="list-group-item list-group-item-primary">
                                                                        <label class="custom-control custom-checkbox error_color">
                                                                            <input <?php if (in_array($menu_id, $menuId)) {
                                                                                        echo "checked";
                                                                                    } ?> type="checkbox" class="pagePrivilege" value="<?php echo $dataMenu->menu_id; ?>" name="menu_id[]">
                                                                            <span class="custom-control-indicator"></span>
                                                                            <span class="custom-control-description"><b><?php echo $dataMenu->menu_name; ?></b></span>
                                                                        </label>
                                                                    </li>
                                                                <?php } else { ?>
                                                                    <li class="list-group-item list-group-item-primary">
                                                                        <label class="custom-control custom-checkbox error_color">
                                                                            <input <?php if (in_array($menu_id, $menuId)) {
                                                                                        echo "checked";
                                                                                    } ?> type="checkbox" class="pagePrivilege" value="<?php echo $dataMenu->menu_id; ?>" name="menu_id[]">
                                                                            <span class="custom-control-indicator"></span>
                                                                            <span class="custom-control-description"><b><?php echo $dataMenu->menu_name; ?></b></span>
                                                                        </label>
                                                                    </li>
                                                                    <?php }
                                                                $pageq = DB::table('menus')->where(['parent_menu_id' => $menu_id, 'page_status' => 1, 'status' => 1])->get();
                                                                foreach ($pageq as $pageData) {
                                                                    $page_id = $pageData->menu_id;
                                                                    if (in_array($page_id, $pagePrivilege)) {
                                                                    ?>
                                                                        <!-- page name -->
                                                                        <li class="list-group-item list-group-item-secondary">
                                                                            <label class="custom-control custom-checkbox error_color">
                                                                                <input <?php if (in_array($page_id, $pagePrivilegeId)) {
                                                                                            echo "checked";
                                                                                        } ?> type="checkbox" class="pagePrivilege" value="<?php echo $pageData->menu_id; ?>" name="pagePrivilege[]">
                                                                                <span class="custom-control-indicator"></span>
                                                                                <span class="custom-control-description"><b><?php echo $pageData->menu_name; ?></b></span>
                                                                            </label>
                                                                        </li>
                                                                    <?php } else { ?>
                                                                        <li class="list-group-item list-group-item-secondary">
                                                                            <label class="custom-control custom-checkbox error_color">
                                                                                <input <?php if (in_array($page_id, $pagePrivilegeId)) {
                                                                                            echo "checked";
                                                                                        } ?> type="checkbox" class="pagePrivilege" value="<?php echo $pageData->menu_id; ?>" name="pagePrivilege[]">
                                                                                <span class="custom-control-indicator"></span>
                                                                                <span class="custom-control-description"><b><?php echo $pageData->menu_name; ?></b></span>
                                                                            </label>
                                                                        </li>
                                                                <?php }
                                                                } ?>
                                                            </ul>
                                                        </div>
                                                        <?php }
                                                } else {
                                                    if ($dataMenu->sub_menu == 1) {
                                                        $ids = array();
                                                        $pageIds = array();
                                                        $ids = join("','", $menuPrivilege);
                                                        $pageIds = join("','", $pagePrivilege);

                                                        $subMenuq = DB::table('menus')->where(['parent_menu_id' => $menu_id, 'page_status' => 0, 'status' => 1])->get();

                                                        $s = DB::table('menus')->where(['parent_menu_id' => $menu_id, 'page_status' => 1, 'status' => 1])->get();

                                                        if ($subMenuq || $s) {
                                                        ?>
                                                            <div class="card-body">
                                                                <ul class="list-group ">
                                                                    <!-- Menu Name that have any sub menu -->
                                                                    <li class="list-group-item list-group-item-primary">
                                                                        <label class=" custom-checkbox error_color">
                                                                            <span class="custom-control-description"><b><?php echo $dataMenu->menu_name; ?></b></span>
                                                                        </label>
                                                                    </li>
                                                                    <!-- sub menu name -->
                                                                    <?php
                                                                    foreach ($subMenuq as $subMeneData) {
                                                                        $sub_menu_id = $subMeneData->menu_id;
                                                                        if (in_array($sub_menu_id, $menuPrivilege)) { ?>
                                                                            <li class="list-group-item list-group-item-success">
                                                                                <label class="custom-control custom-checkbox error_color">
                                                                                    <input <?php if (in_array($sub_menu_id, $menuId)) {
                                                                                                echo "checked";
                                                                                            } ?> type="checkbox" class="pagePrivilege" value="<?php echo $subMeneData->menu_id; ?>" name="menu_id[]">
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description"><?php echo $subMeneData->menu_name; ?></span>
                                                                                </label>
                                                                            </li>
                                                                        <?php } else { ?>
                                                                            <li class="list-group-item list-group-item-success">
                                                                                <label class="custom-control custom-checkbox error_color">
                                                                                    <input <?php if (in_array($sub_menu_id, $menuId)) {
                                                                                                echo "checked";
                                                                                            } ?> type="checkbox" class="pagePrivilege" value="<?php echo $subMeneData->menu_id; ?>" name="menu_id[]">
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description"><?php echo $subMeneData->menu_name; ?></span>
                                                                                </label>
                                                                            </li>
                                                                    <?php }
                                                                    } ?>
                                                                    <!-- page name -->
                                                                    <?php
                                                                    // echo $s; die;
                                                                    foreach ($s as $sData) {
                                                                        //    print_r($sData->menu_id); die;
                                                                        $page_id = $sData->menu_id;
                                                                        if (in_array($page_id, $pagePrivilege)) {
                                                                    ?>
                                                                            <li class="list-group-item list-group-item-secondary">
                                                                                <label class="custom-control custom-checkbox error_color">
                                                                                    <input <?php if (in_array($page_id, $pagePrivilegeId)) {
                                                                                                echo "checked";
                                                                                            } ?> type="checkbox" class="pagePrivilege" value="<?php echo $sData->menu_id; ?>" name="pagePrivilege[]">
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description"><?php echo $sData->menu_name; ?></span>
                                                                                </label>
                                                                            </li>
                                                                        <?php } else { ?>
                                                                            <li class="list-group-item list-group-item-secondary">
                                                                                <label class="custom-control custom-checkbox error_color">
                                                                                    <input <?php if (in_array($page_id, $pagePrivilegeId)) {
                                                                                                echo "checked";
                                                                                            } ?> type="checkbox" class="pagePrivilege" value="<?php echo $sData->menu_id; ?>" name="pagePrivilege[]">
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description"><?php echo $sData->menu_name; ?></span>
                                                                                </label>
                                                                            </li>
                                                                    <?php }
                                                                    } ?>
                                                                </ul>
                                                            </div>
                                            <?php       }
                                                    }
                                                }
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12 text-center mb-4">
                                    <button type="submit" value="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection