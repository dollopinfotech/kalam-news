@extends('layouts.app')
@section('content')
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Roles</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Roles</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ route('admin.addRoles') }}" class="btn btn-primary">New Roles</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for role" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init menu-data-table" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Role Name</th>
                            <th>Role discription</th>
                            <th>Role status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $serialNumber = 1;
                        @endphp
                        @foreach ($roles as $roleData)
                        <tr>
                            <td>{{ $serialNumber++ }}</td>
                            <td>{{ $roleData->role_name}}</td>
                            <td>{{ $roleData->role_desc}}</td>
                            <td>
                                <label class="form-check form-switch">
                                    <input type="checkbox" class="form-check-input is-valid" name="is_active" {{ $roleData->is_active ? 'checked' : '' }} onchange="toggleRolesStatus(event, {{ $roleData->role_id }})">
                                </label>
                            </td>
                            <td>
                                <div class="d-flex">
                                    <form action="{{ route('admin.rolePermission', $roleData->role_id) }}" method="GET">
                                        <button class="btn btn-primary btn-sm me-1" type="submit"><i class="fa fa-eye"></i></button>
                                    </form>
                                    <form action="{{ route('admin.editRole', $roleData->role_id) }}" method="GET">
                                        <button class="btn btn-primary btn-sm me-1" type="submit">Edit</button>
                                    </form>
                                    <form class="deleteForm<?php echo $roleData->role_id; ?>" action="{{ route('admin.destroyRole', $roleData->role_id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" onclick="deleteData('<?php echo $roleData->role_id ?>');" type="button">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
    function toggleRolesStatus(event, role_id) {
        var is_activeInput = event.target;
        var is_active = is_activeInput.checked;
        // Send AJAX request
        $.ajax({
            url: '/statusRole/' + role_id, // Corrected URL
            type: 'POST',
            data: {
                is_active: is_active ? '1' : '0', // Send '1' if checked, '0' if not
                _token: '{{ csrf_token() }}'
            },

            success: function(response) {
                // $('#successText').text(response.success);
                // $('#statusMessage').fadeIn().delay(2000).fadeOut();
            }
        });
        document.location.reload(true);
    }
</script>
