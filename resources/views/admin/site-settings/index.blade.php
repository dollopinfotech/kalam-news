@extends('layouts.app')
@section('content')
<style>
    .ck-editor__editable {
        height: 200px !important;
    }
</style>
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container container--max--lg">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Site Settings</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Site Settings</h1>
                    </div>
                </div>
            </div>
            <form action="{{ route('admin.add.site.settings') }}" method="POST" enctype="multipart/form-data" id="siteSettingsForm" onsubmit="return validateForm()">
                @csrf
                <div class="card">
                    <div class="card-body p-5">
                        <div class="mb-4">
                            <label for="site_name" class="form-label"><strong>Site Name</strong><span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="site_name" name="site_name" placeholder="Your Site Name" value="{{ old('site_name', $siteSettings->site_name ?? '') }}" />
                            <span id="site_name_error" class="error_message" style="color: red;"></span>
                        </div>
                        <div class="mb-4">
                            <label for="site_email" class="form-label"><strong>Site Email</strong><span style="color: red;">*</span></label>
                            <input type="email" class="form-control" id="site_email" name="site_email" placeholder="example@example.com" value="{{ $siteSettings->site_email ?? '' }}" />
                            <span id="site_email_error" class="error_message" style="color: red;"></span>
                        </div>
                        <div class="mb-4">
                            <label for="site_address" class="form-label"><strong>Site Address</strong><span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="site_address" name="site_address" placeholder="Your Site Address" value="{{ $siteSettings->site_address ?? '' }}" />
                            <span id="site_address_error" class="error_message" style="color: red;"></span>
                        </div>
                        <div class="mb-4">
                            <label for="contact_number" class="form-label"><strong>Contact Number</strong><span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="1234567890" value="{{ $siteSettings->contact_number ?? '' }}" />
                            <span id="contact_number_error" class="error_message" style="color: red;"></span>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body p-5">
                        <div class="mb-4">
                            <label for="logo" class="form-label"><strong>Logo</strong><span style="color: red;">*</span></label>
                            <input type="file" class="form-control" id="logo" name="logo" accept="image/*" />
                            <img id="preview_logo" src="{{ file_url($siteSettings->logo) ?? '' }}" alt="your image" class="mt-4" style="width: 25%" />
                            <span id="logo_error" class="error_message" style="color: red;"></span>
                        </div>
                        <div class="mb-4">
                            <label for="favicon" class="form-label"><strong>Favicon</strong><span style="color: red;">*</span></label>
                            <input type="file" class="form-control" id="favicon" name="favicon" accept="image/*" />
                            <img id="preview_favicon" src="{{ file_url($siteSettings->favicon) ?? '' }}" alt="your image" class="mt-4" style="width: 25%" />
                            <span id="favicon_error" class="error_message" style="color: red;"></span>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body p-5">
                        <div class="mb-4">
                            <label for="number_of_stage_level" class="form-label"><strong>Number of Stage Level</strong><span style="color: red;">*</span></label>
                            <input type="number" class="form-control" id="number_of_stage_level" name="number_of_stage_level" placeholder="5" value="{{ $siteSettings->number_of_stage_level ?? '' }}" />
                            <span id="number_of_stage_level_error" class="error_message" style="color: red;"></span>
                        </div>
                    </div>
                </div>

                <div class="card mt-5">
                    <div class="card-body p-5">
                        <h5>Customer Support</h5>
                        <hr>
                        <div class="mb-4 row">
                            <div class="col-md-6">
                                <label for="calling_number">Calling Number<span style="color: red;">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                    </div>
                                    <input type="text" class="form-control" id="calling_number" name="calling_number" value="{{ $siteSettings->calling_number ?? '' }}">
                                </div>
                                <span id="calling_number_error" class="error_message" style="color: red;"></span>
                            </div>
                            <div class="col-md-6">
                                <label for="whatsapp">WhatsApp<span style="color: red;">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-whatsapp"></i></span>
                                    </div>
                                    <input type="text" class="form-control" id="whatsapp" name="whatsapp" value="{{ $siteSettings->whatsapp ?? '' }}">
                                </div>
                                <span id="whatsapp_error" class="error_message" style="color: red;"></span>
                            </div>
                        </div>
                        <div class="mb-4 row">
                            <div class="col-md-6">
                                <label for="telegram">Telegram<span style="color: red;">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-telegram"></i></span>
                                    </div>
                                    <input type="text" class="form-control" id="telegram" name="telegram" value="{{ $siteSettings->telegram ?? '' }}">
                                </div>
                                <span id="telegram_error" class="error_message" style="color: red;"></span>
                            </div>
                            <div class="col-md-6">
                                <label for="email">Email<span style="color: red;">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $siteSettings->email ?? '' }}">
                                </div>
                                <span id="email_error" class="error_message" style="color: red;"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body p-5">
                        <h5>Welcome Message</h5>
                        <hr>
                        <div class="mb-4">
                            <textarea name="welcome_message" id="welcome_message" class="form-control" rows="8">{{ $siteSettings->welcome_message ?? '' }}</textarea>
                        </div>
                        <span id="welcome_message_error" class="error_message" style="color: red;"></span>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body p-5">
                        <h5>Terms & Conditions</h5>
                        <hr>
                        <div class="mb-4">
                            <textarea name="term_condition" id="term_condition" class="form-control" rows="8">{{ $siteSettings->term_condition ?? '' }}</textarea>
                        </div>
                        <span id="term_condition_error" class="error_message" style="color: red;"></span>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body p-5">
                        <h5>Invite Message <span style="color:red; font-size:12px"> (Please use REFERRAL_CODE key in message)</span></h5>
                        <hr>
                        <div class="mb-4">
                            <textarea name="invite_message" id="invite_message" class="form-control" rows="8">{{ $siteSettings->invite_message ?? '' }}</textarea>
                        </div>
                        <span id="invite_message_error" class="error_message" style="color: red;"></span>
                    </div>
                </div>
                <div class="col-auto d-flex" style="justify-content: flex-end; margin-top: 20px;">
                    <button type="submit" class="btn btn-primary" id="saveForm">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'term_condition' );
    CKEDITOR.replace( 'welcome_message' );
    CKEDITOR.replace( 'invite_message' );
</script>
<script>
    logo.onchange = evt => {
        const [file] = logo.files
        if (file) {
            preview_logo.src = URL.createObjectURL(file)
        }
    }
    favicon.onchange = evt => {
        const [file] = favicon.files
        if (file) {
            preview_favicon.src = URL.createObjectURL(file)
        }
    }
</script>
<script>
    function validateForm() {
        var site_name = document.getElementById("site_name").value.trim();
        var site_email = document.getElementById("site_email").value.trim();
        var site_address = document.getElementById("site_address").value.trim();
        var contact_number = document.getElementById("contact_number").value.trim();
        var logo = document.getElementById("logo").value.trim();
        var favicon = document.getElementById("favicon").value.trim();
        var number_of_stage_level = document.getElementById("number_of_stage_level").value.trim();
        var calling_number = document.getElementById("calling_number").value.trim();
        var whatsapp = document.getElementById("whatsapp").value.trim();
        var telegram = document.getElementById("telegram").value.trim();
        var email = document.getElementById("email").value.trim();
        var welcome_message =  document.getElementById("welcome_message").value;
        var term_condition =  document.getElementById("term_condition").value;
        var invite_message =  document.getElementById("invite_message").value;
        var isValid = true;

        // Perform validation for each field
        if (site_name === "") {
            document.getElementById("site_name_error").innerText = "Site name is required.";
            isValid = false;
        }

        if (site_email === "") {
            document.getElementById("site_email_error").innerText = "Site email is required.";
            isValid = false;
        }

        if (site_address === "") {
            document.getElementById("site_address_error").innerText = "Site address is required.";
            isValid = false;
        }

        if (contact_number === "") {
            document.getElementById("contact_number_error").innerText = "Contact number is required.";
            isValid = false;
        } else if (!/^\d+$/.test(contact_number)) {
            document.getElementById("contact_number_error").innerText = "Contact number should contain only numbers.";
            isValid = false;
        }

        if (number_of_stage_level === "") {
            document.getElementById("number_of_stage_level_error").innerText = "Number of stage level is required.";
            isValid = false;
        }
        var textPattern = /^[A-Za-z\s]+$/;
        var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        // Validation for calling number
        if (calling_number === "") {
            document.getElementById("calling_number_error").innerText = "Contact number is required.";
            isValid = false;
        } else if (!/^\d+$/.test(calling_number)) {
            document.getElementById("calling_number_error").innerText = "Contact number should contain only numbers.";
            isValid = false;
        } else {
            document.getElementById("calling_number_error").innerText = "";
        }

        // Validation for whatsapp
        if (whatsapp === "") {
            document.getElementById("whatsapp_error").innerText = "WhatsApp is required.";
            isValid = false;
        } else if (!/^\d+$/.test(whatsapp)) {
            document.getElementById("whatsapp_error").innerText = "WhatsApp should contain only numbers.";
            isValid = false;
        } else {
            document.getElementById("whatsapp_error").innerText = "";
        }
        // Validation for telegram
        if (telegram === "") {
            document.getElementById("telegram_error").innerText = "Telegram is required.";
            isValid = false;
        } else {
            document.getElementById("telegram_error").innerText = "";
        }

        // Validation for email
        if (email === "") {
            document.getElementById("email_error").innerText = "Email is required.";
            isValid = false;
        } else if (!emailPattern.test(email)) {
            document.getElementById("email_error").innerText = "Invalid email format.";
            isValid = false;
        } else {
            document.getElementById("email_error").innerText = "";
        }

        // Validation for Welcome Message
        if (welcome_message === "") {
            document.getElementById("welcome_message_error").innerText = "Welcome message is required.";
            isValid = false;
        } else {
            document.getElementById("welcome_message_error").innerText = "";
        }

        // Validation for Terms & Condition
        if (term_condition === "") {
            document.getElementById("term_condition_error").innerText = "Term & condition is required.";
            isValid = false;
        } else {
            document.getElementById("term_condition_error").innerText = "";
        }

        // Validation for Invite Message
        if (invite_message === "") {
            document.getElementById("invite_message_error").innerText = "Invite message is required.";
            isValid = false;
        } else if (!invite_message.includes("REFERRAL_CODE")) {
            document.getElementById("invite_message_error").innerText = "Please include REFERRAL_CODE variable in message";
            isValid = false;
        } else {
            document.getElementById("invite_message_error").innerText = "";
        }

        return isValid;
    }
</script>
@endsection