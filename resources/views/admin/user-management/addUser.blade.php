@extends('layouts.app')
@section('content')<style>
    h2 {
        color: rgb(32, 101, 157);
    }
</style>
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('adminRegisterUser') }}">Add User</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add User</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Add User</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
                <div class="sa-entity-layout__body">
                    <div class="sa-entity-layout__main">
                        <form method="POST" action="{{ route('admin.storeUser') }}" id="" enctype="multipart/form-data" onsubmit="return validateForm()">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                            <div class="card">
                                <div class="card-body p-5">
                                    <div class="row g-3 mt-3">
                                        <div class="col-sm-6">
                                            <label class="form-label" for="initials">Initials <span style="color:red">*</span></label>
                                            <select name="initials" class="form-control" id="initials">
                                                <option value="">Select Initials</option>
                                                <option value="Mr">Mr</option>
                                                <option value="Mrs">Mrs</option>
                                                <option value="Ms">Ms</option>
                                                <option value="Mst">Mst</option>
                                            </select>
                                            <span id="initials_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="first_name">First Name <span style="color:red">*</span></label>
                                            <input type="hidden" name="user_id" value="">
                                            <input type="text" maxlength="40" name="first_name" class="form-control" value="" id="first_name">
                                            <span id="first_name_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="middle_name">Middle Name</label>
                                            <input type="text" name="middle_name" class="form-control" value=" ">
                                            <span id="" class="error_message" style="color: red;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="last_name">Last Name <span style="color:red">*</span></label>
                                            <input type="text" maxlength="40" name="last_name" class="form-control" value="" id="last_name">
                                            <span id="last_name_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="dob">DOB</label>
                                            <input id="member_date_of_birth" type="text" class="form-control datepicker-here" name="dob" readonly data-language="en" data-date-format="d-MM-yyyy">
                                            <span id="dob_error" class="error_message" style="color: red;"></span>
                                        </div>


                                        <div class="col-sm-6">
                                            <label class="form-label" for="mobile_number">
                                                Mobile Number <span style="color:red">*</span>
                                            </label>
                                            <input id="mobile_number" type="text" class="form-control mobile_number" name="mobile_number" value="" onchange="checkUniqueMobileNumber(this.value)">
                                            <div id="mobile_number_message" style="color:red;"></div>
                                            <span id="mobile_number_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="whatsapp_number">
                                                WhatsApp Number
                                            </label>
                                            <input id="whatsapp_number" type="text" class="form-control whatsapp_number" name="whatsapp_number" value="">
                                            <div id="whatsapp_number_message" style="color:red;"></div>
                                        </div>

                                        <div class="col-sm-6">
                                            <label class="form-label" for="email">
                                                Email
                                            </label>
                                            <input id="email" type="text" class="form-control email" name="email" value="">
                                            <div id="email_message" style="color:red;"></div>
                                            <span id="email_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <!-- onchange="checkUniqueEmail(this.value)" -->
                                        <div class="col-sm-6">
                                            <label class="form-label" for="pincode">Pincode <span style="color:red">*</span></label>
                                            <input id="pincode" type="text" class="form-control pincode" name="pincode" value="">
                                            <span id="pincode_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="state_id">State <span style="color:red">*</span></label>
                                            <select id="state_id" class="form-control" name="state_id" onchange="getCities(this.value)">
                                                <option value="">Select State</option>
                                                @foreach ($states as $state)
                                                <option value="{{ $state->state_id }}">{{ $state->state_name }}
                                                </option>
                                                @endforeach
                                            </select>
                                            <span id="state_id_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="city_id">City <span style="color:red">*</span></label>
                                            <select id="city_id" class="form-control" name="city_id">
                                                <option value="">Select City</option>
                                            </select>
                                            <span id="city_id_error" class="error_message" style="color: red;"></span>
                                        </div>

                                        <div class="col-sm-6">
                                            <label class="form-label" for="address">Address <span style="color:red">*</span></label>
                                            <input id="address" type="text" class="form-control address" name="address" value="">
                                            <span id="address_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="referred_by">Referred By <span style="color:red">*</span></label>
                                            <input id="referred_by" type="text" class="form-control referred_by" name="referred_by" value="" onchange="checkReferralCode()">
                                            <span id="referral_code_error" class="error_message" style="color: red;"></span>
                                            <span id="referral_code_success" class="success_message" style="color: green;"></span>
                                            <br>
                                            <div class="col-sm-6">
                                            <label class="form-label" for="branch">Branch <span style="color:red">*</span></label>
                                                <div class="col-sm-12">
                                                    <input type="radio" id="branch" name="branch" value="left" class="me-2">
                                                    <label for="Leftbranch">Left</label>

                                                    <input type="radio" id="branch1" name="branch" value="right" class="ms-6 me-2">
                                                    <label for="Rightbranch">Right</label>
                                                </div>
                                                <span id="branch_error" class="error_message" style="color: red;"></span>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <label class="form-label" for="profile">Profile <span style="color:red">*</span></label>
                                            <input type="file" class="form-control" id="profile" name="profile" accept="image/png, image/jpg, image/jpeg" />
                                            <img id="preview_users_photo" src="" width="100" height="100" alt="" alt="your image" class="mt-4 @if (!isset($userData->profile)) d-none @endif" style="width: 25%" />
                                            <span id="profile_photo_error" class="error_message" style="color: red;"></span>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mb-4">
                                    <button type="submit" id="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.getElementById('profile').onchange = evt => {
            $("#preview_users_photo").removeClass('d-none');
            const [file] = evt.target.files;
            const preview_users_photo = document.getElementById('preview_users_photo');
            const profile_photo_error = document.getElementById('profile_photo_error');

            if (file) {
                preview_users_photo.src = URL.createObjectURL(file);
                profile_photo_error.textContent = "";
            } else {
                preview_users_photo.src = "";
            }
        };
    });

    $(document).ready(function() {
        var currentDate = new Date();
        var minimumDate = new Date(currentDate.getFullYear() - 18, currentDate.getMonth(), currentDate.getDate());
        var maximumDate = new Date(currentDate.getFullYear() - 100, currentDate.getMonth(), currentDate.getDate());

        $("#member_date_of_birth").datepicker({
            maxDate: minimumDate,
            minDate: maximumDate,
            beforeShowDay: function(date) {
                var age = currentDate.getFullYear() - date.getFullYear();
                var m = currentDate.getMonth() - date.getMonth();
                if (m < 0 || (m === 0 && currentDate.getDate() < date.getDate())) {
                    age--;
                }
                return [age >= 18, ''];
            }
        });
    });

    function validateForm() {
        var initials = document.getElementById("initials").value.trim();
        var first_name = document.getElementById("first_name").value.trim();
        var last_name = document.getElementById("last_name").value.trim();
        var mobile_number = document.getElementById("mobile_number").value.trim();
        var email = document.getElementById("email").value.trim();
        var referred_by = document.getElementById("referred_by").value.trim();
        var dob = document.getElementById("member_date_of_birth").value.trim();
        var address = document.getElementById("address").value.trim();
        var pincode = document.getElementById("pincode").value.trim();
        var profile = document.getElementById("profile").value.trim();
        var state_id = document.getElementById("state_id").value.trim();
        var city_id = document.getElementById("city_id").value.trim();
        var referral_code_error = document.getElementById("referral_code_success").value;
        var branch = document.getElementById("branch").checked;
        var branch1 = document.getElementById("branch1").checked;
        var textPattern = /^[A-Za-z\s]+$/;
        var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        var isValid = true;

        // Perform validation for each field
        if (initials === "") {
            document.getElementById("initials_error").innerText = "Please select initials";
            isValid = false;
        } else {
            document.getElementById("initials_error").innerText = "";
        }

        if (first_name === "") {
            document.getElementById("first_name_error").innerText = "Please enter first name";
            isValid = false;
        } else if (!textPattern.test(first_name)) {
            document.getElementById("first_name_error").innerText = "First name should contain only alphabet";
            isValid = false;
        } else {
            document.getElementById("first_name_error").innerText = "";
        }

        if (last_name === "") {
            document.getElementById("last_name_error").innerText = "Please enter last name";
            isValid = false;
        } else if (!textPattern.test(last_name)) {
            document.getElementById("last_name_error").innerText = "Last name should contain only alphabet";
            isValid = false;
        } else {
            document.getElementById("last_name_error").innerText = "";
        }

        if (mobile_number === "") {
            document.getElementById("mobile_number_error").innerText = "Please enter mobile number";
            isValid = false;
        } else if (!/^\d+$/.test(mobile_number)) {
            document.getElementById("mobile_number_error").innerText = "Mobile number should contain only numbers.";
            isValid = false;
        } else {
            document.getElementById("mobile_number_error").innerText = "";
        }

        // Validation for email
        if (email === "") {
            // document.getElementById("email_error").innerText = "Email is required.";
            // isValid = false;
        } else if (!emailPattern.test(email)) {
            document.getElementById("email_error").innerText = "Invalid email format.";
            isValid = false;
        } else {
            document.getElementById("email_error").innerText = "";
        }

        // Validate Referral code
        if (referred_by === "") {
            document.getElementById("referral_code_error").innerText = "Please enter referral code";
            isValid = false;
        }else if(referral_code_error === ""){
            document.getElementById("referral_code_error").innerText = "Please enter valid referral code";
        } else {
            document.getElementById("referral_code_error").innerText = "";
        }

        // validate date of birth
        if (dob === "") {
            document.getElementById("dob_error").innerText = "Please select date of birth";
            isValid = false;
        } else {
            document.getElementById("dob_error").innerText = "";
        }

        // validate address
        if (address === "") {
            document.getElementById("address_error").innerText = "Please enter address";
            isValid = false;
        } else {
            document.getElementById("address_error").innerText = "";
        }

        // validate pin code
        if (pincode === "") {
            document.getElementById("pincode_error").innerText = "Please enter pin code";
            isValid = false;
        } else {
            document.getElementById("pincode_error").innerText = "";
        }

        // validate state id
        if (state_id === "") {
            document.getElementById("state_id_error").innerText = "Please select state";
            isValid = false;
        } else {
            document.getElementById("state_id_error").innerText = "";
        }

        if (city_id === "") {
            document.getElementById("city_id_error").innerText = "Please select city";
            isValid = false;
        } else {
            document.getElementById("city_id_error").innerText = "";
        }

        if (profile === "") {
            document.getElementById("profile_photo_error").innerText = "Please select profile";
            isValid = false;
        } else {
            document.getElementById("profile_photo_error").innerText = "";
        }

        // validate branch 
        if(branch == false && branch1 == false){
            document.getElementById("branch_error").innerText = "Please select Branch";
            isValid = false;
        }else{
            document.getElementById("branch_error").innerText = "";
        }

        return isValid;
    }
</script>