@extends('layouts.app')
@section('content')
    <div id="top" class="sa-app__body">
        <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
            <div class="container">
                <div class="py-5">
                    <div class="row g-4 align-items-center">
                        <div class="col">
                            <nav class="mb-2" aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-sa-simple">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Users</li>
                                </ol>
                            </nav>
                            <h1 class="h3 m-0">Users</h1>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="p-4">
                        <input type="text" placeholder="Start typing to search for users"
                            class="form-control form-control--search mx-auto" id="table-search" />
                    </div>
                    <div class="sa-divider"></div>
                    <table class="sa-datatables-init users-data-table nowrap" data-sa-search-input="#table-search">
                        <thead>
                            <tr>
                                <th>Sr</th>
                                <th>Referral Code</th>
                                <th>Name</th>
                                <th>mobile No</th>
                                <th>Email</th>
                                <th>Node Count (L|R)</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $userData)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $userData->referral_code }}</td>
                                    <td>{{ $userData->first_name }} {{ $userData->middle_name }} {{ $userData->last_name }}
                                    </td>
                                    <td>{{ $userData->mobile_number }}</td>
                                    <td>{{ $userData->email }}</td>
                                    <td><?php $userNodeCount = getUserNodeCount($userData->user_id);
                                        echo $userNodeCount['l_user_count'] . ' | ' . $userNodeCount['r_user_count'];
                                    ?></td>
                                    <td>
                                        {{-- <a href="{{ route('admin.user', $userData->user_id) }}"title="User Details"><i
                                                class="fa fa-eye"></i></a>
                                        @php
                                            $userPackage = App\Models\UserPackage::where(
                                                'user_id',
                                                $userData->user_id,
                                            )->first();
                                        @endphp
                                        @if ($userPackage)
                                            <a
                                                href="{{ route('admin.showpackage', ['package_id' => $userPackage->package_id]) }}"title="Package Purchased"><i
                                                    class="fa fa-cubes"></i></a>
                                        @endif --}}
                                        <a href="{{ route('admin.user', $userData->user_id) }}" title="User Details"><i class="fa fa-eye"></i></a>
                                        @php
                                            $userPackage = App\Models\UserPackage::where('user_id', $userData->user_id)->first();
                                            $hasTransactions = App\Models\WalletHistory::where('user_id', $userData->user_id)->exists();
                                        @endphp
                                        @if ($userPackage)
                                            <a href="{{ route('admin.showpackage', ['package_id' => $userPackage->package_id]) }}" title="Package Purchased"><i class="fa fa-cubes text-danger"></i></a>
                                        @endif
                                        @if ($hasTransactions)
                                            <a href="{{ route('admin.user.transactions', $userData->user_id) }}" title="User Transaction Details"><i class="fa fa-bank text-warning"></i></a>
                                        @endif
                                       @if (isset($userData->l_user_id) || isset($userData->r_user_id))
                                            <a href="{{ route('admin.userTree', ['parent_user_id' => $userData->user_id]) }}" title="User Tree"><i
                                                    class="fa fa-sitemap text-success"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
