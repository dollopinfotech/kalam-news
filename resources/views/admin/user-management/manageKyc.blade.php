@extends('layouts.app')
@section('content')
<style>
    .preview {
        display: none;
        margin-left: 15px;
        width: 640px;
        height: 400px;
        border: 3px solid orange;
    }

    .cursor-overlay {
        display: none;
        position: fixed;
        pointer-events: none;
    }
</style>
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> -->
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Manage KYC</li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">Manage KYC</h1>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="p-4">
                    <input type="text" placeholder="Start typing to search for users" class="form-control form-control--search mx-auto" id="table-search" />
                </div>
                <div class="sa-divider"></div>
                <table class="sa-datatables-init users-data-table nowrap" data-sa-search-input="#table-search">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Referral Code</th>
                            <th>Name</th>
                            <th>mobile No</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($userDetails as $userData)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $userData->referral_code }}</td>
                            <td>{{ $userData->first_name }} {{ $userData->middle_name }} {{ $userData->last_name }}
                            </td>
                            <td>{{ $userData->mobile_number }}</td>
                            <td>{{ $userData->email }}</td>
                            <td>
                                <!-- <a href="{{ route('admin.user', $userData->user_id) }}" title="User Details"><i class="fa fa-eye"></i></a> -->
                                @php
                                $usersDocuments = App\Models\UsersDocuments::where('user_id', $userData->user_id)->orderBy('docs_id', 'DESC')->first();
                                $bankDetails = App\Models\BankDetail::where('user_id', $userData->user_id)->where('primary_bank', 1)->first();
                                @endphp
                                @if ($usersDocuments)
                                <a href="javascript:void(0)" title="Document KYC" onclick="adharPanKyc('{{ $usersDocuments->docs_id }}', '{{ $usersDocuments->aadhar_card_no }}', '{{ $usersDocuments->aadhar_card_photo_front }}', '{{ $usersDocuments->aadhar_card_photo_back }}', '{{ $usersDocuments->pan_card_no }}', '{{ $usersDocuments->pan_card_photo }}', '{{ $usersDocuments->is_aadhar_verified }}', '{{ $usersDocuments->is_pan_verified }}','{{ $usersDocuments->user_kyc_photo }}')"><i class="fa fa-id-card text-warning" aria-hidden="true"></i>
                                    @endif
                                    @if ($bankDetails)
                                    <a href="javascript:void(0)" title="Bank KYC" onclick="bankKyc('{{ $bankDetails->bank_id }}', '{{ $bankDetails->customer_name }}', '{{ $bankDetails->bank_name }}', '{{ $bankDetails->account_no }}', '{{ $bankDetails->ifsc_code }}', '{{ $bankDetails->bank_branch }}', '{{ $bankDetails->bank_passbook }}', '{{ $bankDetails->is_bank_account_verify }}','{{ $bankDetails->account_type }}','{{ $bankDetails->primary_bank }}')"><i class="fa fa-bank text-success"></i></a>
                                    @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="adharPanKycModal" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Aadhar & Pan KYC</h5>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="adharPanKycForm">
                    <div>
                        <input type="hidden" name="docs_id" id="docs_id" value="">
                    </div>
                    <div class="card mb-3">
                        <div class="card-body" id='card-copy'>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="aadhar_card_no" class="col-form-label">Aadhar Card No</label> <br>
                                    <span id="aadhar_card_no"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="aadhar_card_photo_front" class="col-form-label">Aadhar Card Photo (Front)</label>
                                    <img class="thumb" id="aadhar_card_photo_front" src="" alt="adhar Preview" class="mt-4" style="width: 80%">
                                </div>
                                <div class="col-md-3">
                                    <label for="aadhar_card_photo_back" class="col-form-label">Aadhar Card Photo (Back)</label>
                                    <img class="thumb" id="aadhar_card_photo_back" src="" alt="adhar Preview" class="mt-4" style="width: 80%">
                                </div>
                                <div class="col-md-3">
                                    <label for="is_aadhar_verified" class="col-form-label">Aadhar Card Verify <span style="color:red">*</span></label>
                                    <select name="is_aadhar_verified" class="form-control" id="is_aadhar_verified">
                                        <option value="0">Pending</option>
                                        <option value="1">Approve</option>
                                        <option value="2">Reject</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="pan_card_no" class="col-form-label">Pan Card No</label> <br>
                                    <span id="pan_card_no"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="pan_card_photo" class="col-form-label">Pan Card Photo (Front)</label>
                                    <img class="thumb" id="pan_card_photo" src="" alt="pan Preview" class="mt-4" style="width: 80%">
                                </div>
                                <div class="col-md-3">
                                    <label for="user_kyc_photo" class="col-form-label">User Profile (For KYC)</label>
                                    <img class="thumb" id="user_kyc_photo" src="" alt="kyc Preview" class="mt-4" style="width: 80%">
                                </div>
                                <div class="col-md-3">
                                    <label for="is_pan_verified" class="col-form-label">Pan Card Verify <span style="color:red">*</span></label>
                                    <select name="is_pan_verified" class="form-control" id="is_pan_verified">
                                        <option value="0">Pending</option>
                                        <option value="1">Approve</option>
                                        <option value="2">Reject</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mt-2">
                                <div class="cursor-overlay"></div>
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="changeAdharVerifyStatus()" data-bs-dismiss="modal">Update</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="bankKycModal" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Bank KYC</h5>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" id="bankKycForm">
                    <div>
                        <input type="hidden" name="bank_id" id="bank_id" value="">
                    </div>
                    <div class="card mb-3">
                        <div class="card-body" id='card-copy'>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="customer_name" class="col-form-label">Account Holder</label> <br>
                                    <span id="customer_name"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="bank_name" class="col-form-label">Bank Name</label> <br>
                                    <span id="bank_name"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="account_no" class="col-form-label">Account No</label> <br>
                                    <span id="account_no"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="ifsc_code" class="col-form-label">Ifsc Code</label> <br>
                                    <span id="ifsc_code"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="bank_branch" class="col-form-label">Branch Name</label> <br>
                                    <span id="bank_branch"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="account_type" class="col-form-label">Account Type</label> <br>
                                    <span id="account_type"></span>
                                </div>
                                <div class="col-md-3">
                                    <label for="bank_passbook" class="col-form-label">Bank Passbook</label>
                                    <img class="thumb" id="bank_passbook" src="" alt="bank Preview" class="mt-4" style="width: 80%">
                                </div>
                                <div class="col-md-3">
                                    <label for="is_bank_account_verify" class="col-form-label">Bank Account Verify <span style="color:red">*</span></label>
                                    <select name="is_bank_account_verify" class="form-control" id="is_bank_account_verify">
                                        <option value="0">Pending</option>
                                        <option value="1">Approve</option>
                                        <option value="2">Reject</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mt-2">
                                <div class="cursor-overlay"></div>
                                <div class="preview"></div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="changeBankVerifyStatus()" data-bs-dismiss="modal">Update</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    const ZOOM_LEVEL = 2;

    $(document).ready(function() {
        $(".thumb").mouseenter(enter);
        // $(".thumb").mouseleave(leave);
        $('.thumb').mousemove(zoom);
    });

    function zoom(event) {
        const p = calculateZoomOverlay({
            x: event.pageX,
            y: event.pageY
        }, $(event.target));
        moveCursorOverlay(p.left, p.top);
        movePreviewBackground(p.offsetX, p.offsetY);
    }

    function calculateZoomOverlay(mouse, thumb) {
        let t = thumb.position();
        t.width = thumb.width();
        t.height = thumb.height();

        let z = {}; // Zoom overlay
        z.width = t.width / ZOOM_LEVEL;
        z.height = t.height / ZOOM_LEVEL;
        z.top = mouse.y - z.height / 2;
        z.left = mouse.x - z.width / 2;

        // Bounce off boundary
        if (z.top < t.top) z.top = t.top;
        if (z.left < t.left) z.left = t.left;
        if (z.top + z.height > t.top + t.height) z.top = t.top + t.height - z.height;
        if (z.left + z.width > t.left + t.width) z.left = t.left + t.width - z.width;

        z.offsetX = (z.left - t.left) / z.width * 100;
        z.offsetY = (z.top - t.top) / z.height * 100;

        return z;
    }

    function moveCursorOverlay(left, top) {
        $('.cursor-overlay').css({
            top: top,
            left: left
        });
    }

    function movePreviewBackground(offsetX, offsetY) {
        $('.preview').css({
            'background-position': offsetX + '% ' + offsetY + '%'
        });
    }

    function enter() {
        // Setup preview image
        const imageUrl = $(this).attr('src');
        const backgroundWidth = $('.preview').width() * ZOOM_LEVEL;
        $('.preview').css({
            'background-image': `url(${imageUrl})`,
            'background-size': `${backgroundWidth} auto`
        });
        $('.preview').show();

        $('.cursor-overlay').width($(this).width() / ZOOM_LEVEL);
        $('.cursor-overlay').height($(this).height() / ZOOM_LEVEL);
        $('.cursor-overlay').show();
    }

    function leave() {
        $('.preview').hide();
        $('.cursor-overlay').hide();
    }


    function changeAdharVerifyStatus() {
        var formData = new FormData(document.getElementById("adharPanKycForm"));
        var is_aadhar_verified = document.getElementById("is_aadhar_verified").value.trim();
        var is_pan_verified = document.getElementById("is_pan_verified").value.trim();
        $.ajax({
            url: "changeAdharVerifyStatus",
            type: "POST",
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            contentType: false, // Prevent jQuery from setting Content-Type
            processData: false,
            success: function(result) {
                consol.log(result);
            }
        });
        document.location.reload(true);
    }

    function changeBankVerifyStatus() {
        var formData = new FormData(document.getElementById("bankKycForm"));
        var is_bank_account_verify = document.getElementById("is_bank_account_verify").value.trim();
        $.ajax({
            url: "changeBankVerifyStatus",
            type: "POST",
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            contentType: false,
            processData: false,
            success: function(result) {
                consol.log(result);
            }
        });
        document.location.reload(true);
    }
</script>
@endsection