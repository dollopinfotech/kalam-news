@extends('layouts.app')
@section('content')
<style>
    .bg_primary {
        background-color: #efeded;
    }

</style>
<div id="top" class="sa-app__body px-2 px-lg-4">
    <div class="container pb-6">

        <!-- Content wrapper -->
        {{-- <div class="content-wrapper"> --}}
        <!-- Content -->
        {{-- <div class="container-xxl flex-grow-1 container-p-y"> --}}
        <div class="py-5">
            <div class="row g-4 align-items-center">
                <div class="col">
                    <nav class="mb-2" aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-sa-simple">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.users') }}">Users</a></li>
                        </ol>
                    </nav>
                    <h1 class="h3 m-0">{{ $user->first_name }} {{ $user->middle_name }} {{ $user->last_name }}
                    </h1>
                </div>
                <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="sa-entity-layout__body">

                <!-- User Sidebar -->
                <div class="sa-entity-layout__sidebar">
                    <div class="card" style="margin-right: 5px;">
                        <div class="card-body d-flex flex-column align-items-center">
                            <div class="pt-3">
                                <div class="sa-symbol sa-symbol--shape--circle" style="--sa-symbol--size: 6rem">
                                    <img src="@if (file_url($user->profile)) {{ file_url($user->profile) }} @else {{ file_url('images/user-profile-icon.svg') }} @endif" width="96" height="96" alt="">

                                </div>
                            </div>
                            <div class="text-center mt-4">
                                <div class="fs-exact-16 fw-medium">{{ $user->first_name }}
                                    {{ $user->middle_name }}
                                    {{ $user->last_name }}
                                </div>
                                <div class="fs-exact-13 text-muted">
                                    <div class="mt-1"><span class="me-2"><i class="fa fa-envelope"></i></span>{{ $user->email }}
                                    </div>
                                    <div class="mt-1"><span class="me-2"><i class="fa fa-phone"></i></span>{{ $user->mobile_number }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ User Sidebar -->
                <div class="col-xl-8 col-lg-7 col-md-7 order-0 order-md-1">
                    <div class="nav-align-top nav-tabs-shadow mb-4">
                        <ul class="nav nav-tabs nav-fill" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#personalInfo" aria-controls="personalInfo" aria-selected="true"><i class="fa fa-user me-1" aria-hidden="true"></i>Personal Info</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#contactInfo" aria-controls="contactInfo" aria-selected="true"><i class="fa fa-address-book me-1" aria-hidden="true"></i>Contact Info</button>
                            </li>

                            <li class="nav-item" role="presentation">
                                <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#bankInfo" aria-controls="bankInfo" aria-selected="true"><i class="fa fa-bank me-1"></i>Bank Detail</button>
                            </li>
                            {{-- <li class="nav-item" role="presentation">
                                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                        data-bs-target="#userDocument" aria-controls="userDocument" aria-selected="true"><i
                                            class="fa fa-file me-1" aria-hidden="true"></i>Document</button>
                                </li> --}}
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="personalInfo" role="tabpanel">
                                <form id="updateEmpPersonalInfo" action="{{ route('admin.updateUser', $user->user_id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row g-3">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="personal_info" value="personal_info">
                                            <label class="form-label" for="first_name">First Name</label>
                                            <input type="hidden" name="user_id" value="">
                                            <input type="text" maxlength="40" name="first_name" class="form-control" value="{{ $user->first_name }} ">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="middle_name">Middle
                                                Name</label>
                                            <input type="text" maxlength="40" name="middle_name" class="form-control" value=" {{ $user->middle_name }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="last_name">Last Name</label>
                                            <input type="text" maxlength="40" name="last_name" class="form-control" value="{{ $user->last_name }}">
                                        </div>

                                        <div class="col-sm-6">
                                            <label class="form-label" for="dob">DOB</label>
                                            <input id="member_date_of_birth" type="text" class="form-control" name="dob" value="<?php echo date("d-m-Y", strtotime($user->dob)) ?>" data-language="en" data-date-format="dd-mm-yyyy" readonly>
                                        </div>

                                        <div class="">
                                            <label class="form-label" for="profile">Profile</label>
                                            <input type="file" class="form-control" id="profile" name="profile" accept="image/png, image/jpg, image/jpeg" value="{{ $user->profile ?? '' }}">

                                            <img id="preview_users_photo" src="@if (file_url($user->profile)){{ file_url($user->profile) }}@else{{ file_url('images/user-profile-icon.svg') }}@endisset" width="60" height="60" alt="Your image" class="mt-4">
                                            <span id="profile_photo_error" class="error_message" style="color: red;"></span>
                                        </div>

                                        <hr>
                                        <div class="col md-12 text-center mt-0">
                                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="contactInfo" role="tabpanel">
                                <form id="updateEmpContactDetailForm" action="{{ route('admin.updateUser', $user->user_id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row g-3">
                                        <div class="col-sm-6">
                                            <label class="form-label" for="mobile_number">Mobile
                                                Number</label>
                                            <input id="mobile_number" type="number" class="form-control mobile_number" name="mobile_number" value="{{ $user->mobile_number }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="whatsapp_number">WhatsApp
                                                Number</label>
                                            <input id="whatsapp_number" type="number" class="form-control whatsapp_number" name="whatsapp_number" value="{{ $user->whatsapp_number }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="email"> Email </label>
                                            <input id="email" type="email" class="form-control email" name="email" value="{{ $user->email }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="pincode">Pincode</label>
                                            <input id="pincode" type="text" class="form-control pincode" name="pincode" value="{{ $user->pincode }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="state_id">State</label>
                                            <select id="state_id" class="form-control" name="state_id" onchange="getCityList(this.value)">
                                                <option value="">Select State</option>
                                                @foreach ($states as $state)
                                                {{
                                                        $state->state_id ."ygvblui ".
                                                        $user->state_id
                                                    }}
                                                <option value="{{ $state->state_id }}" @if ($user->state_id == $state->state_id) {{ 'selected' }} @endif>
                                                    {{ $state->state_name }}
                                                </option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('state_id'))
                                            <div class="error-message">
                                                {{ $errors->first('state_id') }}
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="city_id">City</label>
                                            <select id="city_id" class="form-control" name="city_id">
                                                @foreach ($cities as $city)
                                                <option value="{{ $city->city_id }}" @if ($user->city_id == $city->city_id) {{ 'selected' }} @endif>
                                                    {{ $city->city_name }}
                                                </option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('city_id'))
                                            <div class="error-message">
                                                {{ $errors->first('city_id') }}
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="address">Address</label>
                                            <input id="address" type="text" class="form-control address" name="address" value="{{ $user->address }}">
                                        </div>
                                        <hr>
                                        <div class="col md-12 text-center mt-0">
                                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="BankInfo" role="tabpanel">
                                @if ($bankAccounts->isEmpty())
                                <div class="alert alert-info" role="alert">
                                    No bank accounts found.
                                </div>
                                @else
                                @foreach ($bankAccounts as $bankAccount)
                                <form id="updateEmpBankInfo" action="{{ route('admin.updateUserBank', $bankAccount->bank_id) }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row g-3">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="user_id" value="{{ $bankAccount->user_id }}">
                                            <input type="hidden" name="bank_id" value="{{ $bankAccount->bank_id }}">
                                            <label class="form-label" for="customer_name">Customer Name</label>
                                            <input id="customer_name" type="text" class="form-control customer_name" name="customer_name" value="{{ $bankAccount->customer_name }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="bank_name">Bank Name</label>
                                            <input id="bank_name" type="text" class="form-control bank_name" name="bank_name" value="{{ $bankAccount->bank_name }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="account_no">Account Number</label>
                                            <input id="account_no" type="text" class="form-control account_no" name="account_no" value="{{ $bankAccount->account_no }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="ifsc_code">IFSC Code</label>
                                            <input id="ifsc_code" type="text" class="form-control ifsc_code" name="ifsc_code" value="{{ $bankAccount->ifsc_code }}">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="form-label" for="bank_branch">Bank Branch</label>
                                            <input id="bank_branch" type="text" class="form-control bank_branch" name="bank_branch" value="{{ $bankAccount->bank_branch }}">
                                        </div>
                                        <hr>
                                        <div class="col md-12 text-center mt-0">
                                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                        </div>
                                    </div>
                                </form>
                                @endforeach
                                @endif
                            </div>
                            {{-- <div class="tab-pane fade" id="EmpDocument" role="tabpanel">
                                    @if ($userDocs->isEmpty())
                                        <div class="alert alert-info" role="alert">
                                            No documents found.
                                        </div>
                                    @else
                                        @foreach ($userDocs as $userDoc)
                                            <form id="updateEmpDocument"
                                                action="{{ route('admin.updateDocument', $userDoc->docs_id) }}"
                            method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row g-3">
                                <div class="col-sm-5">
                                    <label class="form-label" for="aadhar_card_no">Aadhar Card
                                        Number</label>
                                    <input id="aadhar_card_no" type="text" class="form-control aadhar_card_no" name="aadhar_card_no" value="{{ $userDoc->aadhar_card_no }}">
                                </div>
                                <div class="col-sm-6">
                                    <label class="form-label" for="aadhar_card_photo_front">Aadhar
                                        Card Photo Front</label>
                                    <input type="file" class="form-control" id="aadhar_card_photo_front" name="aadhar_card_photo_front" accept="image/*">

                                    <img id="preview_aadhar_card_photo_front" src="@isset($userDoc->aadhar_card_photo_front){{ asset($userDoc->aadhar_card_photo_front) }}@else{{ asset('images/user-aadhar_card_photo_front-icon.svg') }}@endisset" width="60" height="60" alt="Your image" class="mt-4" style="width: 25%">
                                    <span id="aadhar_card_photo_front_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-sm-6">
                                    <label class="form-label" for="aadhar_card_photo_back">Aadhar
                                        Card Photo Back</label>
                                    <input type="file" class="form-control" id="aadhar_card_photo_back" name="aadhar_card_photo_back" accept="image/*">

                                    <img id="preview_aadhar_card_photo_back" src="@isset($userDoc->aadhar_card_photo_back){{ asset($userDoc->aadhar_card_photo_back) }}@else{{ asset('images/user-aadhar_card_photo_back-icon.svg') }}@endisset" width="60" height="60" alt="Your image" class="mt-4" style="width: 25%">
                                    <span id="aadhar_card_photo_back_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="col-sm-3">
                                    <label class="form-label" for="pan_card_no">Pan Card
                                        Number</label>
                                    <input id="pan_card_no" type="pan_card_no" class="form-control pan_card_no" name="pan_card_no" value="{{ $userDoc->pan_card_no }}">
                                </div>
                                <div class="col-sm-6">
                                    <label class="form-label" for="pan_card_photo">Pan Card
                                        Photo</label>
                                    <input type="file" class="form-control" id="pan_card_photo" name="pan_card_photo" accept="image/*">
                                    <img id="preview_pan_card_photo" src="@isset($userDoc->pan_card_photo){{ asset($userDoc->pan_card_photo) }}@else{{ asset('images/user-pan_card_photo-icon.svg') }}@endisset" width="60" height="60" alt="Your image" class="mt-4" style="width: 25%">
                                    <span id="pan_card_photo_error" class="error_message" style="color: red;"></span>
                                </div>
                                <hr>
                                <div class="col md-12 text-center mt-0">
                                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                </div>
                            </div>
                            </form>
                            @endforeach
                            @endif
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}
    {{-- </div> --}}
</div>
</div>

@php
$citiesJson = $cities->toJson();
@endphp
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}

@endsection
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    var cities = <?php $citiesJson ?>

    function getCityList(stateId) {
        var citySelect = document.getElementById('city_id');
        citySelect.innerHTML = '<option value="">Select City</option>';

        cities.forEach(function(city) {
            if (city.state_id == stateId) {
                var option = document.createElement('option');
                option.value = city.city_id;
                option.textContent = city.city_name;
                citySelect.appendChild(option);
            }
        });
    }

    document.addEventListener("DOMContentLoaded", function() {
        document.getElementById('profile').onchange = evt => {
            const [file] = evt.target.files;
            const preview_users_photo = document.getElementById('preview_users_photo');
            const profile_photo_error = document.getElementById('profile_photo_error');

            if (file) {
                preview_users_photo.src = URL.createObjectURL(file);
                profile_photo_error.textContent = ""; // Clear any previous error message
            } else {
                preview_users_photo.src = ""; // Clear the preview if no file is selected
            }
        };
    });
    // document.addEventListener("DOMContentLoaded", function() {
    //     document.getElementById('aadhar_card_photo_front').onchange = evt => {
    //         const [file] = evt.target.files;
    //         const preview_aadhar_card_photo_front = document.getElementById(
    //             'preview_aadhar_card_photo_front');
    //         const aadhar_card_photo_front_error = document.getElementById('aadhar_card_photo_front_error');

    //         if (file) {
    //             preview_aadhar_card_photo_front.src = URL.createObjectURL(file);
    //             aadhar_card_photo_front_error.textContent = ""; // Clear any previous error message
    //         } else {
    //             preview_aadhar_card_photo_front.src = ""; // Clear the preview if no file is selected
    //         }
    //     };
    // });
    // document.addEventListener("DOMContentLoaded", function() {
    //     document.getElementById('aadhar_card_photo_back').onchange = evt => {
    //         const [file] = evt.target.files;
    //         const preview_aadhar_card_photo_back = document.getElementById(
    //             'preview_aadhar_card_photo_back');
    //         const aadhar_card_photo_back_error = document.getElementById('aadhar_card_photo_back_error');

    //         if (file) {
    //             preview_aadhar_card_photo_back.src = URL.createObjectURL(file);
    //             aadhar_card_photo_back_error.textContent = ""; // Clear any previous error message
    //         } else {
    //             preview_aadhar_card_photo_back.src = ""; // Clear the preview if no file is selected
    //         }
    //     };
    // });
    // document.addEventListener("DOMContentLoaded", function() {
    //     document.getElementById('pan_card_photo').onchange = evt => {
    //         const [file] = evt.target.files;
    //         const preview_pan_card_photo = document.getElementById('preview_pan_card_photo');
    //         const pan_card_photo_error = document.getElementById('pan_card_photo_error');

    //         if (file) {
    //             preview_pan_card_photo.src = URL.createObjectURL(file);
    //             pan_card_photo_error.textContent = ""; // Clear any previous error message
    //         } else {
    //             preview_pan_card_photo.src = ""; // Clear the preview if no file is selected
    //         }
    //     };
    // });

    // function getCityList(stateId) {
    //     alert(stateId);
    //     if (stateId) {
    //         fetch('/api/cities/' + stateId)
    //             .then(response => response.json())
    //             .then(data => {
    //                 const citySelect = document.getElementById('city_id');
    //                 citySelect.innerHTML = '<option value="">Select City</option>';
    //                 data.forEach(city => {
    //                     citySelect.innerHTML += `<option value="${city.id}">${city.name}</option>`;
    //                 });
    //             })
    //             .catch(error => console.error('Error fetching cities:', error));
    //     } else {
    //         const citySelect = document.getElementById('city_id');
    //         citySelect.innerHTML = '<option value="">Select City</option>';
    //     }
    // }
    // $("#member_date_of_birth").datepicker({
    //     maxDate: "-18Y"
    // });


    // $(document).ready(function() {
    //     var currentDate = new Date();
    //     var minimumDate = new Date(currentDate.getFullYear() - 18, currentDate.getMonth(), currentDate.getDate());
    //     var maximumDate = new Date(currentDate.getFullYear() - 100, currentDate.getMonth(), currentDate.getDate());

    //     $("#member_date_of_birth").datepicker({
    //         maxDate: minimumDate,
    //         minDate: maximumDate,
    //         beforeShowDay: function(date) {
    //             var age = currentDate.getFullYear() - date.getFullYear();
    //             var m = currentDate.getMonth() - date.getMonth();
    //             if (m < 0 || (m === 0 && currentDate.getDate() < date.getDate())) {
    //                 age--;
    //             }
    //             return [age >= 18, ''];
    //         }
    //     });
    // });

    $(document).ready(function() {
        var currentDate = new Date();
        var minimumDate = new Date(currentDate.getFullYear() - 18, currentDate.getMonth(), currentDate.getDate());
        var maximumDate = new Date(currentDate.getFullYear() - 100, currentDate.getMonth(), currentDate.getDate());

        $("#member_date_of_birth").datepicker({
            maxDate: minimumDate,
            minDate: maximumDate,
            dateFormat: 'D, MM d, yy', // Adjust date format
            yearRange: "-100:-18", // Specify the range of years
            changeYear: true,
            changeMonth: true,

        });
    });
</script>

