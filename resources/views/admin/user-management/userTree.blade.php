@extends('layouts.app')
@section('content')
<style>
    .tree {
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .node {
        border: 2px solid #ccc;
        border-radius: 12px;
        padding: 15px;
        margin: 20px;
        display: inline-block;
        position: relative;
        text-align: center;
        width: 218x;
    }

    .node img {
        border-radius: 50%;
        width: 60px;
        height: 60px;
        margin-bottom: 10px;
    }

    .node .name {
        font-weight: bold;
        margin: 0;
    }

    .node .underline {
        display: block;
        width: 50%;
        height: 2px;
        background: #000;
        margin: 5px auto;
    }

    .node .values {
        margin-top: 10px;
    }

    .line {
        width: 2px;
        height: 20px;
        background: #ccc;
        margin: 0 auto;
    }

    .children {
        display: flex;
        justify-content: space-around;
        width: 100%;
        text-align: center;
    }

    .node-top {
        background-color: #e0ffe0;
        border: 2px solid #00c851;
    }

    .node-left {
        background-color: #ffe0e0;
        border: 2px solid #ff4444;
    }

    .node-right {
        background-color: #e0e0ff;
        border: 2px solid #33b5e5;
    }

    .node p {
        margin: 5px 0;
        font-size: 16px;
        font-weight: normal;
        color: #333;
    }
</style>
<?php
$userDetailsArr = $data['user_details'];
$lUserArr = $data['lUser'];
$rUserArr = $data['rUser'];
?>
<div id="top" class="sa-app__body">
    <div class="mx-sm-2 px-2 px-sm-3 px-xxl-4 pb-6">
        <div class="container container">
            <div class="py-5">
                <div class="row g-4 align-items-center">
                    <div class="col">
                        <nav class="mb-2" aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-sa-simple">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">User Tree </li>
                            </ol>
                        </nav>
                        <h1 class="h3 m-0">User Tree</h1>
                    </div>
                    <div class="col-auto d-flex"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
            <div class="card" id="card">
                <div class="tree">
                    <div class="node node-top">
                        @if($userDetailsArr['profile'])
                        <img src="{{ file_url($userDetailsArr['profile']) ?? '' }}" alt="Profile">
                        @else
                        <img src="{{ file_url('images/user-profile-icon.svg') ?? '' }}" alt="Profile">
                        @endif
                        <p class="name text-center">{{ $userDetailsArr['first_name'] }} {{ $userDetailsArr['middle_name'] }} {{ $userDetailsArr['last_name'] }}</p>
                        <hr>
                        <p>Left BV: {{ $userDetailsArr['l_bv_count']}} | Right BV: {{ $userDetailsArr['r_bv_count']}}</p>
                    </div>
                    @if($data['parentUserId'] != '')
                    <span>1 Level below <a href="">Go back</a></span>
                    @endif
                    <!-- <div class="line"></div> -->
                    <div class="children">
                        <div class="col-md-6">
                            @if(!empty($lUserArr))
                            <div class="node node-left" <?php if ($lUserArr['l_bv_count'] > 0 || $lUserArr['r_bv_count'] > 0) { ?> style="cursor: pointer;" onclick="getUserTree(<?php echo $lUserArr['user_id']; ?>)" <?php } ?>>
                                @if($lUserArr['profile'])
                                <img src="{{ file_url($lUserArr['profile']) ?? '' }}" alt="Profile">
                                @else
                                <img src="{{ file_url('images/user-profile-icon.svg') ?? '' }}" alt="Profile">
                                @endif
                                <p class="name text-center">{{ $lUserArr['first_name'] }} {{ $lUserArr['middle_name'] }} {{ $lUserArr['last_name'] }}</p>
                                <hr>
                                <p>Left BV: {{ $lUserArr['l_bv_count']}} | Right BV: {{ $lUserArr['r_bv_count']}}</p>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            @if(!empty($rUserArr))
                            <div class="node node-right" <?php if ($rUserArr['l_bv_count'] > 0 || $rUserArr['r_bv_count'] > 0) { ?> style="cursor: pointer;" onclick="getUserTree(<?php echo $rUserArr['user_id']; ?>)" <?php } ?>>
                                @if($rUserArr['profile'])
                                <img src="{{ file_url($rUserArr['profile']) ?? '' }}" alt="Profile">
                                @else
                                <img src="{{ file_url('images/user-profile-icon.svg') ?? '' }}" alt="Profile">
                                @endif
                                <p class="name text-center">{{ $rUserArr['first_name'] }} {{ $rUserArr['middle_name'] }} {{ $rUserArr['last_name'] }}</p>
                                <hr>
                                <p>Left BV: {{ $rUserArr['l_bv_count']}} | Right BV: {{ $rUserArr['r_bv_count']}}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function getUserTree(user_id) {
        var parent_user_id = '<?php echo isset($_GET['parent_user_id']) ? $_GET['parent_user_id'] : ''; ?>';
        $.ajax({
            url: "userTree",
            type: "GET",
            dataType: "json",
            data: {
                user_id: user_id,
                parent_user_id: parent_user_id,
            },
            success: function(response) {
                var userDetailsArr = response.user_details;
                var lUserArr = response.lUser;
                var rUserArr = response.rUser;

                var html = `<div class="tree">
                    <div class="node node-top">`
                if (userDetailsArr.profile) {
                    html += `<img src={{ file_url('`+userDetailsArr.profile+`') ?? '' }} alt="Profile">`;
                } else {
                    html += `<img src={{ file_url('images/user-profile-icon.svg') ?? '' }} alt="Profile">`;
                }
                html += `<p class="name text-center">` + userDetailsArr.first_name + ' ';
                if (userDetailsArr.middle_name) {
                    html += userDetailsArr.middle_name + ' ';
                }
                html += userDetailsArr.last_name + `</p>
                        <hr>
                        <p>Left BV: ` + userDetailsArr.l_bv_count + ` | Right BV: ` + userDetailsArr.r_bv_count + `</p>
                    </div>`;
                if (response.parentUserId != '') {
                    html += `<span style="cursor: pointer;" onclick="getUserTree(` + response.parentUserId + `)">` + userDetailsArr.user_level + ` Level below <span style="color:blue">Go back</span></span>`;
                }
                html += `<div class="children">
                        <div class="col-md-6">`;
                if (lUserArr.user_id > 0) {
                    html += `<div class="node node-left"`;
                    if (lUserArr.l_bv_count > 0 || lUserArr.r_bv_count > 0) {
                        html += `style="cursor: pointer;" onclick="getUserTree(` + lUserArr.user_id + `)"`;
                    }
                    html += `>`;
                    if (lUserArr.profile) {
                        html += `<img src={{ file_url('` + lUserArr.profile + `') ?? '' }} alt="Profile">`;
                    } else {
                        html += `<img src="{{ file_url('images/user-profile-icon.svg') ?? '' }}" alt="Profile">`;
                    }
                    html += `<p class="name text-center">` + lUserArr.first_name + ' ';
                    if (lUserArr.middle_name) {
                        html += lUserArr.middle_name + ' ';
                    }
                    html += lUserArr.last_name + `</p>
                                <hr>
                                <p>Left BV: ` + lUserArr.l_bv_count + ` | Right BV: ` + lUserArr.r_bv_count + `</p>
                            </div>`;
                }
                html += `</div>
                        <div class="col-md-6">`;
                if (rUserArr.user_id > 0) {
                    html += `<div class="node node-right"`;
                    if (rUserArr.l_bv_count > 0 || rUserArr.r_bv_count > 0) {
                        html += `style="cursor: pointer;" onclick="getUserTree(` + rUserArr.user_id + `)"`;
                    }
                    html += `>`;
                    if (rUserArr.profile) {
                        html += `<img src={{ file_url('` + rUserArr.profile + `') ?? '' }} alt="Profile">`;
                    } else {
                        html += `<img src="{{ file_url('images/user-profile-icon.svg') ?? '' }}" alt="Profile">`;
                    }
                    html += `<p class="name text-center">` + rUserArr.first_name + ' ';
                    if (rUserArr.middle_name) {
                        html += rUserArr.middle_name + ' ';
                    }
                    html += rUserArr.last_name + `</p>
                                <hr>
                                <p>Left BV: ` + rUserArr.l_bv_count + ` | Right BV: ` + rUserArr.r_bv_count + `</p>
                            </div>`;
                }
                html += `</div>
                    </div>
                </div>`;
                var card = document.getElementById("card").innerHTML = html;
            },
            error: function(xhr) {
                console.log(xhr.responseText); // Handle error
            },
        });
    }
</script>