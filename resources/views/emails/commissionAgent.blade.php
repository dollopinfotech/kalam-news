<!DOCTYPE html>
<html>

<head>
    <title>Download Our App</title>
    <style>
        body {
            font-family: Helvetica, Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }

        .container {
            width: 100%;
            padding: 20px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .email_templates {
            max-width: 600px;
            width: 100%;
            padding: 20px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .logo {
            text-align: center;
            margin-bottom: 20px;
        }

        .logo img {
            height: 60px;
            width: 60px;
        }

        .background_img {
            background: url('https://kalamnewspublication.in/images/emailIcon/background-img.jpg');
            padding: 40px 20px;
            border-radius: 10px;
            text-align: center;
            margin-bottom: 20px;
        }

        .background_img h1 {
            color: #fff;
            font-size: 24px;
            margin: 0;
        }

        .email_details {
            padding: 0 20px;
        }

        .email_details p {
            color: #384860;
            font-size: 16px;
            line-height: 24px;
            margin-bottom: 20px;
        }

        .sub-heading {
            font-size: 18px;
            font-weight: 700;
            color: #2563EB;
            margin-bottom: 20px;
        }

        .get_app_download {
            display: flex;
            justify-content: space-between;
            margin-bottom: 20px;
        }

        .social_media_btn {
            padding: 10px;
            border-radius: 8px;
            display: flex;
            align-items: center;
            text-align: left;
            cursor: pointer;
            width: 48%;
            box-sizing: border-box;
        }

        .social_media_btn_fill {
            background-color: #000;
            color: #fff;
        }

        .social_media_btn_outline {
            background-color: transparent;
            border: 1px solid #000;
            color: #000;
        }

        .social_media_btn img {
            margin-right: 10px;
        }

        .social_media_btn h2 {
            font-size: 12px;
            margin: 0;
        }

        .social_media_btn h3 {
            font-size: 16px;
            margin: 0;
            font-weight: 500;
        }

        .bottom_section {
            text-align: center;
            margin-top: 20px;
        }

        .primary_btn {
            background-color: #2563EB;
            color: #fff;
            padding: 10px 20px;
            border-radius: 12px;
            display: inline-flex;
            align-items: center;
            text-align: left;
            text-decoration: none;
            margin-bottom: 10px;
        }

        .primary_btn img {
            margin-right: 10px;
        }

        .bottom_btn h2 {
            font-size: 14px;
            margin: 0;
        }

        .bottom_btn h3 {
            font-size: 14px;
            margin: 0;
            font-weight: 500;
        }

        .bottom_card_section {
            background: url('https://kalamnewspublication.in/images/emailIcon/background-img.jpg');
            padding: 30px 20px;
            border-radius: 10px;
            text-align: center;
            margin-top: 20px;
        }

        .bottom_card_heading {
            color: #FFF;
            font-size: 20px;
            font-weight: 800;
            margin-bottom: 20px;
        }

        .social_media_icon img {
            width: 30px;
            margin: 0 10px;
        }

        .bottom_card_section h3 {
            color: #FFF;
            font-size: 12px;
            line-height: 16px;
            font-weight: 400;
        }

        @media (max-width: 575px) {
            .email_templates {
                padding: 15px;
            }

            .email_details p,
            .sub-heading,
            .social_media_btn h2,
            .social_media_btn h3,
            .bottom_btn h2,
            .bottom_btn h3 {
                font-size: 14px;
            }

            .get_app_download {
                flex-direction: column;
            }

            .social_media_btn {
                width: 100%;
                margin-bottom: 10px;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="email_templates">
            <div class="background_img">
                <div class="logo">
                    <img src="www.kalamnewspublication.in/images/emailIcon/knplogo.png" alt="Kalam News Publication Logo">
                </div>
                <h1>KALAM NEWS PUBLICATION</h1>
            </div>
            <div class="email_details">
                <p>Hi Jack,</p>
                <p>We’re glad to have you onboard! You’re already on your way to creating beautiful visual products.</p>
                <h2 class="sub-heading">Download the app now</h2>
                <div class="get_app_download">
                    <div class="social_media_btn social_media_btn_fill">
                        <img src="https://kalamnewspublication.in/images/emailIcon/playstore.svg" alt="Google Play">
                        <div>
                            <h2>GET IT ON</h2>
                            <h3>Google Play</h3>
                        </div>
                    </div>
                    <div class="social_media_btn social_media_btn_outline">
                        <img src="https://kalamnewspublication.in/images/emailIcon/mac.svg" alt="App Store">
                        <div>
                            <h2>Download on the</h2>
                            <h3>App Store</h3>
                        </div>
                    </div>
                </div>
                <p>Please register yourself after downloading the app by using the referral code:</p>
                <h2 class="sub-heading">BH09UGRA</h2>
                <p>If you have any inquiries, feel free to contact us at:</p>
                <div class="bottom_section">
                    <a href="mailto:kalamnews@gmail.com" class="primary_btn">
                        <img src="https://kalamnewspublication.in/images/emailIcon/gmail.svg" alt="Email Icon">
                        <div class="bottom_btn">
                            <h2>Email Us</h2>
                            <h3>kalamnews@gmail.com</h3>
                        </div>
                    </a>
                    <a href="tel:+919876543211" class="primary_btn">
                        <img src="https://kalamnewspublication.in/images/emailIcon/phone.svg" alt="Phone Icon">
                        <div class="bottom_btn">
                            <h2>Contact Us</h2>
                            <h3>+91 - 9876543211</h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="background_img bottom_card_section">
                <h2 class="bottom_card_heading">THANK YOU</h2>
                <div class="social_media_icon">
                    <img src="https://kalamnewspublication.in/images/emailIcon/twitter.svg" alt="Twitter">
                    <img src="https://kalamnewspublication.in/images/emailIcon/facebook.svg" alt="Facebook">
                    <img src="https://kalamnewspublication.in/images/emailIcon/linkedin.svg" alt="LinkedIn">
                </div>
                <h3>Copyright © 2024<br>
                    Kalam News Rewards & Recognition.<br>
                    A better company begins with a personalized employee experience.</h3>
            </div>
        </div>
    </div>
</body>

</html>
