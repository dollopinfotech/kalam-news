@extends('layouts.app')
@section('content')

<div id="top" class="sa-app__body">
    <div class="sa-error">
        <div class="sa-error__background-text">Oops! Error 404</div>
        <div class="sa-error__content">
            <h1 class="sa-error__title">Page Not Found</h1>
            <a class="btn btn-secondary btn-sm" href="{{ route('admin.dashboard') }}">Go To Home Page</a>
        </div>
    </div>
</div>
@endsection