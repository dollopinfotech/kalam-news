@extends('layouts.guest')
@section('content')
<!--For Advertisement-->
<div class="container">
    <div class="contact_box mb_32">
        <h2 class="mb_32">{{ __('For Advertisement') }}</h2>
        <div class="contact_form_card">
            <div class="row contact_img">
                <div class="col-md-6 contact_form_box">
                    <form enctype="multipart/form-data" id="advertisementForm">
                        <div class="contact_form">
                            <div class="inputs">
                                <div class="user_name">
                                    <label for="first_name">{{ __('First Name') }}<span style="color: red;">*</span></label>
                                    <input type="text" name="first_name" id="first_name" placeholder="{{ __('First Name') }}">
                                    <span id="first_name_error" class="error_message" style="color: red;"></span>
                                </div>
                                <div class="user_name">
                                    <label for="last_name">{{ __('Last Name') }}<span style="color: red;">*</span></label>
                                    <input type="text" id="last_name" name="last_name" placeholder="{{ __('Last Name') }}">
                                    <span id="last_name_error" class="error_message" style="color: red;"></span>
                                </div>
                            </div>
                            <div>
                                <label for="user_email">{{ __('Email') }}<span style="color: red;">*</span></label>
                                <input type="email" id="user_email" class="w-100 user_email" name="user_email" placeholder="you@company.com">
                                <span id="user_email_error" class="error_message" style="color: red;"></span>
                            </div>
                            <div>
                                <label for="user_number">{{ __('Mobile number') }}<span style="color: red;">*</span></label>
                                <div>
                                    <div class="select_option d-flex justify-content-center align-items-center position-absolute">
                                        <select class="font-14 font-600" aria-label="Default select example" name="country_code">
                                            <option selected="" value="91" class="select_option">{{ __('IND') }}</option>
                                        </select>
                                    </div>
                                    <input type="text" class="w-100 input_text_number position-relative user_number" id="user_number" name="user_number">
                                    <span id="user_number_error" class="error_message" style="color: red;"></span>
                                </div>
                            </div>
                            <div>
                                <label for="package_id">{{ __('Select packages') }}<span style="color: red;">*</span></label>
                                <div class="col-auto">
                                    <input type="hidden" name="package_id" id="package_id">
                                    {{ __('Click to select package...') }}
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#packageModal">{{ __('View Package') }}</a>
                                </div>
                                <span id="show_package_span" class="success_message" style="color: green;"></span>
                                <span id="package_id_error" class="error_message" style="color: red;"></span>
                            </div>
                            <div>
                                <label for="title">{{ __('Advertisement title') }}<span style="color: red;">*</span></label>
                                <input type="text" class="w-100 title" id="title" name="title" placeholder="{{ __('Advertisement title') }}">
                                <span id="title_error" class="error_message" style="color: red;"></span>
                            </div>
                            <div class="contact_form gap-0">
                                <label for="description">{{ __('Description') }}<span style="color: red;">*</span></label>
                                <textarea id="description" name="description" rows="4" cols="50"></textarea>
                                <span id="description_error" class="error_message" style="color: red;"></span>
                            </div>
                            <div class="contact_form gap-0">
                                <label class="form-label" for="adv_document">{{ __('Document') }}</label>
                                <input type="file" class="form-control" id="adv_document" name="adv_document" accept="image/*,video/*,audio/*,.pdf,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" />
                                <span id="preview_adv_document"></span>
                            </div>
                            <div class="mb-3 d-flex align-items-center gap-2">
                                <input type="checkbox" id="privacy_policy" name="privacy_policy" value="1">
                                <label for="privacy_policy" class="agreement mb-0">{{ __('You agree to our friendly') }} <a href="#" data-bs-toggle="modal" data-bs-target="#privacyModal"><u>{{ __('privacy policy') }}</u></a>.</label><br>
                                <span id="privacy_policy_error" class="error_message" style="color: red;"></span>
                            </div>
                            <button type="button" class="primary_btn medium-text  Medium px-x py-2 rounded-3" id="submit">{{ __('Send message') }}</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-5  contact_form_box">
                    <img src="{{ file_url('welcome_assets/images/png-img/Advertisement.png') ?? '' }}" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Show All Package Modal -->
<div class="modal fade" id="packageModal" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">{{ __('Package Details') }}</h5>
            </div>
            <div class="modal-body">
                @foreach ($packageDetails as $package)
                <a href="#" onclick="GetPackageDetails('{{ $package->package_id }}', '{{ $package->package_name }}', '{{ $package->package_price }}')" name="package_id" data-bs-dismiss="modal">
                    <div class="col-md-12 m-2">
                        <div class="card">
                            <div class="d-flex">
                                <div class="col-md-5 text-center">
                                    <img src="{{ file_url($package->package_image) ?? '' }}" alt="Package Image" style="height:auto; width:100%">
                                    <div class="mt-6">
                                        <h6>{{ $package->package_name }}</h6>
                                        <h6>{{ __('Package price') }}: {{ $package->package_price }}</h6>
                                        <h6>{{ __('Max Advertisement') }}: {{ $package->max_advertisement }}</h6>
                                    </div>
                                </div>
                                <div class="col-md-6 ms-2">
                                    <span class="h6 fw-normal text-justify"><?php echo htmlToPlainText($package->package_desc); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>

<!-- Show Privacy Policy -->
<div class="modal fade" id="privacyModal" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">{{ __('Privacy & Policy') }}</h5>
            </div>
            <div class="modal-body">
                <div class="col-md-12 m-2">
                        <div class="other_content">
                            <p>{{ __('All the educational Packages listed on the website') }} <a href="https://www.kalamnewspublication.in/" rel="external nofollow noopener" target="_blank">https://www.kalamnewspublication.in/</a>, {{ __('access to and use of the website and mobile(Android/IOS) application and the information, materials, products and services available through the Kalam news publication Website are subject to these terms of use (the Website Terms of Use).') }}</p>

                            <p>{{ __('Along with that, for KNP Commission agent , the documents and guidelines mentioning the Distributorship Contract apply and for any purchases of goods or services, the KNP Refund Policy applies.') }}</p>

                            <p>{{ __('The Website Terms of Use may be updated as per changes in business or as per government regulations. Updated versions will be posted on the KNP Website and Mobile Applications. Customers and Commission agent are requested to check the terms regularly.') }}</p>

                            <p>{{ __('The Kalam news publication Website and Mobile Applications provides information on KNP , the KNP Business and KNP Educational and information products and Packages, is intended only for use from and in India only and is based on the laws of Indian Government. Usage of the services or website outside India is not allowed and if used any claims resulting out of it will be not responded by Kalam news publication and its associated teams.') }}</p>

                            <ol>
                                <li>
                                    <p><strong>{{ __('Kalam news publication Privacy Terms and Policies') }}</strong>​​​​​​​<br />
                                        {{ __('All personal data provided to KNP while using the Website or Mobile Applications will be handled in accordance with the Website Privacy Notice. If you register or log in as an commission agent, the Privacy Policy for Commission agent applies in addition else as a customer the policies will be applied.') }}<br />
                                        {{ __('All information provided on KNP Website or Mobile application must be correct, complete, and up to date. If we have reason to believe that incorrect, incomplete or outdated information has been provided, access to the KNP Website or Mobile Application may be limited or blocked. If you intend to update any information please contact customer support.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Copyright and Use of Kalam news publication Website and Mobile application Materials') }}</strong><br />
                                        {{ __('The Kalam news publication Website, Mobile Application and Content and Packages made available are protected by intellectual property rights, including copyrights, trade names and trademarks, including the name Kalam news publication and the KNP logo, and are owned by Kalam news publication or used by KNP under a license or with permission from the owner of such rights. Materials protected by such intellectual property rights include the design, layout, look, appearance, graphics, photos, images, articles, stories and other content available on the Kalam news publication Website and Mobile Application.') }}<br />
                                        {{ __('Any data or information on KNP website or Mobile Application may only be reproduced, distributed, published or otherwise publicly presented based on a prior written consent by Kalam news publication . If you are a commission agent , KNP allows an exception and grants Commission agent a limited, non-exclusive, revocable license to use Website Content and KNP Packages solely for the purposes of operating their Commission agent Business by downloading, storing, printing, copying, sharing and displaying Website Materials, provided that the Material is unaltered and the source of information is quoted in case any Website Materials are disclosed to third parties. Should you have additional questions on the use of the Website or Mobile Application Materials, please contact us through the Contact Us form present on the Contact Us page on website or connect to our customer support.') }}<br />
                                        {{ __('In the event of termination thereof, the Commission agent must delete or destroy all stored, printed or copied materials, unless they must be retained to comply with legal requirements and same should be informed to Kalam news publication immediately.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Links to external Websites and your personal information on such websites') }}</strong><br />
                                        {{ __('We may link to other websites which are not within our control. We are not responsible or liable for the information or materials made available by such third party websites. We encourage you to read the terms of use and privacy statements of all third party websites before using such websites or submitting any personal data or any other information on or through such websites.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Links from Other Websites to the Kalam news publication Website') }}</strong><br />
                                        {{ __('Inserting links from a third-party website to') }} <a href="https://www.kalamnewspublication.in/" rel="external nofollow noopener" target="_blank">https://www.kalamnewspublication.in/</a> {{ __('requires prior written consent from Kalam news publication . If you would like to link from other websites, please contact us.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Password Protected Parts of the Kalam news publication Website') }}</strong><br />
                                        {{ __('The password-protected parts of the KNP Website or Mobile Application are intended exclusively for Commission agent and Customers in India. If you do not already know an commission agent and wish to contact one, please Contact us.') }}<br />
                                        {{ __('We take personal information very seriously and all your passwords are encrypted in our systems. Passwords should not be given to third parties and must be protected from unauthorized access. If you become aware of any unauthorized use of your password, you should notify Kalam news publication immediately. KNP do not take any responsibility for damage caused as a result of improper use of passwords.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Limitation of Liability, Disclaimer of Warranties and Indemnification') }}</strong><br />
                                        {{ __('To the extent permitted by applicable law, neither KNP nor its affiliates shall be liable for any direct, indirect, consequential or other damages whatsoever, including but not limited to property damage, loss of use, loss of business, economic loss, loss of data or loss of profits, arising out of or in connection with your use or access to, or inability to use or access the Kalam news publication Website, Mobile Applications or its content.') }}<br />
                                        {{ __('KNP will use reasonable efforts to ensure that the information and materials provided on this Website are correct. However, KNP cannot guarantee the accuracy of all information and materials and does not assume any responsibility or liability for the accuracy, completeness or authenticity of any information and materials contained on this Website or Mobile Application.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('KNP Website and Mobile Application Software and Technical Information') }}</strong><br />
                                        {{ __('We do not warrant that the operation of this Website or Mobile Application will be uninterrupted or error-free, or that this Website or Mobile Application is free from viruses or other components that may be harmful to equipment or software. KNP does not guarantee that the KNP Website and Mobile Application will be compatible with the equipment and software which you may use and does not guarantee that the will be available all the time or at any specific time.') }}<br />
                                        {{ __('You agree to indemnify, defend and hold KNP and its affiliates harmless from any liability or loss, related to either your violation of these Website Terms of Use or your use of the Kalam news publication Website and Mobile Application.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Restricting or Blocking Access to the KNP Website, Mobile Application for Violations of the Website Terms of Use') }}</strong><br />
                                        {{ __('In case of a violation of these Website Terms of Use, particularly in case of use of the KNP Website or individual elements of the KNP Website anf Mobile Application for other than its intended use, access to the KNP Website and Mobile Application may be restricted or blocked.') }}<br />
                                        {{ __('KNP reserves the right to partially or entirely alter, block, or discontinue the KNP Website, Mobile Application or its content at any time and for any reason.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Severability Clause') }}</strong><br />
                                        {{ __('Should one of the provisions of these Website Terms of Use be invalid or declared invalid by a court, this will not affect the validity of the remaining terms.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Choice of law, Jurisdiction and Venue') }}</strong><br />
                                        {{ __('The use of the Kalam news publication Website, Mobile Application and these Terms of Use are governed by the laws of the Indian Government. The courts of Madhya Pradesh High court have exclusive jurisdiction and venue for any disputes arising from or in connection with the use of the Kalam news publication Website, Mobile Application or these Website Terms of Use. Jurisdiction will be at shehdol district court.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Notice') }}</strong><br />
                                        {{ __('If you have any comments or inquiries about these Terms of use, if you would like to update information we have about you, or to exercise your rights, you may contact our support team vie form present in Contact us page.') }}
                                    </p>
                                </li>
                            </ol>

                            <p></p>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        $('#submit').click(function() {
            validateForm();
        });
    });

    function validateForm() {
        var first_name = document.getElementById("first_name").value.trim();
        var last_name = document.getElementById("last_name").value.trim();
        var user_email = document.getElementById("user_email").value.trim();
        var user_number = document.getElementById("user_number").value.trim();
        var package_id = document.getElementById("package_id").value.trim();
        var title = document.getElementById("title").value.trim();
        var description = document.getElementById("description").value.trim();
        var privacy_policy = document.getElementById("privacy_policy").checked;
        var adv_document = document.getElementById('adv_document').value;

        var isValid = true;
        var textPattern = /^[A-Za-z\s]+$/;
        var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        // Validation for calling number
        if (user_number === "") {
            document.getElementById("user_number_error").innerText = "{{ __('Please Enter Mobile Number') }}";
            isValid = false;
        } else if (!/^\d+$/.test(user_number)) {
            document.getElementById("user_number_error").innerText = "{{ __('Mobile Number Should Contain Only Numbers') }}";
            isValid = false;
        } else {
            document.getElementById("user_number_error").innerText = "";
        }

        // Validation for email
        if (user_email === "") {
            document.getElementById("user_email_error").innerText = "{{ __('Please Enter Email Id') }}";
            isValid = false;
        } else if (!emailPattern.test(user_email)) {
            document.getElementById("user_email_error").innerText = "{{ __('Invalid Email Format') }}";
            isValid = false;
        } else {
            document.getElementById("user_email_error").innerText = "";
        }

        // Validation for First Name
        if (first_name === "") {
            document.getElementById("first_name_error").innerText = "{{ __('Please Enter First Name') }}";
            isValid = false;
        } else if (!textPattern.test(first_name)) {
            document.getElementById("first_name_error").innerText = "{{ __('First Name Should Contain Only Alphabet') }}";
            isValid = false;
        } else {
            document.getElementById("first_name_error").innerText = "";
        }

        // Validation for Last Name
        if (last_name === "") {
            document.getElementById("last_name_error").innerText = "{{ __('Please Enter Last Name') }}";
            isValid = false;
        } else if (!textPattern.test(last_name)) {
            document.getElementById("last_name_error").innerText = "{{ __('Last Name Should Contain Only Alphabet') }}";
            isValid = false;
        } else {
            document.getElementById("last_name_error").innerText = "";
        }

        // Validation for Title
        if (title === "") {
            document.getElementById("title_error").innerText = "{{ __('Please Enter Advertisement Title') }}";
            isValid = false;
        } else {
            document.getElementById("title_error").innerText = "";
        }

        // Validation for Description
        if (description === "") {
            document.getElementById("description_error").innerText = "{{ __('Please Enter Description') }}";
            isValid = false;
        } else {
            document.getElementById("description_error").innerText = "";
        }

        // Validation for Privacy Policy
        if (!privacy_policy) {
            document.getElementById("privacy_policy_error").innerText = "{{ __('Please Accept Privacy Policy') }}";
            isValid = false;
        } else {
            document.getElementById("privacy_policy_error").innerText = "";
        }

        // Validation for Package
        if (package_id === "") {
            document.getElementById("package_id_error").innerText = "{{ __('Please Select Package') }}";
            isValid = false;
        } else {
            document.getElementById("package_id_error").innerText = "";
        }

        if (isValid) {
            formSubmit();
        }
    }

    function formSubmit() {
        var formData = new FormData(document.getElementById("advertisementForm"));
        var first_name = document.getElementById("first_name").value.trim();
        var last_name = document.getElementById("last_name").value.trim();
        var user_email = document.getElementById("user_email").value.trim();
        var user_number = document.getElementById("user_number").value.trim();
        $.ajax({
            url: "addAdvertisement",
            type: "POST",
            dataType: "json",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            contentType: false, // Prevent jQuery from setting Content-Type
            processData: false,
            success: function(result) {
                if (result.advertisement != "") {
                    var options = {
                        "key": "{{ env('RAZORPAY_KEY_ID') }}", // Enter the Key ID generated from the Dashboard
                        "amount": (result.userPackage.package_price * 100), // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                        "currency": "INR",
                        "name": "Kalam News Publication",
                        "description": "Advertisement Transaction",
                        "image": "{{ file_url('images/knplogo.png') }}",
                        "prefill": {
                            "name": first_name + ' ' + last_name,
                            "email": user_email,
                            "contact": user_number
                        },
                        "notes": {
                            "address": "Kalam News Publication"
                        },
                        "theme": {
                            "color": "#3399cc"
                        },
                        "handler": function(response) {
                            console.log(response);
                            $.ajax({
                                url: "razorpayPayment",
                                type: "POST",
                                dataType: "json",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {
                                    'razorpay_payment_id': response.razorpay_payment_id,
                                    'amount': (result.userPackage.package_price * 100),
                                    'name': first_name,
                                    'req_id': result.advertisement.req_id
                                },
                                success: function(data) {
                                    console.log(data);
                                },
                            });
                            document.location.reload(true);
                        },
                        "modal": {
                            "ondismiss": function() {
                                console.log("Modal closed");
                            }
                        },
                        "error": function(response) {
                            console.log("ERROR:::::::::::::");
                        }
                    };

                    var rzp1 = new Razorpay(options);
                    rzp1.open();
                    // e.preventDefault();
                }
            }
        });
    };

    function GetPackageDetails(package_id, package_name, package_price) {
        document.getElementById("package_id").value = package_id;
        document.getElementById("show_package_span").innerHTML = "{{ __('Your selected package is') }}" + " " + package_name + " " + "{{ __('and price is') }}" + " " + package_price;
    }

    // Assuming adv_document is an input element for file selection
    adv_document.onchange = evt => {
        const previewElement = document.getElementById("preview_adv_document");
        previewElement.innerHTML = ""; // Clear any existing content

        const [file] = adv_document.files;
        console.log(file.type);

        if (file.type === 'image/jpg' || file.type === 'image/png' || file.type === 'image/jpeg') {
            const img = document.createElement("img"); // Create img element
            img.src = URL.createObjectURL(file); // Set source to file URL
            img.alt = "document";
            img.classList.add("mt-4");
            img.style.width = "25%";
            previewElement.appendChild(img); // Append img to preview element
        } else if (file.type === 'video/mp4') {
            const video = document.createElement("video"); // Create video element
            video.width = 400;
            video.controls = true;
            const source = document.createElement("source"); // Create source element
            source.src = URL.createObjectURL(file); // Set source to file URL
            source.type = "video/mp4";
            video.appendChild(source); // Append source to video
            previewElement.appendChild(video); // Append video to preview element
        } else if (file.type === 'application/pdf') {
            const iframe = document.createElement("iframe"); // Create iframe element
            iframe.src = URL.createObjectURL(file); // Set source to file URL
            iframe.width = "100%";
            iframe.height = "500px";
            previewElement.appendChild(iframe); // Append iframe to preview element
        } else if (file.type === 'application/msword' || file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            // Create a download link for DOC and DOCX files
            const downloadLink = document.createElement("a");
            downloadLink.href = URL.createObjectURL(file);
            downloadLink.textContent = "{{ __('Download Document') }}";
            previewElement.appendChild(downloadLink); // Append download link to preview element
        } else {
            const unsupportedMessage = document.createElement("p"); // Create paragraph element
            unsupportedMessage.textContent = "{{ __('Unsupported file format') }}"; // Message for unsupported format
            previewElement.appendChild(unsupportedMessage); // Append message to preview element
        }
    };
</script>
@endsection