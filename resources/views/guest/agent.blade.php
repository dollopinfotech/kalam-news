@extends('layouts.guest')
@section('content')
<!-- section-4  -->
<div class="container">
    <div class="contact_box mb_32">
        <h2 class="mb_32">{{ __('Join with us') }}</h2>
        <div class="contact_form_card">
            <div class="row contact_img">
                <div class="col-md-5 contactus_imgs">
                    <img src="{{ file_url('welcome_assets/images/png-img/agents.png') }}" class="img-fluid">
                </div>
                <div class="col-md-6  contact_form_box">
                    <form action="{{ route('admin.addCommissionAgent') }}" id="commisionForm" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="contact_form">
                            <!-- name -->
                            <div class="inputs">
                                <div class="user_name">
                                    <label for="first_name">{{ __('First Name') }}<span style="color: red;"> *</span></label>
                                    <input type="text" class="first_name" name="first_name" placeholder="{{ __('first name')}}">
                                </div>
                                <div class="user_name">
                                    <label for="last_name">{{ __('Last Name') }}<span style="color: red;"> *</span></label>
                                    <input type="text" class="last_name" name="last_name" placeholder="{{__('last name')}}">
                                </div>
                            </div>
                            <!-- email -->
                            <div>
                                <label for="user_email">{{ __('Email') }}<span style="color: red;"> *</span></label>
                                <input type="email" class="w-100 user_email" name="user_email" placeholder="you@company.com">
                            </div>
                            <!-- number -->
                            <div>
                                <label for="user_number">{{ __('Phone Number') }}<span style="color: red;"> *</span></label>
                                <div>
                                    <div class="select_option d-flex justify-content-center align-items-center position-absolute">
                                        <select id="country_code" name="country_code" class="font-14 font-600" aria-label="Default select example">
                                            <option selected="" value="+91" class="select_option">{{ __('IND') }}</option>
                                        </select>
                                    </div>
                                    <input type="tel" class="w-100 input_text_number position-relative" id="user_number" name="user_number" placeholder="9876543210">
                                </div>
                            </div>
                            <div>
                                <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
                                <label for="privacy_policy" class="agreement mb-0">{{ __('You agree to our friendly') }} <a href="#" data-bs-toggle="modal" data-bs-target="#privacyModal"><u>{{ __('privacy policy') }}</u></a>.</label><br>
                            </div>

                            <button class="primary_btn medium-text  Medium px-x py-2 rounded-3">{{ __('Send message') }}</button>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Show Privacy Policy -->
<div class="modal fade" id="privacyModal" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">{{ __('Privacy & Policy') }}</h5>
            </div>
            <div class="modal-body">
                <div class="col-md-12 m-2">
                    <div class="other_content">
                        <p>{{ __('All the educational Packages listed on the website') }} <a href="https://www.kalamnewspublication.in/" rel="external nofollow noopener" target="_blank">https://www.kalamnewspublication.in/</a>, {{ __('access to and use of the website and mobile(Android/IOS) application and the information, materials, products and services available through the Kalam news publication Website are subject to these terms of use (the Website Terms of Use).') }}</p>

                        <p>{{ __('Along with that, for KNP Commission agent , the documents and guidelines mentioning the Distributorship Contract apply and for any purchases of goods or services, the KNP Refund Policy applies.') }}</p>

                        <p>{{ __('The Website Terms of Use may be updated as per changes in business or as per government regulations. Updated versions will be posted on the KNP Website and Mobile Applications. Customers and Commission agent are requested to check the terms regularly.') }}</p>

                        <p>{{ __('The Kalam news publication Website and Mobile Applications provides information on KNP , the KNP Business and KNP Educational and information products and Packages, is intended only for use from and in India only and is based on the laws of Indian Government. Usage of the services or website outside India is not allowed and if used any claims resulting out of it will be not responded by Kalam news publication and its associated teams.') }}</p>

                        <ol>
                            <li>
                                <p><strong>{{ __('Kalam news publication Privacy Terms and Policies') }}</strong>​​​​​​​<br />
                                    {{ __('All personal data provided to KNP while using the Website or Mobile Applications will be handled in accordance with the Website Privacy Notice. If you register or log in as an commission agent, the Privacy Policy for Commission agent applies in addition else as a customer the policies will be applied.') }}<br />
                                    {{ __('All information provided on KNP Website or Mobile application must be correct, complete, and up to date. If we have reason to believe that incorrect, incomplete or outdated information has been provided, access to the KNP Website or Mobile Application may be limited or blocked. If you intend to update any information please contact customer support.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Copyright and Use of Kalam news publication Website and Mobile application Materials') }}</strong><br />
                                    {{ __('The Kalam news publication Website, Mobile Application and Content and Packages made available are protected by intellectual property rights, including copyrights, trade names and trademarks, including the name Kalam news publication and the KNP logo, and are owned by Kalam news publication or used by KNP under a license or with permission from the owner of such rights. Materials protected by such intellectual property rights include the design, layout, look, appearance, graphics, photos, images, articles, stories and other content available on the Kalam news publication Website and Mobile Application.') }}<br />
                                    {{ __('Any data or information on KNP website or Mobile Application may only be reproduced, distributed, published or otherwise publicly presented based on a prior written consent by Kalam news publication . If you are a commission agent , KNP allows an exception and grants Commission agent a limited, non-exclusive, revocable license to use Website Content and KNP Packages solely for the purposes of operating their Commission agent Business by downloading, storing, printing, copying, sharing and displaying Website Materials, provided that the Material is unaltered and the source of information is quoted in case any Website Materials are disclosed to third parties. Should you have additional questions on the use of the Website or Mobile Application Materials, please contact us through the Contact Us form present on the Contact Us page on website or connect to our customer support.') }}<br />
                                    {{ __('In the event of termination thereof, the Commission agent must delete or destroy all stored, printed or copied materials, unless they must be retained to comply with legal requirements and same should be informed to Kalam news publication immediately.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Links to external Websites and your personal information on such websites') }}</strong><br />
                                    {{ __('We may link to other websites which are not within our control. We are not responsible or liable for the information or materials made available by such third party websites. We encourage you to read the terms of use and privacy statements of all third party websites before using such websites or submitting any personal data or any other information on or through such websites.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Links from Other Websites to the Kalam news publication Website') }}</strong><br />
                                    {{ __('Inserting links from a third-party website to') }} <a href="https://www.kalamnewspublication.in/" rel="external nofollow noopener" target="_blank">https://www.kalamnewspublication.in/</a> {{ __('requires prior written consent from Kalam news publication . If you would like to link from other websites, please contact us.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Password Protected Parts of the Kalam news publication Website') }}</strong><br />
                                    {{ __('The password-protected parts of the KNP Website or Mobile Application are intended exclusively for Commission agent and Customers in India. If you do not already know an commission agent and wish to contact one, please Contact us.') }}<br />
                                    {{ __('We take personal information very seriously and all your passwords are encrypted in our systems. Passwords should not be given to third parties and must be protected from unauthorized access. If you become aware of any unauthorized use of your password, you should notify Kalam news publication immediately. KNP do not take any responsibility for damage caused as a result of improper use of passwords.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Limitation of Liability, Disclaimer of Warranties and Indemnification') }}</strong><br />
                                    {{ __('To the extent permitted by applicable law, neither KNP nor its affiliates shall be liable for any direct, indirect, consequential or other damages whatsoever, including but not limited to property damage, loss of use, loss of business, economic loss, loss of data or loss of profits, arising out of or in connection with your use or access to, or inability to use or access the Kalam news publication Website, Mobile Applications or its content.') }}<br />
                                    {{ __('KNP will use reasonable efforts to ensure that the information and materials provided on this Website are correct. However, KNP cannot guarantee the accuracy of all information and materials and does not assume any responsibility or liability for the accuracy, completeness or authenticity of any information and materials contained on this Website or Mobile Application.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('KNP Website and Mobile Application Software and Technical Information') }}</strong><br />
                                    {{ __('We do not warrant that the operation of this Website or Mobile Application will be uninterrupted or error-free, or that this Website or Mobile Application is free from viruses or other components that may be harmful to equipment or software. KNP does not guarantee that the KNP Website and Mobile Application will be compatible with the equipment and software which you may use and does not guarantee that the will be available all the time or at any specific time.') }}<br />
                                    {{ __('You agree to indemnify, defend and hold KNP and its affiliates harmless from any liability or loss, related to either your violation of these Website Terms of Use or your use of the Kalam news publication Website and Mobile Application.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Restricting or Blocking Access to the KNP Website, Mobile Application for Violations of the Website Terms of Use') }}</strong><br />
                                    {{ __('In case of a violation of these Website Terms of Use, particularly in case of use of the KNP Website or individual elements of the KNP Website anf Mobile Application for other than its intended use, access to the KNP Website and Mobile Application may be restricted or blocked.') }}<br />
                                    {{ __('KNP reserves the right to partially or entirely alter, block, or discontinue the KNP Website, Mobile Application or its content at any time and for any reason.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Severability Clause') }}</strong><br />
                                    {{ __('Should one of the provisions of these Website Terms of Use be invalid or declared invalid by a court, this will not affect the validity of the remaining terms.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Choice of law, Jurisdiction and Venue') }}</strong><br />
                                    {{ __('The use of the Kalam news publication Website, Mobile Application and these Terms of Use are governed by the laws of the Indian Government. The courts of Madhya Pradesh High court have exclusive jurisdiction and venue for any disputes arising from or in connection with the use of the Kalam news publication Website, Mobile Application or these Website Terms of Use. Jurisdiction will be at shehdol district court.') }}
                                </p>
                            </li>
                            <li>
                                <p><strong>{{ __('Notice') }}</strong><br />
                                    {{ __('If you have any comments or inquiries about these Terms of use, if you would like to update information we have about you, or to exercise your rights, you may contact our support team vie form present in Contact us page.') }}
                                </p>
                            </li>
                        </ol>

                        <p></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#commisionForm").validate({
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else if (element.hasClass('select2-hidden-accessible')) {
                    error.insertAfter(element.next('span'));
                    element.next('span').addClass('error').removeClass('valid');
                } else {
                    error.insertAfter(element);
                }
            },
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                user_email: {
                    required: true,
                },
                user_number: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10,
                },
            },
            messages: {
                first_name: {
                    required: "{{ __('Please Enter First Name') }}",
                },
                last_name: {
                    required: "{{ __('Please Enter Last Name')}}",
                },
                user_email: {
                    required: "{{ __('Please Enter Email Id')}}",
                },
                user_number: {
                    required: "{{ __('Please Enter Mobile Number')}}",
                },

            },
        });
    });
</script>
@endsection