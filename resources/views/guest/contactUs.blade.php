@extends('layouts.guest')
@section('content')
<!-- section-3 -->
<!-- contact details -->
<div class="container">
    <div class="row">
        <div class="col-md-4   mb_32">
            <div class="contact_card">
                <h2 class="Regular  small-text pb-1 mb-0">{{ __('Mobile number') }}</h2>
                <h3 class="contact_details Regular  small-text mb-0">
                    <div class="icons_back card_border"><img src="{{ file_url('welcome_assets/images/svg-img/Icons phone call.svg') ?? '' }}" class="black_filter"></div>{{ $siteSetting->contact_number }}
                </h3>
            </div>
        </div>
        <div class="col-md-4   mb_32">
            <div class="contact_card">
                <h2 class="Regular  small-text pb-1 mb-0">{{ __('Email') }}</h2>
                <h3 class="contact_details Regular  small-text mb-0">
                    <div class="icons_back card_border"><img src="{{ file_url('welcome_assets/images/svg-img/Icons gmail.svg') ?? '' }}" class="black_filter"></div>{{ $siteSetting->site_email }}
                </h3>
            </div>
        </div>
        <div class="col-md-4   mb_32">
            <div class="contact_card">
                <h2 class="Regular  small-text pb-1 mb-0">{{ __('Address') }}</h2>
                <h3 class="contact_details Regular  small-text mb-0">
                    <div class="icons_back card_border"><img src="{{ file_url('welcome_assets/images/svg-img/Icons location.svg') ?? '' }}" class="black_filter"></div>{{ $siteSetting->site_address }}
                </h3>
            </div>
        </div>
    </div>
</div>
<!-- section-3 -->
<!-- contact form -->
<div class="container">
    <!-- Contact us -->
    <div class="contact_box mb_32">
        <h2 class="mb_32">{{ __('Contact us') }}</h2>
        <div class="contact_form_card">
            <div class="row contact_img">
                <div class="col-md-6  contact_form_box">
                    <div class="contact_form">
                        <!-- name -->
                        <div class="inputs">
                            <div class="user_name">
                                <label for="fname">{{ __('First Name') }}</label>
                                <input type="text" class="" placeholder="{{ __('First Name') }}">
                            </div>
                            <div class="user_name">
                                <label for="fname">{{ __('Last name') }}</label>
                                <input type="text" class="" placeholder="{{ __('Last name') }}">
                            </div>
                        </div>
                        <!-- email -->
                        <div>
                            <label for="fname">{{ __('Email') }}</label>
                            <input type="email" class="w-100" placeholder="you@company.com">
                        </div>
                        <!-- number -->
                        <div>
                            <label for="fname">{{ __('Phone number') }}</label>
                            <div>
                                <div class="select_option d-flex justify-content-center align-items-center position-absolute">
                                    <select class="font-14 font-600" aria-label="Default select example">
                                        <option selected="" class="select_option">IND</option>
                                        <option value="1" class="select_option">USA</option>

                                    </select>
                                </div>
                                <input type="tel" class="w-100 input_text_number position-relative" id="phone" name="phone" placeholder="+91 - 9876543210" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}">
                            </div>
                        </div>
                        <!-- textarea -->
                        <div class="contact_form gap-0">
                            <label for="w3review">{{ __('Message') }}</label>
                            <textarea id="w3review" name="w3review" rows="4" cols="50"></textarea>
                        </div>
                        <!-- checkbox -->
                        <form action="/action_page.php" class="mb-3 d-flex align-items-center gap-2">
                            <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
                            <label for="privacy_policy" class="agreement mb-0">{{ __('You agree to our friendly') }} <a href="#" data-bs-toggle="modal" data-bs-target="#privacyModal"><u>{{ __('privacy policy') }}</u></a>.</label><br>
                        </form>
                        <!-- button -->
                        <button class="primary_btn medium-text  Medium px-x py-2 rounded-3">{{ __('Send message') }}</button>
                    </div>
                </div>
                <div class="col-md-6 contactus_imgs">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d3664.4041749173257!2d81.361858!3d23.301090000000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zMjPCsDE4JzAzLjkiTiA4McKwMjEnNDIuNyJF!5e0!3m2!1sen!2sin!4v1716184652828!5m2!1sen!2sin" width="550" height="550" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Show Privacy Policy -->
<div class="modal fade" id="privacyModal" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">{{ __('Privacy & Policy') }}</h5>
            </div>
            <div class="modal-body">
                <div class="col-md-12 m-2">
                        <div class="other_content">
                            <p>{{ __('All the educational Packages listed on the website') }} <a href="https://www.kalamnewspublication.in/" rel="external nofollow noopener" target="_blank">https://www.kalamnewspublication.in/</a>, {{ __('access to and use of the website and mobile(Android/IOS) application and the information, materials, products and services available through the Kalam news publication Website are subject to these terms of use (the Website Terms of Use).') }}</p>

                            <p>{{ __('Along with that, for KNP Commission agent , the documents and guidelines mentioning the Distributorship Contract apply and for any purchases of goods or services, the KNP Refund Policy applies.') }}</p>

                            <p>{{ __('The Website Terms of Use may be updated as per changes in business or as per government regulations. Updated versions will be posted on the KNP Website and Mobile Applications. Customers and Commission agent are requested to check the terms regularly.') }}</p>

                            <p>{{ __('The Kalam news publication Website and Mobile Applications provides information on KNP , the KNP Business and KNP Educational and information products and Packages, is intended only for use from and in India only and is based on the laws of Indian Government. Usage of the services or website outside India is not allowed and if used any claims resulting out of it will be not responded by Kalam news publication and its associated teams.') }}</p>

                            <ol>
                                <li>
                                    <p><strong>{{ __('Kalam news publication Privacy Terms and Policies') }}</strong>​​​​​​​<br />
                                        {{ __('All personal data provided to KNP while using the Website or Mobile Applications will be handled in accordance with the Website Privacy Notice. If you register or log in as an commission agent, the Privacy Policy for Commission agent applies in addition else as a customer the policies will be applied.') }}<br />
                                        {{ __('All information provided on KNP Website or Mobile application must be correct, complete, and up to date. If we have reason to believe that incorrect, incomplete or outdated information has been provided, access to the KNP Website or Mobile Application may be limited or blocked. If you intend to update any information please contact customer support.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Copyright and Use of Kalam news publication Website and Mobile application Materials') }}</strong><br />
                                        {{ __('The Kalam news publication Website, Mobile Application and Content and Packages made available are protected by intellectual property rights, including copyrights, trade names and trademarks, including the name Kalam news publication and the KNP logo, and are owned by Kalam news publication or used by KNP under a license or with permission from the owner of such rights. Materials protected by such intellectual property rights include the design, layout, look, appearance, graphics, photos, images, articles, stories and other content available on the Kalam news publication Website and Mobile Application.') }}<br />
                                        {{ __('Any data or information on KNP website or Mobile Application may only be reproduced, distributed, published or otherwise publicly presented based on a prior written consent by Kalam news publication . If you are a commission agent , KNP allows an exception and grants Commission agent a limited, non-exclusive, revocable license to use Website Content and KNP Packages solely for the purposes of operating their Commission agent Business by downloading, storing, printing, copying, sharing and displaying Website Materials, provided that the Material is unaltered and the source of information is quoted in case any Website Materials are disclosed to third parties. Should you have additional questions on the use of the Website or Mobile Application Materials, please contact us through the Contact Us form present on the Contact Us page on website or connect to our customer support.') }}<br />
                                        {{ __('In the event of termination thereof, the Commission agent must delete or destroy all stored, printed or copied materials, unless they must be retained to comply with legal requirements and same should be informed to Kalam news publication immediately.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Links to external Websites and your personal information on such websites') }}</strong><br />
                                        {{ __('We may link to other websites which are not within our control. We are not responsible or liable for the information or materials made available by such third party websites. We encourage you to read the terms of use and privacy statements of all third party websites before using such websites or submitting any personal data or any other information on or through such websites.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Links from Other Websites to the Kalam news publication Website') }}</strong><br />
                                        {{ __('Inserting links from a third-party website to') }} <a href="https://www.kalamnewspublication.in/" rel="external nofollow noopener" target="_blank">https://www.kalamnewspublication.in/</a> {{ __('requires prior written consent from Kalam news publication . If you would like to link from other websites, please contact us.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Password Protected Parts of the Kalam news publication Website') }}</strong><br />
                                        {{ __('The password-protected parts of the KNP Website or Mobile Application are intended exclusively for Commission agent and Customers in India. If you do not already know an commission agent and wish to contact one, please Contact us.') }}<br />
                                        {{ __('We take personal information very seriously and all your passwords are encrypted in our systems. Passwords should not be given to third parties and must be protected from unauthorized access. If you become aware of any unauthorized use of your password, you should notify Kalam news publication immediately. KNP do not take any responsibility for damage caused as a result of improper use of passwords.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Limitation of Liability, Disclaimer of Warranties and Indemnification') }}</strong><br />
                                        {{ __('To the extent permitted by applicable law, neither KNP nor its affiliates shall be liable for any direct, indirect, consequential or other damages whatsoever, including but not limited to property damage, loss of use, loss of business, economic loss, loss of data or loss of profits, arising out of or in connection with your use or access to, or inability to use or access the Kalam news publication Website, Mobile Applications or its content.') }}<br />
                                        {{ __('KNP will use reasonable efforts to ensure that the information and materials provided on this Website are correct. However, KNP cannot guarantee the accuracy of all information and materials and does not assume any responsibility or liability for the accuracy, completeness or authenticity of any information and materials contained on this Website or Mobile Application.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('KNP Website and Mobile Application Software and Technical Information') }}</strong><br />
                                        {{ __('We do not warrant that the operation of this Website or Mobile Application will be uninterrupted or error-free, or that this Website or Mobile Application is free from viruses or other components that may be harmful to equipment or software. KNP does not guarantee that the KNP Website and Mobile Application will be compatible with the equipment and software which you may use and does not guarantee that the will be available all the time or at any specific time.') }}<br />
                                        {{ __('You agree to indemnify, defend and hold KNP and its affiliates harmless from any liability or loss, related to either your violation of these Website Terms of Use or your use of the Kalam news publication Website and Mobile Application.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Restricting or Blocking Access to the KNP Website, Mobile Application for Violations of the Website Terms of Use') }}</strong><br />
                                        {{ __('In case of a violation of these Website Terms of Use, particularly in case of use of the KNP Website or individual elements of the KNP Website anf Mobile Application for other than its intended use, access to the KNP Website and Mobile Application may be restricted or blocked.') }}<br />
                                        {{ __('KNP reserves the right to partially or entirely alter, block, or discontinue the KNP Website, Mobile Application or its content at any time and for any reason.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Severability Clause') }}</strong><br />
                                        {{ __('Should one of the provisions of these Website Terms of Use be invalid or declared invalid by a court, this will not affect the validity of the remaining terms.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Choice of law, Jurisdiction and Venue') }}</strong><br />
                                        {{ __('The use of the Kalam news publication Website, Mobile Application and these Terms of Use are governed by the laws of the Indian Government. The courts of Madhya Pradesh High court have exclusive jurisdiction and venue for any disputes arising from or in connection with the use of the Kalam news publication Website, Mobile Application or these Website Terms of Use. Jurisdiction will be at shehdol district court.') }}
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('Notice') }}</strong><br />
                                        {{ __('If you have any comments or inquiries about these Terms of use, if you would like to update information we have about you, or to exercise your rights, you may contact our support team vie form present in Contact us page.') }}
                                    </p>
                                </li>
                            </ol>

                            <p></p>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('Close') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- section-5 -->
@endsection 