@extends('layouts.guest')
@section('content')
<!-- section-3 -->
<!-- slider start -->
<div id="carouselExampleDark" class="carousel carousel-dark slide mb_32" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
    </div>
</div>
<!-- section-4 -->
<div class="container mb_32">
    <div class="social_discription">
        <h2>{{ __('Our Efforts, Your Cooperation') }}</h2>
        <div class="row news_details">
            <div class="col-md-4 service_images">
                <div class="service">
                    <img src="{{ file_url('welcome_assets/images/png-img/Service P3.png') ?? '' }}" class="img-fluid">
                </div>
            </div>
            <div class="col-md-7">
                <p class=" mb-2">{{ __( 'The organization publishes the newspaper through Kalam News Publication. The weekly newspaper named Kalam News is the main unit of the organization. Along with this newspaper, which is registered through the Newspaper Registration Office of the Government of India, the organization also provides the currently available modern It also broadcasts and publishes blogs, videos and news through resources like YouTube, WhatsApp, Facebook, Instagram etc.') }}</p>
                <p>{{ __( 'Presently Madhya Pradesh is trying to take the work towards progress by appointing its representatives in every district of the state. In the coming time, the organization is trying to appoint its representatives in every district in every state of India. Try to take the organization to greater heights. For this, the organization has tried to create economic sources through its own economic sources as well as through advertisements issued by individual, institutional, business establishments, business institutions, state and central governments. Kalam News Publication Commission Agent Scheme has been started so that the organization continues to receive adequate amount of advertisements. Under this scheme, individual advertisers get financial benefits and after giving advertisement to the organization once, they can partner with the organization as a commission agent on the basis of this advertisement amount and work as part-time or full-time commission agent. It is the intention of the organization to achieve economic benefits while doing so. Taking inspiration from Multi Level Marketing legally recognized by the Government of India, Kalam News Publications has divided the commission scheme into 15 different stages through which advertisers working part-time or full-time can earn multiple times the amount of advertising they provide. Can get more commission benefits.') }}</p>
                <p>{{ __( 'As a form of your cooperation, we expect an advertisement from you and through this advertisement you can get financial benefits for yourself by joining the organization and working as a commission agent. Please cooperate with us.') }}</p>
            </div>
        </div>
    </div>

</div>
<!-- section-6 -->
@endsection