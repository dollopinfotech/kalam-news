@extends('layouts.guest')
@section('content')
<!-- section-3 -->
<!-- slider start -->
<div id="carouselExampleDark" class="carousel carousel-dark slide mb_32" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
    </div>
</div>
<!-- section-4 -->
<div class="container mb_32">
    <div class="social_discription">
        <h2>{{ __('From The Editor\'s Pen') }}<img src="{{ file_url('welcome_assets/images/svg-img/Simplification.svg') ?? '' }}"></h2>
        <div class="row news_details">
            <div class="col-md-4  service_images">
                <div class="service">
                    <img src="{{ file_url('welcome_assets/images/png-img/service.png') ?? '' }}" height="455" width="379">
                    <h3 class="large-text Bolder mb-0">{{ __('Ajay Kumar Shrivastava') }}</h3>
                </div>
            </div>
            <div class="col-md-7">
                <p class=" mb-2">{{ __('Every Indian born in the nation of India considers himself fortunate that he was born in this holy land.') }} </p>
                <p>{{ __('India is a country with different religions, different castes, different languages. Where followers of Hindu religion worship idols in temples.') }}</p>
                <p>{{ __('Muslim communities offer namaz and worship their prophets in mosques and tombs.') }}</p>
                <p>{{ __('Jain community where Mahavir ji is worshipped.') }}</p>
                <p>{{ __('Buddhists undertake their spiritual journey by following the path shown by Shri Gautam Buddha.') }}</p>
                <p>{{ __('Those who believe in Sikhism make their lives successful by following the path shown by Guru Nanak Ji.') }}</p>
                <p>{{ __('Many of us have made our life spiritually pleasant and successful by following the path shown by our Gurus.') }}</p>
                <p>{{ __('To successfully discharge our spiritual and worldly responsibilities, we need the guidance shown by our elders and teachers at every step.') }}</p>
                <p>{{ __('The knowledge of which our ancestors had acquired centuries ago, they were perfect in both modern and spiritual arts and its depiction for their future generations can be seen in ancient inscriptions, temples and man-made caves etc.') }}</p>
                <p>{{ __('Whenever we have forgotten the mutual brotherhood taught by our ancestors, we have paid the price of that mistake in the form of slavery for centuries. It was only on the strength of mutual unity and cooperation of the patriots that the people\'s heroes had achieved success in liberating this country which had been enslaved for centuries.') }}</p>
                <p>{{ __('We find examples of the power of unity many times in our history.') }}</p>
                <p>{{ __('Recognizing the power of this unity, Kalam News Publication has come forward to serve you all with the power of unity and to cooperate with the present government to fulfill the spiritual and modern needs of our lives.') }}</p>
                <p class="mt-5 mb-0">{{ __('I not only hope but am confident that you will recognize the power of unity and provide us with your invaluable support.') }}</p>
                <h2 class="medium-text text-end mt-4"><img src="{{ file_url('welcome_assets/images/svg-img/Simplification.svg') ?? '' }}">&nbsp;{{ __('Author') }}</h2>

            </div>
        </div>
    </div>

</div>
@endsection