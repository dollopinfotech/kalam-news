@extends('layouts.guest')
@section('content')
<!-- section-3 -->
<!-- slider start -->
<div id="carouselExampleDark" class="carousel carousel-dark slide mb_32" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
    </div>
</div>
<!-- section-4 -->
<div class="container mb_32">
    <div class="social_discription">
        <h2>{{ __('Process To Join Us') }}</h2>
        <div class="row news_details">
            <div class="col-md-7">
                <p class=" mb-0">{{ __('Kalam News Publication provides this opportunity to those readers who wish to work full time or part time with the organization. Interested readers fill the personal information form available in the website run by the organization and also ensure their participation by advertising the amount fixed by the organization.') }}</p>
                <p class="mb-4">{{ __('They have to provide this advertisement only once.') }}</p>
                <p>{{ __('The organization provides its products for a fee or free of charge depending on the resources available, such as a copy of the newspaper published by the organization is available at a fixed fee. Modern resources: Information, news etc. published by the organization are available for free on Facebook, YouTube, Twitter etc.') }}</p>
                <p>{{ __('Persons who wish to work part-time or full-time by associating with Kalam News Publication ensure their cooperation through the website run by the organization.') }}</p>

                <p>{{ __('By filling the personal information form available on the website, a pre-determined amount is provided to the advertising organization. After the amount is available, the organization provides a special identity card to the interested person and also provides a password to operate the organization\'s website. With the help of these identity cards and passwords, the participating commission agents can see and know the complete information about their work with the organization.') }}</p>

                <p class="mb-4">{{ __('Five types of information are available in it.') }}</p>
                <p>{{ __('The first part contains the complete information provided by the applicant to the institution.') }}</p>
                <p>{{ __('In the second part, photocopies of the documents available for verification of personal information provided by the reader to the institution are available.') }}</p>

                <p>{{ __('In the third part, information about other interested persons who have achieved partnership with the organization through the efforts of the reader is available.') }}</p>
                <p>{{ __('The fourth part contains complete details of the economic transactions of the work done by the commission agent.') }}</p>
                <p>{{ __('In the fifth and final part, special awards and benefits given by the institution on the major special tasks and task completion by the Parliament are adjusted. A person engaged as a commission agent with Kalam News Publications has to perform mainly two tasks. So that they continue to get financial benefits.') }}</p>
                <p>{{ __('The first task is to publicize the information published by the organization through social media by publishing it through the social media itself.') }}</p>
                <p class="mb-4">{{ __('Advertisers have to ensure participation with the organization by giving information about the work being done by the partner organization and the benefits received through the work and the financial benefits of working as a commission agent.') }}</p>
                <p>{{ __('The second major task is to build a group through advertising. While completing the first phase on the basis of two advertisers, we have to provide continuous support from our partners to complete the other phases. So that with the cooperation of all the associates, the remaining steps can be completed and the entire benefits provided by the organization can be achieved.') }}</p>
                <p>{{ __('Note:- It is mandatory to do both the tasks prescribed by the organization continuously, only then the financial payment is made for the work done by the organization.') }}</p>

            </div>
            <div class="col-md-4 service_images">
                <div class="service">
                    <img src="{{ file_url('welcome_assets/images/png-img/Service P4.png') ?? '' }}" class="img-fluid">
                </div>
            </div>

        </div>
    </div>

</div>
<!-- section-6 -->
@endsection