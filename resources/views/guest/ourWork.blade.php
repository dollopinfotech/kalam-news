@extends('layouts.guest')
@section('content')
<!-- section-3 -->
<!-- slider start -->
<div id="carouselExampleDark" class="carousel carousel-dark slide mb_32" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
    </div>
</div>
<!-- section-4 -->
<div class="container mb_32">
    <div class="social_discription">
        <h2>{{ __('Our Work')}}</h2>
        <div class="row news_details">
            <div class="col-md-7">
                <p>{{ __('Many schemes are implemented by the Central and State Government for the welfare of the countrymen. Due to lack of information, the general public is not able to get proper benefits of these schemes. To prevent this from happening, Kalam News Publication is determined to provide complete information related to the schemes in simple language in a systematic manner. Kalam News Publication is working to ensure that the complete benefits of the schemes reach the last person standing in the last queue, for which cooperation of all of you is needed so that the benefits of the schemes reach the last person, this is our main objective.') }}</p>
                <p>{{ __('The first objective of Kalam News Publication is to provide information about all the schemes of the Central and State Government, along with this, a major objective is to convey what our ancestors have taught us to the present generation.') }}</p>
                <p>{{ __('The strength of any structure depends on its strong foundation. To become an efficient and capable citizen, complete knowledge of the subject matter is required.') }}</p>
                <p>{{ __('When we have more and more information about the subject matter, we will become proficient and to fulfill this purpose, Kalam News Publication on different subjects is a means of providing correct and complete information in easy and simple language with the help of experts related to the subject. Provides a powerful medium.') }}</p>
                <p>{{ __('To fulfill the material needs, it is necessary that you have sufficient financial inflow, for this, Kalam News Publication is also working to provide financial benefits to every person associated with the organization through full-time or part-time work. Its complete information is available in the website run by the organization, by viewing which you can get financial benefits by joining the organization.') }}</p>

                <h2 class="medium-text text-end mt-4"><img src="{{ file_url('welcome_assets/images/svg-img/Simplification.svg') ?? '' }}">&nbsp;{{ __('Author') }}</h2>
            </div>
            <div class="col-md-4 service_images">
                <div class="service">
                    <img src="{{ file_url('welcome_assets/images/png-img/servicep2.png') ?? '' }}" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- section-6 -->
@endsection