@extends('layouts.guest')
@section('content')
<!-- section-4 -->
<!-- Privacy & Policy -->
<div class="container">
    <div class="other_content">
        <h2 class="mb_32">{{ __('Privacy & Policy') }}</h2>
        <p>{{ __('All the advertisement Packages listed on the website') }} <a href="https://www.kalamnewspublication.in/" rel="external nofollow noopener" target="_blank">https://www.kalamnewspublication.in/</a>, {{ __('access to and use of the website and mobile(Android/IOS) application and the information, materials, products and services available through the Kalam news publication Website are subject to these terms of use (the Website Terms of Use).') }}</p>

        <p>{{ __('Along with that, for KNP Commission agent , the documents and guidelines mentioning the Distributorship Contract apply and for any purchases of goods or services, the KNP Refund Policy applies.') }}</p>

        <p>{{ __('The Website Terms of Use may be updated as per changes in business or as per government regulations. Updated versions will be posted on the KNP Website and Mobile Applications. Customers and Commission agent are requested to check the terms regularly.') }}</p>

        <p>{{ __('The Kalam news publication Website and Mobile Applications provides information on KNP , the KNP Business and KNP advertisement and information products and Packages, is intended only for use from and in India only and is based on the laws of Indian Government. Usage of the services or website outside India is not allowed and if used any claims resulting out of it will be not responded by Kalam news publication and its associated teams.') }}</p>

        <ol>
            <li>
                <p><strong>{{ __('Kalam news publication Privacy Terms and Policies') }}</strong>​​​​​​​<br />
                    {{ __('All personal data provided to KNP while using the Website or Mobile Applications will be handled in accordance with the Website Privacy Notice. If you register or log in as an commission agent, the Privacy Policy for Commission agent applies in addition else as a customer the policies will be applied.') }}<br />
                    {{ __('All information provided on KNP Website or Mobile application must be correct, complete, and up to date. If we have reason to believe that incorrect, incomplete or outdated information has been provided, access to the KNP Website or Mobile Application may be limited or blocked. If you intend to update any information please contact customer support.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Copyright and Use of Kalam news publication Website and Mobile application Materials') }}</strong><br />
                    {{ __('The Kalam news publication Website, Mobile Application and Content and Packages made available are protected by intellectual property rights, including copyrights, trade names and trademarks, including the name Kalam news publication and the KNP logo, and are owned by Kalam news publication or used by KNP under a license or with permission from the owner of such rights. Materials protected by such intellectual property rights include the design, layout, look, appearance, graphics, photos, images, articles, stories and other content available on the Kalam news publication Website and Mobile Application.') }}<br />
                    {{ __('Any data or information on KNP website or Mobile Application may only be reproduced, distributed, published or otherwise publicly presented based on a prior written consent by Kalam news publication . If you are a commission agent , KNP allows an exception and grants Commission agent a limited, non-exclusive, revocable license to use Website Content and KNP Packages solely for the purposes of operating their Commission agent Business by downloading, storing, printing, copying, sharing and displaying Website Materials, provided that the Material is unaltered and the source of information is quoted in case any Website Materials are disclosed to third parties. Should you have additional questions on the use of the Website or Mobile Application Materials, please contact us through the Contact Us form present on the Contact Us page on website or connect to our customer support.') }}<br />
                    {{ __('In the event of termination thereof, the Commission agent must delete or destroy all stored, printed or copied materials, unless they must be retained to comply with legal requirements and same should be informed to Kalam news publication immediately.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Links to external Websites and your personal information on such websites') }}</strong><br />
                    {{ __('We may link to other websites which are not within our control. We are not responsible or liable for the information or materials made available by such third party websites. We encourage you to read the terms of use and privacy statements of all third party websites before using such websites or submitting any personal data or any other information on or through such websites.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Links from Other Websites to the Kalam news publication Website') }}</strong><br />
                    {{ __('Inserting links from a third-party website to') }} <a href="https://www.kalamnewspublication.in/" rel="external nofollow noopener" target="_blank">https://www.kalamnewspublication.in/</a> {{ __('requires prior written consent from Kalam news publication . If you would like to link from other websites, please contact us.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Password Protected Parts of the Kalam news publication Website') }}</strong><br />
                    {{ __('The password-protected parts of the KNP Website or Mobile Application are intended exclusively for Commission agent and Customers in India. If you do not already know an commission agent and wish to contact one, please Contact us.') }}<br />
                    {{ __('We take personal information very seriously and all your passwords are encrypted in our systems. Passwords should not be given to third parties and must be protected from unauthorized access. If you become aware of any unauthorized use of your password, you should notify Kalam news publication immediately. KNP do not take any responsibility for damage caused as a result of improper use of passwords.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Limitation of Liability, Disclaimer of Warranties and Indemnification') }}</strong><br />
                    {{ __('To the extent permitted by applicable law, neither KNP nor its affiliates shall be liable for any direct, indirect, consequential or other damages whatsoever, including but not limited to property damage, loss of use, loss of business, economic loss, loss of data or loss of profits, arising out of or in connection with your use or access to, or inability to use or access the Kalam news publication Website, Mobile Applications or its content.') }}<br />
                    {{ __('KNP will use reasonable efforts to ensure that the information and materials provided on this Website are correct. However, KNP cannot guarantee the accuracy of all information and materials and does not assume any responsibility or liability for the accuracy, completeness or authenticity of any information and materials contained on this Website or Mobile Application.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('KNP Website and Mobile Application Software and Technical Information') }}</strong><br />
                    {{ __('We do not warrant that the operation of this Website or Mobile Application will be uninterrupted or error-free, or that this Website or Mobile Application is free from viruses or other components that may be harmful to equipment or software. KNP does not guarantee that the KNP Website and Mobile Application will be compatible with the equipment and software which you may use and does not guarantee that the will be available all the time or at any specific time.') }}<br />
                    {{ __('You agree to indemnify, defend and hold KNP and its affiliates harmless from any liability or loss, related to either your violation of these Website Terms of Use or your use of the Kalam news publication Website and Mobile Application.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Restricting or Blocking Access to the KNP Website, Mobile Application for Violations of the Website Terms of Use') }}</strong><br />
                    {{ __('In case of a violation of these Website Terms of Use, particularly in case of use of the KNP Website or individual elements of the KNP Website anf Mobile Application for other than its intended use, access to the KNP Website and Mobile Application may be restricted or blocked.') }}<br />
                    {{ __('KNP reserves the right to partially or entirely alter, block, or discontinue the KNP Website, Mobile Application or its content at any time and for any reason.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Severability Clause') }}</strong><br />
                    {{ __('Should one of the provisions of these Website Terms of Use be invalid or declared invalid by a court, this will not affect the validity of the remaining terms.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Choice of law, Jurisdiction and Venue') }}</strong><br />
                    {{ __('The use of the Kalam news publication Website, Mobile Application and these Terms of Use are governed by the laws of the Indian Government. The courts of Madhya Pradesh High court have exclusive jurisdiction and venue for any disputes arising from or in connection with the use of the Kalam news publication Website, Mobile Application or these Website Terms of Use. Jurisdiction will be at shehdol district court.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Notice') }}</strong><br />
                    {{ __('If you have any comments or inquiries about these Terms of use, if you would like to update information we have about you, or to exercise your rights, you may contact our support team vie form present in Contact us page.') }}
                </p>
            </li>
        </ol>

        <p></p>
    </div>
</div>
<!-- section-5 -->
@endsection