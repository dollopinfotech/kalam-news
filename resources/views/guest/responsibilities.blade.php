@extends('layouts.guest')
@section('content')
<!-- section-3 -->
<!-- slider start -->
<div id="carouselExampleDark" class="carousel carousel-dark slide mb_32" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
        <div class="carousel-item" data-bs-interval="2000">
            <div class="overlay"></div><!-- Overlay div -->
            <img src="{{ file_url('welcome_assets/images/png-img/slider2.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
        </div>
    </div>
</div>
<!-- section-4 -->
<div class="container mb_32">
    <div class="social_discription">
        <h2>{{ __('Our Social Responsibilities')}}</h2>
        <div class="row news_details">
            <div class="col-md-7 service_content">
                <p>{{ __('Kalam News Publication first of all ensures that it fulfills its social responsibilities towards the nation on priority. As an elite society we live our lives happily using all the resources provided by nature. Just as nature has made its resources available to everyone in general without any discrimination. Similarly, topics related to social concerns are common for everyone. We try to discharge these duties as per the situation and circumstances as much as possible.') }}</p>

                <p>{{ __('Naturally, we will always be ready for the welfare of water, forest, land, people and animals and will try as much as possible to do it ourselves and also inspire everyone along with us.') }}</p>
                <p class="mb-4">{{ __('The organization also ensures that it participates in the work being done in the public interest by the Central and State Governments.') }}</p>
                <P>{{ __('Kalam News Publication will continue to make every possible effort to organize the implementation of such works which will give confidence to every member directly or indirectly associated with the organization that in adverse circumstances, Kalam News Publication will exceed the normal situation and observe no profit or loss. Will continue to provide its support to all members.') }}</P>

            </div>
            <div class="col-md-4 ">
                <div class="service">
                    <img src="{{ file_url('welcome_assets/images/png-img/Service P5.png') ?? '' }}" class="img-fluid">
                </div>
            </div>

        </div>
    </div>

</div>
<!-- section-6 -->
@endsection