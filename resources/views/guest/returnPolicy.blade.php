@extends('layouts.guest')
@section('content')
<!-- section-4 -->
<!-- Privacy & Policy -->
<div class="container">
    <div class="other_content">
        <h2 class="mb_32">{{ __('Return Policy') }}</h2>
        <p>{{ __('Kalam news publication is very firm about the quality of the service and assures at most satisfaction and customer support. KNP also obliges to Government of India rules and regulations under Customer rights. If for any reason you are not completely satisfied with the advertising sources , you may return it within 10-12 hours of order for a refund as per the terms of the Returns Policy.') }}</p>

        <p>{{ __('The refund policy is applicable only for packages in marketable condition, and totally in unused condition. This policy does not apply to advertisement that have been intentionally damaged or misused. If the published advertisement is Miss printed . then refund policy will not be applicable.') }}</p>

        <p><strong>{{ __('In below conditions the refund will not be processed') }}</strong>​​​​​​​<br /></p>
        <ul>
            <li>
                <p>{{ __('The publishing order of advertisement can be cancelled within 10-12 hours from the time of order.') }}</p>
            </li>
            <li>
                <p>{{ __('Once the advertisement have been published. We will not be entitled for refund if it is Miss printed also.') }}</p>
            </li>
            <li>
                <p>{{ __('The order will not be entitled for refund if anything is manually written or electronically printed on published platform.') }}</p>
            </li>
            <li>
                <p>{{ __('The advertisement order will not be entitled for refund if the advertisement is activated electronically using.') }}</p>
            </li>
        </ul>
        <p>{{ __('In all other cases the refund will be fully processed. The refund will be made in the payment mode used by customer for making the purchase.') }}
            {{ __('Customer will get his money in 5-7 working days.') }} <br />
            {{ __('Once the refund is processed, the order of advertisement will be listed under cancelled advertisement order list in the  application under Customers login. The Customer can again order the same Package by making the appropriate payment.') }}<br />
            {{ __('Kalam news publication will never ask for the money from the customer in case of return is claimed but the same will be adjusted in the subsequent commission.') }}<br />
            {{ __('In case multiple refund requested are made under a same commission agent and if the wallet amount in the commission agent account goes below 1000 INR means 1000 INR in negative, the account will be blocked/locked right away. In order to again activate the account the agent needs to make the payment for the negative amount.') }}
        </p>
    </div>
</div>
<!-- section-5 -->
@endsection