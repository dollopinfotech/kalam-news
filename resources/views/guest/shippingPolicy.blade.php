@extends('layouts.guest')
@section('content')
<!-- section-4 -->
<!-- Privacy & Policy -->
<div class="container">
    <div class="other_content">
        <h2 class="mb_32">{{ __('Shipping Policy') }}</h2>
        <p>{{ __('Since Kalam News Publication offers digital news content and advertisement services, a physical shipping policy isn\'t applicable. Here\'s an alternative policy focusing on delivery and access') }}</p>

        <ol>
            <li>
                <p><strong>{{ __('Delivery of Digital Content') }}</strong>​​​​​​​<br />
                    {{ __('Kalam News Publication provides digital news content and advertisement services through online access.') }}<br />
                    <strong>{{ __('News Content: ') }}</strong>​​​​​​
                    {{ __('Upon successful subscription or purchase of a specific news package, you will receive immediate access to the content. This access can be through') }}<br />
                    <strong>{{ __('Website: ') }}</strong>​​​​​​
                    {{ __('Login to your Kalam News Publication account on the website to access subscribed content. Mobile App (if applicable): Download and log in to the Kalam News Publication app (if available) using your account credentials to access subscribed content.') }}<br />
                    <strong>{{ __('Advertisements: ') }}</strong>​​​​​​
                    {{ __(' Advertisers will be provided with detailed reports and analytics on their campaign performance electronically through their Kalam News Publication account dashboard.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Access Duration') }}</strong><br />
                    <strong>{{ __('Subscriptions: ') }}</strong>​​​​​​
                    {{ __('Your access to subscribed news content will be ongoing throughout your chosen subscription period (monthly, yearly, etc.).') }}<br />
                    <strong>{{ __('Individual Purchases: ') }}</strong>​​​​​​
                    {{ __('For one-time purchases of specific news content (e.g., e-editions, special reports), access will be granted immediately upon purchase confirmation and remain accessible for the duration outlined in the product description (e.g., permanent access, limited-time access).') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Technical Requirements') }}</strong><br />
                    {{ __('To access Kalam News Publication\'s digital content, you will need a device with a reliable internet connection and a compatible web browser or mobile app (if applicable). System requirements for the app will be clearly listed on the app download page.') }}
                </p>
            </li>
            <li>
                <p><strong>{{ __('Customer Support') }}</strong><br />
                    {{ __('If you encounter any issues accessing your subscribed content or have questions about the delivery process, please contact our customer support team through the following methods.') }} <br />
                    {{ __('Contact form on the Kalam News Publication website') }} <br />
                    <strong>{{ __('Email') }} :</strong> {{ $siteSetting->site_email }} <br />
                    <strong>{{ __('Mobile number') }} :</strong> {{ $siteSetting->contact_number }}
                </p>
                <p><strong>{{ __('We strive to ensure a smooth and seamless delivery of our digital content and advertisement services. If you have any concerns, please don\'t hesitate to contact us.') }} <br />
                        {{ __('Kalam News Publication Team') }}</strong></p>
            </li>
        </ol>
    </div>
</div>
<!-- section-5 -->
@endsection