@extends('layouts.guest')
@section('content')
<!-- section-4 -->
<!-- Privacy & Policy -->
<div class="container">
    <div class="other_content">
        <h2 class="mb_32">{{ __('Term & Conditions') }}</h2>
        <p>{{ __('These Terms and Conditions govern your use of the Kalam News website and services. By accessing or using Kalam News, you agree to be bound by these Terms and Conditions.') }}</p>
        <ol>
            <li><p><strong>{{ __('Content') }}</strong></p>
                <ol>
                    <li><p>{{ __('Kalam News reserves the right to modify, suspend, or discontinue any aspect of the website or services at any time.') }}</p></li>
                    <li><p>{{ __('All content provided on Kalam News is for informational purposes only. We do not endorse any views expressed by contributors or users.') }}</p></li>
                    <li><p>{{ __('Users may not reproduce, distribute, or modify any content from Kalam News without prior written consent.') }}</p></li>
                </ol>
            </li>
            <li><p><strong>{{ __('User Conduct') }}</strong></p>
                <ol>
                    <li><p>{{ __('Users must not engage in any activity that disrupts or interferes with the functioning of Kalam News or violates any laws or regulations.') }}</p></li>
                    <li><p>{{ __('Users are responsible for maintaining the confidentiality of their account credentials and for all activities that occur under their account.') }}</p></li>
                </ol>
            </li>
            <li><p><strong>{{ __('Refund Policy') }}</strong></p>
                <ol>
                    <li><p>{{ __('If a customer informs us within 10 hours of his/her order, we will deduct all taxes and refund the amount.') }}</p></li>
                </ol>
            </li>
            <li><p><strong>{{ __('Intellectual Property') }}</strong></p>
                <ol>
                    <li><p>{{ __('All content on Kalam News, including text, graphics, logos, and images, is the property of Kalam News and protected by copyright laws.') }}</p></li>
                    <li><p>{{ __('Users may not use any trademarks or logos of Kalam News without prior written consent.') }}</p></li>
                </ol>
            </li>
            <li><p><strong>{{ __('Disclaimer') }}</strong></p>
                <ol>
                    <li><p>{{ __('Kalam News makes no representations or warranties of any kind, express or implied, regarding the accuracy, reliability, or completeness of any content on the website.') }}</p></li>
                    <li><p>{{ __('Kalam News shall not be liable for any direct, indirect, incidental, special, or consequential damages arising out of or in any way connected with the use of or inability to use the website or services.') }}</p></li>
                </ol>
            </li>
            <li><p><strong>{{ __('Governing Law') }}</strong></p>
                <ol>
                    <li><p>{{ __('These Terms and Conditions shall be governed by and construed in accordance with the laws of india, without regard to its conflict of law provisions.') }}</p></li>
                </ol>
            </li>
            <li><p><strong>{{ __('Changes to Terms and Conditions') }}</strong></p>
                <ol>
                    <li><p>{{ __('Kalam News reserves the right to update or revise these Terms and Conditions at any time. Continued use of the website or services after any such changes shall constitute your consent to such changes.') }}</p></li>
                </ol>
            </li>
            <li><p><strong>{{ __('Contact Us') }}</strong></p>
                <ol>
                    <li><p>{{ __('If you have any questions or concerns about these Terms and Conditions, please contact us at') }}</p></li>
                </ol>
            </li>
        </ol>
    </div>
</div>
<!-- section-5 -->
@endsection