<?php
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Expires: 0");
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr" data-scompiler-id="0">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $siteSettings->site_name ?? "" }}</title>
    <!-- icon -->
    @php
    $favicon = $siteSettings->favicon ?? "";
    @endphp
    <link rel="icon" type="image/png" href="{{ asset($siteSettings->favicon) }}" />

    <!-- fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" />
    <!-- css -->
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.ltr.css" />
    <link rel="stylesheet" href="../vendor/highlight.js/styles/github.css" />
    <link rel="stylesheet" href="../vendor/simplebar/simplebar.min.css" />
    <link rel="stylesheet" href="../vendor/quill/quill.snow.css" />
    <link rel="stylesheet" href="../vendor/air-datepicker/css/datepicker.min.css" />
    <link rel="stylesheet" href="../vendor/select2/css/select2.min.css" />
    <link rel="stylesheet" href="../vendor/datatables/css/dataTables.bootstrap5.min.css" />
    <link rel="stylesheet" href="../vendor/nouislider/nouislider.min.css" />
    <link rel="stylesheet" href="../vendor/fullcalendar/main.min.css" />
    <link rel="stylesheet" href="../css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" /> --}}
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet"> --}}

</head>

<body>
    <!-- sa-app -->
    @include('admin.include.rolePermission')
    @include('admin.include.sidebar')
    <!-- sa-app end -->
    <!-- scripts -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/validation/jquery.validate.min.js"></script>
    <script src="../vendor/feather-icons/feather.min.js"></script>
    <script src="../vendor/simplebar/simplebar.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../vendor/highlight.js/highlight.pack.js"></script>
    <script src="../vendor/quill/quill.min.js"></script>
    <script src="../vendor/air-datepicker/js/datepicker.min.js"></script>
    <script src="../vendor/air-datepicker/js/i18n/datepicker.en.js"></script>
    <script src="../vendor/select2/js/select2.min.js"></script>
    {{-- <script src="../vendor/fontawesome/js/all.min.js" data-auto-replace-svg="" async=""></script> --}}
    <script src="../vendor/chart.js/chart.min.js"></script>
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables/js/dataTables.bootstrap5.min.js"></script>
    <script src="../vendor/nouislider/nouislider.min.js"></script>
    <script src="../vendor/fullcalendar/main.min.js"></script>
    <script src="../js/stroyka.js"></script>
    <script src="../js/custom.js"></script>
    <script src="../js/calendar.js"></script>
    <script src="../js/demo.js"></script>
    <script src="../js/demo-chart-js.js"></script>
    <script src="../js/validate.js"></script>
    <script src="../js/sweet-alert-script.js"></script>
    <script src="../js/sweetalert.min.js"></script>
    <script src="../js/admin/custom.js"></script>
    <script>
        window.onpageshow = function(event) {
            if (event.persisted) {
                window.location.reload();
            }
        };
    </script>
</body>

</html>
@include('admin.include.sweetAlert')