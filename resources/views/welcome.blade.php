@extends('layouts.guest')
@section('content')
<!-- section-3 -->
<!-- slider start -->
<div class="position-relative">
    <div id="carouselExampleDark" class="carousel carousel-dark slide mb_32" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active" data-bs-interval="10000">
                <div class="overlay"></div><!-- Overlay div -->
                <img src="{{ file_url('welcome_assets/images/png-img/slider-img.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
            </div>
            <!-- Second Slide -->
            <div class="carousel-item" data-bs-interval="2000">
                <div class="overlay"></div><!-- Overlay div -->
                <img src="{{ file_url('welcome_assets/images/png-img/slider-img.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
            </div>
            <!-- Third Slide -->
            <div class="carousel-item" data-bs-interval="2000">
                <div class="overlay"></div><!-- Overlay div -->
                <img src="{{ file_url('welcome_assets/images/png-img/slider-img.png') ?? '' }}" class="d-block w-100 slider_img" alt="...">
            </div>
        </div>
    </div>
</div>

<div class="container mb_32">
    <div class="social_discription">
        <h2>{{ __('From The Editor\'s Pen') }}<img src="{{ file_url('welcome_assets/images/svg-img/Simplification.svg') ?? '' }}"></h2>
        <div class="row news_details">
            <div class="col-md-4  service_images">
                <div class="service">
                    <img src="{{ file_url('welcome_assets/images/png-img/service.png') ?? '' }}" height="455" width="379">
                    <h3 class="large-text Bolder mb-0">{{ __('Ajay Kumar Shrivastava') }}</h3>
                </div>
            </div>
            <div class="col-md-7">
                <p class=" mb-2">{{ __('Every Indian born in the nation of India considers himself fortunate that he was born in this holy land.') }} </p>
                <p>{{ __('India is a country with different religions, different castes, different languages. Where followers of Hindu religion worship idols in temples.') }}</p>
                <p>{{ __('Muslim communities offer namaz and worship their prophets in mosques and tombs.') }}</p>
                <p>{{ __('Jain community where Mahavir ji is worshipped.') }}</p>
                <p>{{ __('Buddhists undertake their spiritual journey by following the path shown by Shri Gautam Buddha.') }}</p>
                <p>{{ __('Those who believe in Sikhism make their lives successful by following the path shown by Guru Nanak Ji.') }}</p>
                <p>{{ __('Many of us have made our life spiritually pleasant and successful by following the path shown by our Gurus.') }}</p>
                <p>{{ __('To successfully discharge our spiritual and worldly responsibilities, we need the guidance shown by our elders and teachers at every step.') }}</p>
                <p>{{ __('The knowledge of which our ancestors had acquired centuries ago, they were perfect in both modern and spiritual arts and its depiction for their future generations can be seen in ancient inscriptions, temples and man-made caves etc.') }}</p>
                <p>{{ __('Whenever we have forgotten the mutual brotherhood taught by our ancestors, we have paid the price of that mistake in the form of slavery for centuries. It was only on the strength of mutual unity and cooperation of the patriots that the people\'s heroes had achieved success in liberating this country which had been enslaved for centuries.') }}</p>
                <p>{{ __('We find examples of the power of unity many times in our history.') }}</p>
                <p>{{ __('Recognizing the power of this unity, Kalam News Publication has come forward to serve you all with the power of unity and to cooperate with the present government to fulfill the spiritual and modern needs of our lives.') }}</p>
                <p class="mt-2 mb-0">{{ __('I not only hope but am confident that you will recognize the power of unity and provide us with your invaluable support.') }}</p>
                <h2 class="medium-text text-end mt-2"><img src="{{ file_url('welcome_assets/images/svg-img/Simplification.svg') ?? '' }}">&nbsp;{{ __('Author') }}</h2>

            </div>
        </div>
    </div>
</div>
<!-- section-4 -->
<!-- news discription -->
<div class="container mb_32">
    <div class="social_discription">
        <h2>{{ __('Our Social Responsibilities') }}</h2>
        <div>
            <p>{{ __('Kalam News Publication is determined to fulfill all its social responsibilities towards the nation on as much priority as possible. As an elite society, we live our lives happily using all the resources provided by nature. Just as nature has made its abundant resources available to all of us without any discrimination. Similarly, we also consider it our responsibility to give back something with respect to nature and to convey various points related to social concerns to the needy and marginalized people as much as possible. We are determined and committed to discharge these duties!') }}</p>
            <p>{{ __('. Realizing the definition of coexistence, keeping in mind the conservation and balance of nature, we will always be ready for the interests of water, forest, land, people and animals and will try as much as possible that the community also considers it as their responsibility.')}}</p>
            <p>{{ __('. The aim of the organization is also to ensure its active participation in the work being done in the public interest by the Central and State Governments and to spread awareness and publicity in the said context.') }}</p>
            <p>{{ __('. Kalam News Publication will continuously try to contribute as much as possible to the efforts made by organizations and individuals working on social issues.') }}</p>

            <p>{{ __('. The founders and members of Kalam News have a clear opinion that no matter how difficult the circumstances may be! We will continue to move forward on the path of duty at a reasonable pace, regardless of profit or loss.') }}</p>
        </div>
        <h3>{{ __('The issues on which the organization will specifically focus are as follows:') }}</h3>
    </div>
</div>

<!-- section-5 -->
<!-- news -->
<div class="container">
    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-1.png') ?? '' }}">
                </div>

                <div class="card_right h-100" style="margin-left: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Efforts towards water conservation') }}</h2>
                        <h3>{{ __('It is said that there is immense water wealth present in the oceans, but the water for human use is very less in the entire earth. Our effort will be to protect the water sources as much as possible by motivating people to use minimum water so that our future The generation should not have to suffer from lack of clean and safe drinking water.') }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="card_right h-100" style="margin-right: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Efforts to prevent air pollution') }}</h2>
                        <h3>{{ __('Today, many metropolitan cities of India have made their place in the list of 20 most polluted cities of the world as severely air polluted cities. Air pollution plays an important role in making the lungs sick and causing many health problems. Air pollution can be reduced/controlled. Will cooperate with the social organizations working in this direction') }}</h3>
                    </div>
                </div>
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-2.png') ?? '' }}">
                </div>

            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-3.png') ?? '' }}">
                </div>

                <div class="card_right h-100" style="margin-left: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Donating blood regularly and helping in organizing blood donation camps') }}</h2>
                        <h3>{{ __('Death of every person born on earth is inevitable, but every life lost due to lack of blood is like a slap on the face of the civilized society. Even after years, many misconceptions regarding blood donation have prevented blood donors from donating happily. The subject is that there are many blood donors in the society who have completed a century of donating blood, while there are also crores of adults who do not know their blood group. Blood donors will be encouraged to address this situation so that there is continuous supply of blood in the blood bank. Every precious life can be saved by ensuring availability and timely availability of blood.') }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="card_right h-100" style="margin-right: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Free support to Pau operators in extreme heat') }}</h2>
                        <h3>{{ __('Bottled water and various cold beverages are available to provide relief to dry throats in the scorching heat, but for those who have difficulty in getting even two meals a day, it is very difficult to buy bottled water to provide relief to such dry throats. The Pau is operated free of cost by social organizations and social workers. This is a very important social work in the scorching summer season. The Pau operated will be supported as much as possible.') }}</h3>
                    </div>
                </div>
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-4.png') ?? '' }}">
                </div>

            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-5.png') ?? '' }}">
                </div>

                <div class="card_right h-100" style="margin-left: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Efforts to prevent violence against women and sexual harassment') }}</h2>
                        <h3>{{ __('It is said that we are living in the twenty-first century and it is true that in this century women are creating new steps of success and history by standing shoulder to shoulder with men, but the other and sad side of the coin is that women are increasing in violence and sexual harassment. Every year, new records are set in different states of the country by social organizations that are making efforts towards prevention of violence against women and sexual harassment - maximum support will be provided to them from the organization.') }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="card_right h-100" style="margin-right: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Promote organic farming')}}</h2>
                        <h3>{{ __('The need for more food for the rapidly increasing population pushed agriculture towards the use of chemical fertilizers and chemical pesticides, but it is no longer hidden from anyone that excessive use of chemicals in food items is the cause of many health problems. Understanding the usefulness and importance of organic farming, it becomes necessary to encourage the organizations promoting organic farming so that there is minimal use of chemicals in food items. This is effective and useful in improving human health. There will be a step.') }}</h3>
                    </div>
                </div>
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-6.png') ?? '' }}">
                </div>

            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-7.png') ?? '' }}">
                </div>

                <div class="card_right h-100" style="margin-left: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Regular plantation for environmental protection') }}</h2>
                        <h3>{{ __('The COVID-19 pandemic highlighted the critical importance of oxygen, a free gift from nature. However, in our relentless pursuit of development, this generation has caused unprecedented environmental damage. We benefited from the environment our elders cherished but failed to conserve it. Over the past four decades, we\'ve increased the Earth\'s temperature by about 25 degrees, threatening global populations and risking water crises from melting glaciers. Although serious conservation efforts are overdue, it\'s not too late. To protect the environment, especially during the rainy season, we must plant trees on a large scale, encourage tree planting, and support the growth of these plants into trees. Our organization will contribute as much as possible to these efforts.') }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="card_right h-100" style="margin-right: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Providing employment opportunities to the unemployed') }}</h2>
                        <h3>{{ __('It is true that the state and central governments are responsible for many services and the elected governments discharge their responsibilities as much as possible, but a civilized, aware and educated society makes its own efforts at the local level without looking to the government for every task. The organization will also make serious efforts to connect the educated unemployed with various small industries so that even a small number of educated unemployed can earn a living for themselves and their families through small employment.') }}</h3>
                    </div>
                </div>
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-8.png') ?? '' }}">
                </div>

            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-9.png') ?? '' }}">
                </div>

                <div class="card_right h-100" style="margin-left: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Providing food to the disabled and helpless in nominal amount') }}</h2>
                        <h3>{{ __('Almost none of us are aware of real hunger because in the families we all come from, hunger only means that when we are hungry - after some time food or any food item becomes available but real hunger is that. When that happens, you don\'t know when you will get food? Where to get it? How much will you get? Will I get it or not? For those needy sections of the society who are disabled, helpless, crippled, who do not even have money to buy cheap food, the organization will provide as much support as possible to such organizations which are providing their services towards providing free food.') }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="card_right h-100" style="margin-right: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Support for pilgrimages') }}</h2>
                        <h3>{{ __('Religiosity requires that we get the privilege of visiting the pilgrimages/important places of whatever religion we follow, but not every human being is capable enough to take himself and his family on a pilgrimage after discharging all his primary responsibilities. Considering this religious faith as its social responsibility, the organization will provide as much support as possible to low income families for pilgrimage every year.') }}</h3>
                    </div>
                </div>
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-10.png') ?? '' }}">
                </div>

            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-11.png') ?? '' }}">
                </div>

                <div class="card_right h-100" style="margin-left: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Making students aware about good touch and bad touch') }}</h2>
                        <h3>{{ __('There are many infections which are a threat to human health but there are some other infections which pose a threat to the civilized society. Young children, especially girls, have to face this unpleasant situation in their lifetime when their close relatives or acquaintances. Attempts are made to touch them inappropriately. In such a situation, it becomes necessary that the students should be aware of what could be the meaning if someone other than their parents touches any part of their body. ? And what should they do in such a difficult situation? For this, students in educational institutions will be made aware through Good Touch Bad Touch.') }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="card_right h-100" style="margin-right: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Controlling the use of polythene') }}</h2>
                        <h3>{{ __('We have developed such a demon in the form of polythene/plastic - which we are unable to kill even if we want to. One of the main objectives of the organization is to encourage minimum use of polythene/plastic for environmental protection. All possible support will be given to the organizations which are striving towards controlled use of polythene/plastic.') }}</h3>
                    </div>
                </div>
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-12.png') ?? '' }}">
                </div>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-13.png') ?? '' }}">
                </div>

                <div class="card_right h-100" style="margin-left: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Honoring meritorious students') }}</h2>
                        <h3>{{ __('In the present times, we all are aware of the importance and utility of education. The organization will try to publicly honor the meritorious students (who have made it to the merit list at the district, state and national level). To attract more and more attention in the field of study among the students also.') }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-5">
        <div class="cardss">
            <div class="d-flex align-items-center cards_content">
                <div class="card_right h-100" style="margin-right: -64px">
                    <div class="background_cards">
                        <h2>{{ __('Respecting social workers') }}</h2>
                        <h3>{{ __('There are many such social workers in our society who have dedicated a large part of their lives to social work and even at present, they are continuously providing their services to the society by being concerned with social concerns. From time to time, such social workers are publicly recognized for their social work. The organization considers it its ultimate duty to honor.') }}</h3>
                    </div>
                </div>
                <div class="background_img">
                    <img src="{{ file_url('welcome_assets/images/png-img/background-14.png') ?? '' }}">
                </div>

            </div>
        </div>
    </div>
</div>

@endsection