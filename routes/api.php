<?php

use App\Http\Controllers\Api\PackageController;
use App\Http\Controllers\Api\BankDetailController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\BannersController;
use App\Http\Controllers\Api\CommonController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::controller(AuthController::class)->group(function () {
    Route::post('checkReferral', 'checkReferral');
    Route::post('register', 'register');
    Route::post('matchOtp', 'matchOtp');
    Route::post('resendOtp', 'resendOtp');
    Route::post('login', 'login');
    Route::post('resetPassword', 'resetPassword');
    Route::post('forgotPassword', 'forgotPassword');
    Route::post('changePassword', 'changePassword');
    Route::post('registerReferred_by', 'registerReferred_by');
});


Route::controller(BankDetailController::class)->group(function () {
    Route::post('addBankDetails', 'addBankDetails');
    Route::post('editBankDetails', 'editBankDetails');
    Route::get('getBankDetails/{id}', 'getBankDetails');

    Route::post('addBankAccount', 'addBankAccount');
    Route::get('/getBankAccounts', 'getBankAccounts');
    Route::delete('/deleteBankAccount',  'deleteBankAccount');
    Route::post('/updateBankAccounts',  'updateBankAccounts');
    Route::post('/changePrimaryBankAccount', 'changePrimaryBankAccount');
    Route::post('/withdrawCommission', 'withdrawCommission');
});

Route::controller(PackageController::class)->group(function () {
    Route::get('getPackage', 'getPackage');
    Route::get('getPackageDetail', 'getPackageDetail');
    Route::post('purchasePackage', 'purchasePackage');
});


Route::controller(UserController::class)->group(function () {
    Route::get('getPaymentHistory', 'getPaymentHistory');
    Route::post('transferToBank', 'transferToBank');
    Route::get('getProfile', 'getProfile');
    Route::post('updateProfile', 'updateProfile');
    Route::post('userKycDocument', 'userKycDocument');
    Route::post('updateKycDocument', 'updateKycDocument');
    Route::post('updatePanCard', 'updatePanCard');
    Route::get('getUserTree', 'getUserTree');
});

Route::get('getBannerList', [BannersController::class, 'getBannerList']);


Route::controller(CommonController::class)->group(function () {
    Route::get('getStateList', 'getStateList');
    Route::get('getCityList', 'getCityList'); 
    Route::get('getAppSetting', 'getAppSetting');
    Route::get('getUserDashboardBVCount', 'getUserDashboardBVCount');
    Route::get('getWalletHistory', 'getWalletHistory');
});
