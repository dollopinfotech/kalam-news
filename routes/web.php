<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RolesController;
use App\Http\Controllers\Admin\SocialMediaController;
use App\Http\Controllers\Admin\IconsController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\AdvRequestController;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\languageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('admin.auth.login');
})->name('admin.auth.login');

// Admin Management Routes
Route::POST('/adminLogin', [AdminController::class, 'adminLogin'])->name('admin.login');
Route::get('/logout', [AdminController::class, 'logout'])->name('admin.logout');
Route::get('/404', [AdminController::class, 'notFoundPage'])->name('admin.404');
Route::POST('/sendForgotPasswordOtp', [AdminController::class, 'sendForgotPasswordOtp'])->name('admin.sendForgotPasswordOtp');
Route::get('/adminOtp', [AdminController::class, 'adminOtp'])->name('admin.adminOtp');
Route::get('/forgotPassword', [AdminController::class, 'forgotPassword'])->name('admin.forgotPassword');
Route::post('/adminForgotPassword', [AdminController::class, 'adminForgotPassword'])->name('admin.adminForgotPassword');
Route::POST('/adminResetPassword', [AdminController::class, 'adminResetPassword'])->name('admin.adminResetPassword');
Route::get('getCitiesByState', [AdminController::class, 'getCitiesByState'])->name('admin.getCitiesByState');
Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');
Route::get('/site-settings', [AdminController::class, 'showSiteSettings'])->name('admin.site.settings');
Route::post('/add-site-settings', [AdminController::class, 'addSiteSettings'])->name('admin.add.site.settings');
Route::get('/admin-profile', [AdminController::class, 'showadminprofile'])->name('admin.profile');
Route::post('/update-profile', [AdminController::class, 'updateadminProfile'])->name('admin.update.profile');
Route::get('/admin/packageSoldData', [AdminController::class, 'getPackageSoldData']);

// Menu Management Routes
Route::get('/menu', [MenuController::class, 'getMenu'])->name('admin.menu');
Route::get('/createMenu', [MenuController::class, 'createMenu'])->name('admin.createMenu');
Route::post('/storeMenu', [MenuController::class, 'storeMenu'])->name('admin.storeMenu');
Route::get('/editMenu/{id}', [MenuController::class, 'editMenu'])->name('admin.editMenu');
Route::put('/updateMenu/{id}', [MenuController::class, 'updateMenu'])->name('admin.updateMenu');
Route::delete('/deleteMenu/{id}', [MenuController::class, 'deleteMenu'])->name('admin.deleteMenu');
Route::post('/menuStatus/{menu_id}', [MenuController::class, 'menuStatus'])->name('admin.menuStatus');

// Sub Menu Management Routes
Route::get('/subMenu', [MenuController::class, 'getSubMenu'])->name('admin.subMenu');
Route::get('/createSubMenu', [MenuController::class, 'createSubMenu'])->name('admin.createSubMenu');
Route::post('/storeSubMenu', [MenuController::class, 'storeSubMenu'])->name('admin.storeSubMenu');
Route::get('/editSubMenu/{id}', [MenuController::class, 'editSubMenu'])->name('admin.editSubMenu');
Route::put('/updateSubMenu/{id}', [MenuController::class, 'updateSubMenu'])->name('admin.updateSubMenu');
Route::delete('/deleteSubMenu/{id}', [MenuController::class, 'deleteSubMenu'])->name('admin.deleteSubMenu');
Route::post('/subMenuStatus/{menu_id}', [MenuController::class, 'subMenuStatus'])->name('admin.subMenuStatus');

// Pages Management Routes
Route::get('/pages', [MenuController::class, 'getPages'])->name('admin.pages');
Route::get('/createPages', [MenuController::class, 'createPages'])->name('admin.createPages');
Route::post('/storePages', [MenuController::class, 'storePages'])->name('admin.storePages');
Route::get('/editPages/{id}', [MenuController::class, 'editPages'])->name('admin.editPages');
Route::put('/updatePages/{id}', [MenuController::class, 'updatePages'])->name('admin.updatePages');
Route::delete('/deletePages/{id}', [MenuController::class, 'deletePages'])->name('admin.deletePages');
Route::post('/pageStatus/{menu_id}', [MenuController::class, 'pageStatus'])->name('admin.pageStatus');

// User Management Routes
Route::get('/users', [UserController::class, 'getUsers'])->name('admin.users');
Route::get('/user/{id}', [UserController::class, 'getUser'])->name('admin.user');
Route::get('/userPackage/{userPackage_id}', [UserController::class, 'userPackage'])->name('admin.userPackage');
Route::get('/adminRegisterUser', [UserController::class, 'adminRegisterUser'])->name('adminRegisterUser');
Route::post('/storeUser', [UserController::class, 'storeUser'])->name('admin.storeUser');
Route::get('/checkMobile', [UserController::class, 'checkMobile'])->name('admin.checkMobile');
Route::get('/userTransaction/{user_id}', [UserController::class, 'showUserTransactions'])->name('admin.user.transactions');

// Roles Management  Routes
Route::get('/addRoles', [RolesController::class, 'addRoles'])->name('admin.addRoles');
Route::get('/roles', [RolesController::class, 'getRoles'])->name('admin.roles');
Route::post('/storeRoles', [RolesController::class, 'storeRoles'])->name('admin.storeRoles');
Route::get('/editRole/{role_id}', [RolesController::class, 'editRole'])->name('admin.editRole');
Route::delete('/deleteRole/{role_id}', [RolesController::class, 'deleteRole'])->name('admin.deleteRole');
Route::put('/updateRole/{role_id}', [RolesController::class, 'updateRole'])->name('admin.updateRole');
Route::delete('/destroyRole/{role_id}', [RolesController::class, 'destroyRole'])->name('admin.destroyRole');
Route::get('/rolePermission/{id}', [RolesController::class, 'rolePermission'])->name('admin.rolePermission');
Route::put('/addPermission/{id}', [RolesController::class, 'addPermission'])->name('admin.addPermission');
Route::post('/statusRole/{role_id}', [RolesController::class, 'statusRole'])->name('admin.statusRole');

// User Management Route
Route::PUT('/updateUser/{user_id}', [UserController::class, 'updateUser'])->name('admin.updateUser');
Route::put('/updateUserBank/{bank_id}', [UserController::class, 'updateUserBank'])->name('admin.updateUserBank');
Route::put('/updateDocument/{docs_id}', [UserController::class, 'updateDocument'])->name('admin.updateDocument');
Route::get('/checkReferralCode', [UserController::class, 'checkReferralCode']);
Route::get('checkUniqueMobileNumber', [UserController::class, 'checkUniqueMobileNumber']);
Route::get('checkUniqueWhatsAppNumber', [UserController::class, 'checkUniqueWhatsAppNumber']);
Route::get('checkUniqueEmail', [UserController::class, 'checkUniqueEmail']);
Route::get('manageKyc', [UserController::class, 'manageKyc'])->name('admin.manageKyc');
Route::post('changeAdharVerifyStatus', [UserController::class, 'changeAdharVerifyStatus'])->name('admin.changeAdharVerifyStatus');
Route::post('changeBankVerifyStatus', [UserController::class, 'changeBankVerifyStatus'])->name('admin.changeBankVerifyStatus');

// Package Management Routes
Route::get('/add-packages', [PackageController::class, 'createPackages'])->name('admin.create.packages');
Route::post('/store-packages', [PackageController::class, 'storePackages'])->name('admin.store.packages');
Route::get('/edit-package/{package_id}', [PackageController::class, 'editPackages'])->name('admin.edit.package');
Route::post('/update-package/{package_id}', [PackageController::class, 'updatePackage'])->name('admin.update.package');
Route::get('/showPackage/{package_id}', [PackageController::class, 'showPackage'])->name('admin.showpackage');
Route::get('/userTree', [UserController::class, 'userTree'])->name('admin.userTree');
Route::delete('/delete-package/{package_id}', [PackageController::class, 'deletePackage'])->name('admin.delete.package');
Route::get('/packages-list', [PackageController::class, 'showPackages'])->name('admin.show.packages');
Route::post('/packages-status/{package_id}', [PackageController::class, 'packageStatus'])->name('packages.changeStatus');
Route::post('/packageStatus/{package_id}', [PackageController::class, 'packageStatus'])->name('packageStatus');

// Faq Management Routes
Route::get('/faq', [FaqController::class, 'getFaq'])->name('admin.getFaq');
Route::get('/createFaq', [FaqController::class, 'createFaq'])->name('admin.createFaq');
Route::post('/storeFaq', [FaqController::class, 'storeFaq'])->name('admin.storeFaq');
Route::get('/editFaq/{id}', [FaqController::class, 'editFaq'])->name('admin.editFaq');
Route::put('/updateFaq/{id}', [FaqController::class, 'updateFaq'])->name('admin.updateFaq');
Route::delete('/deleteFaq/{id}', [FaqController::class, 'deleteFaq'])->name('admin.deleteFaq');

// Icon management Routes
Route::get('/icons', [IconsController::class, 'getIcons'])->name('admin.getIcons');
Route::get('/getIcons', [IconsController::class, 'getIconsList'])->name('admin.icons');
Route::get('/createIcon', [IconsController::class, 'createIcon'])->name('admin.createIcon');
Route::post('/storeIcon', [IconsController::class, 'storeIcon'])->name('admin.storeIcon');
Route::get('/editIcon/{icon_id}', [IconsController::class, 'editIcon'])->name('admin.editIcon');
Route::put('/updateIcon/{icon_id}', [IconsController::class, 'updateIcon'])->name('admin.updateIcon');
Route::delete('/deleteIcon/{icon_id}', [IconsController::class, 'deleteIcon'])->name('admin.deleteIcon');

// Banner management Routes
Route::get('/banner', [BannerController::class, 'getBanner'])->name('admin.getBanner');
Route::get('/createBanner', [BannerController::class, 'createBanner'])->name('admin.createBanner');
Route::post('/storeBanner', [BannerController::class, 'storeBanner'])->name('admin.storeBanner');
Route::get('/editBanner/{banner_id}', [BannerController::class, 'editBanner'])->name('admin.editBanner');
Route::put('/updateBanner/{banner_id}', [BannerController::class, 'updateBanner'])->name('admin.updateBanner');
Route::delete('/deleteBanner/{banner_id}', [BannerController::class, 'deleteBanner'])->name('admin.deleteBanner');
Route::post('/bannerStatus/{banner_id}', [BannerController::class, 'bannerStatus'])->name('bannerStatus');

//Social Link Management Routes
Route::get('/follow-us', [SocialMediaController::class, 'showLinks'])->name('admin.showLinks');
Route::post('/social-media', [SocialMediaController::class, 'store'])->name('admin.store');
Route::delete('/delete-social-media/{social_id}', [SocialMediaController::class, 'deleteLinks'])->name('delete.links');
Route::get('/editSocial/{social_id}', [SocialMediaController::class, 'editFollow'])->name('admin.editFollow');
Route::post('/updatesocial-media', [SocialMediaController::class, 'updateFollow'])->name('admin.updateFollow');

// Advertisement-Request
Route::get('/adminAdvertisement', [AdvRequestController::class, 'advertisement'])->name('admin.advertisement');
Route::get('/commissionAgent', [AdvRequestController::class, 'commissionAgent'])->name('admin.commissionAgent');
Route::post('/advertisementApproved', [AdvRequestController::class, 'advertisementApproved'])->name('admin.advertisementApproved');
Route::post('/commissionApproved', [AdvRequestController::class, 'commissionApproved'])->name('admin.commissionApproved');
Route::post('/addAdvertisement', [AdvRequestController::class, 'addAdvertisement'])->name('guest.addAdvertisement');
Route::post('/razorpayPayment', [AdvRequestController::class, 'razorpayPayment'])->name('guest.razorpayPaymnet');
Route::post('/addCommissionAgent', [AdvRequestController::class, 'addCommissionAgent'])->name('admin.addCommissionAgent');
Route::post('/approveCommissionAgent', [AdvRequestController::class, 'approveCommissionAgent'])->name('admin.approveCA');
Route::get('/withdrawRequest', [AdvRequestController::class, 'withdrawRequest'])->name('admin.withdrawRequest');
Route::delete('/deleteWithdrawRequest', [AdvRequestController::class, 'deleteWithdrawRequest'])->name('admin.deleteWithdrawRequest');
Route::post('/approveWithdrawRequest', [AdvRequestController::class, 'approveWithdrawRequest'])->name('admin.approveWithdrawRequest');


// Guest Management Routes
Route::get('/agent', [WelcomeController::class, 'agent'])->name('guest.agent');
Route::get('/advertisement', [WelcomeController::class, 'advertisement'])->name('guest.advertisement');
Route::get('/contactUs', [WelcomeController::class, 'contactUs'])->name('guest.contactUs');
Route::get('/cooperation', [WelcomeController::class, 'cooperation'])->name('guest.cooperation');
Route::get('/editorPen', [WelcomeController::class, 'editorPen'])->name('guest.editorPen');
Route::get('/joinUs', [WelcomeController::class, 'joinUs'])->name('guest.joinUs');
Route::get('/ourTeam', [WelcomeController::class, 'ourTeam'])->name('guest.ourTeam');
Route::get('/ourWork', [WelcomeController::class, 'ourWork'])->name('guest.ourWork');
Route::get('/privacyPolicy', [WelcomeController::class, 'privacyPolicy'])->name('guest.privacyPolicy');
Route::get('/responsibilities', [WelcomeController::class, 'responsibilities'])->name('guest.responsibilities');
Route::get('/termConditions', [WelcomeController::class, 'termConditions'])->name('guest.termConditions');
Route::get('/returnPolicy', [WelcomeController::class, 'returnPolicy'])->name('guest.returnPolicy');
Route::get('/shippingPolicy', [WelcomeController::class, 'shippingPolicy'])->name('guest.shippingPolicy');

// Language Management Routes
Route::get('/langSwitch/{locale}',[languageController::class,'langSwitch'])->name('admin.langSwitch');

// Admin Management Routes
Route::get('/adminList', [AdminController::class, 'getadminList'])->name('admin.adminList');
Route::get('/addAdmin', [AdminController::class, 'addAdmin'])->name('admin.addAdmin');
Route::post('/storeAdmin', [AdminController::class, 'storeAdmin'])->name('admin.storeAdmin');
Route::get('/editAdmin/{admin_id}', [AdminController::class, 'editAdmin'])->name('admin.editAdmin');
Route::put('/updateAdmin/{admin_id}', [AdminController::class, 'updateAdmin'])->name('admin.updateAdmin');
Route::delete('/deleteAdmin/{admin_id}', [AdminController::class, 'deleteAdmin'])->name('admin.deleteAdmin');
Route::post('/statusAdmin/{admin_id}', [AdminController::class, 'AdminStatus'])->name('admin.statusadmin');
Route::get('/checkUniqueAdminEmail', [AdminController::class, 'checkUniqueAdminEmail']);
Route::get('/checkUniqueAdminMobile', [AdminController::class, 'checkUniqueAdminMobile']);
